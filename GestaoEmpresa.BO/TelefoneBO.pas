unit TelefoneBO;

interface

uses
  TelefoneDAO, Telefone, FireDAC.Comp.Client, System.Generics.Collections;

type
  TTelefoneBO = class(TTelefoneDAO)

  protected
    procedure antesAlterar(entidade: TTelefone); override;
    procedure antesExcluir(entidade: TTelefone); override;
    procedure antesIncluir(entidade: TTelefone); override;
    procedure depoisAlterar(entidade: TTelefone); override;
    procedure depoisExcluir(entidade: TTelefone); override;
    procedure depoisIncluir(entidade: TTelefone); override;
    procedure validar(entidade: TTelefone); override;
  public
    procedure excluirPorPessoa(entidade: TTelefone);
    function listarPorPessoa(entidade: TTelefone): TFDMemTable;
    function obterPorPessoa(entidade: TTelefone): TList<TTelefone>;
  end;

implementation

uses
  System.SysUtils, Funcoes, Entities.Enums.ETipoTelefone, EnumConverterStrings;

{ TTelefoneBO }

procedure TTelefoneBO.antesAlterar(entidade: TTelefone);
begin
  inherited;
  entidade.Numero := TFuncoes.unMask(entidade.Numero);
end;

procedure TTelefoneBO.antesExcluir(entidade: TTelefone);
begin
  inherited;

end;

procedure TTelefoneBO.antesIncluir(entidade: TTelefone);
begin
  inherited;
  entidade.Numero := TFuncoes.unMask(entidade.Numero);
end;

procedure TTelefoneBO.depoisAlterar(entidade: TTelefone);
begin
  inherited;

end;

procedure TTelefoneBO.depoisExcluir(entidade: TTelefone);
begin
  inherited;

end;

procedure TTelefoneBO.depoisIncluir(entidade: TTelefone);
begin
  inherited;

end;

procedure TTelefoneBO.excluirPorPessoa(entidade: TTelefone);
begin
  if entidade.CodigoPessoa <= 0 then
    raise Exception.Create('Telefone: C�digo da pessoa n�o foi informado');

  Self.excluirTelefonePorPessoa(entidade.CodigoPessoa);

end;

procedure TTelefoneBO.validar(entidade: TTelefone);
begin
  inherited;
  if Trim(entidade.Numero) = EmptyStr then
    raise Exception.Create('Telefone: N�mero n�o foi informado');

  if entidade.CodigoPessoa <= 0 then
    raise Exception.Create('Telefone: C�digo da pessoa n�o foi informado');

end;

function TTelefoneBO.listarPorPessoa(entidade: TTelefone): TFDMemTable;
var
  listaParametros: TDictionary<String, Variant>;
begin
  if entidade.CodigoPessoa <= 0 then
    raise Exception.Create('Telefone: C�digo da pessoa n�o foi informado');

  listaParametros := TDictionary<String, Variant>.Create();
  listaParametros.Add('CodigoPessoa', entidade.CodigoPessoa);
  result := Self.listarPor(listaParametros);
end;

function TTelefoneBO.obterPorPessoa(entidade: TTelefone): TList<TTelefone>;
var
  listaTelefone: TList<TTelefone>;
  dados: TFDMemTable;
begin
  listaTelefone:= nil;
  dados := Self.listarPorPessoa(entidade);
  if Not dados.IsEmpty then
  begin
    listaTelefone:= TList<TTelefone>.Create;
     while Not dados.Eof do
     begin
       listaTelefone.Add(
        TTelefone.Create(dados.FieldByName('CodigoTelefone').AsInteger,
        dados.FieldByName('Numero').AsString,
        TConvert<TETipoTelefone>.StrConvertEnum(dados.FieldByName('Tipo').AsString),
        dados.FieldByName('Principal').AsBoolean)
       );
       dados.Next;
     end;
  end;

  result:= listaTelefone;
end;

end.

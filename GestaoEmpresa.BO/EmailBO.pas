unit EmailBO;

interface

uses
  EmailDAO, Email, FireDAC.Comp.Client;

type
  TEmailBO = class(TEmailDAO)
  private
  protected
    procedure antesAlterar(entidade: TEmail); override;
    procedure antesExcluir(entidade: TEmail); override;
    procedure antesIncluir(entidade: TEmail); override;
    procedure depoisAlterar(entidade: TEmail); override;
    procedure depoisExcluir(entidade: TEmail); override;
    procedure depoisIncluir(entidade: TEmail); override;
    procedure validar(entidade: TEmail); override;
  public
    procedure excluirEmailPorPessoa(entidade: TEmail);
    function listarPorPessoa(entidade: TEmail): TFDMemTable;
    function obterPorPessoa(entidade: TEmail): TEmail;
  end;

implementation

uses
  System.SysUtils, System.Generics.Collections;

{ TEmailBO }

procedure TEmailBO.antesAlterar(entidade: TEmail);
begin
  inherited;

end;

procedure TEmailBO.antesExcluir(entidade: TEmail);
begin
  inherited;

end;

procedure TEmailBO.antesIncluir(entidade: TEmail);
begin
  inherited;

end;

procedure TEmailBO.depoisAlterar(entidade: TEmail);
begin
  inherited;

end;

procedure TEmailBO.depoisExcluir(entidade: TEmail);
begin
  inherited;

end;

procedure TEmailBO.depoisIncluir(entidade: TEmail);
begin
  inherited;

end;

procedure TEmailBO.excluirEmailPorPessoa(entidade: TEmail);
begin
  if entidade.CodigoPessoa <= 0 then
  begin
    raise Exception.Create('Email: C�digo da pessoa n�o foi informado');
  end;

  Self.exlcuirEmailPorPessoa(entidade.CodigoPessoa);
end;

procedure TEmailBO.validar(entidade: TEmail);
begin
  inherited;
  if Trim(entidade.Endereco) = EmptyStr then
  begin
    raise Exception.Create('Email: Endere�o n�o foi informado');
  end;

end;

function TEmailBO.listarPorPessoa(entidade: TEmail): TFDMemTable;
var
  listaParametros: TDictionary<String, Variant>;
begin
  if entidade.CodigoPessoa <= 0 then
    raise Exception.Create('Telefone: C�digo da pessoa n�o foi informado');

  listaParametros := TDictionary<String, Variant>.Create();
  listaParametros.Add('CodigoPessoa', entidade.CodigoPessoa);
  result := Self.listarPor(listaParametros);
end;

function TEmailBO.obterPorPessoa(entidade:TEmail):TEmail;
var
  dados:TFDMemTable;
  Email:TEmail;
begin
   Email:= nil;
   dados:= Self.listarPorPessoa(entidade);

   if Not dados.IsEmpty then
   begin
       dados.First;
       Email:= TEmail.Create(
       dados.FieldByName('CodigoEmail').AsInteger,
       dados.FieldByName('Endereco').AsString,
       dados.FieldByName('Principal').AsBoolean
       );
   end;

   result:= Email;
end;

end.

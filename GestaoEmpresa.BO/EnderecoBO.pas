unit EnderecoBO;

interface

uses
  EnderecoDAO, Endereco, FireDAC.Comp.Client;

type
  TEnderecoBO = class(TEnderecoDAO)
  private


  protected
    procedure antesAlterar(entidade: TEndereco); override;
    procedure antesExcluir(entidade: TEndereco); override;
    procedure antesIncluir(entidade: TEndereco); override;
    procedure depoisAlterar(entidade: TEndereco); override;
    procedure depoisExcluir(entidade: TEndereco); override;
    procedure depoisIncluir(entidade: TEndereco); override;
    procedure validar(entidade: TEndereco); override;
  public
    procedure excluirPorPessoa(entidade: TEndereco);
    function listarPorPessoa(entidade: TEndereco): TFDMemTable;
    function obterPorPessoa(codigoPessoa: Integer): TEndereco;
  end;

implementation

uses
  System.SysUtils, BairroBO, System.Generics.Collections, EnumConverterStrings,
  Entities.Enums.ETipoEndereco, Bairro, Cidade, Estado;

{ TEnderecoBO }

procedure TEnderecoBO.antesAlterar(entidade: TEndereco);
begin
  inherited;

end;

procedure TEnderecoBO.antesExcluir(entidade: TEndereco);
begin
  inherited;

end;

procedure TEnderecoBO.antesIncluir(entidade: TEndereco);
var
  BairroBO: TBairroBO;
begin
  inherited;
  BairroBO := TBairroBO.Create(Self.Conexao);
  BairroBO.incluir(entidade.Bairro);
  BairroBO.Free;

end;

procedure TEnderecoBO.depoisAlterar(entidade: TEndereco);
begin
  inherited;

end;

procedure TEnderecoBO.depoisExcluir(entidade: TEndereco);
var
  BairroBO:TBairroBO;
begin
  inherited;
  if Assigned(entidade.Bairro) then
  begin
    BairroBO:= TBairroBO.Create(Self.Conexao);
    BairroBO.excluir(entidade.Bairro);
  end;
end;

procedure TEnderecoBO.depoisIncluir(entidade: TEndereco);
begin
  inherited;

end;

procedure TEnderecoBO.excluirPorPessoa(entidade: TEndereco);
begin
  if entidade.CodigoPessoa <= 0 then
    raise Exception.Create('Endere�o: C�digo da pessoa n�o foi informado');

  Self.excluirEnderecoPorPessoa(entidade.CodigoPessoa);
end;

procedure TEnderecoBO.validar(entidade: TEndereco);
begin
  inherited;
  if Not Assigned(entidade.Bairro) then
  begin
    raise Exception.Create('Endere�o: Bairro n�o foi informado');
  end;
end;

function TEnderecoBO.listarPorPessoa(entidade: TEndereco): TFDMemTable;
var
  listaParametros:TDictionary<String, Variant>;
begin
  if entidade.CodigoPessoa <= 0 then
    raise Exception.Create('Endere�o: C�digo da pessoa n�o foi informado');

   listaParametros:= TDictionary<String,Variant>.Create();
   listaParametros.Add('CodigoPessoa', entidade.CodigoPessoa);
   result:= Self.listarPor(listaParametros);
end;

function TEnderecoBO.obterPorPessoa(codigoPessoa:Integer):TEndereco;
var
  Endereco:TEndereco;
  dados:TFDMemTable;
begin

   dados:= Self.listarPorPessoa(TEndereco.Create(codigoPessoa));
   if Not dados.IsEmpty then
   begin
   dados.First;
     Endereco:= TEndereco.Create(
       dados.FieldByName('CodigoEndereco').AsInteger,
       dados.FieldByName('Logradouro').AsString,
       dados.FieldByName('Numero').AsString,
       dados.FieldByName('Complemento').AsString,
       dados.FieldByName('Principal').AsBoolean,
       TConvert<TETipoEndereco>.StrConvertEnum(dados.FieldByName('Tipo').AsString),
       TBairro.Create(dados.FieldByName('CodigoBairro').AsInteger, dados.FieldByName('Bairro').AsString),
       TCidade.Create(dados.FieldByName('CodigoCidade').AsInteger,
       TEstado.Create( dados.FieldByName('CodigoEstado').AsInteger))
     );
   end;

   result := Endereco;
end;

end.

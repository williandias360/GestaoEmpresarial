unit BairroBO;

interface

uses
  BairroDAO, Bairro;
type
  TBairroBO = class(TBairroDAO)
  protected
    procedure antesAlterar(entidade: TBairro); override;
    procedure antesExcluir(entidade: TBairro); override;
    procedure antesIncluir(entidade: TBairro); override;
    procedure depoisAlterar(entidade: TBairro); override;
    procedure depoisExcluir(entidade: TBairro); override;
    procedure depoisIncluir(entidade: TBairro); override;
    procedure validar(entidade: TBairro); override;
  end;

implementation

uses
  System.SysUtils;

{ TBairroBO }

procedure TBairroBO.antesAlterar(entidade: TBairro);
begin
  inherited;

end;

procedure TBairroBO.antesExcluir(entidade: TBairro);
begin
  inherited;

end;

procedure TBairroBO.antesIncluir(entidade: TBairro);
begin
  inherited;

end;

procedure TBairroBO.depoisAlterar(entidade: TBairro);
begin
  inherited;

end;

procedure TBairroBO.depoisExcluir(entidade: TBairro);
begin
  inherited;

end;

procedure TBairroBO.depoisIncluir(entidade: TBairro);
begin
  inherited;

end;

procedure TBairroBO.validar(entidade: TBairro);
begin
  inherited;

end;

end.

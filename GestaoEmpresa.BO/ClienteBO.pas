unit ClienteBO;

interface

uses
  ClienteDAO, Cliente, FireDAC.Comp.Client;

type
  TClienteBO = class(TClienteDAO)
  private

  public
    function obterPorCodigo(entidade: TCliente): TCliente;
    function listarClientePor(entidade: TCliente): TFDMemTable;
  protected
    procedure validar(entidade: TCliente); override;
    procedure antesIncluir(entidade: TCliente); override;
    procedure depoisIncluir(entidade: TCliente); override;
    procedure antesAlterar(entidade: TCliente); override;
    procedure depoisAlterar(entidade: TCliente); override;
    procedure antesExcluir(entidade: TCliente); override;
    procedure depoisExcluir(entidade: TCliente); override;
  end;

implementation

uses
  System.SysUtils, PessoaBO, System.Generics.Collections, Pessoa,
  PessoaJuridica, PessoaFisica;

{ TClienteNegocios }

procedure TClienteBO.antesAlterar(entidade: TCliente);
var
  PessoaBO:TPessoaBO;
begin
  entidade.Pessoa.PerfilCliente := true;
  PessoaBO:= TPessoaBO.Create(Self.Conexao);
  PessoaBO.alterar(entidade.Pessoa);
  PessoaBO.Free;
end;

procedure TClienteBO.antesExcluir(entidade: TCliente);
begin

end;

procedure TClienteBO.antesIncluir(entidade: TCliente);
var
  PessoaBO: TPessoaBO;
begin
  PessoaBO := TPessoaBO.Create(Self.Conexao);
  entidade.Pessoa.PerfilCliente := true;
  PessoaBO.incluir(entidade.Pessoa);
  PessoaBO.Free;
end;

procedure TClienteBO.depoisAlterar(entidade: TCliente);
begin

end;

procedure TClienteBO.depoisExcluir(entidade: TCliente);
var
  Pessoa:TPessoa;
  PessoaBO:TPessoaBO;
begin
   Pessoa:= TPessoa.Create(entidade.CodigoCliente);
   PessoaBO:= TPessoaBO.Create(Self.Conexao);
   PessoaBO.excluir(pessoa);
   PessoaBO.Free;
end;

procedure TClienteBO.depoisIncluir(entidade: TCliente);
begin

end;

procedure TClienteBO.validar(entidade: TCliente);
begin
  if entidade.Pessoa = nil then
  begin
    raise Exception.Create('Pessoa n�o foi informada');
  end;

end;

function TClienteBO.listarClientePor(entidade: TCliente): TFDMemTable;
var
  listaParametros: TDictionary<String, Variant>;
begin
  listaParametros := TDictionary<String, Variant>.Create();
  if entidade.CodigoCliente > 0 then
    listaParametros.Add('CodigoCliente', entidade.CodigoCliente);
  if Trim(entidade.Pessoa.Nome) <> EmptyStr then
    listaParametros.Add('Nome', entidade.Pessoa.Nome);
  if Trim(entidade.Pessoa.PessoaFisica.Cpf) <> EmptyStr then
    listaParametros.Add('Cpf', entidade.Pessoa.PessoaFisica.Cpf);
  if Trim(entidade.Pessoa.PessoaJuridica.Cnpj) <> EmptyStr then
    listaParametros.Add('Cnpj', entidade.Pessoa.PessoaJuridica.Cnpj);

  result := Self.listarPor(listaParametros);

end;

function TClienteBO.obterPorCodigo(entidade: TCliente): TCliente;
var
  listaParametros: TDictionary<String, Variant>;
  dados: TFDMemTable;
  Cliente: TCliente;
  Pessoa: TPessoa;
  CpfCnpj, RgIe: String;
begin
  if entidade.CodigoCliente <= 0 then
    raise Exception.Create('Cliente: C�digo n�o foi informado');

  listaParametros := TDictionary<String, Variant>.Create();
  listaParametros.Add('CodigoCliente', entidade.CodigoCliente);

  dados := Self.listarPor(listaParametros);

  if Not dados.IsEmpty then
  begin
    Pessoa := TPessoa.Create(dados.FieldByName('CodigoCliente').AsInteger,
      dados.FieldByName('Nome').AsString, dados.FieldByName('Observacao')
      .AsString);

    Pessoa.PerfilCliente := true;

    Cliente := TCliente.Create(dados.FieldByName('Limite').AsCurrency,
      dados.FieldByName('Bloqueado').AsBoolean,
      dados.FieldByName('EnviarEmailFinalizarVenda').AsBoolean, Pessoa);

    Cliente.CodigoCliente := dados.FieldByName('CodigoCliente').AsInteger;

    CpfCnpj := dados.FieldByName('CpfCnpj').AsString;
    RgIe := dados.FieldByName('RgIe').AsString;

    if CpfCnpj.Trim <> EmptyStr then
    begin
      if CpfCnpj.Length = 14 then
      begin
        Pessoa.PessoaJuridica := TPessoaJuridica.Create(CpfCnpj, RgIe,
          dados.FieldByName('RazaoSocial').AsString);
      end
      else
      begin
        Pessoa.PessoaFisica := TPessoaFisica.Create(CpfCnpj, RgIe);
      end;
    end;

  end;

  result := Cliente;
end;

end.

unit EmpresaBO;

interface

uses EmpresaDAO, Empresa, FireDAC.Comp.Client, System.Generics.Collections;

type
  TEmpresaBO = class(TEmpresaDAO)
  private
  protected
    procedure validar(entidade: TEmpresa); override;
    procedure antesIncluir(entidade: TEmpresa); override;
    procedure depoisIncluir(entidade: TEmpresa); override;
    procedure antesAlterar(entidade: TEmpresa); override;
    procedure depoisAlterar(entidade: TEmpresa); override;
    procedure antesExcluir(entidade: TEmpresa); override;
    procedure depoisExcluir(entidade: TEmpresa); override;
  public
    function ListarEmpresaPor(listaParametros: TDictionary<String, Variant>)
      : TFDMemTable;
    function ObterPorCodigo(CodigoEmpresa: Integer): TEmpresa;
    function ListarEmpresasDisponivel: TFDMemTable;
  end;

implementation

{ TEmpresaBO }

uses PessoaBO, System.SysUtils, Pessoa, EnderecoBO, Endereco, Email,
  EnumConverterStrings, Entities.Enums.ETipoRegimeTributario, PessoaJuridica,
  PessoaFisica, System.Variants, Entities.Enums.ETipoEndereco, Bairro, Cidade,
  Estado, TelefoneBO, EmailBO, Telefone;

procedure TEmpresaBO.antesAlterar(entidade: TEmpresa);
var
  PessoaBO: TPessoaBO;
  TelefoneBO: TTelefoneBO;
  EmailBO: TEmailBO;
  EnderecoBO: TEnderecoBO;
begin
  EmailBO := TEmailBO.Create(Self.Conexao);
  EmailBO.excluirEmailPorPessoa(TEmail.Create(entidade.Pessoa.CodigoPessoa));

  TelefoneBO := TTelefoneBO.Create(Self.Conexao);
  TelefoneBO.excluirPorPessoa(TTelefone.Create(entidade.Pessoa.CodigoPessoa));

  EnderecoBO := TEnderecoBO.Create(Self.Conexao);
  EnderecoBO.excluirPorPessoa(TEndereco.Create(entidade.Pessoa.CodigoPessoa));

  entidade.Pessoa.PerfilEmpresa := True;
  PessoaBO := TPessoaBO.Create(Self.Conexao);
  PessoaBO.alterar(entidade.Pessoa);
  PessoaBO.Free;
end;

procedure TEmpresaBO.antesExcluir(entidade: TEmpresa);
begin
  inherited;

end;

procedure TEmpresaBO.antesIncluir(entidade: TEmpresa);
var
  PessoaBO: TPessoaBO;
begin
  entidade.Pessoa.PerfilEmpresa := True;
  PessoaBO := TPessoaBO.Create(Self.Conexao);
  PessoaBO.incluir(entidade.Pessoa);
  PessoaBO.Free;

end;

procedure TEmpresaBO.depoisAlterar(entidade: TEmpresa);
begin

end;

procedure TEmpresaBO.depoisExcluir(entidade: TEmpresa);
begin
  inherited;

end;

procedure TEmpresaBO.depoisIncluir(entidade: TEmpresa);
begin
  inherited;

end;

procedure TEmpresaBO.validar(entidade: TEmpresa);
begin
  if entidade.Pessoa = nil then
  begin
    raise Exception.Create('Pessoa n�o foi informada');
  end;

end;

function TEmpresaBO.ListarEmpresaPor(listaParametros
  : TDictionary<String, Variant>): TFDMemTable;
begin
  listaParametros.Add('Disponivel', True);
  result := Self.listarPor(listaParametros);
end;

function TEmpresaBO.ListarEmpresasDisponivel: TFDMemTable;
var
  listaParametros: TDictionary<String, Variant>;
begin
  listaParametros := TDictionary<String, Variant>.Create();
  listaParametros.Add('Disponivel', True);
  result := Self.listarPor(listaParametros)
end;

function TEmpresaBO.ObterPorCodigo(CodigoEmpresa: Integer): TEmpresa;
var
  listaParametros: TDictionary<String, Variant>;
  dados: TFDMemTable;
  Empresa: TEmpresa;
  Pessoa: TPessoa;
  CpfCnpj, RgIe: String;
  Endereco: TEndereco;
  Email: TEmail;
  TelefoneBO: TTelefoneBO;
  ListaTelefone: TList<TTelefone>;
  Telefone: TTelefone;
begin
  Empresa := Nil;
  if CodigoEmpresa <= 0 then
    raise Exception.Create('C�digo da empresa n�o foi informado');

  listaParametros := TDictionary<String, Variant>.Create;
  listaParametros.Add('CodigoEmpresa', CodigoEmpresa);

  dados := Self.listarPor(listaParametros);
  if Not dados.IsEmpty then
  begin
    Pessoa := TPessoa.Create(dados.FieldByName('CodigoEmpresa').AsInteger,
      dados.FieldByName('Nome').AsString, dados.FieldByName('Observacao')
      .AsString);

    Pessoa.PerfilEmpresa := True;

    Empresa := TEmpresa.Create(TConvert<TETipoRegimeTributario>.StrConvertEnum
      (dados.FieldByName('TipoRegimeTributario').AsString), Pessoa);

    CpfCnpj := dados.FieldByName('CpfCnpj').AsString;
    RgIe := dados.FieldByName('RgIe').AsString;

    if CpfCnpj.Trim <> EmptyStr then
    begin
      if CpfCnpj.Length = 14 then
      begin
        Pessoa.PessoaJuridica := TPessoaJuridica.Create(CpfCnpj, RgIe,
          dados.FieldByName('RazaoSocial').AsString);
      end
      else
      begin
        Pessoa.PessoaFisica := TPessoaFisica.Create(CpfCnpj, RgIe);
      end;
    end;

    if dados.FieldByName('CodigoEndereco').Value <> Null then
    begin
      Endereco := TEndereco.Create(dados.FieldByName('CodigoEndereco')
        .AsInteger, dados.FieldByName('Logradouro').AsString,
        dados.FieldByName('Numero').AsString, dados.FieldByName('Complemento')
        .AsString, True, TETipoEndereco.COMERCIAL,
        TBairro.Create(dados.FieldByName('CodigoBairro').AsInteger,
        dados.FieldByName('Bairro').AsString),
        TCidade.Create(dados.FieldByName('CodigoCidade').AsInteger,
        TEstado.Create(dados.FieldByName('CodigoEstado').AsInteger)));

      Pessoa.adicionarEndereco(Endereco);
    end;

    if dados.FieldByName('CodigoEmail').Value <> Null then
    begin
      Pessoa.adicionarEmail(TEmail.Create(dados.FieldByName('CodigoEmail')
        .AsInteger, dados.FieldByName('Email').AsString, True));
    end;

    TelefoneBO := TTelefoneBO.Create;
    ListaTelefone := TelefoneBO.obterPorPessoa
      (TTelefone.Create(Pessoa.CodigoPessoa));

    if Assigned(ListaTelefone) then
    begin
      for Telefone in ListaTelefone do
      begin
        Pessoa.adicionarTelefone(Telefone);
      end;
    end;

  end;

  result := Empresa;

end;

end.

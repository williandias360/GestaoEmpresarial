unit ProdutoGrupoBO;

interface

uses
  ProdutoGrupoDAO, ProdutoGrupo, System.Generics.Collections,
  FireDAC.Comp.Client;

type
  TProdutoGrupoBO = class(TProdutoGrupoDAO)
  private

  public
    function ListarProdutoGrupoPor(listaParametros
      : TDictionary<String, Variant>): TFDMemTable;
    function ObterPorCodigo(codigo: Integer): TProdutoGrupo;
    function ListarProdutoGrupoDisponivel: TFDMemTable;
  protected
    procedure validar(entidade: TProdutoGrupo); override;
    procedure antesIncluir(entidade: TProdutoGrupo); override;
    procedure depoisIncluir(entidade: TProdutoGrupo); override;
    procedure antesAlterar(entidade: TProdutoGrupo); override;
    procedure depoisAlterar(entidade: TProdutoGrupo); override;
    procedure antesExcluir(entidade: TProdutoGrupo); override;
    procedure depoisExcluir(entidade: TProdutoGrupo); override;
  end;

implementation

uses
  System.SysUtils, ProdutoGrupoEmpresaBO, ProdutoGrupoEmpresa, Globals;

{ TProdutoGrupoBO }

procedure TProdutoGrupoBO.antesAlterar(entidade: TProdutoGrupo);
begin
  inherited;

end;

procedure TProdutoGrupoBO.antesExcluir(entidade: TProdutoGrupo);
begin
  inherited;

end;

procedure TProdutoGrupoBO.antesIncluir(entidade: TProdutoGrupo);
begin
  inherited;
  entidade.Disponivel := True;

end;

procedure TProdutoGrupoBO.depoisAlterar(entidade: TProdutoGrupo);
begin
  inherited;

end;

procedure TProdutoGrupoBO.depoisExcluir(entidade: TProdutoGrupo);
begin
  inherited;

end;

procedure TProdutoGrupoBO.depoisIncluir(entidade: TProdutoGrupo);
var
  ProdutoGrupoEmpresaBO: TProdutoGrupoEmpresaBO;
begin

  ProdutoGrupoEmpresaBO := TProdutoGrupoEmpresaBO.Create(Self.Conexao);
  ProdutoGrupoEmpresaBO.incluir
    (TProdutoGrupoEmpresa.Create(entidade.CodigoProdutoGrupo,
    TGlobals.GetEmpresa.CodigoEmpresa));

end;

function TProdutoGrupoBO.ListarProdutoGrupoPor(listaParametros
  : TDictionary<String, Variant>): TFDMemTable;
begin
  listaParametros.Add('CodigoEmpresa', TGlobals.GetEmpresa.CodigoEmpresa);
  listaParametros.Add('Disponivel', True);
  Result := Self.listarPor(listaParametros);
end;

function TProdutoGrupoBO.ListarProdutoGrupoDisponivel(): TFDMemTable;
var
  listaParametros: TDictionary<String, Variant>;
begin
  listaParametros := TDictionary<String, Variant>.Create();
  Result := Self.ListarProdutoGrupoPor(listaParametros);
end;

function TProdutoGrupoBO.ObterPorCodigo(codigo: Integer): TProdutoGrupo;
var
  listaParametros: TDictionary<String, Variant>;
  dados: TFDMemTable;
  ProdutoGrupo: TProdutoGrupo;
begin
  ProdutoGrupo := nil;
  if codigo <= 0 then
    raise Exception.Create('C�digo do grupo n�o foi informado');

  listaParametros := TDictionary<String, Variant>.Create();
  listaParametros.Add('CodigoProdutoGrupo', codigo);
  listaParametros.Add('CodigoEmpresa', TGlobals.GetEmpresa.CodigoEmpresa);
  listaParametros.Add('Disponivel', True);

  dados := Self.listarPor(listaParametros);

  if Not dados.IsEmpty then
  begin
    ProdutoGrupo := TProdutoGrupo.Create(dados.FieldByName('CodigoProdutoGrupo')
      .AsInteger, dados.FieldByName('Nome').AsString);
  end;

  Result := ProdutoGrupo;

end;

procedure TProdutoGrupoBO.validar(entidade: TProdutoGrupo);
begin
  inherited;
  if Trim(entidade.Nome) = EmptyStr then
    raise Exception.Create('Nome do grupo n�o foi informado');
end;

end.

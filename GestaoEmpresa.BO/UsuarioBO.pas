unit UsuarioBO;

interface

uses UsuarioDAO, Usuario, System.Generics.Collections, FireDAC.Comp.Client;

type
  TUsuarioBO = class(TUsuarioDAO)
  private
    function FormatarUsuario(dados: TFDMemTable): TUsuario;

  protected
    procedure validar(entidade: TUsuario); override;
    procedure antesIncluir(entidade: TUsuario); override;
    procedure depoisIncluir(entidade: TUsuario); override;
    procedure antesAlterar(entidade: TUsuario); override;
    procedure depoisAlterar(entidade: TUsuario); override;
    procedure antesExcluir(entidade: TUsuario); override;
    procedure depoisExcluir(entidade: TUsuario); override;
  public
    function listarUsuarioPor(listaParametros: TDictionary<String, Variant>)
      : TFDMemTable;
    function ObterPorCodigo(codigoUsuario: Integer): TUsuario;
    function ListarUsuarioPorEmpresa(CodigoEmpresa: Integer): TFDMemTable;
    function ObterPorLoginSenha(login, senha: String): TUsuario;
  end;

implementation

{ TUsuarioBO }

uses PessoaBO, EmailBO, TelefoneBO, Email, Telefone, Pessoa, System.SysUtils,
  Globals;

procedure TUsuarioBO.antesAlterar(entidade: TUsuario);
var
  EmailBO: TEmailBO;
  TelefoneBO: TTelefoneBO;
  PessoaBO: TPessoaBO;
begin
  EmailBO := TEmailBO.Create(Self.Conexao);
  EmailBO.excluirEmailPorPessoa(TEmail.Create(entidade.Pessoa.CodigoPessoa));

  TelefoneBO := TTelefoneBO.Create(Self.Conexao);
  TelefoneBO.excluirPorPessoa(TTelefone.Create(entidade.Pessoa.CodigoPessoa));

  PessoaBO := TPessoaBO.Create(Self.Conexao);
  entidade.Pessoa.PerfilUsuario := True;
  PessoaBO.alterar(entidade.Pessoa);
  PessoaBO.Free;

end;

procedure TUsuarioBO.antesExcluir(entidade: TUsuario);
begin
  inherited;

end;

procedure TUsuarioBO.antesIncluir(entidade: TUsuario);
var
  PessoaBO: TPessoaBO;
begin
  PessoaBO := TPessoaBO.Create(Self.Conexao);
  entidade.Pessoa.PerfilUsuario := True;
  PessoaBO.incluir(entidade.Pessoa);
  PessoaBO.Free;
end;

procedure TUsuarioBO.depoisAlterar(entidade: TUsuario);
begin
  inherited;

end;

procedure TUsuarioBO.depoisExcluir(entidade: TUsuario);
var
  Pessoa: TPessoa;
  PessoaBO: TPessoaBO;
begin
  Pessoa := TPessoa.Create(entidade.codigoUsuario);
  PessoaBO := TPessoaBO.Create(Self.Conexao);
  PessoaBO.excluir(Pessoa);
  PessoaBO.Free;
  Pessoa.Free;

end;

procedure TUsuarioBO.depoisIncluir(entidade: TUsuario);
begin
  inherited;

end;

procedure TUsuarioBO.validar(entidade: TUsuario);
begin
  if entidade.Pessoa = nil then
  begin
    raise Exception.Create('Pessoa n�o foi informada');
  end;

end;

function TUsuarioBO.listarUsuarioPor(listaParametros
  : TDictionary<String, Variant>): TFDMemTable;
begin
  listaParametros.Add('Disponivel', True);
  result := Self.listarPor(listaParametros);
end;

function TUsuarioBO.ListarUsuarioPorEmpresa(CodigoEmpresa: Integer)
  : TFDMemTable;
var
  listaParametros: TDictionary<String, Variant>;
begin
  listaParametros := TDictionary<String, Variant>.Create;
  listaParametros.Add('CodigoEmpresa', CodigoEmpresa);
  result := Self.listarUsuarioPor(listaParametros);
end;

function TUsuarioBO.ObterPorCodigo(codigoUsuario: Integer): TUsuario;
var
  listaParametros: TDictionary<String, Variant>;
begin
  if codigoUsuario <= 0 then
    raise Exception.Create('C�digo do usu�rio n�o foi informado');

  listaParametros := TDictionary<String, Variant>.Create;
  listaParametros.Add('CodigoUsuario', codigoUsuario);

  result := FormatarUsuario(Self.listarPor(listaParametros));
end;

function TUsuarioBO.ObterPorLoginSenha(login, senha: String): TUsuario;
var
  listaParametros: TDictionary<String, Variant>;
  dados: TFDMemTable;
begin
  listaParametros := TDictionary<String, Variant>.Create;
  listaParametros.Add('Login', login);
  listaParametros.Add('Senha', senha);
  listaParametros.Add('Disponivel', True);

  result := FormatarUsuario(Self.listarPor(listaParametros));
end;

function TUsuarioBO.FormatarUsuario(dados: TFDMemTable): TUsuario;
var
  Pessoa: TPessoa;
  TelefoneBO: TTelefoneBO;
  Telefone: TTelefone;
  listaTelefone: TList<TTelefone>;
  EmailBO: TEmailBO;
  Email: TEmail;
  Usuario: TUsuario;
begin
  Usuario := nil;
  if Not dados.IsEmpty then
  begin
    with dados do
    begin
      Pessoa := TPessoa.Create(FieldByName('CodigoUsuario').AsInteger,
        FieldByName('Nome').AsString, FieldByName('Observacao').AsString);

      Usuario := TUsuario.Create(FieldByName('Login').AsString,
        FieldByName('Senha').AsString, Pessoa);

      TelefoneBO := TTelefoneBO.Create;
      listaTelefone := TelefoneBO.obterPorPessoa
        (TTelefone.Create(Pessoa.CodigoPessoa));

      if Assigned(listaTelefone) then
      begin
        for Telefone in listaTelefone do
        begin
          Pessoa.adicionarTelefone(Telefone);
        end;
      end;

      EmailBO := TEmailBO.Create;
      Email := EmailBO.obterPorPessoa(TEmail.Create(Pessoa.CodigoPessoa));

      if Assigned(Email) then
      begin
        Pessoa.adicionarEmail(Email);
      end;

      TelefoneBO.Free;
      EmailBO.Free;
    end;
  end;

  result := Usuario;
end;

end.

unit PessoaJuridicaBO;

interface

uses
  PessoaJuridicaDAO, PessoaJuridica, Funcoes;

type
  TPessoaJuridicaBO = class(TPessoaJuridicaDAO)

  protected
    procedure antesAlterar(entidade: TPessoaJuridica); override;
    procedure antesExcluir(entidade: TPessoaJuridica); override;
    procedure antesIncluir(entidade: TPessoaJuridica); override;
    procedure depoisAlterar(entidade: TPessoaJuridica); override;
    procedure depoisExcluir(entidade: TPessoaJuridica); override;
    procedure depoisIncluir(entidade: TPessoaJuridica); override;
    procedure validar(entidade: TPessoaJuridica); override;
  end;

implementation

uses
  System.SysUtils;

{ TPessoaJuridicaBO }

procedure TPessoaJuridicaBO.antesAlterar(entidade: TPessoaJuridica);
begin
  inherited;

end;

procedure TPessoaJuridicaBO.antesExcluir(entidade: TPessoaJuridica);
begin
  inherited;

end;

procedure TPessoaJuridicaBO.antesIncluir(entidade: TPessoaJuridica);
begin
  inherited;

end;

procedure TPessoaJuridicaBO.depoisAlterar(entidade: TPessoaJuridica);
begin
  inherited;

end;

procedure TPessoaJuridicaBO.depoisExcluir(entidade: TPessoaJuridica);
begin
  inherited;

end;

procedure TPessoaJuridicaBO.depoisIncluir(entidade: TPessoaJuridica);
begin
  inherited;

end;

procedure TPessoaJuridicaBO.validar(entidade: TPessoaJuridica);
begin
  inherited;
  entidade.Cnpj := TFuncoes.unMask(entidade.Cnpj);
  if Trim(entidade.Cnpj) = EmptyStr then
    raise Exception.Create('Pessoa Jur�dica: CNPJ n�o foi informado');
  if Trim(entidade.RazaoSocial) = EmptyStr then
    raise Exception.Create('Pessoa Jur�dica: Raz�o social n�o foi informado');
end;

end.

unit PessoaEmpresaBO;

interface

uses PessoaEmpresaDAO, PessoaEmpresa;

type
  TPessoaEmpresaBO = class(TPessoaEmpresaDAO)
    procedure validar(entidade: TPessoaEmpresa); override;
    procedure antesIncluir(entidade: TPessoaEmpresa); override;
    procedure depoisIncluir(entidade: TPessoaEmpresa); override;
    procedure antesAlterar(entidade: TPessoaEmpresa); override;
    procedure depoisAlterar(entidade: TPessoaEmpresa); override;
    procedure antesExcluir(entidade: TPessoaEmpresa); override;
    procedure depoisExcluir(entidade: TPessoaEmpresa); override;
  end;

implementation

{ TPessoaEmpresaBO }

procedure TPessoaEmpresaBO.antesAlterar(entidade: TPessoaEmpresa);
begin
  inherited;

end;

procedure TPessoaEmpresaBO.antesExcluir(entidade: TPessoaEmpresa);
begin
  inherited;

end;

procedure TPessoaEmpresaBO.antesIncluir(entidade: TPessoaEmpresa);
begin
  inherited;

end;

procedure TPessoaEmpresaBO.depoisAlterar(entidade: TPessoaEmpresa);
begin
  inherited;

end;

procedure TPessoaEmpresaBO.depoisExcluir(entidade: TPessoaEmpresa);
begin
  inherited;

end;

procedure TPessoaEmpresaBO.depoisIncluir(entidade: TPessoaEmpresa);
begin
  inherited;

end;

procedure TPessoaEmpresaBO.validar(entidade: TPessoaEmpresa);
begin
  inherited;

end;


end.

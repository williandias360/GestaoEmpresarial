unit EstadoBO;

interface

uses
  EstadoDAO, Estado, FireDAC.Comp.Client;
type
  TEstadoBO = class(TEstadoDAO)
  public
    function listarEstados:TFDMemTable;
  protected
    procedure antesAlterar(entidade: TEstado); override;
    procedure antesExcluir(entidade: TEstado); override;
    procedure antesIncluir(entidade: TEstado); override;
    procedure depoisAlterar(entidade: TEstado); override;
    procedure depoisExcluir(entidade: TEstado); override;
    procedure depoisIncluir(entidade: TEstado); override;
    procedure validar(entidade: TEstado); override;
  end;

implementation

{ TEstadoBO }

procedure TEstadoBO.antesAlterar(entidade: TEstado);
begin
  inherited;

end;

procedure TEstadoBO.antesExcluir(entidade: TEstado);
begin
  inherited;

end;

procedure TEstadoBO.antesIncluir(entidade: TEstado);
begin
  inherited;

end;

procedure TEstadoBO.depoisAlterar(entidade: TEstado);
begin
  inherited;

end;

procedure TEstadoBO.depoisExcluir(entidade: TEstado);
begin
  inherited;

end;

procedure TEstadoBO.depoisIncluir(entidade: TEstado);
begin
  inherited;

end;


procedure TEstadoBO.validar(entidade: TEstado);
begin
  inherited;

end;

function TEstadoBO.listarEstados: TFDMemTable;
begin
  result:= Self.listarPor;
end;

end.

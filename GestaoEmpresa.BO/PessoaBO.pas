unit PessoaBO;

interface

uses
  PessoaDAO, Pessoa, EmailBO, Bairro;

type
  TPessoaBO = class(TPessoaDAO)
  protected
    procedure validar(entidade: TPessoa); override;
    procedure antesIncluir(entidade: TPessoa); override;
    procedure depoisIncluir(entidade: TPessoa); override;
    procedure antesAlterar(entidade: TPessoa); override;
    procedure depoisAlterar(entidade: TPessoa); override;
    procedure antesExcluir(entidade: TPessoa); override;
    procedure depoisExcluir(entidade: TPessoa); override;
  end;

implementation

uses
  PessoaFisicaBO, PessoaJuridicaBO, Endereco, EnderecoBO, Email, TelefoneBO,
  Telefone, System.SysUtils, FireDAC.Comp.Client, PessoaFisica, PessoaJuridica;

{ TPessoaNegocios }

procedure TPessoaBO.antesAlterar(entidade: TPessoa);
var
  TelefoneBO: TTelefoneBO;
  EnderecoBO: TEnderecoBO;
  EmailBO: TEmailBO;
  Email: TEmail;
  Endereco: TEndereco;
  Telefone: TTelefone;
  listaEndereco: TFDMemTable;
  TemPrincipal:Boolean;
begin
  EmailBO := TEmailBO.Create(Self.Conexao);
  if Not Assigned(entidade.ListaEmail) then
  begin
    EmailBO.excluirEmailPorPessoa(TEmail.Create(entidade.CodigoPessoa));
  end
  else
  begin
    TemPrincipal:= False;
    for Email in entidade.ListaEmail do
    begin
      Email.CodigoPessoa := entidade.CodigoPessoa;
      if Email.CodigoEmail > 0 then
        EmailBO.alterar(Email)
      else
        EmailBO.incluir(Email);
      if Email.Principal then
       TemPrincipal:= True;
    end;

    if Not TemPrincipal then
    begin
      Email:= entidade.ListaEmail[0];
      Email.Principal:= True;
      EmailBO.alterar(Email);
    end;

  end;
  EmailBO.Free;

  EnderecoBO := TEnderecoBO.Create(Self.Conexao);
  if Not Assigned(entidade.listaEndereco) then
  begin
    listaEndereco := EnderecoBO.listarPorPessoa
      (TEndereco.Create(entidade.CodigoPessoa));
    listaEndereco.First;
    while Not listaEndereco.Eof do
    begin
      Endereco := TEndereco.Create(listaEndereco.FieldByName('CodigoEndereco')
        .AsInteger,

        TBairro.Create(listaEndereco.FieldByName('CodigoBairro').AsInteger));
      EnderecoBO.excluir(Endereco);

      listaEndereco.Next;
    end;

  end
  else
  begin
    TemPrincipal:= False;
    for Endereco in entidade.listaEndereco do
    begin
      Endereco.CodigoPessoa := entidade.CodigoPessoa;
      if Endereco.CodigoEndereco > 0 then
        EnderecoBO.alterar(Endereco)
      else
        EnderecoBO.incluir(Endereco);
      if Endereco.Principal then
        TemPrincipal:= True;
    end;

    if Not TemPrincipal then
    begin
      Endereco:= entidade.ListaEndereco[0];
      Endereco.Principal:= True;
      EnderecoBO.alterar(Endereco);
    end;

  end;
  EnderecoBO.Free;

  TelefoneBO := TTelefoneBO.Create(Self.Conexao);
  if Not Assigned(entidade.ListaTelefone) then
  begin
    TelefoneBO.excluirPorPessoa(TTelefone.Create(entidade.CodigoPessoa));
  end
  else
  begin
    TemPrincipal:= False;
    for Telefone in entidade.ListaTelefone do
    begin
      Telefone.CodigoPessoa := entidade.CodigoPessoa;
      if Telefone.CodigoTelefone > 0 then
        TelefoneBO.alterar(Telefone)
      else
        TelefoneBO.incluir(Telefone);
      if Telefone.Principal then
        TemPrincipal:= True;
    end;
    if Not TemPrincipal then
    begin
      Telefone:= entidade.ListaTelefone[0];
      Telefone.Principal:= True;
      TelefoneBO.alterar(Telefone);
    end;
  end;
  TelefoneBO.Free;
end;

procedure TPessoaBO.antesExcluir(entidade: TPessoa);
var
  pessoaJuridicaBO:TPessoaJuridicaBO;
  pessoaFisicaBO:TPessoaFisicaBO;
  telefoneBO:TTelefoneBO;
  endereco:TEndereco;
  enderecoBO:TEnderecoBO;
  email:TEmail;
  emailBO:TEmailBO;
begin
    pessoaJuridicaBO:= TPessoaJuridicaBO.Create(Self.Conexao);
    pessoaJuridicaBO.excluir(TPessoaJuridica.Create(entidade.CodigoPessoa));

    pessoaFisicaBO:= TPessoaFisicaBO.Create(Self.Conexao);
    pessoaFisicaBO.excluir(TPessoaFisica.Create(entidade.CodigoPessoa));

    telefoneBO:= TTelefoneBO.Create(Self.Conexao);
    telefoneBO.excluirPorPessoa(TTelefone.Create(entidade.CodigoPessoa));

    enderecoBO:= TEnderecoBO.Create(Self.Conexao);
    enderecoBO.excluirPorPessoa(TEndereco.Create(entidade.CodigoPessoa));

    emailBO:= TEmailBO.Create(Self.Conexao);
    emailBO.excluirEmailPorPessoa(TEmail.Create(entidade.CodigoPessoa));

    pessoaJuridicaBO.Free;
    pessoaFisicaBO.Free;
    telefoneBO.Free;
    enderecoBO.Free;
    emailBO.Free;
end;

procedure TPessoaBO.antesIncluir(entidade: TPessoa);
begin

end;

procedure TPessoaBO.depoisAlterar(entidade: TPessoa);
var
  PessoaFisicaBO: TPessoaFisicaBO;
  PessoaJuridicaBO: TPessoaJuridicaBO;
begin
  PessoaFisicaBO := TPessoaFisicaBO.Create(Self.Conexao);
  PessoaFisicaBO.excluir(TPessoaFisica.Create(entidade.CodigoPessoa));

  if Assigned(entidade.PessoaFisica) then
  begin
    entidade.PessoaFisica.CodigoPessoa := entidade.CodigoPessoa;
    PessoaFisicaBO.incluir(entidade.PessoaFisica);
  end;
  PessoaFisicaBO.Free;

  PessoaJuridicaBO := TPessoaJuridicaBO.Create(Self.Conexao);
  PessoaJuridicaBO.excluir(TPessoaJuridica.Create(entidade.CodigoPessoa));

  if Assigned(entidade.PessoaJuridica) then
  begin
    entidade.PessoaJuridica.CodigoPessoa := entidade.CodigoPessoa;
    PessoaJuridicaBO.incluir(entidade.PessoaJuridica);
  end;
  PessoaJuridicaBO.Free;

end;

procedure TPessoaBO.depoisExcluir(entidade: TPessoa);
begin

end;

procedure TPessoaBO.depoisIncluir(entidade: TPessoa);
var
  PessoaFisicaBO: TPessoaFisicaBO;
  PessoaJuridicaBO: TPessoaJuridicaBO;
  Endereco: TEndereco;
  EnderecoBO: TEnderecoBO;
  Email: TEmail;
  EmailBO: TEmailBO;
  Telefone: TTelefone;
  TelefoneBO: TTelefoneBO;
  TemPrincipal:Boolean;
begin
  if entidade.PessoaFisica <> Nil then
  begin
    PessoaFisicaBO := TPessoaFisicaBO.Create(Self.Conexao);
    entidade.PessoaFisica.CodigoPessoa := entidade.CodigoPessoa;
    PessoaFisicaBO.incluir(entidade.PessoaFisica);
    PessoaFisicaBO.Free;
  end;

  if entidade.PessoaJuridica <> Nil then
  begin
    PessoaJuridicaBO := TPessoaJuridicaBO.Create(Self.Conexao);
    entidade.PessoaJuridica.CodigoPessoa := entidade.CodigoPessoa;
    PessoaJuridicaBO.incluir(entidade.PessoaJuridica);
    PessoaJuridicaBO.Free;
  end;

  if Assigned(entidade.listaEndereco) then
  begin
    TemPrincipal:= False;
    EnderecoBO := TEnderecoBO.Create(Self.Conexao);
    for Endereco in entidade.listaEndereco do
    begin
      Endereco.CodigoPessoa := entidade.CodigoPessoa;
      EnderecoBO.incluir(Endereco);
      if Endereco.Principal then
        TemPrincipal := True;
    end;

    if Not TemPrincipal then
    begin
      Endereco:= entidade.ListaEndereco[0];
      Endereco.Principal:= True;
      EnderecoBO.alterar(Endereco);
    end;

    EnderecoBO.Free;
  end;

  if Assigned(entidade.ListaEmail) then
  begin
    TemPrincipal := False;
    EmailBO := TEmailBO.Create(Self.Conexao);
    for Email in entidade.ListaEmail do
    begin
      Email.CodigoPessoa := entidade.CodigoPessoa;
      EmailBO.incluir(Email);
      if Email.Principal then
        TemPrincipal := True;
    end;

    if Not TemPrincipal then
    begin
      Email:= entidade.ListaEmail[0];
      Email.Principal:= True;
      EmailBO.alterar(Email);
    end;

    EmailBO.Free;
  end;

  if Assigned(entidade.ListaTelefone) then
  begin
    TemPrincipal := False;
    TelefoneBO := TTelefoneBO.Create(Self.Conexao);
    for Telefone in entidade.ListaTelefone do
    begin
      Telefone.CodigoPessoa := entidade.CodigoPessoa;
      TelefoneBO.incluir(Telefone);
      if Telefone.Principal then
        TemPrincipal := True;
    end;

    if Not TemPrincipal then
    begin
      Telefone:= entidade.ListaTelefone[0];
      Telefone.Principal:= True;
      TelefoneBO.alterar(Telefone);
    end;

    TelefoneBO.Free;
  end;

end;

procedure TPessoaBO.validar(entidade: TPessoa);
begin
  if Trim(entidade.Nome) = EmptyStr then
    raise Exception.Create('Pessoa: Nome n�o foi informado');
end;

end.

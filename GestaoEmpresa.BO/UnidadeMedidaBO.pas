unit UnidadeMedidaBO;

interface

uses
  UnidadeMedidaDAO, UnidadeMedida, FireDAC.Comp.Client,
  System.Generics.Collections, Globals;

type
  TUnidadeMedidaBO = class(TUnidadeMedidaDAO)
  private

  public
    function ListarUnidadeMedidaPor(listaParametros: TDictionary<String, Variant>)
      : TFDMemTable;
    function ObterPorCodigo(codigo: Integer): TUnidadeMedida;
    function ListarUnidadeMedidaDisponivel: TFDMemTable;
  protected
    procedure validar(entidade: TUnidadeMedida); override;
    procedure antesIncluir(entidade: TUnidadeMedida); override;
    procedure depoisIncluir(entidade: TUnidadeMedida); override;
    procedure antesAlterar(entidade: TUnidadeMedida); override;
    procedure depoisAlterar(entidade: TUnidadeMedida); override;
    procedure antesExcluir(entidade: TUnidadeMedida); override;
    procedure depoisExcluir(entidade: TUnidadeMedida); override;
  end;

implementation

uses
  System.SysUtils, UnidadeMedidaEmpresaBO, UnidadeMedidaEmpresa;

{ TUnidadeMedidaBO }

procedure TUnidadeMedidaBO.antesAlterar(entidade: TUnidadeMedida);
begin
  inherited;

end;

procedure TUnidadeMedidaBO.antesExcluir(entidade: TUnidadeMedida);
begin
  inherited;

end;

procedure TUnidadeMedidaBO.antesIncluir(entidade: TUnidadeMedida);
begin
  inherited;

end;

procedure TUnidadeMedidaBO.depoisAlterar(entidade: TUnidadeMedida);
begin
  inherited;

end;

procedure TUnidadeMedidaBO.depoisExcluir(entidade: TUnidadeMedida);
begin
  inherited;

end;

procedure TUnidadeMedidaBO.depoisIncluir(entidade: TUnidadeMedida);
var
  UnidadeMedidaEmpresaBO:TUnidadeMedidaEmpresaBO;
begin
  UnidadeMedidaEmpresaBO:= TUnidadeMedidaEmpresaBO.Create(Self.Conexao);
  UnidadeMedidaEmpresaBO.incluir(TUnidadeMedidaEmpresa.Create(entidade.CodigoUnidadeMedida, TGlobals.GetEmpresa.CodigoEmpresa));

end;

procedure TUnidadeMedidaBO.validar(entidade: TUnidadeMedida);
begin
  if Trim(entidade.Nome) = EmptyStr then
    raise Exception.Create('Nome da unidade de medida n�o foi informado');

  if Trim(entidade.Sigla) = EmptyStr then
    raise Exception.Create('Sigla da entidade de medida n�o foi informado');

end;

function TUnidadeMedidaBO.ListarUnidadeMedidaPor(listaParametros
  : TDictionary<String, Variant>): TFDMemTable;
begin
  listaParametros.Add('CodigoEmpresa', TGlobals.GetEmpresa.CodigoEmpresa);
  result := Self.listarPor(listaParametros)
end;

function TUnidadeMedidaBO.ListarUnidadeMedidaDisponivel():TFDMemTable;
var
  listaParametros:TDictionary<String, Variant>;
begin
   listaParametros:= TDictionary<String,Variant>.Create();
    result:= Self.ListarUnidadeMedidaPor(listaParametros);
end;

function TUnidadeMedidaBO.ObterPorCodigo(codigo: Integer): TUnidadeMedida;
var
  listaParametros: TDictionary<String, Variant>;
  UnidadeMedida: TUnidadeMedida;
  dados: TFDMemTable;
begin
  UnidadeMedida := nil;
  if codigo <= 0 then
    raise Exception.Create('C�digo da unidade de medida n�o foi informado');

  listaParametros := TDictionary<String, Variant>.Create();
  listaParametros.Add('CodigoUnidadeMedida', codigo);
  listaParametros.Add('CodigoEmpresa', TGlobals.GetEmpresa.CodigoEmpresa);

  dados := Self.listarPor(listaParametros);
  if Not dados.IsEmpty then
  begin
    UnidadeMedida := TUnidadeMedida.Create
      (dados.FieldByName('CodigoUnidadeMedida').AsInteger,
      dados.FieldByName('Nome').AsString, dados.FieldByName('Sigla').AsString);
  end;

  result := UnidadeMedida;

end;

end.

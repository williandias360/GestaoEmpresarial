unit CidadeBO;

interface

uses
  CidadeDAO, Cidade, FireDAC.Comp.Client;
type
  TCidadeBO = class(TCidadeDAO)
  public
    function listarPorEstado(codigoEstado: Integer): TFDMemTable;

  protected
    procedure antesAlterar(entidade: TCidade); override;
    procedure antesExcluir(entidade: TCidade); override;
    procedure antesIncluir(entidade: TCidade); override;
    procedure depoisAlterar(entidade: TCidade); override;
    procedure depoisExcluir(entidade: TCidade); override;
    procedure depoisIncluir(entidade: TCidade); override;
    procedure validar(entidade: TCidade); override;
  end;

implementation

uses
  System.SysUtils, Tabelas, Campo, ICampo, Valor, System.Generics.Collections, 
  Pair;

{ TCidadeBO }

procedure TCidadeBO.antesAlterar(entidade: TCidade);
begin
  inherited;

end;

procedure TCidadeBO.antesExcluir(entidade: TCidade);
begin
  inherited;

end;

procedure TCidadeBO.antesIncluir(entidade: TCidade);
begin
  inherited;

end;

procedure TCidadeBO.depoisAlterar(entidade: TCidade);
begin
  inherited;

end;

procedure TCidadeBO.depoisExcluir(entidade: TCidade);
begin
  inherited;

end;

procedure TCidadeBO.depoisIncluir(entidade: TCidade);
begin
  inherited;

end;

procedure TCidadeBO.validar(entidade: TCidade);
begin
  inherited;

end;

function TCidadeBO.listarPorEstado(codigoEstado:Integer):TFDMemTable;
begin
  if CodigoEstado = 0 then
  begin 
    raise Exception.Create('Cidade: C�digo do estado n�o foi informado');
  end;
  
  result:= Self.listarPorUf(codigoEstado);
end;

end.

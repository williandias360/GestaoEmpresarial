unit ProdutoBO;

interface

uses
  ProdutoDAO, Produto, FireDAC.Comp.Client, System.Generics.Collections;

type
  TProdutoBO = class(TProdutoDAO)
  public
    function ListarProdutoPor(listaParametros: TDictionary<String, Variant>)
      : TFDMemTable;
    function ObterPorCodigo(CodigoProduto: Integer): TProduto;
  protected
    procedure antesAlterar(entidade: TProduto); override;
    procedure antesExcluir(entidade: TProduto); override;
    procedure antesIncluir(entidade: TProduto); override;
    procedure depoisAlterar(entidade: TProduto); override;
    procedure depoisExcluir(entidade: TProduto); override;
    procedure depoisIncluir(entidade: TProduto); override;
    procedure validar(entidade: TProduto); override;
  end;

implementation

uses
  System.SysUtils, ProdutoEmpresaBO, ProdutoEmpresa, Globals, ProdutoPrecoBO,
  ProdutoPreco;

{ TProdutoBO }

procedure TProdutoBO.antesAlterar(entidade: TProduto);
begin
  if entidade.QuantidadePorCaixa <= 0 then
    entidade.QuantidadePorCaixa := 1;

end;

procedure TProdutoBO.antesExcluir(entidade: TProduto);
begin
  inherited;

end;

procedure TProdutoBO.antesIncluir(entidade: TProduto);
begin
  if entidade.QuantidadePorCaixa <= 0 then
    entidade.QuantidadePorCaixa := 1;

end;

procedure TProdutoBO.depoisAlterar(entidade: TProduto);
var
  ProdutoPrecoBO: TProdutoPrecoBO;
  ProdutoPreco: TProdutoPreco;
begin
  ProdutoPrecoBO := TProdutoPrecoBO.Create(Self.Conexao);
  ProdutoPrecoBO.excluir(TProdutoPreco.Create(entidade.CodigoProduto));

  for ProdutoPreco in entidade.ListaProdutoPreco do
  begin
    ProdutoPreco.CodigoProduto:= entidade.CodigoProduto;
    ProdutoPrecoBO.incluir(ProdutoPreco);
  end;
  ProdutoPrecoBO.Free;

end;

procedure TProdutoBO.depoisExcluir(entidade: TProduto);
begin
  inherited;

end;

procedure TProdutoBO.depoisIncluir(entidade: TProduto);
var
  ProdutoEmpresaBO: TProdutoEmpresaBO;
  ProdutoPrecoBO: TProdutoPrecoBO;
  ProdutoPreco: TProdutoPreco;
begin
  ProdutoEmpresaBO := TProdutoEmpresaBO.Create(Self.Conexao);
  ProdutoEmpresaBO.incluir(TProdutoEmpresa.Create(entidade.CodigoProduto,
    TGlobals.GetEmpresa.CodigoEmpresa));
  ProdutoEmpresaBO.Free;

  ProdutoPrecoBO := TProdutoPrecoBO.Create(Self.Conexao);
  for ProdutoPreco in entidade.ListaProdutoPreco do
  begin
    ProdutoPreco.CodigoProduto := entidade.CodigoProduto;
    ProdutoPrecoBO.incluir(ProdutoPreco);
  end;
  ProdutoPrecoBO.Free;

end;

procedure TProdutoBO.validar(entidade: TProduto);
begin
  if Trim(entidade.Nome) = EmptyStr then
    raise Exception.Create('Nome do produto n�o foi informado');

end;

function TProdutoBO.ListarProdutoPor(listaParametros
  : TDictionary<String, Variant>): TFDMemTable;
begin
  listaParametros.Add('CodigoEmpresa', TGlobals.GetEmpresa.CodigoEmpresa);
  listaParametros.Add('Disponivel', True);
  Result := Self.ListarPor(listaParametros);
end;

function TProdutoBO.ObterPorCodigo(CodigoProduto: Integer): TProduto;
var
  Produto: TProduto;
  listaParametros: TDictionary<String, Variant>;
  dados: TFDMemTable;
  ProdutoPrecoBO: TProdutoPrecoBO;
begin
  Produto := nil;
  if CodigoProduto <= 0 then
    raise Exception.Create('C�digo do produto inv�lido');

  listaParametros := TDictionary<String, Variant>.Create();;
  listaParametros.Add('CodigoProduto', CodigoProduto);

  dados := Self.ListarProdutoPor(listaParametros);

  if Not dados.IsEmpty then
  begin
    Produto := TProduto.Create;
    with dados do
    begin
      Produto.CodigoProduto := FieldByName('CodigoProduto').AsInteger;
      Produto.CodigoProdutoGrupo := FieldByName('CodigoProdutoGrupo').AsInteger;
      Produto.CodigoUnidadeMedida := FieldByName('CodigoUnidadeMedida')
        .AsInteger;
      Produto.Nome := FieldByName('Nome').AsString;
      Produto.QuantidadePorCaixa := FieldByName('QuantidadePorCaixa').AsFloat;
      Produto.CodigoBarras := FieldByName('CodigoBarras').AsString;
      Produto.EstoqueAtual := FieldByName('EstoqueAtual').AsFloat;
      Produto.Ncm:= FieldByName('Ncm').AsString;
      Produto.EstoqueMinimo:= FieldByName('EstoqueMinimo').AsFloat;

      ProdutoPrecoBO := TProdutoPrecoBO.Create;
      Produto.ListaProdutoPreco := ProdutoPrecoBO.ListarPrecoPorProduto
        (Produto.CodigoProduto);

    end;

  end;

  Result:= Produto;

end;

end.

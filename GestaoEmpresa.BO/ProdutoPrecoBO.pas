unit ProdutoPrecoBO;

interface

uses
  ProdutoPrecoDAO, ProdutoPreco, System.Classes, System.Generics.Collections;

type
  TProdutoPrecoBO = class(TProdutoPrecoDAO)
  protected
    procedure validar(entidade: TProdutoPreco); override;
    procedure antesIncluir(entidade: TProdutoPreco); override;
    procedure depoisIncluir(entidade: TProdutoPreco); override;
    procedure antesAlterar(entidade: TProdutoPreco); override;
    procedure depoisAlterar(entidade: TProdutoPreco); override;
    procedure antesExcluir(entidade: TProdutoPreco); override;
    procedure depoisExcluir(entidade: TProdutoPreco); override;
  public
    function ListarPrecoPorProduto(codigoProduto: Integer)
      : TObjectList<TProdutoPreco>;
  end;

implementation

uses
  System.SysUtils, FireDAC.Comp.Client;

{ TProdutoPrecoBO }

procedure TProdutoPrecoBO.antesAlterar(entidade: TProdutoPreco);
begin
  inherited;

end;

procedure TProdutoPrecoBO.antesExcluir(entidade: TProdutoPreco);
begin
  inherited;

end;

procedure TProdutoPrecoBO.antesIncluir(entidade: TProdutoPreco);
begin
  inherited;

end;

procedure TProdutoPrecoBO.depoisAlterar(entidade: TProdutoPreco);
begin
  inherited;

end;

procedure TProdutoPrecoBO.depoisExcluir(entidade: TProdutoPreco);
begin
  inherited;

end;

procedure TProdutoPrecoBO.depoisIncluir(entidade: TProdutoPreco);
begin
  inherited;

end;

function TProdutoPrecoBO.ListarPrecoPorProduto(codigoProduto: Integer)
  : TObjectList<TProdutoPreco>;
var
  listaParametros:TDictionary<String,Variant>;
  ListaProdutoPreco:TObjectList<TProdutoPreco>;
  dados:TFDMemTable;
  ProdutoPreco:TProdutoPreco;
begin
  if codigoProduto <= 0 then
    raise Exception.Create('C�digo do produto n�o foi informado');

    listaParametros:= TDictionary<String,Variant>.Create();
    listaParametros.Add('CodigoProduto', codigoProduto);

    ListaProdutoPreco:= TObjectList<TProdutoPreco>.Create;

    dados:= Self.ListarPor(listaParametros);
    if Not dados.IsEmpty then
    begin
      dados.First;
      while Not dados.Eof do
      begin
        ProdutoPreco:= TProdutoPreco.Create;

        ProdutoPreco.CodigoProduto:= dados.FieldByName('codigoProduto').AsInteger;
        ProdutoPreco.CodigoProdutoPrecoTipo:= dados.FieldByName('CodigoProdutoPrecoTipo').AsInteger;
        ProdutoPreco.Valor:= dados.FieldByName('Valor').AsCurrency;
        ListaProdutoPreco.Add(ProdutoPreco);

        dados.Next;
      end;
    end;

    Result:= ListaProdutoPreco;
end;

procedure TProdutoPrecoBO.validar(entidade: TProdutoPreco);
begin
  inherited;

end;

end.

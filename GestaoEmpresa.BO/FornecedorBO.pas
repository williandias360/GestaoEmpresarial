unit FornecedorBO;

interface

uses
  FornecedorDAO, Fornecedor, FireDAC.Comp.Client, System.Generics.Collections;

type
  TFornecedorBO = class(TFornecedorDAO)
  private

  protected
    procedure validar(entidade: TFornecedor); override;
    procedure antesIncluir(entidade: TFornecedor); override;
    procedure depoisIncluir(entidade: TFornecedor); override;
    procedure antesAlterar(entidade: TFornecedor); override;
    procedure depoisAlterar(entidade: TFornecedor); override;
    procedure antesExcluir(entidade: TFornecedor); override;
    procedure depoisExcluir(entidade: TFornecedor); override;
  public
    function listarFornecedorPor(listaParametros: TDictionary<String, Variant>)
      : TFDMemTable;
    function obterPorCodigo(codigoFornecedor: Integer): TFornecedor;
  end;

implementation

uses
  PessoaBO, Pessoa, System.SysUtils, PessoaJuridica, PessoaFisica, Endereco,
  EnderecoBO, TelefoneBO, Telefone, EmailBO, Email;

{ TFornecedorBO }

procedure TFornecedorBO.antesAlterar(entidade: TFornecedor);
var
  PessoaBO: TPessoaBO;
  TelefoneBO:TTelefoneBO;
  EmailBO:TEmailBO;
  EnderecoBO:TEnderecoBO;
begin
  EmailBO:= TEmailBO.Create(Self.Conexao);
  EmailBO.excluirEmailPorPessoa(TEmail.Create(entidade.Pessoa.CodigoPessoa));

  TelefoneBO:= TTelefoneBO.Create(Self.Conexao);
  TelefoneBO.excluirPorPessoa(TTelefone.Create(entidade.Pessoa.CodigoPessoa));

  EnderecoBO:= TEnderecoBO.Create(Self.Conexao);
  EnderecoBO.excluirPorPessoa(TEndereco.Create(entidade.Pessoa.CodigoPessoa));

  PessoaBO := TPessoaBO.Create(Self.Conexao);
  entidade.Pessoa.PerfilFornecedor := True;
  PessoaBO.alterar(entidade.Pessoa);
  PessoaBO.Free;
  EmailBO.Free;
  TelefoneBO.Free;
  EnderecoBO.Free;
end;

procedure TFornecedorBO.antesExcluir(entidade: TFornecedor);
begin

end;

procedure TFornecedorBO.antesIncluir(entidade: TFornecedor);
var
  PessoaBO: TPessoaBO;
begin
  PessoaBO := TPessoaBO.Create(Self.Conexao);
  entidade.Pessoa.PerfilFornecedor := True;
  PessoaBO.incluir(entidade.Pessoa);
  PessoaBO.Free;
end;

procedure TFornecedorBO.depoisAlterar(entidade: TFornecedor);
begin
  inherited;
end;

procedure TFornecedorBO.depoisExcluir(entidade: TFornecedor);
var
  Pessoa: TPessoa;
  PessoaBO: TPessoaBO;
begin
  Pessoa := TPessoa.Create(entidade.codigoFornecedor);
  PessoaBO := TPessoaBO.Create(Self.Conexao);
  PessoaBO.excluir(Pessoa);
  PessoaBO.Free;
  Pessoa.Free;
end;

procedure TFornecedorBO.depoisIncluir(entidade: TFornecedor);
begin
  inherited;

end;

procedure TFornecedorBO.validar(entidade: TFornecedor);
begin
  if entidade.Pessoa = nil then
  begin
    raise Exception.Create('Pessoa n�o foi informada');
  end;

end;

function TFornecedorBO.listarFornecedorPor(listaParametros
  : TDictionary<String, Variant>): TFDMemTable;
begin
  listaParametros.Add('Disponivel', True);

  result := Self.listarPor(listaParametros);
end;

function TFornecedorBO.obterPorCodigo(codigoFornecedor: Integer): TFornecedor;
var
  listaParametros: TDictionary<String, Variant>;
  dados: TFDMemTable;
  Pessoa: TPessoa;
  Fornecedor: TFornecedor;
  CpfCnpj, RgIe: String;
  EnderecoBO:TEnderecoBO;
  Endereco:TEndereco;
  TelefoneBO:TTelefoneBO;
  Telefone:TTelefone;
  listaTelefone:TList<TTelefone>;
  EmailBO:TEmailBO;
  Email:TEmail;
begin
  Fornecedor:= nil;
  if codigoFornecedor <= 0 then
    raise Exception.Create('C�digo do fornecedor n�o foi informado');

  listaParametros := TDictionary<String, Variant>.Create;
  listaParametros.Add('CodigoFornecedor', codigoFornecedor);

  dados := Self.listarPor(listaParametros);

  if Not dados.IsEmpty then
  begin
    Pessoa := TPessoa.Create(dados.FieldByName('CodigoFornecedor').AsInteger,
      dados.FieldByName('Nome').AsString, dados.FieldByName('Observacao')
      .AsString);

    Pessoa.PerfilFornecedor := True;

    Fornecedor := TFornecedor.Create(0, Pessoa);

    CpfCnpj := dados.FieldByName('CpfCnpj').AsString;
    RgIe := dados.FieldByName('RgIe').AsString;

    if CpfCnpj.Trim <> EmptyStr then
    begin
      if CpfCnpj.Length = 14 then
      begin
        Pessoa.PessoaJuridica := TPessoaJuridica.Create(CpfCnpj, RgIe,
          dados.FieldByName('RazaoSocial').AsString);
      end
      else
      begin
        Pessoa.PessoaFisica := TPessoaFisica.Create(CpfCnpj, RgIe);
      end;
    end;

    EnderecoBO:= TEnderecoBO.Create;
    Endereco:= EnderecoBO.obterPorPessoa(Pessoa.CodigoPessoa);
    Pessoa.adicionarEndereco(Endereco);

    TelefoneBO:= TTelefoneBO.Create;
    listaTelefone:= TelefoneBO.obterPorPessoa(TTelefone.Create(Pessoa.CodigoPessoa));

    if Assigned(listaTelefone) then
    begin
      for  Telefone in listaTelefone do
      begin
          Pessoa.adicionarTelefone(Telefone);
      end;
    end;

    EmailBO:= TEmailBO.Create;
    Email:= EmailBO.obterPorPessoa(TEmail.Create(Pessoa.CodigoPessoa));

    if Assigned(Email) then
    begin
      Pessoa.adicionarEmail(Email);
    end;

    EnderecoBO.Free;
    EmailBO.Free;

  end;

  result := Fornecedor;

end;

end.

unit UnidadeMedidaEmpresaBO;

interface

uses
  UnidadeMedidaEmpresaDAO, UnidadeMedidaEmpresa;

type
  TUnidadeMedidaEmpresaBO = class(TUnidadeMedidaEmpresaDAO)
  protected
    procedure validar(entidade: TUnidadeMedidaEmpresa); override;
    procedure antesIncluir(entidade: TUnidadeMedidaEmpresa); override;
    procedure depoisIncluir(entidade: TUnidadeMedidaEmpresa); override;
    procedure antesAlterar(entidade: TUnidadeMedidaEmpresa); override;
    procedure depoisAlterar(entidade: TUnidadeMedidaEmpresa); override;
    procedure antesExcluir(entidade: TUnidadeMedidaEmpresa); override;
    procedure depoisExcluir(entidade: TUnidadeMedidaEmpresa); override;
  end;

implementation

{ TUnidadeMedidaBO }

procedure TUnidadeMedidaEmpresaBO.antesAlterar(entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

procedure TUnidadeMedidaEmpresaBO.antesExcluir(entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

procedure TUnidadeMedidaEmpresaBO.antesIncluir(entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

procedure TUnidadeMedidaEmpresaBO.depoisAlterar(entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

procedure TUnidadeMedidaEmpresaBO.depoisExcluir(entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

procedure TUnidadeMedidaEmpresaBO.depoisIncluir(entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

procedure TUnidadeMedidaEmpresaBO.validar(entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

end.

unit PessoaFisicaBO;

interface

uses
  PessoaFisicaDAO, PessoaFisica, Funcoes;
type
  TPessoaFisicaBO = class(TPessoaFisicaDAO)

  protected
    procedure antesAlterar(entidade: TPessoaFisica); override;
    procedure antesExcluir(entidade: TPessoaFisica); override;
    procedure antesIncluir(entidade: TPessoaFisica); override;
    procedure depoisAlterar(entidade: TPessoaFisica); override;
    procedure depoisExcluir(entidade: TPessoaFisica); override;
    procedure depoisIncluir(entidade: TPessoaFisica); override;
    procedure validar(entidade: TPessoaFisica); override;
  end;

implementation

uses
  System.SysUtils;


{ TPessoaFisicaBO }

procedure TPessoaFisicaBO.antesAlterar(entidade: TPessoaFisica);
begin
  inherited;

end;

procedure TPessoaFisicaBO.antesExcluir(entidade: TPessoaFisica);
begin
  inherited;

end;

procedure TPessoaFisicaBO.antesIncluir(entidade: TPessoaFisica);
begin
  inherited;

end;

procedure TPessoaFisicaBO.depoisAlterar(entidade: TPessoaFisica);
begin
  inherited;

end;

procedure TPessoaFisicaBO.depoisExcluir(entidade: TPessoaFisica);
begin
  inherited;

end;

procedure TPessoaFisicaBO.depoisIncluir(entidade: TPessoaFisica);
begin
  inherited;

end;

procedure TPessoaFisicaBO.validar(entidade: TPessoaFisica);
begin
  inherited;
  entidade.Cpf:= TFuncoes.unMask(entidade.Cpf);
  if Trim(entidade.Cpf) = EmptyStr then
  raise Exception.Create('Pessoa Fis�ca: CPF n�o foi informado');

end;

end.

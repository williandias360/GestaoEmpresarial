unit ProdutoEmpresaBO;

interface

uses
  ProdutoEmpresaDAO, ProdutoEmpresa;

type
  TProdutoEmpresaBO = class(TProdutoEmpresaDAO)
  protected
    procedure antesAlterar(entidade: TProdutoEmpresa); override;
    procedure antesExcluir(entidade: TProdutoEmpresa); override;
    procedure antesIncluir(entidade: TProdutoEmpresa); override;
    procedure depoisAlterar(entidade: TProdutoEmpresa); override;
    procedure depoisExcluir(entidade: TProdutoEmpresa); override;
    procedure depoisIncluir(entidade: TProdutoEmpresa); override;
    procedure validar(entidade: TProdutoEmpresa); override;
  end;

implementation

{ TProdutoEmpresaBO }

procedure TProdutoEmpresaBO.antesAlterar(entidade: TProdutoEmpresa);
begin
  inherited;

end;

procedure TProdutoEmpresaBO.antesExcluir(entidade: TProdutoEmpresa);
begin
  inherited;

end;

procedure TProdutoEmpresaBO.antesIncluir(entidade: TProdutoEmpresa);
begin
  inherited;

end;

procedure TProdutoEmpresaBO.depoisAlterar(entidade: TProdutoEmpresa);
begin
  inherited;

end;

procedure TProdutoEmpresaBO.depoisExcluir(entidade: TProdutoEmpresa);
begin
  inherited;

end;

procedure TProdutoEmpresaBO.depoisIncluir(entidade: TProdutoEmpresa);
begin
  inherited;

end;

procedure TProdutoEmpresaBO.validar(entidade: TProdutoEmpresa);
begin
  inherited;

end;

end.

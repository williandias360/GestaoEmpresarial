unit ProdutoGrupoEmpresaBO;

interface

uses
  ProdutoGrupoEmpresaDAO, ProdutoGrupoEmpresa;

type
  TProdutoGrupoEmpresaBO = class(TProdutoGrupoEmpresaDAO)
  protected
    procedure validar(entidade: TProdutoGrupoEmpresa); override;
    procedure antesIncluir(entidade: TProdutoGrupoEmpresa); override;
    procedure depoisIncluir(entidade: TProdutoGrupoEmpresa); override;
    procedure antesAlterar(entidade: TProdutoGrupoEmpresa); override;
    procedure depoisAlterar(entidade: TProdutoGrupoEmpresa); override;
    procedure antesExcluir(entidade: TProdutoGrupoEmpresa); override;
    procedure depoisExcluir(entidade: TProdutoGrupoEmpresa); override;
  end;

implementation

{ TProdutoGrupoEmpresaBO }

procedure TProdutoGrupoEmpresaBO.antesAlterar(entidade: TProdutoGrupoEmpresa);
begin
  inherited;

end;

procedure TProdutoGrupoEmpresaBO.antesExcluir(entidade: TProdutoGrupoEmpresa);
begin
  inherited;

end;

procedure TProdutoGrupoEmpresaBO.antesIncluir(entidade: TProdutoGrupoEmpresa);
begin
  inherited;

end;

procedure TProdutoGrupoEmpresaBO.depoisAlterar(entidade: TProdutoGrupoEmpresa);
begin
  inherited;

end;

procedure TProdutoGrupoEmpresaBO.depoisExcluir(entidade: TProdutoGrupoEmpresa);
begin
  inherited;

end;

procedure TProdutoGrupoEmpresaBO.depoisIncluir(entidade: TProdutoGrupoEmpresa);
begin
  inherited;

end;

procedure TProdutoGrupoEmpresaBO.validar(entidade: TProdutoGrupoEmpresa);
begin
  inherited;

end;

end.

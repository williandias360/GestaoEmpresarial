unit uFormPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.ImageList, Vcl.ImgList,
  Vcl.ToolWin, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, IForm,
  JvComponentBase, JvEnterTab, System.Generics.Collections,
  FireDAC.Comp.Client;

type
  TFormPadrao = class abstract(TForm)
    ImageList1: TImageList;
    Panel1: TPanel;
    btnNovo: TBitBtn;
    btnAlterar: TBitBtn;
    btnExcluir: TBitBtn;
    btnSalvar: TBitBtn;
    btnCancelar: TBitBtn;
    btnPesquisar: TBitBtn;
    JvEnterAsTab2: TJvEnterAsTab;
    procedure FormCreate(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
  private
    FListaCamposVazios: TStringList;

  protected
    function FormatarTipoDocumento(indice: Integer): String;
    procedure LimparCampos; overload; virtual;
    procedure HabilitaControlesVisuais(Status: Boolean); overload; virtual;
    procedure HabilitaControles(novo, salvar, alterar, excluir, cancelar,
      pesquisar: Boolean);
    procedure LimparCampos(listaComponentes: TList<TControl>); overload;
    function CamposEntradaObrigatorios(listaCampos: TList < TPair < TControl,
      String >> ): Boolean;
    function ObterCamposObrigatorios: String;
    procedure ListarCidadesPorEstado(rmdCidades: TFDMemTable;
      codigoEstado: Integer);
    procedure ListarEstados(rmdEstados: TFDMemTable);
  published
    { Private declarations }
    property ListaCamposVazios: TStringList read FListaCamposVazios;
  public
    { Public declarations }
    procedure ChangeEnter(Sender: TObject);
    procedure ChangeExit(Sender: TObject);
  end;

var
  FormPadrao: TFormPadrao;

implementation

uses
  Vcl.Mask, JvDBLookup, JvBaseEdits, JvDBGrid, EstadoBO, CidadeBO, Vcl.DBGrids;

{$R *.dfm}
{ TForm1 }

procedure TFormPadrao.btnAlterarClick(Sender: TObject);
begin
  HabilitaControlesVisuais(true);
  HabilitaControles(false, true, false, false, true, false);
end;

procedure TFormPadrao.btnCancelarClick(Sender: TObject);
begin
  HabilitaControlesVisuais(false);
  HabilitaControles(true, false, false, false, false, true);
  LimparCampos;
end;

procedure TFormPadrao.btnExcluirClick(Sender: TObject);
begin
  HabilitaControlesVisuais(false);
  HabilitaControles(true, false, false, false, false, true);
  LimparCampos;
  Application.MessageBox(pWideChar('Exclu�do com sucesso'), 'Mensagem',
    MB_OK + MB_ICONASTERISK);
end;

procedure TFormPadrao.btnNovoClick(Sender: TObject);
begin
  HabilitaControlesVisuais(true);
  HabilitaControles(false, true, false, false, true, false);
end;

function TFormPadrao.CamposEntradaObrigatorios(listaCampos: TList < TPair <
  TControl, String >> ): Boolean;
var
  I: Integer;
begin
  if Not Assigned(ListaCamposVazios) then
    FListaCamposVazios := TStringList.Create;

  for I := 0 to listaCampos.Count - 1 do
  begin
    if listaCampos[I].Key is TEdit then
    begin
      if Trim(TEdit(listaCampos[I].Key).Text) = EmptyStr then
        ListaCamposVazios.Add(listaCampos[I].Value)
    end
    else if listaCampos[I].Key is TJvCalcEdit then
    begin
      if TJvCalcEdit(listaCampos[I].Key).Value <= 0 then
        ListaCamposVazios.Add(listaCampos[I].Value)
    end
    else if listaCampos[I].Key is TMaskEdit then
    begin
      if Trim(TMaskEdit(listaCampos[I].Key).Text) = EmptyStr then
        ListaCamposVazios.Add(listaCampos[I].Value)
    end
    else if listaCampos[I].Key is TJvDBLookupCombo then
    begin
      if Trim(TJvDBLookupCombo(listaCampos[I].Key).Text) = EmptyStr then
        ListaCamposVazios.Add(listaCampos[I].Value)
    end
    else if listaCampos[I].Key is TMemo then
    begin
      if Trim(TMemo(listaCampos[I].Key).Text) = EmptyStr then
        ListaCamposVazios.Add(listaCampos[I].Value)
    end
    else if listaCampos[I].Key is TComboBox then
    begin
      if (Trim(TComboBox(listaCampos[I].Key).Text) = EmptyStr) then
        ListaCamposVazios.Add(listaCampos[I].Value)
    end;
  end;

  result := ListaCamposVazios.Count = 0;
end;

procedure TFormPadrao.ChangeEnter(Sender: TObject);
var
  color: TColor;
begin
  color := clInfoBk;
  if Sender is TEdit then
    TEdit(Sender).color := color
  else if Sender is TMaskEdit then
    TMaskEdit(Sender).color := color
  else if Sender is TJvDBLookupCombo then
    TJvDBLookupCombo(Sender).color := color
  else if Sender is TJvCalcEdit then
    TJvCalcEdit(Sender).color := color
  else if Sender is TComboBox then
    TComboBox(Sender).color := color
  else if Sender is TMemo then
    TMemo(Sender).color := color;
end;

procedure TFormPadrao.ChangeExit(Sender: TObject);
var
  color: TColor;
begin
  color := clWindow;
  if Sender is TEdit then
    TEdit(Sender).color := color
  else if Sender is TMaskEdit then
    TMaskEdit(Sender).color := color
  else if Sender is TJvDBLookupCombo then
    TJvDBLookupCombo(Sender).color := color
  else if Sender is TJvCalcEdit then
    TJvCalcEdit(Sender).color := color
  else if Sender is TComboBox then
    TComboBox(Sender).color := color
  else if Sender is TMemo then
    TMemo(Sender).color := color;
end;

procedure TFormPadrao.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    if Components[I] is TEdit then
    begin
      TEdit(Components[I]).OnEnter := ChangeEnter;
      TEdit(Components[I]).OnExit := ChangeExit;
    end
    else if Components[I] is TMaskEdit then
    begin
      TMaskEdit(Components[I]).OnEnter := ChangeEnter;
      TMaskEdit(Components[I]).OnExit := ChangeExit;
    end
    else if Components[I] is TJvDBLookupCombo then
    begin
      TJvDBLookupCombo(Components[I]).OnEnter := ChangeEnter;
      TJvDBLookupCombo(Components[I]).OnExit := ChangeExit;
    end
    else if Components[I] is TMemo then
    begin
      TMemo(Components[I]).OnEnter := ChangeEnter;
      TMemo(Components[I]).OnExit := ChangeExit;
    end
    else if Components[I] is TJvCalcEdit then
    begin
//      TJvCalcEdit(Components[I]).OnEnter := ChangeEnter;
//      TJvCalcEdit(Components[I]).OnExit := ChangeExit;
    end;
  end;

  HabilitaControlesVisuais(false);
  HabilitaControles(true, false, false, false, false, true);
end;

procedure TFormPadrao.HabilitaControles(novo, salvar, alterar, excluir,
  cancelar, pesquisar: Boolean);
begin
  btnNovo.Enabled := novo;
  btnSalvar.Enabled := salvar;
  btnAlterar.Enabled := alterar;
  btnExcluir.Enabled := excluir;
  btnCancelar.Enabled := cancelar;
  btnPesquisar.Enabled := pesquisar;
end;

procedure TFormPadrao.HabilitaControlesVisuais(Status: Boolean);
var
  I: Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    if Components[I] is TEdit then
      TEdit(Components[I]).Enabled := Status
    else if Components[I] is TMaskEdit then
      TMaskEdit(Components[I]).Enabled := Status
    else if Components[I] is TJvDBLookupCombo then
      TJvDBLookupCombo(Components[I]).Enabled := Status
    else if Components[I] is TMemo then
      TMemo(Components[I]).Enabled := Status
    else if Components[I] is TComboBox then
      TComboBox(Components[I]).Enabled := Status
    else if Components[I] is TBitBtn then
      TBitBtn(Components[I]).Enabled := Status
    else if Components[I] is TButton then
      TButton(Components[I]).Enabled := Status
    else if Components[I] is TCheckBox then
      TCheckBox(Components[I]).Enabled := Status
    else if Components[I] is TJvDBGrid then
      TJvDBGrid(Components[I]).Enabled := Status
    else if Components[I] Is TDBGrid then
      TDBGrid(Components[I]).Enabled := Status;
  end;
end;

procedure TFormPadrao.LimparCampos(listaComponentes: TList<TControl>);
var
  I: Integer;
begin
  for I := 0 to listaComponentes.Count - 1 do
  begin
    if listaComponentes[I] is TEdit then
      TEdit(listaComponentes[I]).Text := ''
    else if listaComponentes[I] is TJvCalcEdit then
      TJvCalcEdit(listaComponentes[I]).Value := 0
    else if listaComponentes[I] is TMaskEdit then
      TMaskEdit(listaComponentes[I]).Text := ''
    else if listaComponentes[I] is TJvDBLookupCombo then
      TJvDBLookupCombo(listaComponentes[I]).ClearValue
    else if listaComponentes[I] is TMemo then
      TMemo(listaComponentes[I]).Clear
    else if listaComponentes[I] is TComboBox then
      TComboBox(listaComponentes[I]).ItemIndex := 0
    else if listaComponentes[I] is TCheckBox then
      TCheckBox(listaComponentes[I]).Checked := false;
  end;
end;

function TFormPadrao.ObterCamposObrigatorios: String;
var
  listaCampos: String;
begin
  listaCampos := 'Alguns campos n�o foram preenchidos. Verifique' + sLineBreak +
    FListaCamposVazios.Text.Join(sLineBreak, FListaCamposVazios.ToStringArray);
  FListaCamposVazios.Clear;
  FreeAndNil(FListaCamposVazios);

  result := listaCampos;
end;

procedure TFormPadrao.LimparCampos();
var
  I: Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    if Components[I] is TEdit then
      TEdit(Components[I]).Text := ''
    else if Components[I] is TJvCalcEdit then
      TJvCalcEdit(Components[I]).Value := 0
    else if Components[I] is TMaskEdit then
      TMaskEdit(Components[I]).Text := ''
    else if Components[I] is TJvDBLookupCombo then
      TJvDBLookupCombo(Components[I]).ClearValue
    else if Components[I] is TMemo then
      TMemo(Components[I]).Clear
    else if Components[I] is TComboBox then
      TComboBox(Components[I]).ItemIndex := 0
    else if Components[I] is TFDMemTable then
      TFDMemTable(Components[I]).Close
    else if Components[I] is TCheckBox then
      TCheckBox(Components[I]).Checked := false;
  end;

end;

function TFormPadrao.FormatarTipoDocumento(indice: Integer): String;
begin
  case indice of
    0:
      result := '!99\.999\.999\/9999\-99;0;_';
    1:
      result := '!999\.999\.999\-99;0;_';
  end;

end;

procedure TFormPadrao.ListarEstados(rmdEstados: TFDMemTable);
var
  EstadoBO: TEstadoBO;
begin
  rmdEstados.Close;
  EstadoBO := TEstadoBO.Create;
  rmdEstados.CopyDataSet(EstadoBO.ListarEstados);
  EstadoBO.Free;
end;

procedure TFormPadrao.ListarCidadesPorEstado(rmdCidades: TFDMemTable;
  codigoEstado: Integer);
var
  CidadeBO: TCidadeBO;
begin
  rmdCidades.Close;
  CidadeBO := TCidadeBO.Create;
  rmdCidades.CopyDataSet(CidadeBO.listarPorEstado(codigoEstado));
  CidadeBO.Free;
end;

end.

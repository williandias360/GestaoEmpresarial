unit Pair;

interface

type
  TPair<ICampo, TValor> = class
  private
    FKey: ICampo;
    FValue: TValor;
  published
    property Key: ICampo read FKey write FKey;
    property Value: TValor read FValue write FValue;
  public
    constructor Create(Key: ICampo; Value: TValor); overload;
  end;

implementation

{ TPair<TKey, TValue> }

constructor TPair<ICampo, TValor>.Create(Key: ICampo; Value: TValor);
begin
  FKey := Key;
  FValue := Value;
end;

end.

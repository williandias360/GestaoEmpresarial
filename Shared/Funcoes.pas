unit Funcoes;

interface

type
  TFuncoes = class
  public
    class function unMask(masked: String): String; static;
  end;

implementation

uses
  System.SysUtils;

class function TFuncoes.unMask(masked: String): String;
begin
  result := masked.Replace(',', '').Replace('.', '').Replace('-', '')
    .Replace('_', '').Replace('/', '').Replace('(', '').Replace(')', '');
end;

end.

unit Campo;

interface

uses
  ICampo, Valor;

type
  TCampo<T> = class(TInterfacedObject, TICampo)
  public
    constructor Create(NomeCampo:TValor<String>; Valor:TValor<T>);overload;
  private
  var
    FNomeCampo: TValor<string>;
    FCampo: TValor<T>;
    function GetNomeCampo: string; stdcall;
    function GetTipo: TTypeKind; stdcall;
    procedure SetNomeCampo(const Value: string); stdcall;
    function GetValor: T;
    procedure SetValor(const Value: T);
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    property Tipo: TTypeKind read GetTipo;
    property NomeCampo: String read GetNomeCampo write SetNomeCampo;
    property Valor: T read GetValor write SetValor;
  end;

implementation

{ TCampo<T> }

procedure TCampo<T>.AfterConstruction;
begin
  inherited;
  FCampo := TValor<T>.create;
  FNomeCampo := TValor<string>.create;
end;

procedure TCampo<T>.BeforeDestruction;
begin
  if (FCampo <> Nil) then
  begin
    FCampo.Destroy;
    FCampo := Nil;
  end;

  if (FNomeCampo <> Nil) then
  begin
    FNomeCampo.Destroy;
    FNomeCampo := Nil;
  end;
end;

constructor TCampo<T>.Create(NomeCampo: TValor<String>; Valor: TValor<T>);
begin
  FNomeCampo:= NomeCampo;
  FCampo:= Valor;
end;

function TCampo<T>.GetNomeCampo: string;
begin
  result := FNomeCampo.Valor
end;

function TCampo<T>.GetTipo: TTypeKind;
begin
  result := FCampo.Tipo
end;

function TCampo<T>.GetValor: T;
begin
  result := FCampo.Valor
end;

procedure TCampo<T>.SetNomeCampo(const Value: string);
begin
  FNomeCampo.Valor := Value
end;

procedure TCampo<T>.SetValor(const Value: T);
begin
  FCampo.Valor := Value
end;

end.

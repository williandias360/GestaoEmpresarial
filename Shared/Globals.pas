unit Globals;

interface

uses Usuario, Empresa;

type
  TGlobals = class abstract
  private
    class var FUsuario: TUsuario;
    class var FEmpresa: TEmpresa;
  public
    class procedure SetEmpresa(Empresa: TEmpresa); static;
    class function GetEmpresa(): TEmpresa; static;
    class procedure SetUsuario(Usuario: TUsuario); static;
    class function GetUsuario(): TUsuario; static;
  published

  end;

implementation

{ TGlobals }

class function TGlobals.GetEmpresa: TEmpresa;
begin
  result := TGlobals.FEmpresa;
end;

class function TGlobals.GetUsuario: TUsuario;
begin
  result := TGlobals.FUsuario;
end;

class procedure TGlobals.SetEmpresa(Empresa: TEmpresa);
begin
  TGlobals.FEmpresa := Empresa;
end;

class procedure TGlobals.SetUsuario(Usuario: TUsuario);
begin
  TGlobals.FUsuario := Usuario;
end;

end.

unit Tabelas;

interface

uses
  System.Classes, ICampo, System.Generics.Collections;

type
  TTabelas = class
  private
  var
    fCampos: TList<TICampo>;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure AdicionaCampo(const ACampo: TICampo);
    function RetornaCampo(const ANomeCampo: String): TICampo;
    property ListaCampos: TList<TICampo> read FCampos;
  end;

implementation

{ TTabelas }
     { TODO 1 -oWillian -cContinuar : https://www.devmedia.com.br/tipos-genericos-no-delphi/26388 }

procedure TTabelas.AdicionaCampo(const ACampo: TICampo);
begin
  fCampos.Add(ACampo)
end;

procedure TTabelas.AfterConstruction;
begin
  inherited;
  fCampos := TList<TICampo>.Create;
end;

procedure TTabelas.BeforeDestruction;
begin
  inherited;
  if (fCampos <> Nil) then
  begin
    fCampos.Clear;
    fCampos.Destroy;
    fCampos := Nil;
  end;
end;

function TTabelas.RetornaCampo(const ANomeCampo: String): TICampo;
var
  oCampo: TICampo;
begin
  for oCampo in fCampos do
    if oCampo.NomeCampo = ANomeCampo then
    begin
      result := oCampo;
      break;
    end;

end;

end.

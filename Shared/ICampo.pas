unit ICampo;

interface

type
  TICampo = interface
    ['{A86E7A32-A3BD-41CC-BC74-A56F8C00099A}']
    function GetNomeCampo: String; stdCall;
    function GetTipo: TTypeKind; stdCall;
    // procedures
    procedure SetNomeCampo(const Value: String); stdCall;
    // propriedades
    property Tipo: TTypeKind read GetTipo;
    property NomeCampo: String read GetNomeCampo write SetNomeCampo;
  end;

implementation

end.

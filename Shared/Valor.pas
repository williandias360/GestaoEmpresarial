unit Valor;

interface

type
  TValor<T> = class
  private
    FValor: T;
    FTipo: TTypeKind;
    function GetValor: T;
    procedure SetValor(const Value: T);
    function GetTipo: TTypeKind;
  public
    procedure AfterConstruction; override;
    property Valor: T read GetValor write SetValor;
    property Tipo: TTypeKind read GetTipo;
  end;

implementation

uses
  System.TypInfo;

{ TValor<T> }

procedure TValor<T>.AfterConstruction;
var
  Info: PTypeInfo;
begin

  Info := System.TypeInfo(T);
  try
    if Info <> nil then
      FTipo := Info^.Kind;
  finally
    Info := nil;
  end;
end;

function TValor<T>.GetTipo: TTypeKind;
begin
  result := FTipo;
end;

function TValor<T>.GetValor: T;
begin
   result := FValor;
end;

procedure TValor<T>.SetValor(const Value: T);
begin
   FValor := Value;
end;

end.

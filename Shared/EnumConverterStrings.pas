unit EnumConverterStrings;

interface

uses
  classes, Generics.Collections;

Type
  TGenerico = 0 .. 255;

  TConvert<T: record > = class
  private
  public
    class procedure PopulateListEnum(AList: TStrings);
    class function StrConvertEnum(const AStr: string): T;
    class function EnumConvertStr(const eEnum: T): string;
    class function EnumByPosition(index:Integer):T;
  end;

implementation

uses
  Variants, SysUtils, TypInfo;

{ TConvert }

class procedure TConvert<T>.PopulateListEnum(AList: TStrings);
var
  i: integer;
  StrTexto: String;
  Enum: integer;
begin
  i := 0;
  try
    repeat
      StrTexto := trim(GetEnumName(TypeInfo(T), i));
      Enum := GetEnumValue(TypeInfo(T), StrTexto);
      AList.Add(StrTexto);
      inc(i);
    until Enum < 0;
    AList.Delete(pred(AList.Count));
  except
    ;
    raise EConvertError.Create
      ('O Par�metro passado n�o corresponde a um Tipo ENUM');
  end;
end;

{ ******************************************************************************* }

class function TConvert<T>.StrConvertEnum(const AStr: string): T;
var
  P: ^T;
  num: integer;
begin
  try
    num := GetEnumValue(TypeInfo(T), AStr);
    if num = -1 then
      abort;
    P := @num;
    result := P^;
  except
    raise EConvertError.Create('O Par�metro "' + AStr + '" passado n�o ' +
      sLineBreak + ' corresponde a um Tipo Enumerado');
  end;
end;

{ ****************************************************************************** }

class function TConvert<T>.EnumByPosition(index: Integer): T;
begin

end;

class function TConvert<T>.EnumConvertStr(const eEnum: T): String;
var
  P: PInteger;
  num: integer;
begin
  try
    P := @eEnum;
    num := integer(TGenerico((P^)));
    result := GetEnumName(TypeInfo(T), num);
  except
    raise EConvertError.Create('O Par�metro passado n�o corresponde a ' +
      sLineBreak + 'um inteiro Ou a um Tipo Enumerado');
  end;
end;

end.

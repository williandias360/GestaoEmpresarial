unit Shared.Entities.Entity;

interface

uses
  System.SysUtils;

type
  TEntity = class abstract
  public
    constructor Create; virtual;
  private
    FDataCriacao: TDateTime;
  published
    property DataCriacao: TDateTime read FDataCriacao;
  end;

implementation

{ TEntity }

constructor TEntity.Create;
begin
  FDataCriacao := Now;
end;

end.

unit Utils;

interface

function Confirmacao(mensagem, titulo:String):Boolean;
implementation

uses
  Vcl.Forms, Winapi.Windows;

function Confirmacao(mensagem, titulo: String): Boolean;
begin
  result := Application.MessageBox(pWideChar(mensagem), pWideChar(titulo),
    MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2) = ID_YES;
end;

end.

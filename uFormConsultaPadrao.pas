unit uFormConsultaPadrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ExtCtrls;

type
  TFFormConsultaPadrao = class abstract(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    cbFiltrarPor: TComboBox;
    cbOrdenarPor: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    dbgDadosConsulta: TJvDBGrid;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    edtBusca: TEdit;
    StatusBar1: TStatusBar;
    btnConfirmar: TBitBtn;
    procedure dbgDadosConsultaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private

    { Private declarations }
  public
    { Public declarations }
    procedure CarregarFiltrosBusca(filtrosBusca: array of String);
    procedure CarregarFiltrosOrdenacao(filtrosOrdenacao: array of String);
    procedure atribuirTotalRegistrosStatusBar(quantidadeRegistros: Integer);
  end;

var
  FFormConsultaPadrao: TFFormConsultaPadrao;

implementation

{$R *.dfm}

procedure TFFormConsultaPadrao.atribuirTotalRegistrosStatusBar
  (quantidadeRegistros: Integer);
begin
  StatusBar1.Panels[1].Text := 'Registros encontrados: ' +
    IntToStr(quantidadeRegistros);
end;

procedure TFFormConsultaPadrao.dbgDadosConsultaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if (not odd(dbgDadosConsulta.DataSource.DataSet.RecNo)) then
  begin
    if not(gdSelected in State) then
    begin
      // Define uma cor de fundo
      dbgDadosConsulta.Canvas.Brush.Color := $00F1F1F1;
      dbgDadosConsulta.Canvas.FillRect(Rect); // Pinta a c�lula
      // pinta o texto padr�o
      dbgDadosConsulta.DefaultDrawDataCell(Rect, Column.Field, State);
    end;

    if (gdSelected in State) then
    begin
      // Define uma cor de fundo
      dbgDadosConsulta.Canvas.Brush.Color := RGB(217, 233, 252);
      dbgDadosConsulta.Canvas.FillRect(Rect); // Pinta a c�lula
      // pinta o texto padr�o
      dbgDadosConsulta.Canvas.Font.Color := $000000;
      dbgDadosConsulta.DefaultDrawDataCell(Rect, Column.Field, State);
    end;
  end;
end;

procedure TFFormConsultaPadrao.CarregarFiltrosBusca(filtrosBusca
  : Array Of String);
var
  i: Integer;
begin
  for i := Low(filtrosBusca) to High(filtrosBusca) do
    cbFiltrarPor.Items.Add(filtrosBusca[i]);
  cbFiltrarPor.ItemIndex := 0;
end;

procedure TFFormConsultaPadrao.CarregarFiltrosOrdenacao(filtrosOrdenacao
  : Array Of String);
var
  i: Integer;
begin
  for i := Low(filtrosOrdenacao) to High(filtrosOrdenacao) do
    cbOrdenarPor.Items.Add(filtrosOrdenacao[i]);
  cbOrdenarPor.ItemIndex := 0;
end;

end.

unit uCadastroProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormPadrao, System.ImageList,
  Vcl.ImgList, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, JvBaseEdits, JvComponentBase, JvEnterTab, JvExControls,
  JvDBLookup,
  Vcl.ComCtrls, JvExExtCtrls, JvImage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  System.Classes, Utils, Vcl.ExtDlgs;

type
  TFCadastroProduto = class(TFormPadrao)
    Label1: TLabel;
    Label2: TLabel;
    edtNome: TEdit;
    edtCodigo: TJvCalcEdit;
    edtCodigoBarras: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    dbLookupProdutoGrupo: TJvDBLookupCombo;
    Label5: TLabel;
    dbLookupUnidadeMedida: TJvDBLookupCombo;
    edtQuantidadePorCaixa: TJvCalcEdit;
    Label6: TLabel;
    edtEstoqueAtual: TJvCalcEdit;
    Label7: TLabel;
    Label8: TLabel;
    edtEstoqueMinimo: TJvCalcEdit;
    Label9: TLabel;
    btnAbrirImagem: TBitBtn;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    edtPrecoVista: TJvCalcEdit;
    Label11: TLabel;
    edtPrecoPrazo: TJvCalcEdit;
    Label12: TLabel;
    edtPrecoCusto: TJvCalcEdit;
    GroupBox2: TGroupBox;
    imgFotoProduto: TJvImage;
    edtNcm: TEdit;
    dsUnidadeMedida: TDataSource;
    rmdUnidadeMedida: TFDMemTable;
    dsProdutoGrupo: TDataSource;
    rmdProdutoGrupo: TFDMemTable;
    rmdProdutoGrupoCodigoProdutoGrupo: TIntegerField;
    rmdProdutoGrupoNome: TStringField;
    rmdUnidadeMedidaCodigoUnidadeMedida: TIntegerField;
    rmdUnidadeMedidaNome: TStringField;
    rmdUnidadeMedidaSigla: TStringField;
    OpenPictureDialog: TOpenPictureDialog;
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure LimparCampos; override;
    procedure HabilitaControlesVisuais(Status: Boolean); override;
    procedure btnPesquisarClick(Sender: TObject);
    procedure edtCodigoExit(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnAbrirImagemClick(Sender: TObject);
  private
    procedure ListarUnidadeMedida;
    procedure ListarProdutoGrupo;
    procedure Salvar;
    procedure CarregarProduto(CodigoProduto: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroProduto: TFCadastroProduto;

implementation

{$R *.dfm}

uses UnidadeMedidaBO, ProdutoGrupoBO, Produto, ProdutoBO,
  System.Generics.Collections, ProdutoPreco, ProdutoPrecoTipo, uConsultaProduto;

procedure TFCadastroProduto.btnAbrirImagemClick(Sender: TObject);
var
  memoryStream:TMemoryStream;
begin
  inherited;
  if OpenPictureDialog.Execute() then
  begin
     imgFotoProduto.Picture.LoadFromFile(OpenPictureDialog.FileName);
  end;
end;

procedure TFCadastroProduto.btnExcluirClick(Sender: TObject);
var
  ProdutoBO:TProdutoBO;
begin
  if Confirmacao('Confirma a exclus�o do produto?', 'Aten��o') then
  begin
    ProdutoBO:= TProdutoBO.Create;
    ProdutoBO.excluir(TProduto.Create(edtCodigo.AsInteger));
    ProdutoBO.Free;
    inherited;
  end;
end;

procedure TFCadastroProduto.btnNovoClick(Sender: TObject);
begin
  inherited;
  ListarUnidadeMedida;
  ListarProdutoGrupo;
  edtNome.SetFocus;
  edtCodigo.Enabled := False;
end;

procedure TFCadastroProduto.ListarUnidadeMedida;
var
  UnidadeMedidaBO: TUnidadeMedidaBO;
begin
  UnidadeMedidaBO := TUnidadeMedidaBO.Create;
  rmdUnidadeMedida.Close;
  rmdUnidadeMedida.CopyDataSet(UnidadeMedidaBO.ListarUnidadeMedidaDisponivel);
  UnidadeMedidaBO.Free;
end;

procedure TFCadastroProduto.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TFConsultaProduto, FConsultaProduto);
  FConsultaProduto.ShowModal;
  If FConsultaProduto.CodigoProduto > 0 then
  begin
    CarregarProduto(FConsultaProduto.CodigoProduto);
  end;
  FConsultaProduto.Free;
end;

procedure TFCadastroProduto.btnSalvarClick(Sender: TObject);
begin
  inherited;
  Salvar;
end;

procedure TFCadastroProduto.HabilitaControlesVisuais(Status: Boolean);
begin
  inherited;
  edtQuantidadePorCaixa.Enabled := Status;
  edtEstoqueMinimo.Enabled := Status;
  edtEstoqueAtual.Enabled := False;
  edtPrecoVista.Enabled := Status;
  edtPrecoPrazo.Enabled := Status;
  edtPrecoCusto.Enabled := Status;
end;

procedure TFCadastroProduto.LimparCampos;
begin
  inherited;
  edtQuantidadePorCaixa.Enabled := False;
  edtEstoqueMinimo.Enabled := False;
  edtCodigo.Enabled := True;
end;

procedure TFCadastroProduto.ListarProdutoGrupo;
var
  ProdutoGrupoBO: TProdutoGrupoBO;
begin
  ProdutoGrupoBO := TProdutoGrupoBO.Create;
  rmdProdutoGrupo.Close;
  rmdProdutoGrupo.CopyDataSet(ProdutoGrupoBO.ListarProdutoGrupoDisponivel);
  ProdutoGrupoBO.Free;
end;

procedure TFCadastroProduto.Salvar;
var
  Produto: TProduto;
  ProdutoBO: TProdutoBO;
  listaCampos: TList<TPair<TControl, String>>;
begin
  Try
    listaCampos := TList < TPair < TControl, String >>.Create;
    listaCampos.Add(TPair<TControl, String>.Create(edtNome, 'Nome do produto'));

    if Not CamposEntradaObrigatorios(listaCampos) then
    begin
      FreeAndNil(listaCampos);
      MessageDlg(ObterCamposObrigatorios, mtError, [mbOK], 0, mbOK);
      Exit;
    end;

    FreeAndNil(listaCampos);

    Produto := TProduto.Create(
        edtCodigo.AsInteger,
        edtNome.Text,
        edtCodigoBarras.Text,
        edtNcm.Text,
        edtQuantidadePorCaixa.Value,
        edtEstoqueMinimo.Value
      );

    if (dbLookupProdutoGrupo.KeyValue <> Null) then
      Produto.CodigoProdutoGrupo := dbLookupProdutoGrupo.KeyValue;

    if (dbLookupUnidadeMedida.KeyValue <> Null) then
      Produto.CodigoUnidadeMedida := dbLookupUnidadeMedida.KeyValue;

    if edtPrecoVista.Value > 0 then
    begin
      Produto.AdicionarPreco
        (TProdutoPreco.Create(TProdutoPrecoTipo.TIPO_PRECO_VISTA,
        edtPrecoVista.Value));
    end;

    if edtPrecoPrazo.Value > 0 then
    begin
      Produto.AdicionarPreco
        (TProdutoPreco.Create(TProdutoPrecoTipo.TIPO_PRECO_PRAZO,
        edtPrecoPrazo.Value));
    end;

    if edtPrecoCusto.Value > 0 then
    begin
      Produto.AdicionarPreco
        (TProdutoPreco.Create(TProdutoPrecoTipo.TIPO_PRECO_CUSTO,
        edtPrecoCusto.Value));
    end;

    if (Produto.ListaProdutoPreco.Count = 0) then
    begin
      Application.MessageBox
        (pWideChar('Necess�rio informar pelo menos um pre�o para o produto'),
        'Mensagem', MB_OK + MB_ICONERROR);
      Exit;
    end;

    ProdutoBO := TProdutoBO.Create;

    if Produto.CodigoProduto > 0 then
      ProdutoBO.alterar(Produto)
    else
      ProdutoBO.incluir(Produto);

    Application.MessageBox(pWideChar('Registrado com sucesso!' + sLineBreak +
      'C�digo registrado: ' + IntToStr(Produto.CodigoProduto)), 'Mensagem',
      MB_OK + MB_ICONASTERISK);
    LimparCampos;
    HabilitaControlesVisuais(False);
    HabilitaControles(True, False, False, False, False, True);

  Except
    On E: Exception do
    begin
      MessageDlg('Falha ao cadastrar produto: ' + sLineBreak + E.Message,
        mtError, [mbOK], 0, mbOK);
    end;

  End;
end;

procedure TFCadastroProduto.CarregarProduto(CodigoProduto: Integer);
var
  Produto: TProduto;
  ProdutoBO: TProdutoBO;
  ProdutoPreco: TProdutoPreco;
begin
  LimparCampos;
  ProdutoBO := TProdutoBO.Create;

  Produto := ProdutoBO.ObterPorCodigo(CodigoProduto);

  if Produto = Nil then
  begin
    MessageDlg('Produto n�o encontrado', mtInformation, [mbOK], 0, mbOK);
    Exit;
  end;

  ListarUnidadeMedida;
  ListarProdutoGrupo;

  edtCodigo.Value := Produto.CodigoProduto;
  edtCodigo.Enabled := False;
  edtNome.Text := Produto.Nome;
  edtCodigoBarras.Text := Produto.CodigoBarras;

  if Produto.CodigoProdutoGrupo > 0 then
    dbLookupProdutoGrupo.KeyValue := Produto.CodigoProdutoGrupo;

  if Produto.CodigoUnidadeMedida > 0 then
    dbLookupUnidadeMedida.KeyValue := Produto.CodigoUnidadeMedida;

  edtQuantidadePorCaixa.Value := Produto.QuantidadePorCaixa;
  edtEstoqueAtual.Value := Produto.EstoqueAtual;
  edtNcm.Text:= Produto.Ncm;
  edtEstoqueMinimo.Value:= Produto.EstoqueMinimo;

  for ProdutoPreco in Produto.ListaProdutoPreco do
  begin

    if ProdutoPreco.CodigoProdutoPrecoTipo = TProdutoPrecoTipo.TIPO_PRECO_VISTA
    then
      edtPrecoVista.Value := ProdutoPreco.Valor
    else if ProdutoPreco.CodigoProdutoPrecoTipo = TProdutoPrecoTipo.TIPO_PRECO_PRAZO
    then
      edtPrecoPrazo.Value := ProdutoPreco.Valor
    else
      edtPrecoCusto.Value := ProdutoPreco.Valor;

  end;

  HabilitaControles(False, False, True, True, True, True);
  Produto.Free;
  ProdutoBO.Free;

end;

procedure TFCadastroProduto.edtCodigoExit(Sender: TObject);
begin
  inherited;
  if edtCodigo.AsInteger > 0 then
    CarregarProduto(edtCodigo.AsInteger);
end;

end.

inherited FCadastroUsuario: TFCadastroUsuario
  BorderStyle = bsSingle
  Caption = 'Cadastro Usu'#225'rio'
  ClientHeight = 351
  ExplicitHeight = 380
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 59
    Width = 33
    Height = 13
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [1]
    Left = 143
    Top = 59
    Width = 27
    Height = 13
    Caption = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [2]
    Left = 18
    Top = 105
    Width = 25
    Height = 13
    Caption = 'Login'
  end
  object Label4: TLabel [3]
    Left = 143
    Top = 105
    Width = 30
    Height = 13
    Caption = 'Senha'
  end
  object Label11: TLabel [4]
    Left = 18
    Top = 151
    Width = 42
    Height = 13
    Caption = 'Telefone'
  end
  object Label12: TLabel [5]
    Left = 107
    Top = 151
    Width = 33
    Height = 13
    Caption = 'Celular'
  end
  object Label13: TLabel [6]
    Left = 18
    Top = 197
    Width = 63
    Height = 13
    Caption = 'Observa'#231#245'es'
  end
  object Label5: TLabel [7]
    Left = 213
    Top = 151
    Width = 24
    Height = 13
    Caption = 'Email'
  end
  inherited Panel1: TPanel
    TabOrder = 7
    inherited btnSalvar: TBitBtn
      OnClick = btnSalvarClick
    end
    inherited btnPesquisar: TBitBtn
      OnClick = btnPesquisarClick
    end
  end
  object edtNome: TEdit [9]
    Left = 143
    Top = 78
    Width = 353
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 350
    TabOrder = 0
  end
  object edtCodigo: TJvCalcEdit [10]
    Left = 16
    Top = 78
    Width = 120
    Height = 21
    Alignment = taCenter
    DecimalPlaces = 0
    DisplayFormat = '0000'
    ShowButton = False
    TabOrder = 8
    DecimalPlacesAlwaysShown = False
    OnExit = edtCodigoExit
  end
  object edtLogin: TEdit [11]
    Left = 18
    Top = 124
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 1
  end
  object edtSenha: TEdit [12]
    Left = 143
    Top = 124
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 10
    PasswordChar = '*'
    TabOrder = 2
  end
  object edtEmail: TEdit [13]
    Left = 213
    Top = 170
    Width = 369
    Height = 21
    CharCase = ecLowerCase
    TabOrder = 5
  end
  object edtTelefone: TMaskEdit [14]
    Left = 18
    Top = 170
    Width = 84
    Height = 21
    EditMask = '!\(99\)0000-0000;0;_'
    MaxLength = 13
    TabOrder = 3
    Text = ''
  end
  object edtCelular: TMaskEdit [15]
    Left = 107
    Top = 170
    Width = 96
    Height = 21
    EditMask = '!\(99\)00000-0000;0;_'
    MaxLength = 14
    TabOrder = 4
    Text = ''
  end
  object memoObservacao: TMemo [16]
    Left = 18
    Top = 216
    Width = 568
    Height = 120
    TabOrder = 6
  end
  inherited JvEnterAsTab2: TJvEnterAsTab
    Left = 424
    Top = 120
  end
end

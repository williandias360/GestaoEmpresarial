unit uCadastroProdutoGrupo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormPadrao, JvComponentBase,
  JvEnterTab, System.ImageList, Vcl.ImgList, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Mask, JvExMask, JvToolEdit, JvBaseEdits, System.Classes;

type
  TFCadastroProdutoGrupo = class(TFormPadrao)
    Label1: TLabel;
    Label2: TLabel;
    edtNome: TEdit;
    edtCodigo: TJvCalcEdit;
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure edtCodigoExit(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
  private
    { Private declarations }
    procedure LimparCampos; override;
    procedure Salvar;
    procedure CarregarProdutoGrupo(codigo: Integer);
  public
    { Public declarations }
  end;

var
  FCadastroProdutoGrupo: TFCadastroProdutoGrupo;

implementation

uses
  System.Generics.Collections, ProdutoGrupo, ProdutoGrupoBO, uConsultaGrupo;

{$R *.dfm}

procedure TFCadastroProdutoGrupo.btnExcluirClick(Sender: TObject);
var
  ProdutoGrupo: TProdutoGrupo;
  ProdutoGrupoBO: TProdutoGrupoBO;
begin
  if Application.MessageBox('Confirmar a exclus�o do grupo?',
    'Aten��o', MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2) = IDYES then
  begin
    Try
      Try
        ProdutoGrupo := TProdutoGrupo.Create(edtCodigo.AsInteger);

        ProdutoGrupoBO := TProdutoGrupoBO.Create;
        ProdutoGrupoBO.excluir(ProdutoGrupo);
        inherited;
      Except
        On E: Exception do
          Application.MessageBox(pWideChar('Falha ao exluir grupo:'
            + E.Message), 'Aten��o', MB_OK + MB_ICONERROR);
      End;
    Finally
      ProdutoGrupo.Free;
      ProdutoGrupoBO.Free;
    End;
  end;
end;

procedure TFCadastroProdutoGrupo.btnNovoClick(Sender: TObject);
begin
  inherited;
  edtCodigo.ReadOnly := True;
  edtNome.SetFocus;
end;

procedure TFCadastroProdutoGrupo.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TFConsultaGrupo, FConsultaGrupo);
  FConsultaGrupo.ShowModal;
  if FConsultaGrupo.CodigoProdutoGrupo > 0 then
  begin
    CarregarProdutoGrupo(FConsultaGrupo.CodigoProdutoGrupo);
  end;
  FreeAndNil(FConsultaGrupo);
end;

procedure TFCadastroProdutoGrupo.btnSalvarClick(Sender: TObject);
begin
  inherited;
  Salvar;
end;

procedure TFCadastroProdutoGrupo.LimparCampos;
begin
  inherited;
  edtCodigo.ReadOnly := False;
end;

procedure TFCadastroProdutoGrupo.Salvar;
var
  listaCampos: TList<TPair<TControl, String>>;
  ProdutoGrupo: TProdutoGrupo;
  ProdutoGrupoBO: TProdutoGrupoBO;
begin
  Try
    listaCampos := TList < TPair < TControl, String >>.Create;
    listaCampos.Add(TPair<TControl, String>.Create(edtNome, 'Nome do grupo'));

    if Not CamposEntradaObrigatorios(listaCampos) then
    begin
      FreeAndNil(listaCampos);
      MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
      Exit;
    end;

    FreeAndNil(listaCampos);

    ProdutoGrupo := TProdutoGrupo.Create(edtCodigo.AsInteger, edtNome.Text);

    ProdutoGrupoBO := TProdutoGrupoBO.Create;

    if ProdutoGrupo.CodigoProdutoGrupo > 0 then
      ProdutoGrupoBO.alterar(ProdutoGrupo)
    else
      ProdutoGrupoBO.incluir(ProdutoGrupo);

    Application.MessageBox(pWideChar('Registrado com sucesso!' + sLineBreak +
      'C�digo registrado: ' + IntToStr(ProdutoGrupo.CodigoProdutoGrupo)),
      'Mensagem', MB_OK + MB_ICONASTERISK);
    LimparCampos;
    HabilitaControlesVisuais(False);
    HabilitaControles(True, False, False, False, False, True);

  Except
    On E: Exception do
    begin
      MessageDlg('Falha ao cadastrar grupo: ' + sLineBreak + E.Message,
        mtError, [mbOk], 0, mbOk);
    end;

  End;
end;

procedure TFCadastroProdutoGrupo.CarregarProdutoGrupo(codigo: Integer);
var
  ProdutoGrupo: TProdutoGrupo;
  ProdutoGrupoBO: TProdutoGrupoBO;
begin
  LimparCampos;

  ProdutoGrupoBO := TProdutoGrupoBO.Create;
  ProdutoGrupo := ProdutoGrupoBO.ObterPorCodigo(codigo);

  if Not Assigned(ProdutoGrupo) then
  begin
    MessageDlg('Grupo n�o foi encontrado', mtWarning, [mbOk], 0, mbOk);
    Exit;
  end;

  edtCodigo.ReadOnly := True;
  edtCodigo.Value := ProdutoGrupo.CodigoProdutoGrupo;
  edtNome.Text := ProdutoGrupo.Nome;

  HabilitaControles(False, False, True, True, True, True);
  ProdutoGrupo.Free;
  ProdutoGrupoBO.Free;

end;

procedure TFCadastroProdutoGrupo.edtCodigoExit(Sender: TObject);
begin
  inherited;
  if edtCodigo.AsInteger > 0 then
  begin
    CarregarProdutoGrupo(edtCodigo.AsInteger);
  end;
end;

end.

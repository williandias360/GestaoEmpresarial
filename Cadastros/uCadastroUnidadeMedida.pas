unit uCadastroUnidadeMedida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormPadrao, JvComponentBase,
  JvEnterTab, System.ImageList, Vcl.ImgList, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Mask, JvExMask, JvToolEdit, JvBaseEdits;

type
  TFCadastroUnidadeMedida = class(TFormPadrao)
    Label1: TLabel;
    Label2: TLabel;
    edtNome: TEdit;
    edtCodigo: TJvCalcEdit;
    Label3: TLabel;
    edtSigla: TEdit;
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure edtCodigoExit(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
  private
    procedure Salvar;
    procedure LimparCampos; override;
    procedure CarregarUnidadeMedida(codigo: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroUnidadeMedida: TFCadastroUnidadeMedida;

implementation

{$R *.dfm}

uses UnidadeMedida, UnidadeMedidaBO, System.Generics.Collections,
  uConsultaUnidadeMedida;

procedure TFCadastroUnidadeMedida.btnAlterarClick(Sender: TObject);
begin
  inherited;
  edtNome.SetFocus;
end;

procedure TFCadastroUnidadeMedida.btnExcluirClick(Sender: TObject);
var
  UnidadeMedida:TUnidadeMedida;
  UnidadeMedidaBO:TUnidadeMedidaBO;
begin
    if Application.MessageBox('Confirmar a exclus�o da unidade de medida?', 'Aten��o',
    MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2) = IDYES then
  begin
    Try
      Try
        UnidadeMedida:= TUnidadeMedida.Create(edtCodigo.AsInteger);
        UnidadeMedidaBO:= TUnidadeMedidaBO.Create;
        UnidadeMedidaBO.excluir(UnidadeMedida);
        inherited;

      Except
        On E: Exception do
          Application.MessageBox(pWideChar('Falha ao exluir unidade de medida:' + e.Message), 'Aten��o', MB_OK+MB_ICONERROR);
      End;
    Finally
      UnidadeMedida.Free;
      UnidadeMedidaBO.Free;
    End;
  end;
end;

procedure TFCadastroUnidadeMedida.btnNovoClick(Sender: TObject);
begin
  inherited;
  edtCodigo.ReadOnly := True;
  edtNome.SetFocus;
end;

procedure TFCadastroUnidadeMedida.btnPesquisarClick(Sender: TObject);
begin
  Application.CreateForm(TFConsultaUnidadeMedida, FConsultaUnidadeMedida);
  FConsultaUnidadeMedida.ShowModal;
  if FConsultaUnidadeMedida.CodigoUnidadeMedida > 0 then
  begin
    CarregarUnidadeMedida(FConsultaUnidadeMedida.CodigoUnidadeMedida);
  end;
  FConsultaUnidadeMedida.Free;
end;

procedure TFCadastroUnidadeMedida.btnSalvarClick(Sender: TObject);
begin
  inherited;
  Salvar;
end;

procedure TFCadastroUnidadeMedida.LimparCampos;
begin
  inherited;
  edtCodigo.ReadOnly := False;
end;

procedure TFCadastroUnidadeMedida.Salvar;
var
  UnidadeMedidaBO: TUnidadeMedidaBO;
  UnidadeMedida: TUnidadeMedida;
  listaCampos: TList<TPair<TControl, String>>;
begin
  Try
    listaCampos := TList < TPair < TControl, String >>.Create;
    listaCampos.Add(TPair<TControl, String>.Create(edtNome,
      'Nome da unidade medida'));
    listaCampos.Add(TPair<TControl, String>.Create(edtSigla,
      'Sigla da unidade de medida'));

    if Not CamposEntradaObrigatorios(listaCampos) then
    begin
      FreeAndNil(listaCampos);
      MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
      Exit;
    end;

    FreeAndNil(listaCampos);

    UnidadeMedida := TUnidadeMedida.Create(edtCodigo.AsInteger, edtNome.Text,
      edtSigla.Text);

    UnidadeMedidaBO := TUnidadeMedidaBO.Create;

    if UnidadeMedida.CodigoUnidadeMedida > 0 then
      UnidadeMedidaBO.alterar(UnidadeMedida)
    else
      UnidadeMedidaBO.incluir(UnidadeMedida);

    Application.MessageBox(pWideChar('Registrado com sucesso!' + sLineBreak +
      'C�digo registrado: ' + IntToStr(UnidadeMedida.CodigoUnidadeMedida)),
      'Mensagem', MB_OK + MB_ICONASTERISK);
    LimparCampos;
    HabilitaControlesVisuais(False);
    HabilitaControles(True, False, False, False, False, True);
  Except
    On E: Exception do
    begin
      MessageDlg('Falha ao cadastrar fornecedor: ' + sLineBreak + E.Message,
        mtError, [mbOk], 0, mbOk);
    end;
  End;

end;

procedure TFCadastroUnidadeMedida.CarregarUnidadeMedida(codigo: Integer);
var
  UnidadeMedida: TUnidadeMedida;
  UnidadeMedidaBO: TUnidadeMedidaBO;
begin
  LimparCampos;

  UnidadeMedidaBO := TUnidadeMedidaBO.Create;
  UnidadeMedida := UnidadeMedidaBO.ObterPorCodigo(codigo);

  if Not Assigned(UnidadeMedida) then
  begin
    MessageDlg('Unidade de medida n�o foi encontrado', mtWarning,
      [mbOk], 0, mbOk);
    Exit;
  end;

  edtCodigo.ReadOnly := True;
  edtCodigo.Value := UnidadeMedida.CodigoUnidadeMedida;
  edtNome.Text := UnidadeMedida.Nome;
  edtSigla.Text := UnidadeMedida.Sigla;

  HabilitaControles(False, False, true, true, true, true);
  UnidadeMedida.Free;
  UnidadeMedidaBO.Free;
end;

procedure TFCadastroUnidadeMedida.edtCodigoExit(Sender: TObject);
begin
  inherited;
  if edtCodigo.AsInteger > 0 then
  begin
    CarregarUnidadeMedida(edtCodigo.AsInteger);
  end;
end;

end.

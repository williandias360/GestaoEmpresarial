inherited FCadastroFornecedor: TFCadastroFornecedor
  BorderStyle = bsSingle
  Caption = 'Cadastro de Fornecedor'
  ClientHeight = 507
  ClientWidth = 597
  ExplicitWidth = 603
  ExplicitHeight = 536
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 59
    Width = 33
    Height = 13
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [1]
    Left = 143
    Top = 59
    Width = 27
    Height = 13
    Caption = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel [2]
    Left = 16
    Top = 110
    Width = 76
    Height = 13
    Caption = 'Tipo documento'
  end
  object Label13: TLabel [3]
    Left = 143
    Top = 110
    Width = 48
    Height = 13
    Caption = 'CPF/CNPJ'
  end
  object Label14: TLabel [4]
    Left = 292
    Top = 110
    Width = 105
    Height = 13
    Caption = 'RG/Inscri'#231#227'o Estadual'
  end
  object labelRazaoSocial: TLabel [5]
    Left = 436
    Top = 110
    Width = 60
    Height = 13
    Caption = 'Raz'#227'o Social'
  end
  object Label4: TLabel [6]
    Left = 16
    Top = 160
    Width = 55
    Height = 13
    Caption = 'Logradouro'
  end
  object Label6: TLabel [7]
    Left = 436
    Top = 160
    Width = 37
    Height = 13
    Caption = 'N'#250'mero'
  end
  object Label5: TLabel [8]
    Left = 16
    Top = 209
    Width = 28
    Height = 13
    Caption = 'Bairro'
  end
  object Label7: TLabel [9]
    Left = 263
    Top = 209
    Width = 65
    Height = 13
    Caption = 'Complemento'
  end
  object Label8: TLabel [10]
    Left = 16
    Top = 252
    Width = 13
    Height = 13
    Caption = 'UF'
  end
  object Label9: TLabel [11]
    Left = 105
    Top = 252
    Width = 33
    Height = 13
    Caption = 'Cidade'
  end
  object Label18: TLabel [12]
    Left = 211
    Top = 304
    Width = 28
    Height = 13
    Caption = 'E-mail'
  end
  object Label11: TLabel [13]
    Left = 16
    Top = 304
    Width = 42
    Height = 13
    Caption = 'Telefone'
  end
  object Label3: TLabel [14]
    Left = 105
    Top = 304
    Width = 33
    Height = 13
    Caption = 'Celular'
  end
  object Label10: TLabel [15]
    Left = 16
    Top = 350
    Width = 63
    Height = 13
    Caption = 'Observa'#231#245'es'
  end
  inherited Panel1: TPanel
    Width = 597
    TabOrder = 15
    ExplicitWidth = 597
    inherited btnSalvar: TBitBtn
      OnClick = btnSalvarClick
    end
    inherited btnPesquisar: TBitBtn
      OnClick = btnPesquisarClick
    end
  end
  object edtNome: TEdit [17]
    Left = 143
    Top = 78
    Width = 353
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
  end
  object cbBloqueado: TCheckBox [18]
    Left = 511
    Top = 80
    Width = 96
    Height = 17
    Caption = 'Bloqueado'
    TabOrder = 16
  end
  object edtCodigoFornecedor: TJvCalcEdit [19]
    Left = 16
    Top = 78
    Width = 120
    Height = 21
    Alignment = taCenter
    DecimalPlaces = 0
    DisplayFormat = '0000'
    ShowButton = False
    TabOrder = 17
    DecimalPlacesAlwaysShown = False
    OnExit = edtCodigoFornecedorExit
  end
  object cbTipoDocumento: TComboBox [20]
    Left = 16
    Top = 129
    Width = 120
    Height = 22
    Style = csOwnerDrawFixed
    ItemIndex = 0
    TabOrder = 1
    Text = 'CNPJ'
    OnChange = cbTipoDocumentoChange
    Items.Strings = (
      'CNPJ'
      'CPF')
  end
  object edtCpfCnpj: TMaskEdit [21]
    Left = 142
    Top = 129
    Width = 144
    Height = 21
    TabOrder = 2
    Text = ''
  end
  object edtRgInscricaoEstadual: TEdit [22]
    Left = 292
    Top = 129
    Width = 138
    Height = 21
    TabOrder = 3
  end
  object edtRazaoSocial: TEdit [23]
    Left = 436
    Top = 129
    Width = 148
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 4
  end
  object edtLogradouro: TEdit [24]
    Left = 16
    Top = 179
    Width = 414
    Height = 21
    CharCase = ecUpperCase
    ParentShowHint = False
    ShowHint = False
    TabOrder = 5
  end
  object edtNumero: TEdit [25]
    Left = 436
    Top = 179
    Width = 148
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 6
  end
  object edtBairro: TEdit [26]
    Left = 16
    Top = 228
    Width = 241
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 7
  end
  object edtComplemento: TEdit [27]
    Left = 263
    Top = 228
    Width = 321
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 8
  end
  object dbLookupEstados: TJvDBLookupCombo [28]
    Left = 16
    Top = 271
    Width = 83
    Height = 21
    Hint = 'Estado'
    LookupField = 'CodigoEstado'
    LookupDisplay = 'Sigla'
    LookupSource = dsEstados
    TabOrder = 9
    OnChange = dbLookupEstadosChange
  end
  object dbLookupCidades: TJvDBLookupCombo [29]
    Left = 105
    Top = 271
    Width = 479
    Height = 21
    Hint = 'Cidade'
    LookupField = 'CodigoCidade'
    LookupDisplay = 'Nome'
    LookupSource = dsCidades
    TabOrder = 10
  end
  object edtEmail: TEdit [30]
    Left = 211
    Top = 323
    Width = 373
    Height = 21
    CharCase = ecLowerCase
    TabOrder = 13
  end
  object edtTelefone: TMaskEdit [31]
    Left = 16
    Top = 323
    Width = 86
    Height = 21
    EditMask = '!\(99\)0000-0000;0;_'
    MaxLength = 13
    TabOrder = 11
    Text = ''
  end
  object edtCelular: TMaskEdit [32]
    Left = 105
    Top = 323
    Width = 96
    Height = 21
    EditMask = '!\(99\)00000-0000;0;_'
    MaxLength = 14
    TabOrder = 12
    Text = ''
  end
  object memoObservacao: TMemo [33]
    Left = 16
    Top = 369
    Width = 568
    Height = 120
    TabOrder = 14
  end
  object rmdEstados: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 440
    Top = 360
    object rmdEstadosCodigoEstado: TIntegerField
      FieldName = 'CodigoEstado'
    end
    object rmdEstadosNome: TStringField
      FieldName = 'Nome'
      Size = 30
    end
    object rmdEstadosSigla: TStringField
      FieldName = 'Sigla'
      Size = 2
    end
  end
  object dsEstados: TDataSource
    DataSet = rmdEstados
    Left = 484
    Top = 360
  end
  object rmdCidades: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 256
    Top = 368
    object rmdCidadesCodigoCidade: TIntegerField
      FieldName = 'CodigoCidade'
    end
    object rmdCidadesCodigoEstado: TIntegerField
      FieldName = 'CodigoEstado'
    end
    object rmdCidadesNome: TStringField
      FieldName = 'Nome'
      Size = 200
    end
    object rmdCidadesCodigoIbge: TStringField
      FieldName = 'CodigoIbge'
      Size = 10
    end
  end
  object dsCidades: TDataSource
    DataSet = rmdCidades
    Left = 224
    Top = 368
  end
end

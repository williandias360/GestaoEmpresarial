unit uCadastroFornecedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormPadrao, JvComponentBase,
  JvEnterTab, System.ImageList, Vcl.ImgList, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, JvExControls, JvDBLookup, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Classes;

type
  TFCadastroFornecedor = class(TFormPadrao)
    Label1: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    labelRazaoSocial: TLabel;
    edtNome: TEdit;
    cbBloqueado: TCheckBox;
    edtCodigoFornecedor: TJvCalcEdit;
    cbTipoDocumento: TComboBox;
    edtCpfCnpj: TMaskEdit;
    edtRgInscricaoEstadual: TEdit;
    edtRazaoSocial: TEdit;
    Label4: TLabel;
    edtLogradouro: TEdit;
    Label6: TLabel;
    edtNumero: TEdit;
    Label5: TLabel;
    edtBairro: TEdit;
    Label7: TLabel;
    edtComplemento: TEdit;
    Label8: TLabel;
    dbLookupEstados: TJvDBLookupCombo;
    dbLookupCidades: TJvDBLookupCombo;
    Label9: TLabel;
    Label18: TLabel;
    edtEmail: TEdit;
    Label11: TLabel;
    edtTelefone: TMaskEdit;
    Label3: TLabel;
    edtCelular: TMaskEdit;
    memoObservacao: TMemo;
    Label10: TLabel;
    rmdEstados: TFDMemTable;
    rmdEstadosCodigoEstado: TIntegerField;
    rmdEstadosNome: TStringField;
    rmdEstadosSigla: TStringField;
    dsEstados: TDataSource;
    rmdCidades: TFDMemTable;
    rmdCidadesCodigoCidade: TIntegerField;
    rmdCidadesCodigoEstado: TIntegerField;
    rmdCidadesNome: TStringField;
    rmdCidadesCodigoIbge: TStringField;
    dsCidades: TDataSource;
    procedure btnNovoClick(Sender: TObject);
    procedure dbLookupEstadosChange(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure cbTipoDocumentoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure CarregarFornecedor(CodigoFornecedor: Integer);
    procedure edtCodigoFornecedorExit(Sender: TObject);
  private
    procedure Salvar;
    procedure LimparCampos; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroFornecedor: TFCadastroFornecedor;

implementation

{$R *.dfm}

uses EstadoBO, CidadeBO, Fornecedor, Pessoa, Endereco, Telefone, Email, Bairro,
  Cidade, System.Generics.Collections, Funcoes, PessoaFisica,
  PessoaJuridica, Entities.Enums.ETipoEndereco, Estado,
  Entities.Enums.ETipoTelefone, FornecedorBO, uConsultaFornecedor, EnderecoBO;

procedure TFCadastroFornecedor.btnNovoClick(Sender: TObject);
begin
  inherited;
  ListarEstados(rmdEstados);
  edtCodigoFornecedor.ReadOnly := True;
  edtNome.SetFocus;
end;

procedure TFCadastroFornecedor.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TFConsultaFornecedor, FConsultaFornecedor);
  FConsultaFornecedor.ShowModal;
  if FConsultaFornecedor.CodigoFornecedor > 0 then
  begin
    CarregarFornecedor(FConsultaFornecedor.CodigoFornecedor);
  end;
  FConsultaFornecedor.Free;
end;

procedure TFCadastroFornecedor.btnSalvarClick(Sender: TObject);
begin
  inherited;
  Salvar;
end;

procedure TFCadastroFornecedor.CarregarFornecedor(CodigoFornecedor: Integer);
var
  FornecedorBO: TFornecedorBO;
  Fornecedor: TFornecedor;
  cpfCnpj, rgIe: String;
  Endereco: TEndereco;
  Telefone: TTelefone;
begin
  LimparCampos;
  FornecedorBO := TFornecedorBO.Create;
  Fornecedor := FornecedorBO.obterPorCodigo(CodigoFornecedor);

  if Fornecedor = Nil then
  begin
    MessageDlg('Fornecedor n�o encontrado', mtInformation, [mbOk], 0, mbOk);
    Exit;
  end;

  ListarEstados(rmdEstados);
  edtCodigoFornecedor.Value := Fornecedor.Pessoa.CodigoPessoa;
  edtCodigoFornecedor.ReadOnly := True;
  edtNome.Text := Fornecedor.Pessoa.Nome;
  memoObservacao.Lines.Add(Fornecedor.Pessoa.Observacao);

  if Assigned(Fornecedor.Pessoa.PessoaFisica) then
  begin
    cbTipoDocumento.ItemIndex := 1;
    cpfCnpj := Fornecedor.Pessoa.PessoaFisica.Cpf;
    rgIe := Fornecedor.Pessoa.PessoaFisica.Rg;
  end;

  if Assigned(Fornecedor.Pessoa.PessoaJuridica) then
  begin
    cbTipoDocumento.ItemIndex := 0;
    cpfCnpj := Fornecedor.Pessoa.PessoaJuridica.Cnpj;
    rgIe := Fornecedor.Pessoa.PessoaJuridica.InscricaoEstadual;
    edtRazaoSocial.Text := Fornecedor.Pessoa.PessoaJuridica.RazaoSocial;
  end;

  cbTipoDocumentoChange(nil);

  edtCpfCnpj.Text := cpfCnpj;
  edtRgInscricaoEstadual.Text := rgIe;

  if Assigned(Fornecedor.Pessoa.ListaEndereco) and
    (Fornecedor.Pessoa.ListaEndereco.Count > 0) then
  begin

    Endereco := Fornecedor.Pessoa.ListaEndereco[0];
    edtLogradouro.Text := Endereco.Logradouro;
    edtBairro.Text := Endereco.Bairro.Nome;
    edtNumero.Text := Endereco.Numero;
    edtComplemento.Text := Endereco.Complemento;
    dbLookupEstados.KeyValue := Endereco.Cidade.Estado.CodigoEstado;
    dbLookupCidades.KeyValue := Endereco.Cidade.CodigoCidade;
  end;

  if Assigned(Fornecedor.Pessoa.ListaTelefone) and
    (Fornecedor.Pessoa.ListaTelefone.Count > 0) then
  begin
    for Telefone in Fornecedor.Pessoa.ListaTelefone do
    begin
      if Telefone.Tipo = TETipoTelefone.FIXO then
        edtTelefone.Text := Telefone.Numero
      else
        edtCelular.Text := Telefone.Numero;
    end;
  end;

  if Assigned(Fornecedor.Pessoa.ListaEmail) and
    (Fornecedor.Pessoa.ListaEmail.Count > 0) then
  begin
    edtEmail.Text := Fornecedor.Pessoa.ListaEmail[0].Endereco;
  end;

  HabilitaControles(False, False, True, True, True, True);
  FornecedorBO.Free;
  Fornecedor.Free;
  Endereco.Free;

end;

procedure TFCadastroFornecedor.cbTipoDocumentoChange(Sender: TObject);
begin
  inherited;
  edtCpfCnpj.EditMask := FormatarTipoDocumento(cbTipoDocumento.ItemIndex);
  edtRazaoSocial.Visible := cbTipoDocumento.ItemIndex = 0;
  labelRazaoSocial.Visible := edtRazaoSocial.Visible;
  edtCpfCnpj.Text := '';
end;

procedure TFCadastroFornecedor.dbLookupEstadosChange(Sender: TObject);
var
  CodigoEstado: Integer;
begin
  inherited;
  CodigoEstado := rmdEstados.FieldByName('CodigoEstado').AsInteger;
  ListarCidadesPorEstado(rmdCidades, CodigoEstado);
end;

procedure TFCadastroFornecedor.edtCodigoFornecedorExit(Sender: TObject);
begin
  inherited;
  if edtCodigoFornecedor.AsInteger > 0 then
  begin
    CarregarFornecedor(edtCodigoFornecedor.AsInteger);
  end;
end;

procedure TFCadastroFornecedor.FormCreate(Sender: TObject);
begin
  inherited;
  cbTipoDocumentoChange(nil);
end;

procedure TFCadastroFornecedor.Salvar;
var
  FornecedorBO: TFornecedorBO;
  Fornecedor: TFornecedor;
  Pessoa: TPessoa;
  Endereco: TEndereco;
  Telefone: TTelefone;
  Email: TEmail;
  Bairro: TBairro;
  Cidade: TCidade;
  cpfCnpj: String;
  rgInscricao: String;
  listaCampos: TList<TPair<TControl, String>>;
  telefonePrincipal: Boolean;
begin
  Try
    listaCampos := TList < TPair < TControl, String >>.Create;
    listaCampos.Add(TPair<TControl, String>.Create(edtNome,
      'Nome do fornecedor'));
    if cbTipoDocumento.ItemIndex = 0 then
      listaCampos.Add(TPair<TControl, String>.Create(edtRazaoSocial,
        'Raz�o Social'));

    if Not CamposEntradaObrigatorios(listaCampos) then
    begin
      FreeAndNil(listaCampos);
      MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
      Exit;
    end;

    FreeAndNil(listaCampos);

    cpfCnpj := TFuncoes.unMask(edtCpfCnpj.Text);
    rgInscricao := edtRgInscricaoEstadual.Text;
    Pessoa := TPessoa.Create(edtCodigoFornecedor.AsInteger, edtNome.Text,
      memoObservacao.Lines.Text);

    if Trim(cpfCnpj) <> EmptyStr then
    begin
      if cbTipoDocumento.ItemIndex = 0 then
        Pessoa.PessoaJuridica := TPessoaJuridica.Create(cpfCnpj, rgInscricao,
          edtRazaoSocial.Text)
      else
        Pessoa.PessoaFisica := TPessoaFisica.Create(cpfCnpj, rgInscricao);
    end;

    Endereco := TEndereco.Create(0, edtLogradouro.Text, edtNumero.Text,
      edtComplemento.Text, True, TETipoEndereco.COMERCIAL,
      TBairro.Create(edtBairro.Text),
      TCidade.Create(rmdCidades.FieldByName('CodigoCidade').AsInteger,
      TEstado.Create(rmdEstados.FieldByName('CodigoEstado').AsInteger)));

    Pessoa.adicionarEndereco(Endereco);
    telefonePrincipal := False;
    if Trim(edtTelefone.Text) <> EmptyStr then
    begin
      Pessoa.adicionarTelefone(TTelefone.Create(0, edtTelefone.Text,
        TETipoTelefone.FIXO, True));
      telefonePrincipal := True;
    end;

    if Trim(edtCelular.Text) <> EmptyStr then
    begin
      Pessoa.adicionarTelefone(TTelefone.Create(0, edtCelular.Text,
        TETipoTelefone.CELULAR, Not telefonePrincipal));
    end;

    if Trim(edtEmail.Text) <> EmptyStr then
    begin
      Pessoa.adicionarEmail(TEmail.Create(0, Trim(edtEmail.Text), True));
    end;

    Fornecedor := TFornecedor.Create(0, Pessoa);
    FornecedorBO := TFornecedorBO.Create;

    if (Fornecedor.Pessoa.CodigoPessoa > 0) then
      FornecedorBO.alterar(Fornecedor)
    else
      FornecedorBO.incluir(Fornecedor);

    Application.MessageBox(pWideChar('Registrado com sucesso!' + sLineBreak +
      'C�digo registrado: ' + IntToStr(Pessoa.CodigoPessoa)), 'Mensagem',
      MB_OK + MB_ICONASTERISK);
    LimparCampos;
    HabilitaControlesVisuais(False);
    HabilitaControles(True, False, False, False, False, True);
  Except
    On E: Exception do
    begin
      MessageDlg('Falha ao cadastrar fornecedor: ' + sLineBreak + E.Message,
        mtError, [mbOk], 0, mbOk);
    end;
  End;
end;

procedure TFCadastroFornecedor.LimparCampos;
begin
  inherited;
  edtCodigoFornecedor.ReadOnly := False;
end;

end.

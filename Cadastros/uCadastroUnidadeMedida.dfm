inherited FCadastroUnidadeMedida: TFCadastroUnidadeMedida
  Caption = 'Cadastro de Unidade de Medida'
  ClientHeight = 129
  ExplicitHeight = 158
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 59
    Width = 33
    Height = 13
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [1]
    Left = 143
    Top = 59
    Width = 27
    Height = 13
    Caption = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [2]
    Left = 444
    Top = 59
    Width = 22
    Height = 13
    Caption = 'Sigla'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  inherited Panel1: TPanel
    TabOrder = 3
    inherited btnSalvar: TBitBtn
      OnClick = btnSalvarClick
    end
    inherited btnPesquisar: TBitBtn
      OnClick = btnPesquisarClick
    end
  end
  object edtNome: TEdit [4]
    Left = 143
    Top = 78
    Width = 295
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
  end
  object edtCodigo: TJvCalcEdit [5]
    Left = 16
    Top = 78
    Width = 120
    Height = 21
    Alignment = taCenter
    DecimalPlaces = 0
    DisplayFormat = '0000'
    ShowButton = False
    TabOrder = 0
    DecimalPlacesAlwaysShown = False
    OnExit = edtCodigoExit
  end
  object edtSigla: TEdit [6]
    Left = 444
    Top = 78
    Width = 162
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 3
    TabOrder = 2
  end
end

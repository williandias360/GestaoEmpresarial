inherited FCadastroEmpresa: TFCadastroEmpresa
  Caption = 'Cadastro da Empresa'
  ClientHeight = 463
  ClientWidth = 603
  ExplicitWidth = 609
  ExplicitHeight = 492
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 59
    Width = 33
    Height = 13
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [1]
    Left = 143
    Top = 59
    Width = 27
    Height = 13
    Caption = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel [2]
    Left = 16
    Top = 110
    Width = 76
    Height = 13
    Caption = 'Tipo documento'
  end
  object Label13: TLabel [3]
    Left = 143
    Top = 110
    Width = 48
    Height = 13
    Caption = 'CPF/CNPJ'
  end
  object Label14: TLabel [4]
    Left = 292
    Top = 110
    Width = 105
    Height = 13
    Caption = 'RG/Inscri'#231#227'o Estadual'
  end
  object labelRazaoSocial: TLabel [5]
    Left = 436
    Top = 110
    Width = 60
    Height = 13
    Caption = 'Raz'#227'o Social'
  end
  object Label4: TLabel [6]
    Left = 16
    Top = 160
    Width = 55
    Height = 13
    Caption = 'Logradouro'
  end
  object Label6: TLabel [7]
    Left = 436
    Top = 160
    Width = 37
    Height = 13
    Caption = 'N'#250'mero'
  end
  object Label5: TLabel [8]
    Left = 16
    Top = 209
    Width = 28
    Height = 13
    Caption = 'Bairro'
  end
  object Label7: TLabel [9]
    Left = 263
    Top = 209
    Width = 65
    Height = 13
    Caption = 'Complemento'
  end
  object Label8: TLabel [10]
    Left = 16
    Top = 252
    Width = 13
    Height = 13
    Caption = 'UF'
  end
  object Label9: TLabel [11]
    Left = 105
    Top = 252
    Width = 33
    Height = 13
    Caption = 'Cidade'
  end
  object Label18: TLabel [12]
    Left = 275
    Top = 296
    Width = 28
    Height = 13
    Caption = 'E-mail'
  end
  object Label10: TLabel [13]
    Left = 436
    Top = 252
    Width = 107
    Height = 13
    Caption = 'Tipo Regime Tribut'#225'rio'
  end
  object Label15: TLabel [14]
    Left = 16
    Top = 296
    Width = 20
    Height = 13
    Caption = 'Tipo'
  end
  object Label16: TLabel [15]
    Left = 133
    Top = 296
    Width = 37
    Height = 13
    Caption = 'N'#250'mero'
  end
  object Label17: TLabel [16]
    Left = 16
    Top = 441
    Width = 204
    Height = 13
    Caption = 'Duplo click para excluir um  telefone'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited Panel1: TPanel
    Width = 603
    ExplicitWidth = 603
    inherited btnSalvar: TBitBtn
      OnClick = btnSalvarClick
    end
    inherited btnPesquisar: TBitBtn
      OnClick = btnPesquisarClick
    end
  end
  object edtNome: TEdit [18]
    Left = 143
    Top = 78
    Width = 437
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
  end
  object edtCodigo: TJvCalcEdit [19]
    Left = 16
    Top = 78
    Width = 116
    Height = 21
    Alignment = taCenter
    DecimalPlaces = 0
    DisplayFormat = '0000'
    ShowButton = False
    TabOrder = 14
    DecimalPlacesAlwaysShown = False
    OnExit = edtCodigoExit
  end
  object cbTipoDocumento: TComboBox [20]
    Left = 16
    Top = 129
    Width = 116
    Height = 22
    Style = csOwnerDrawFixed
    ItemIndex = 0
    TabOrder = 2
    Text = 'CNPJ'
    OnChange = cbTipoDocumentoChange
    Items.Strings = (
      'CNPJ'
      'CPF')
  end
  object edtCpfCnpj: TMaskEdit [21]
    Left = 142
    Top = 129
    Width = 140
    Height = 21
    TabOrder = 3
    Text = ''
  end
  object edtRgInscricaoEstadual: TEdit [22]
    Left = 292
    Top = 129
    Width = 134
    Height = 21
    TabOrder = 4
  end
  object edtRazaoSocial: TEdit [23]
    Left = 436
    Top = 129
    Width = 144
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 5
  end
  object edtLogradouro: TEdit [24]
    Left = 16
    Top = 179
    Width = 410
    Height = 21
    CharCase = ecUpperCase
    ParentShowHint = False
    ShowHint = False
    TabOrder = 6
  end
  object edtNumero: TEdit [25]
    Left = 436
    Top = 179
    Width = 144
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 7
  end
  object edtBairro: TEdit [26]
    Left = 16
    Top = 228
    Width = 237
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 8
  end
  object edtComplemento: TEdit [27]
    Left = 263
    Top = 228
    Width = 317
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 9
  end
  object dbLookupEstados: TJvDBLookupCombo [28]
    Left = 16
    Top = 271
    Width = 79
    Height = 21
    Hint = 'Estado'
    LookupField = 'CodigoEstado'
    LookupDisplay = 'Sigla'
    LookupSource = dsEstados
    TabOrder = 10
    OnChange = dbLookupEstadosChange
  end
  object dbLookupCidades: TJvDBLookupCombo [29]
    Left = 105
    Top = 271
    Width = 321
    Height = 21
    Hint = 'Cidade'
    LookupField = 'CodigoCidade'
    LookupDisplay = 'Nome'
    LookupSource = dsCidades
    TabOrder = 11
  end
  object edtEmail: TEdit [30]
    Left = 275
    Top = 315
    Width = 305
    Height = 21
    CharCase = ecLowerCase
    TabOrder = 13
  end
  object cbTipoRegimeTributario: TComboBox [31]
    Left = 436
    Top = 271
    Width = 144
    Height = 21
    Enabled = False
    ItemIndex = 0
    TabOrder = 12
    Text = 'Simples Nacional'
    Items.Strings = (
      'Simples Nacional'
      'Lucro Real'
      'Lucro Presumido')
  end
  object dbgTelefones: TDBGrid [32]
    Left = 16
    Top = 344
    Width = 253
    Height = 95
    DataSource = dsTelefones
    ReadOnly = True
    TabOrder = 15
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = dbgTelefonesDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoTelefone'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'Tipo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Numero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Principal'
        Visible = False
      end>
  end
  object cbTipoTelefone: TComboBox [33]
    Left = 16
    Top = 316
    Width = 109
    Height = 22
    Style = csOwnerDrawFixed
    DragCursor = crArrow
    TabOrder = 16
    OnChange = cbTipoTelefoneChange
  end
  object edtNumeroTelefone: TMaskEdit [34]
    Left = 133
    Top = 317
    Width = 100
    Height = 21
    EditMask = '!\(99\)0000-0000;0;_'
    MaxLength = 13
    TabOrder = 17
    Text = ''
  end
  object btnAdicionarTelefone: TBitBtn [35]
    Left = 239
    Top = 313
    Width = 30
    Height = 25
    Glyph.Data = {
      F6060000424DF606000000000000360000002800000018000000180000000100
      180000000000C0060000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7E0E0E0D4D4D4CDCDCDCCCCCCCC
      CCCCCCCCCCB7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7CCCCCCCCCCCCCCCCCC
      CDCDCDD3D3D3DCDCDCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF008553008553008553008553008553008553FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00875429CE8E2FCE9129CA
      8C29CA8C008754FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008855
      2DCF9104C47A05C77D32CF94008855FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF008A572ACF9104C67D04C57C2ACF91008A57FFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF008C5929CA8F05C9800ACA8329CE91008C59FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5008E5A2AD29504C78006CB
      832AD295008E5AE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF00915C00915C00915C00915C00915C00915C11AC73
      2AD29505C98305C9832AD19511AB7300915C00915C00915C00915C00915C0091
      5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00935E2ED59B2AD4982BD4992B
      D4992ACB922BD49916D08F06CD8711CE8C12C9882ED59B34D59D2AD2972BD499
      2AD2972ED59B00935EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00955F2AD4
      9A06CF8A05CE8905CE8906CF8A05CC8806CF8A05C88505CC8805C88505C68412
      D08F05CC8805CE8905C7852AD59B00955FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF0097612BD49B11D19007D08C06CD8A06CC8914D19111D19006CB8906CA
      8806CC8908D18D07D08C07D08C0BD18E06CB892BD69C009761FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF00996341EBCF42ECCF51ECD142ECCF41EACE2FD9A1
      14D39406D08D06CF8D13D0922ED9A041EACD50ECD141E9CD41EACD41EBCE0099
      63FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF009C65009C65009C65009C6500
      9C65009C6514B67E2CD69F09D39107CE8D2CD39C18B67F009C65009C65009C65
      009C65009C65009C65FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF009E663ADAA607D09007D08F2CD7A1009E66FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A06837DAA608D39309D4
      9437DAA600A068FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A26A
      2FDAA508D3940BD5962DD7A100A26AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF00A36B2ED8A40AD5970CD6982ED9A500A36BFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00A56C47EDD345ECD245ECD245EDD300A56CFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A66D00A66D00A66D00A6
      6D00A66D00A66DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    TabOrder = 18
    OnClick = btnAdicionarTelefoneClick
  end
  object dsCidades: TDataSource
    DataSet = rmdCidades
    Left = 408
    Top = 343
  end
  object rmdCidades: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 440
    Top = 343
    object rmdCidadesCodigoCidade: TIntegerField
      FieldName = 'CodigoCidade'
    end
    object rmdCidadesCodigoEstado: TIntegerField
      FieldName = 'CodigoEstado'
    end
    object rmdCidadesNome: TStringField
      FieldName = 'Nome'
      Size = 200
    end
    object rmdCidadesCodigoIbge: TStringField
      FieldName = 'CodigoIbge'
      Size = 10
    end
  end
  object rmdEstados: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 440
    Top = 335
    object rmdEstadosCodigoEstado: TIntegerField
      FieldName = 'CodigoEstado'
    end
    object rmdEstadosNome: TStringField
      FieldName = 'Nome'
      Size = 30
    end
    object rmdEstadosSigla: TStringField
      FieldName = 'Sigla'
      Size = 2
    end
  end
  object dsEstados: TDataSource
    DataSet = rmdEstados
    Left = 484
    Top = 335
  end
  object rmdTelefones: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 320
    Top = 416
    object rmdTelefonesCodigoTelefone: TIntegerField
      FieldName = 'CodigoTelefone'
    end
    object rmdTelefonesTipo: TStringField
      FieldName = 'Tipo'
      Size = 10
    end
    object rmdTelefonesNumero: TStringField
      FieldName = 'Numero'
      Size = 14
    end
    object rmdTelefonesPrincipal: TBooleanField
      FieldName = 'Principal'
    end
  end
  object dsTelefones: TDataSource
    DataSet = rmdTelefones
    Left = 335
    Top = 414
  end
end

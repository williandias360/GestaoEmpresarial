inherited FCadastroCliente: TFCadastroCliente
  BorderStyle = bsSingle
  Caption = 'Cadastro de clientes'
  ClientHeight = 525
  ClientWidth = 720
  ExplicitWidth = 726
  ExplicitHeight = 554
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 59
    Width = 33
    Height = 13
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [1]
    Left = 143
    Top = 59
    Width = 27
    Height = 13
    Caption = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel [2]
    Left = 16
    Top = 110
    Width = 76
    Height = 13
    Caption = 'Tipo documento'
  end
  object Label13: TLabel [3]
    Left = 143
    Top = 110
    Width = 48
    Height = 13
    Caption = 'CPF/CNPJ'
  end
  object Label14: TLabel [4]
    Left = 292
    Top = 110
    Width = 105
    Height = 13
    Caption = 'RG/Inscri'#231#227'o Estadual'
  end
  object labelRazaoSocial: TLabel [5]
    Left = 436
    Top = 110
    Width = 60
    Height = 13
    Caption = 'Raz'#227'o Social'
  end
  inherited Panel1: TPanel
    Width = 720
    TabOrder = 6
    ExplicitWidth = 720
    inherited btnSalvar: TBitBtn
      OnClick = btnSalvarClick
    end
    inherited btnPesquisar: TBitBtn
      OnClick = btnPesquisarClick
    end
  end
  object edtNome: TEdit [7]
    Left = 143
    Top = 78
    Width = 404
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
  end
  object PageControl1: TPageControl [8]
    Left = 16
    Top = 163
    Width = 695
    Height = 331
    ActivePage = TabEnderecos
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 5
    object TabEnderecos: TTabSheet
      Caption = 'Endere'#231'os'
      object Label3: TLabel
        Left = 6
        Top = 16
        Width = 20
        Height = 13
        Caption = 'Tipo'
      end
      object Label4: TLabel
        Left = 123
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Logradouro'
      end
      object Label5: TLabel
        Left = 6
        Top = 63
        Width = 28
        Height = 13
        Caption = 'Bairro'
      end
      object Label6: TLabel
        Left = 439
        Top = 16
        Width = 37
        Height = 13
        Caption = 'N'#250'mero'
      end
      object Label7: TLabel
        Left = 243
        Top = 62
        Width = 65
        Height = 13
        Caption = 'Complemento'
      end
      object Label9: TLabel
        Left = 123
        Top = 105
        Width = 33
        Height = 13
        Caption = 'Cidade'
      end
      object Label8: TLabel
        Left = 6
        Top = 105
        Width = 13
        Height = 13
        Caption = 'UF'
      end
      object Label16: TLabel
        Left = 3
        Top = 286
        Width = 207
        Height = 13
        Caption = 'Duplo click para excluir um endere'#231'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cbTipoEndereco: TComboBox
        Left = 6
        Top = 35
        Width = 111
        Height = 22
        Style = csOwnerDrawFixed
        DragCursor = crArrow
        TabOrder = 0
      end
      object dbgEndrecos: TJvDBGrid
        Left = 3
        Top = 155
        Width = 672
        Height = 126
        DataSource = dsEnderecos
        ReadOnly = True
        TabOrder = 8
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = dbgEndrecosDblClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'Principal'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Logradouro'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Numero'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Bairro'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Complemento'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cidade'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Sigla'
            Title.Caption = 'Estado'
            Visible = True
          end>
      end
      object edtLogradouro: TEdit
        Left = 123
        Top = 35
        Width = 310
        Height = 21
        CharCase = ecUpperCase
        ParentShowHint = False
        ShowHint = False
        TabOrder = 1
      end
      object edtNumero: TEdit
        Left = 439
        Top = 35
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 2
      end
      object edtComplemento: TEdit
        Left = 243
        Top = 81
        Width = 317
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 4
      end
      object dbLookupCidades: TJvDBLookupCombo
        Left = 123
        Top = 124
        Width = 346
        Height = 21
        Hint = 'Cidade'
        LookupField = 'Nome'
        LookupSource = dsCidades
        TabOrder = 6
      end
      object dbLookupEstados: TJvDBLookupCombo
        Left = 6
        Top = 124
        Width = 111
        Height = 21
        Hint = 'Estado'
        LookupField = 'Sigla'
        LookupSource = dsEstados
        TabOrder = 5
        OnChange = dbLookupEstadosChange
      end
      object edtBairro: TEdit
        Left = 6
        Top = 81
        Width = 231
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 3
      end
      object cbPrincipal: TCheckBox
        Left = 475
        Top = 128
        Width = 97
        Height = 17
        Caption = 'Principal'
        TabOrder = 9
      end
      object btnAdicionarEndereco: TBitBtn
        Left = 542
        Top = 124
        Width = 30
        Height = 25
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7E0E0E0D4D4D4CDCDCDCCCCCCCC
          CCCCCCCCCCB7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7CCCCCCCCCCCCCCCCCC
          CDCDCDD3D3D3DCDCDCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF008553008553008553008553008553008553FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00875429CE8E2FCE9129CA
          8C29CA8C008754FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008855
          2DCF9104C47A05C77D32CF94008855FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF008A572ACF9104C67D04C57C2ACF91008A57FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF008C5929CA8F05C9800ACA8329CE91008C59FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5008E5A2AD29504C78006CB
          832AD295008E5AE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF00915C00915C00915C00915C00915C00915C11AC73
          2AD29505C98305C9832AD19511AB7300915C00915C00915C00915C00915C0091
          5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00935E2ED59B2AD4982BD4992B
          D4992ACB922BD49916D08F06CD8711CE8C12C9882ED59B34D59D2AD2972BD499
          2AD2972ED59B00935EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00955F2AD4
          9A06CF8A05CE8905CE8906CF8A05CC8806CF8A05C88505CC8805C88505C68412
          D08F05CC8805CE8905C7852AD59B00955FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF0097612BD49B11D19007D08C06CD8A06CC8914D19111D19006CB8906CA
          8806CC8908D18D07D08C07D08C0BD18E06CB892BD69C009761FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF00996341EBCF42ECCF51ECD142ECCF41EACE2FD9A1
          14D39406D08D06CF8D13D0922ED9A041EACD50ECD141E9CD41EACD41EBCE0099
          63FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF009C65009C65009C65009C6500
          9C65009C6514B67E2CD69F09D39107CE8D2CD39C18B67F009C65009C65009C65
          009C65009C65009C65FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF009E663ADAA607D09007D08F2CD7A1009E66FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A06837DAA608D39309D4
          9437DAA600A068FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A26A
          2FDAA508D3940BD5962DD7A100A26AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF00A36B2ED8A40AD5970CD6982ED9A500A36BFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF00A56C47EDD345ECD245ECD245EDD300A56CFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A66D00A66D00A66D00A6
          6D00A66D00A66DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        TabOrder = 7
        OnClick = btnAdicionarEnderecoClick
      end
    end
    object TabContatos: TTabSheet
      Caption = 'Contatos'
      ImageIndex = 1
      object Label10: TLabel
        Left = 16
        Top = 16
        Width = 20
        Height = 13
        Caption = 'Tipo'
      end
      object Label11: TLabel
        Left = 133
        Top = 16
        Width = 37
        Height = 13
        Caption = 'N'#250'mero'
      end
      object Label17: TLabel
        Left = 16
        Top = 284
        Width = 204
        Height = 13
        Caption = 'Duplo click para excluir um  telefone'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 416
        Top = 16
        Width = 28
        Height = 13
        Caption = 'E-mail'
      end
      object Label19: TLabel
        Left = 416
        Top = 284
        Width = 193
        Height = 13
        Caption = 'Duplo click para excluir um  e-mail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbgTelefones: TJvDBGrid
        Left = 16
        Top = 87
        Width = 252
        Height = 191
        DataSource = dsTelefones
        ReadOnly = True
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = dbgTelefonesDblClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'Principal'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Tipo'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Numero'
            Width = 105
            Visible = True
          end>
      end
      object cbTipoTelefone: TComboBox
        Left = 16
        Top = 36
        Width = 111
        Height = 22
        Style = csOwnerDrawFixed
        DragCursor = crArrow
        TabOrder = 0
        OnChange = cbTipoTelefoneChange
      end
      object edtNumeroTelefone: TMaskEdit
        Left = 133
        Top = 37
        Width = 100
        Height = 21
        EditMask = '!\(99\)0000-0000;0;_'
        MaxLength = 13
        TabOrder = 1
        Text = ''
      end
      object cbTelefonePrincipal: TCheckBox
        Left = 19
        Top = 64
        Width = 97
        Height = 17
        Caption = 'Principal'
        TabOrder = 4
      end
      object btnAdicionarTelefone: TBitBtn
        Left = 239
        Top = 33
        Width = 30
        Height = 25
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7E0E0E0D4D4D4CDCDCDCCCCCCCC
          CCCCCCCCCCB7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7CCCCCCCCCCCCCCCCCC
          CDCDCDD3D3D3DCDCDCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF008553008553008553008553008553008553FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00875429CE8E2FCE9129CA
          8C29CA8C008754FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008855
          2DCF9104C47A05C77D32CF94008855FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF008A572ACF9104C67D04C57C2ACF91008A57FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF008C5929CA8F05C9800ACA8329CE91008C59FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5008E5A2AD29504C78006CB
          832AD295008E5AE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF00915C00915C00915C00915C00915C00915C11AC73
          2AD29505C98305C9832AD19511AB7300915C00915C00915C00915C00915C0091
          5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00935E2ED59B2AD4982BD4992B
          D4992ACB922BD49916D08F06CD8711CE8C12C9882ED59B34D59D2AD2972BD499
          2AD2972ED59B00935EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00955F2AD4
          9A06CF8A05CE8905CE8906CF8A05CC8806CF8A05C88505CC8805C88505C68412
          D08F05CC8805CE8905C7852AD59B00955FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF0097612BD49B11D19007D08C06CD8A06CC8914D19111D19006CB8906CA
          8806CC8908D18D07D08C07D08C0BD18E06CB892BD69C009761FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF00996341EBCF42ECCF51ECD142ECCF41EACE2FD9A1
          14D39406D08D06CF8D13D0922ED9A041EACD50ECD141E9CD41EACD41EBCE0099
          63FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF009C65009C65009C65009C6500
          9C65009C6514B67E2CD69F09D39107CE8D2CD39C18B67F009C65009C65009C65
          009C65009C65009C65FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF009E663ADAA607D09007D08F2CD7A1009E66FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A06837DAA608D39309D4
          9437DAA600A068FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A26A
          2FDAA508D3940BD5962DD7A100A26AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF00A36B2ED8A40AD5970CD6982ED9A500A36BFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF00A56C47EDD345ECD245ECD245EDD300A56CFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A66D00A66D00A66D00A6
          6D00A66D00A66DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        TabOrder = 2
        OnClick = btnAdicionarTelefoneClick
      end
      object edtEmail: TEdit
        Left = 416
        Top = 37
        Width = 214
        Height = 21
        CharCase = ecLowerCase
        TabOrder = 5
      end
      object btnAdicionarEmail: TBitBtn
        Left = 634
        Top = 33
        Width = 30
        Height = 25
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7E0E0E0D4D4D4CDCDCDCCCCCCCC
          CCCCCCCCCCB7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7CCCCCCCCCCCCCCCCCC
          CDCDCDD3D3D3DCDCDCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF008553008553008553008553008553008553FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00875429CE8E2FCE9129CA
          8C29CA8C008754FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008855
          2DCF9104C47A05C77D32CF94008855FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF008A572ACF9104C67D04C57C2ACF91008A57FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF008C5929CA8F05C9800ACA8329CE91008C59FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5008E5A2AD29504C78006CB
          832AD295008E5AE5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF00915C00915C00915C00915C00915C00915C11AC73
          2AD29505C98305C9832AD19511AB7300915C00915C00915C00915C00915C0091
          5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00935E2ED59B2AD4982BD4992B
          D4992ACB922BD49916D08F06CD8711CE8C12C9882ED59B34D59D2AD2972BD499
          2AD2972ED59B00935EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00955F2AD4
          9A06CF8A05CE8905CE8906CF8A05CC8806CF8A05C88505CC8805C88505C68412
          D08F05CC8805CE8905C7852AD59B00955FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF0097612BD49B11D19007D08C06CD8A06CC8914D19111D19006CB8906CA
          8806CC8908D18D07D08C07D08C0BD18E06CB892BD69C009761FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF00996341EBCF42ECCF51ECD142ECCF41EACE2FD9A1
          14D39406D08D06CF8D13D0922ED9A041EACD50ECD141E9CD41EACD41EBCE0099
          63FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF009C65009C65009C65009C6500
          9C65009C6514B67E2CD69F09D39107CE8D2CD39C18B67F009C65009C65009C65
          009C65009C65009C65FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF009E663ADAA607D09007D08F2CD7A1009E66FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A06837DAA608D39309D4
          9437DAA600A068FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A26A
          2FDAA508D3940BD5962DD7A100A26AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF00A36B2ED8A40AD5970CD6982ED9A500A36BFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF00A56C47EDD345ECD245ECD245EDD300A56CFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00A66D00A66D00A66D00A6
          6D00A66D00A66DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        TabOrder = 6
        OnClick = btnAdicionarEmailClick
      end
      object dbgEmails: TJvDBGrid
        Left = 416
        Top = 87
        Width = 252
        Height = 191
        DataSource = dsEmails
        ReadOnly = True
        TabOrder = 7
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnDblClick = dbgEmailsDblClick
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 17
        TitleRowHeight = 17
        Columns = <
          item
            Expanded = False
            FieldName = 'Principal'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Endereco'
            Width = 190
            Visible = True
          end>
      end
      object cbEmailPrincipal: TCheckBox
        Left = 416
        Top = 64
        Width = 97
        Height = 17
        Caption = 'Principal'
        TabOrder = 8
      end
    end
    object TabObservacao: TTabSheet
      Caption = 'Observa'#231#245'es'
      ImageIndex = 2
      object memoObservacao: TMemo
        Left = 0
        Top = 0
        Width = 687
        Height = 303
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object cbBloqueado: TCheckBox [9]
    Left = 556
    Top = 82
    Width = 96
    Height = 17
    Caption = 'Bloqueado'
    TabOrder = 7
  end
  object edtCodigoCliente: TJvCalcEdit [10]
    Left = 16
    Top = 78
    Width = 120
    Height = 21
    Alignment = taCenter
    DecimalPlaces = 0
    DisplayFormat = '0000'
    ShowButton = False
    TabOrder = 8
    DecimalPlacesAlwaysShown = False
    OnExit = edtCodigoClienteExit
  end
  object cbTipoDocumento: TComboBox [11]
    Left = 16
    Top = 129
    Width = 120
    Height = 22
    Style = csOwnerDrawFixed
    ItemIndex = 0
    TabOrder = 1
    Text = 'CNPJ'
    OnChange = cbTipoDocumentoChange
    Items.Strings = (
      'CNPJ'
      'CPF')
  end
  object edtCpfCnpj: TMaskEdit [12]
    Left = 142
    Top = 129
    Width = 144
    Height = 21
    TabOrder = 2
    Text = ''
  end
  object edtRgInscricaoEstadual: TEdit [13]
    Left = 292
    Top = 129
    Width = 138
    Height = 21
    TabOrder = 3
  end
  object edtRazaoSocial: TEdit [14]
    Left = 436
    Top = 129
    Width = 276
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 4
  end
  object StatusBar1: TStatusBar [15]
    Left = 0
    Top = 506
    Width = 720
    Height = 19
    Panels = <>
  end
  object rmdEnderecos: TFDMemTable [17]
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 612
    Top = 310
    object rmdEnderecosCodigoEndereco: TIntegerField
      FieldName = 'CodigoEndereco'
    end
    object rmdEnderecosTipo: TStringField
      FieldName = 'Tipo'
      Size = 10
    end
    object rmdEnderecosLogradouro: TStringField
      FieldName = 'Logradouro'
      Size = 250
    end
    object rmdEnderecosNumero: TStringField
      FieldName = 'Numero'
      Size = 10
    end
    object rmdEnderecosCodigoBairro: TIntegerField
      FieldName = 'CodigoBairro'
    end
    object rmdEnderecosBairro: TStringField
      FieldName = 'Bairro'
      Size = 90
    end
    object rmdEnderecosComplemento: TStringField
      FieldName = 'Complemento'
      Size = 160
    end
    object rmdEnderecosCodigoCidade: TIntegerField
      FieldName = 'CodigoCidade'
    end
    object rmdEnderecosCidade: TStringField
      FieldName = 'Cidade'
      Size = 200
    end
    object rmdEnderecosCodigoEstado: TIntegerField
      FieldName = 'CodigoEstado'
    end
    object rmdEnderecosEstado: TStringField
      FieldName = 'Sigla'
      Size = 2
    end
    object rmdEnderecosPrincipal: TBooleanField
      FieldName = 'Principal'
    end
  end
  object dsEnderecos: TDataSource [18]
    DataSet = rmdEnderecos
    Left = 644
    Top = 310
  end
  object rmdTelefones: TFDMemTable [19]
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 560
    Top = 376
    object rmdTelefonesCodigoTelefone: TIntegerField
      FieldName = 'CodigoTelefone'
    end
    object rmdTelefonesTipo: TStringField
      FieldName = 'Tipo'
      Size = 10
    end
    object rmdTelefonesNumero: TStringField
      FieldName = 'Numero'
      Size = 14
    end
    object rmdTelefonesPrincipal: TBooleanField
      FieldName = 'Principal'
    end
  end
  object dsTelefones: TDataSource [20]
    DataSet = rmdTelefones
    Left = 588
    Top = 374
  end
  object rmdEstados: TFDMemTable [21]
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 440
    Top = 360
    object rmdEstadosCodigoEstado: TIntegerField
      FieldName = 'CodigoEstado'
    end
    object rmdEstadosNome: TStringField
      FieldName = 'Nome'
      Size = 30
    end
    object rmdEstadosSigla: TStringField
      FieldName = 'Sigla'
      Size = 2
    end
  end
  object dsEstados: TDataSource [22]
    DataSet = rmdEstados
    Left = 484
    Top = 360
  end
  object rmdCidades: TFDMemTable [23]
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 256
    Top = 368
    object rmdCidadesCodigoCidade: TIntegerField
      FieldName = 'CodigoCidade'
    end
    object rmdCidadesCodigoEstado: TIntegerField
      FieldName = 'CodigoEstado'
    end
    object rmdCidadesNome: TStringField
      FieldName = 'Nome'
      Size = 200
    end
    object rmdCidadesCodigoIbge: TStringField
      FieldName = 'CodigoIbge'
      Size = 10
    end
  end
  object dsCidades: TDataSource [24]
    DataSet = rmdCidades
    Left = 224
    Top = 368
  end
  object rmdEmails: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 352
    Top = 264
    object rmdEmailsCodigoEmail: TIntegerField
      FieldName = 'CodigoEmail'
    end
    object rmdEmailsPrincipal: TBooleanField
      FieldName = 'Principal'
    end
    object rmdEmailsEndereco: TStringField
      FieldName = 'Endereco'
      Size = 90
    end
  end
  object dsEmails: TDataSource
    DataSet = rmdEmails
    Left = 380
    Top = 267
  end
end

unit uCadastroUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormPadrao, JvComponentBase,
  JvEnterTab, System.ImageList, Vcl.ImgList, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Mask, JvExControls, JvDBLookup, JvExMask, JvToolEdit,
  JvBaseEdits;

type
  TFCadastroUsuario = class(TFormPadrao)
    Label1: TLabel;
    Label2: TLabel;
    edtNome: TEdit;
    edtCodigo: TJvCalcEdit;
    Label3: TLabel;
    edtLogin: TEdit;
    Label4: TLabel;
    edtSenha: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    edtEmail: TEdit;
    edtTelefone: TMaskEdit;
    edtCelular: TMaskEdit;
    memoObservacao: TMemo;
    Label5: TLabel;
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure edtCodigoExit(Sender: TObject);
  private
    procedure Salvar;
    procedure CarregarUsuario(CodigoUsuario: Integer);
    procedure LimparCampos; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroUsuario: TFCadastroUsuario;

implementation

uses
  Pair, Pessoa, Telefone,
  Entities.Enums.ETipoTelefone, Email, Usuario, UsuarioBO,
  System.Generics.Collections, uConsultaUsuario;

{$R *.dfm}

procedure TFCadastroUsuario.btnNovoClick(Sender: TObject);
begin
  inherited;
  edtCodigo.ReadOnly := True;
  edtNome.SetFocus;
end;

procedure TFCadastroUsuario.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TFConsultaUsuario, FConsultaUsuario);
  FConsultaUsuario.ShowModal;
  if FConsultaUsuario.CodigoUsuario > 0 then
  begin
    CarregarUsuario(FConsultaUsuario.CodigoUsuario);
  end;
  FConsultaUsuario.Free;
end;

procedure TFCadastroUsuario.btnSalvarClick(Sender: TObject);
begin
  inherited;
  Salvar;
end;

procedure TFCadastroUsuario.Salvar;
var
  listaCampos: TList<TPair<TControl, String>>;
  telefonePrincipal: Boolean;
  Pessoa: TPessoa;
  Usuario: TUsuario;
  UsuarioBO: TUsuarioBO;
begin
  Try
    Try
      listaCampos := TList < TPair < TControl, String >>.Create;
      listaCampos.Add(TPair<TControl, String>.Create(edtNome,
        'Nome do usu�rio'));

      listaCampos.Add(TPair<TControl, String>.Create(edtLogin,
        'Login n�o foi informado'));

      listaCampos.Add(TPair<TControl, String>.Create(edtSenha,
        'Senha do usu�rio n�o foi informado'));

      if Length(edtSenha.Text) < 3 then
        listaCampos.Add(TPair<TControl, String>.Create(edtSenha,
          'Senha deve ter no m�nimo 3 caracteres'));

      if Not CamposEntradaObrigatorios(listaCampos) then
      begin
        FreeAndNil(listaCampos);
        MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
        Exit;
      end;

      FreeAndNil(listaCampos);

      Pessoa := TPessoa.Create(edtCodigo.AsInteger, edtNome.Text,
        memoObservacao.Lines.Text);

      telefonePrincipal := False;
      if Trim(edtTelefone.Text) <> EmptyStr then
      begin
        Pessoa.adicionarTelefone(TTelefone.Create(0, edtTelefone.Text,
          TETipoTelefone.FIXO, True));
        telefonePrincipal := True;
      end;

      if Trim(edtCelular.Text) <> EmptyStr then
      begin
        Pessoa.adicionarTelefone(TTelefone.Create(0, edtCelular.Text,
          TETipoTelefone.CELULAR, Not telefonePrincipal));
      end;

      if Trim(edtEmail.Text) <> EmptyStr then
      begin
        Pessoa.adicionarEmail(TEmail.Create(0, edtEmail.Text, True));
      end;

      Usuario := TUsuario.Create(edtLogin.Text, edtSenha.Text, Pessoa);
      UsuarioBO := TUsuarioBO.Create;

      if Usuario.Pessoa.CodigoPessoa > 0 then
        UsuarioBO.alterar(Usuario)
      else
        UsuarioBO.incluir(Usuario);

      Application.MessageBox(pWideChar('Registrado com sucesso!' + sLineBreak +
        'C�digo registrado: ' + IntToStr(Usuario.Pessoa.CodigoPessoa)),
        'Mensagem', MB_OK + MB_ICONASTERISK);
      LimparCampos;
      HabilitaControlesVisuais(False);
      HabilitaControles(True, False, False, False, False, True);
    Except
      On E: Exception do
      begin
        MessageDlg('Falha ao cadastrar cliente: ' + sLineBreak + E.Message,
          mtError, [mbOk], 0, mbOk);
      end;
    End;
  Finally
    Usuario.Free;
    UsuarioBO.Free;
    Pessoa.Free;
  End;

end;

procedure TFCadastroUsuario.CarregarUsuario(CodigoUsuario: Integer);
var
  UsuarioBO: TUsuarioBO;
  Usuario: TUsuario;
  Telefone: TTelefone;
begin
  LimparCampos;

  UsuarioBO := TUsuarioBO.Create;
  Usuario := UsuarioBO.ObterPorCodigo(CodigoUsuario);

  if Not Assigned(Usuario) then
  begin
    MessageDlg('Usu�rio n�o foi encontrado', mtWarning, [mbOk], 0, mbOk);
    Exit;
  end;

  edtCodigo.Value := Usuario.Pessoa.CodigoPessoa;
  edtCodigo.ReadOnly:= True;
  edtNome.Text := Usuario.Pessoa.Nome;
  edtLogin.Text := Usuario.Login;
  edtSenha.Text := Usuario.Senha;
  memoObservacao.Lines.Add(Usuario.Pessoa.Observacao);

  if Assigned(Usuario.Pessoa.ListaTelefone) And
    (Usuario.Pessoa.ListaTelefone.Count > 0) then
  begin
    for Telefone in Usuario.Pessoa.ListaTelefone do
    begin
      if Telefone.Tipo = TETipoTelefone.FIXO then
        edtTelefone.Text := Telefone.Numero
      else
        edtCelular.Text := Telefone.Numero;
    end;
  end;

  if Assigned(USuario.Pessoa.ListaEmail) AND (Usuario.Pessoa.ListaEmail.Count > 0) then
  begin
    edtEmail.Text:= Usuario.Pessoa.ListaEmail[0].Endereco;
  end;

  HabilitaControles(False, False, true, true, true, true);
  UsuarioBO.Free;
  Usuario.Free;
end;

procedure TFCadastroUsuario.edtCodigoExit(Sender: TObject);
begin
  inherited;
  if edtCodigo.AsInteger > 0 then
  begin
    CarregarUsuario(edtCodigo.AsInteger);
  end;
end;

procedure TFCadastroUsuario.LimparCampos;
begin
  inherited;
  edtCodigo.ReadOnly:= False;
end;

end.

unit uCadastroEmpresa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormPadrao, JvComponentBase,
  JvEnterTab, System.ImageList, Vcl.ImgList, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, JvExControls, JvDBLookup, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids;

type
  TFCadastroEmpresa = class(TFormPadrao)
    Label1: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    labelRazaoSocial: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label18: TLabel;
    edtNome: TEdit;
    edtCodigo: TJvCalcEdit;
    cbTipoDocumento: TComboBox;
    edtCpfCnpj: TMaskEdit;
    edtRgInscricaoEstadual: TEdit;
    edtRazaoSocial: TEdit;
    edtLogradouro: TEdit;
    edtNumero: TEdit;
    edtBairro: TEdit;
    edtComplemento: TEdit;
    dbLookupEstados: TJvDBLookupCombo;
    dbLookupCidades: TJvDBLookupCombo;
    edtEmail: TEdit;
    dsCidades: TDataSource;
    rmdCidades: TFDMemTable;
    rmdCidadesCodigoCidade: TIntegerField;
    rmdCidadesCodigoEstado: TIntegerField;
    rmdCidadesNome: TStringField;
    rmdCidadesCodigoIbge: TStringField;
    rmdEstados: TFDMemTable;
    rmdEstadosCodigoEstado: TIntegerField;
    rmdEstadosNome: TStringField;
    rmdEstadosSigla: TStringField;
    dsEstados: TDataSource;
    Label10: TLabel;
    cbTipoRegimeTributario: TComboBox;
    dbgTelefones: TDBGrid;
    Label15: TLabel;
    cbTipoTelefone: TComboBox;
    edtNumeroTelefone: TMaskEdit;
    Label16: TLabel;
    btnAdicionarTelefone: TBitBtn;
    rmdTelefones: TFDMemTable;
    rmdTelefonesCodigoTelefone: TIntegerField;
    rmdTelefonesTipo: TStringField;
    rmdTelefonesNumero: TStringField;
    rmdTelefonesPrincipal: TBooleanField;
    dsTelefones: TDataSource;
    Label17: TLabel;
    procedure btnNovoClick(Sender: TObject);
    procedure dbLookupEstadosChange(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure cbTipoDocumentoChange(Sender: TObject);
    procedure edtCodigoExit(Sender: TObject);
    procedure cbTipoTelefoneChange(Sender: TObject);
    procedure btnAdicionarTelefoneClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dbgTelefonesDblClick(Sender: TObject);
  private
    procedure Salvar;
    procedure CarregarEmpresa(CodigoEmpresa: Integer);
    procedure LimparCamposTelefone;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastroEmpresa: TFCadastroEmpresa;

implementation

uses System.Generics.Collections, Funcoes, Pessoa, EmpresaBO, PessoaFisica,
  PessoaJuridica, Endereco, Bairro, Cidade, Entities.Enums.ETipoEndereco,
  Estado, Telefone, Email, Empresa, System.SysUtils,
  Entities.Enums.ETipoRegimeTributario, uConsultaEmpresa, EnumConverterStrings,
  Entities.Enums.ETipoTelefone;
{$R *.dfm}

procedure TFCadastroEmpresa.btnAdicionarTelefoneClick(Sender: TObject);
var
  listaCampos: TList<TPair<TControl, String>>;
begin
  inherited;
  listaCampos := TList < TPair < TControl, String >>.Create;
  listaCampos.Add(TPair<TControl, String>.Create(edtNumeroTelefone,
    'N�mero do telefone'));

  if Not CamposEntradaObrigatorios(listaCampos) then
  begin
    FreeAndNil(listaCampos);
    MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
    Exit;
  end;

  FreeAndNil(listaCampos);

  if Not rmdTelefones.Active then
  begin
    rmdTelefones.Open;
  end;

  with rmdTelefones do
  begin
    Append;
    FieldByName('CodigoTelefone').AsInteger := 0;
    FieldByName('Tipo').AsString := cbTipoTelefone.Text;
    FieldByName('Numero').AsString := edtNumeroTelefone.Text;
    FieldByName('Principal').AsBoolean := False;
    Post;
  end;

  LimparCamposTelefone;
  edtNumeroTelefone.SetFocus;
end;

procedure TFCadastroEmpresa.btnAlterarClick(Sender: TObject);
begin
  inherited;
  cbTipoRegimeTributario.Enabled := False;
end;

procedure TFCadastroEmpresa.btnNovoClick(Sender: TObject);
begin
  inherited;
  ListarEstados(rmdEstados);
  edtCodigo.ReadOnly := True;
  edtNome.SetFocus;
  cbTipoRegimeTributario.Enabled := False;
end;

procedure TFCadastroEmpresa.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TFConsultaEmpresa, FConsultaEmpresa);
  FConsultaEmpresa.ShowModal;
  if FConsultaEmpresa.CodigoEmpresa > 0 then
  begin
    CarregarEmpresa(FConsultaEmpresa.CodigoEmpresa);
  end;
  FConsultaEmpresa.Free;
end;

procedure TFCadastroEmpresa.btnSalvarClick(Sender: TObject);
begin
  inherited;
  Salvar;
end;

procedure TFCadastroEmpresa.dbgTelefonesDblClick(Sender: TObject);
begin
  inherited;
  if rmdTelefones.Active and Not rmdTelefones.IsEmpty and btnSalvar.Enabled then
  begin
    if Application.MessageBox('Confirmar a exclus�o do telefone?', 'Aten��o',
      MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON1) = ID_YES then
    begin
      rmdTelefones.Delete;
    end;
  end;
end;

procedure TFCadastroEmpresa.dbLookupEstadosChange(Sender: TObject);
begin
  inherited;
  ListarCidadesPorEstado(rmdCidades, rmdEstados.FieldByName('CodigoEstado')
    .AsInteger);
end;

procedure TFCadastroEmpresa.edtCodigoExit(Sender: TObject);
begin
  inherited;
  if edtCodigo.AsInteger > 0 then
  begin
    CarregarEmpresa(edtCodigo.AsInteger);
  end;
end;

procedure TFCadastroEmpresa.FormCreate(Sender: TObject);
begin
  inherited;
  TConvert<TETipoTelefone>.PopulateListEnum(cbTipoTelefone.Items);
  cbTipoTelefone.ItemIndex := 0;
end;

procedure TFCadastroEmpresa.Salvar;
var
  Pessoa: TPessoa;
  EmpresaBO: TEmpresaBO;
  Endereco: TEndereco;
  Empresa: TEmpresa;
  cpfCnpj: String;
  rgInscricao: String;
  listaCampos: TList<TPair<TControl, String>>;
begin
  Try
    listaCampos := TList < TPair < TControl, String >>.Create;
    listaCampos.Add(TPair<TControl, String>.Create(edtNome,
      'Nome do fornecedor'));
    if cbTipoDocumento.ItemIndex = 0 then
      listaCampos.Add(TPair<TControl, String>.Create(edtRazaoSocial,
        'Raz�o Social'));

    if Not CamposEntradaObrigatorios(listaCampos) then
    begin
      FreeAndNil(listaCampos);
      MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
      Exit;
    end;

    FreeAndNil(listaCampos);

    cpfCnpj := TFuncoes.unMask(edtCpfCnpj.Text);
    rgInscricao := edtRgInscricaoEstadual.Text;

    Pessoa := TPessoa.Create(edtCodigo.AsInteger, edtNome.Text, '');

    if Trim(cpfCnpj) <> EmptyStr then
    begin
      if cbTipoDocumento.ItemIndex = 0 then
        Pessoa.PessoaJuridica := TPessoaJuridica.Create(cpfCnpj, rgInscricao,
          edtRazaoSocial.Text)
      else
        Pessoa.PessoaFisica := TPessoaFisica.Create(cpfCnpj, rgInscricao);
    end;

    Endereco := TEndereco.Create(0, edtLogradouro.Text, edtNumero.Text,
      edtComplemento.Text, True, TETipoEndereco.COMERCIAL,
      TBairro.Create(edtBairro.Text),
      TCidade.Create(rmdCidades.FieldByName('CodigoCidade').AsInteger,
      TEstado.Create(rmdEstados.FieldByName('CodigoEstado').AsInteger)));

    Pessoa.adicionarEndereco(Endereco);
    if rmdTelefones.Active then
    begin
      rmdTelefones.First;
      while Not rmdTelefones.Eof do
      begin
        Pessoa.adicionarTelefone
          (TTelefone.Create(rmdTelefones.FieldByName('CodigoTelefone')
          .AsInteger, rmdTelefones.FieldByName('Numero').AsString,
          TConvert<TETipoTelefone>.StrConvertEnum(rmdTelefones.FieldByName
          ('Tipo').AsString), rmdTelefones.FieldByName('Principal').AsBoolean));
        rmdTelefones.Next;
      end;
    end;

    if Trim(edtEmail.Text) <> EmptyStr then
    begin
      Pessoa.adicionarEmail(TEmail.Create(0, Trim(edtEmail.Text), True));
    end;

    Empresa := TEmpresa.Create(TETipoRegimeTributario.SIMPLESNACIONAL, Pessoa);
    EmpresaBO := TEmpresaBO.Create;

    if Empresa.Pessoa.CodigoPessoa > 0 then
      EmpresaBO.alterar(Empresa)
    else
      EmpresaBO.incluir(Empresa);

    Application.MessageBox(pWideChar('Registrado com sucesso!' + sLineBreak +
      'C�digo registrado: ' + IntToStr(Pessoa.CodigoPessoa)), 'Mensagem',
      MB_OK + MB_ICONASTERISK);
    LimparCampos;
    HabilitaControlesVisuais(False);
    HabilitaControles(True, False, False, False, False, True);
  Except
    On E: Exception do
    begin
      MessageDlg('Falha ao cadastrar empresa: ' + sLineBreak + E.Message,
        mtError, [mbOk], 0, mbOk);
    end;

  End;

end;

procedure TFCadastroEmpresa.CarregarEmpresa(CodigoEmpresa: Integer);
var
  EmpresaBO: TEmpresaBO;
  Empresa: TEmpresa;
  cpfCnpj, rgIe: String;
  Endereco: TEndereco;
  Telefone:TTelefone;
begin
  LimparCampos;
  EmpresaBO := TEmpresaBO.Create;

  Empresa := EmpresaBO.ObterPorCodigo(CodigoEmpresa);
  if Empresa = Nil then
  begin
    MessageDlg('Empresa n�o encontrada', mtInformation, [mbOk], 0, mbOk);
    Exit;
  end;

  ListarEstados(rmdEstados);
  edtCodigo.Value := Empresa.Pessoa.CodigoPessoa;
  edtCodigo.ReadOnly := True;
  edtNome.Text := Empresa.Pessoa.Nome;

  if Assigned(Empresa.Pessoa.PessoaFisica) then
  begin
    cbTipoDocumento.ItemIndex := 1;
    cpfCnpj := Empresa.Pessoa.PessoaFisica.Cpf;
    rgIe := Empresa.Pessoa.PessoaFisica.Rg;
  end;

  if Assigned(Empresa.Pessoa.PessoaJuridica) then
  begin
    cbTipoDocumento.ItemIndex := 0;
    cpfCnpj := Empresa.Pessoa.PessoaJuridica.Cnpj;
    rgIe := Empresa.Pessoa.PessoaJuridica.InscricaoEstadual;
    edtRazaoSocial.Text := Empresa.Pessoa.PessoaJuridica.RazaoSocial;
  end;

  cbTipoDocumentoChange(nil);

  edtCpfCnpj.Text := cpfCnpj;
  edtRgInscricaoEstadual.Text := rgIe;

  if Assigned(Empresa.Pessoa.ListaEndereco) and
    (Empresa.Pessoa.ListaEndereco.Count > 0) then
  begin

    Endereco := Empresa.Pessoa.ListaEndereco[0];
    edtLogradouro.Text := Endereco.Logradouro;
    edtBairro.Text := Endereco.Bairro.Nome;
    edtNumero.Text := Endereco.Numero;
    edtComplemento.Text := Endereco.Complemento;
    dbLookupEstados.KeyValue := Endereco.Cidade.Estado.CodigoEstado;
    dbLookupCidades.KeyValue := Endereco.Cidade.CodigoCidade;
  end;

  if Assigned(Empresa.Pessoa.ListaEmail) and
    (Empresa.Pessoa.ListaEmail.Count > 0) then
  begin
    edtEmail.Text := Empresa.Pessoa.ListaEmail[0].Endereco;
  end;

  if Assigned(Empresa.Pessoa.ListaTelefone) then
  begin
    rmdTelefones.Close;
    rmdTelefones.Open;
    for Telefone in Empresa.Pessoa.ListaTelefone do
    begin
       with rmdTelefones do
       begin
          Append;
          FieldByName('CodigoTelefone').AsInteger:= Telefone.CodigoTelefone;
          FieldByName('Numero').AsString:= Telefone.Numero;
          FieldByName('Tipo').AsString:= TConvert<TETipoTelefone>.EnumConvertStr(Telefone.Tipo);
          FieldByName('Principal').AsBoolean:= Telefone.Principal;
          Post;
       end;
    end;
  end;

  HabilitaControles(False, False, True, True, True, True);
  EmpresaBO.Free;
  Empresa.Free;
  Endereco.Free;

end;

procedure TFCadastroEmpresa.cbTipoDocumentoChange(Sender: TObject);
begin
  inherited;
  edtCpfCnpj.EditMask := FormatarTipoDocumento(cbTipoDocumento.ItemIndex);
  edtRazaoSocial.Visible := cbTipoDocumento.ItemIndex = 0;
  labelRazaoSocial.Visible := edtRazaoSocial.Visible;
  edtCpfCnpj.Text := '';
end;

procedure TFCadastroEmpresa.cbTipoTelefoneChange(Sender: TObject);
begin
  inherited;
  case cbTipoTelefone.ItemIndex of
    1:
      edtNumeroTelefone.EditMask := '!\(99\)99999-9999;0;_';
  else
    edtNumeroTelefone.EditMask := '!\(99\)9999-9999;0;_';
  end;
end;

procedure TFCadastroEmpresa.LimparCamposTelefone;
var
  listaComponentes: TList<TControl>;
begin
  listaComponentes := TList<TControl>.Create;
  listaComponentes.Add(cbTipoTelefone);
  listaComponentes.Add(edtNumeroTelefone);

  LimparCampos(listaComponentes);
  listaComponentes.Free;
end;

end.

unit uCadastroCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormPadrao, System.ImageList,
  Vcl.ImgList, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, JvComponentBase, JvEnterTab,
  Vcl.ComCtrls, JvExMask, JvToolEdit, JvBaseEdits, Vcl.Mask, JvExControls,
  JvDBLookup, Vcl.Grids, Vcl.DBGrids, IForm, System.Classes, JvExDBGrids,
  JvDBGrid, JvMaskEdit;

type
  TFCadastroCliente = class(TFormPadrao, TIForm)
    Label1: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    labelRazaoSocial: TLabel;
    edtNome: TEdit;
    PageControl1: TPageControl;
    TabEnderecos: TTabSheet;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label16: TLabel;
    cbTipoEndereco: TComboBox;
    dbgEndrecos: TJvDBGrid;
    edtLogradouro: TEdit;
    edtNumero: TEdit;
    edtComplemento: TEdit;
    dbLookupCidades: TJvDBLookupCombo;
    dbLookupEstados: TJvDBLookupCombo;
    edtBairro: TEdit;
    cbPrincipal: TCheckBox;
    btnAdicionarEndereco: TBitBtn;
    TabContatos: TTabSheet;
    Label10: TLabel;
    Label11: TLabel;
    Label17: TLabel;
    dbgTelefones: TJvDBGrid;
    cbTipoTelefone: TComboBox;
    edtNumeroTelefone: TMaskEdit;
    cbTelefonePrincipal: TCheckBox;
    btnAdicionarTelefone: TBitBtn;
    TabObservacao: TTabSheet;
    memoObservacao: TMemo;
    cbBloqueado: TCheckBox;
    edtCodigoCliente: TJvCalcEdit;
    cbTipoDocumento: TComboBox;
    edtCpfCnpj: TMaskEdit;
    edtRgInscricaoEstadual: TEdit;
    edtRazaoSocial: TEdit;
    StatusBar1: TStatusBar;
    rmdEnderecos: TFDMemTable;
    rmdEnderecosCodigoEndereco: TIntegerField;
    rmdEnderecosTipo: TStringField;
    rmdEnderecosLogradouro: TStringField;
    rmdEnderecosNumero: TStringField;
    rmdEnderecosCodigoBairro: TIntegerField;
    rmdEnderecosBairro: TStringField;
    rmdEnderecosComplemento: TStringField;
    rmdEnderecosCodigoCidade: TIntegerField;
    rmdEnderecosCidade: TStringField;
    rmdEnderecosCodigoEstado: TIntegerField;
    rmdEnderecosEstado: TStringField;
    rmdEnderecosPrincipal: TBooleanField;
    dsEnderecos: TDataSource;
    rmdTelefones: TFDMemTable;
    rmdTelefonesCodigoTelefone: TIntegerField;
    rmdTelefonesTipo: TStringField;
    rmdTelefonesNumero: TStringField;
    rmdTelefonesPrincipal: TBooleanField;
    dsTelefones: TDataSource;
    rmdEstados: TFDMemTable;
    rmdEstadosCodigoEstado: TIntegerField;
    rmdEstadosNome: TStringField;
    rmdEstadosSigla: TStringField;
    dsEstados: TDataSource;
    rmdCidades: TFDMemTable;
    rmdCidadesCodigoCidade: TIntegerField;
    rmdCidadesCodigoEstado: TIntegerField;
    rmdCidadesNome: TStringField;
    rmdCidadesCodigoIbge: TStringField;
    dsCidades: TDataSource;
    edtEmail: TEdit;
    Label18: TLabel;
    btnAdicionarEmail: TBitBtn;
    dbgEmails: TJvDBGrid;
    cbEmailPrincipal: TCheckBox;
    Label19: TLabel;
    rmdEmails: TFDMemTable;
    dsEmails: TDataSource;
    rmdEmailsPrincipal: TBooleanField;
    rmdEmailsEndereco: TStringField;
    rmdEmailsCodigoEmail: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure cbTipoDocumentoChange(Sender: TObject);
    procedure dbLookupEstadosChange(Sender: TObject);
    procedure cbTipoTelefoneChange(Sender: TObject);
    procedure dbgEndrecosDblClick(Sender: TObject);
    procedure dbgTelefonesDblClick(Sender: TObject);
    procedure btnAdicionarEnderecoClick(Sender: TObject);
    procedure btnAdicionarTelefoneClick(Sender: TObject);
    procedure btnAdicionarEmailClick(Sender: TObject);
    procedure dbgEmailsDblClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure edtCodigoClienteExit(Sender: TObject);
  private
    procedure IniciarTipoTelefone;
    procedure IniciarTipoEndereco;
    procedure AdicionarEnderecoLista;
    procedure AdicionarTelefoneLista;
    procedure Salvar;
    procedure AdicionarEmailLista;
    procedure CarregarCliente(codigoCliente: Int64);
    procedure LimparCamposEndereco;
    procedure LimparCamposTelefone;
    procedure LimparCamposEmail;
    procedure LimparCampos; override;

    { Private declarations }
  public
    function Validar: Boolean;
    { Public declarations }
  end;

var
  FCadastroCliente: TFCadastroCliente;

implementation

uses
  Entities.Enums.ETipoTelefone, System.TypInfo, Entities.Enums.ETipoEndereco,
  EstadoBO, EnumConverterStrings, CidadeBO, Cliente, Pessoa, PessoaFisica,
  PessoaJuridica, Endereco, Bairro, Cidade, Estado, Telefone,
  ClienteBO, System.Generics.Collections, Funcoes, Email, uConsultaCliente,
  EnderecoBO, TelefoneBO, EmailBO;

{$R *.dfm}

procedure TFCadastroCliente.FormCreate(Sender: TObject);
begin
  inherited;
  IniciarTipoTelefone;
  IniciarTipoEndereco;
  cbTipoDocumentoChange(nil);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFCadastroCliente.btnAdicionarEmailClick(Sender: TObject);
begin
  inherited;
  AdicionarEmailLista;
end;

procedure TFCadastroCliente.btnAdicionarEnderecoClick(Sender: TObject);
begin
  inherited;
  AdicionarEnderecoLista;
end;

procedure TFCadastroCliente.btnAdicionarTelefoneClick(Sender: TObject);
begin
  inherited;
  AdicionarTelefoneLista;
end;

procedure TFCadastroCliente.btnAlterarClick(Sender: TObject);
begin
  inherited;
  ListarEstados(rmdEstados);
end;

procedure TFCadastroCliente.btnExcluirClick(Sender: TObject);
var
  Cliente: TCliente;
  ClienteBO: TClienteBO;
begin
  if Application.MessageBox('Confirmar a exclus�o do cliente?', 'Aten��o',
    MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON2) = IDYES then
  begin
    Try
      Try
        Cliente := TCliente.Create(edtCodigoCliente.AsInteger);
        ClienteBO := TClienteBO.Create;
        ClienteBO.excluir(Cliente);
        inherited;

      Except
        On E: Exception do
          Application.MessageBox(pWideChar('Falha ao exluir cliente:' + e.Message), 'Aten��o', MB_OK+MB_ICONERROR);
      End;
    Finally
      Cliente.Free;
      ClienteBO.Free;
    End;
  end;
end;

procedure TFCadastroCliente.btnNovoClick(Sender: TObject);
begin
  inherited;
  edtNome.SetFocus;
  ListarEstados(rmdEstados);
  edtCodigoCliente.ReadOnly:= True;
end;

procedure TFCadastroCliente.btnPesquisarClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TFConsultaCliente, FConsultaCliente);
  FConsultaCliente.ShowModal;
  if FConsultaCliente.CodigoPessoa > 0 then
  begin
    CarregarCliente(FConsultaCliente.CodigoPessoa);
  end;
  FConsultaCliente.Free;
end;

procedure TFCadastroCliente.btnSalvarClick(Sender: TObject);
begin
  inherited;
  Salvar;
end;

procedure TFCadastroCliente.cbTipoDocumentoChange(Sender: TObject);
begin
  inherited;
  edtCpfCnpj.EditMask:= FormatarTipoDocumento(cbTipoDocumento.ItemIndex);
  edtRazaoSocial.Visible := cbTipoDocumento.ItemIndex = 0;
  labelRazaoSocial.Visible:= edtRazaoSocial.Visible;
  edtCpfCnpj.Text := '';
end;

procedure TFCadastroCliente.cbTipoTelefoneChange(Sender: TObject);
begin
  inherited;
  case cbTipoTelefone.ItemIndex of
    1:
      edtNumeroTelefone.EditMask := '!\(99\)99999-9999;0;_';
  else
    edtNumeroTelefone.EditMask := '!\(99\)9999-9999;0;_';
  end;
  // '!999\.999\.999\-99;0;_';
end;

procedure TFCadastroCliente.dbgEmailsDblClick(Sender: TObject);
begin
  inherited;
  if rmdEmails.Active and Not rmdEmails.IsEmpty then
  begin
    if Application.MessageBox('Confirma a exclus�o do e-mail?', 'Aten��o',
      MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON1) = ID_YES then
    begin
      rmdEmails.Delete;
    end;
  end;

end;

procedure TFCadastroCliente.dbgEndrecosDblClick(Sender: TObject);
begin
  inherited;
  if rmdEnderecos.Active and Not rmdEnderecos.IsEmpty then
  begin
    if Application.MessageBox('Confirma a exclus�o do endere�o?', 'Aten��o',
      MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON1) = ID_YES then
    begin
      rmdEnderecos.Delete;
    end;
  end;
end;

procedure TFCadastroCliente.dbgTelefonesDblClick(Sender: TObject);
begin
  inherited;
  if rmdTelefones.Active and Not rmdTelefones.IsEmpty then
  begin
    if Application.MessageBox('Confirmar a exclus�o do telefone?', 'Aten��o',
      MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON1) = ID_YES then
    begin
      rmdTelefones.Delete;
    end;
  end;
end;

procedure TFCadastroCliente.dbLookupEstadosChange(Sender: TObject);
var
  CidadeBO: TCidadeBO;
  codigoEstado: Integer;
begin
  inherited;
  codigoEstado := rmdEstados.FieldByName('CodigoEstado').AsInteger;
  ListarCidadesPorEstado(rmdCidades, codigoEstado);
end;

procedure TFCadastroCliente.edtCodigoClienteExit(Sender: TObject);
begin
  inherited;
  if edtCodigoCliente.AsInteger > 0 then
  begin
    CarregarCliente(edtCodigoCliente.AsInteger);
  end;

end;

procedure TFCadastroCliente.IniciarTipoTelefone();
begin
  TConvert<TETipoTelefone>.PopulateListEnum(cbTipoTelefone.Items);
  cbTipoTelefone.ItemIndex := 0;
end;

procedure TFCadastroCliente.IniciarTipoEndereco();
begin
  TConvert<TETipoEndereco>.PopulateListEnum(cbTipoEndereco.Items);
  cbTipoEndereco.ItemIndex := 0;
end;


procedure TFCadastroCliente.AdicionarEnderecoLista;
var
  listaCampos: TList<TPair<TControl, String>>;
begin
  listaCampos := TList < TPair < TControl, String >>.Create;
  listaCampos.Add(TPair<TControl, String>.Create(edtLogradouro, 'Logradouro'));
  listaCampos.Add(TPair<TControl, String>.Create(edtBairro, 'Bairro'));
  listaCampos.Add(TPair<TControl, String>.Create(edtNumero, 'Numero'));
  listaCampos.Add(TPair<TControl, String>.Create(dbLookupEstados, 'UF'));
  listaCampos.Add(TPair<TControl, String>.Create(dbLookupCidades, 'Cidade'));

  if Not CamposEntradaObrigatorios(listaCampos) then
  begin
    FreeAndNil(listaCampos);
    MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
    Exit;
  end;

  FreeAndNil(listaCampos);
  if Not rmdEnderecos.Active then
  begin
    rmdEnderecos.Open;
  end;

  if cbPrincipal.Checked then
  begin
    rmdEnderecos.First;
    while Not rmdEnderecos.Eof do
    begin
      rmdEnderecos.Edit;
      rmdEnderecos.FieldByName('Principal').AsBoolean := False;
      rmdEnderecos.Post;
      rmdEnderecos.Next;
    end;
  end;

  with rmdEnderecos do
  begin
    Append;
    FieldByName('CodigoEndereco').AsInteger := 0;
    FieldByName('Tipo').AsString := cbTipoEndereco.Text;
    FieldByName('Logradouro').AsString := edtLogradouro.Text;
    FieldByName('Numero').AsString := edtNumero.Text;
    FieldByName('CodigoBairro').AsInteger := 0;
    FieldByName('Bairro').AsString := edtBairro.Text;
    FieldByName('Complemento').AsString := edtComplemento.Text;
    FieldByName('CodigoCidade').AsInteger :=
      rmdCidades.FieldByName('CodigoCidade').AsInteger;
    FieldByName('Cidade').AsString := rmdCidades.FieldByName('Nome').AsString;
    FieldByName('CodigoEstado').AsInteger :=
      rmdEstados.FieldByName('CodigoEstado').AsInteger;
    FieldByName('Sigla').AsString := rmdEstados.FieldByName('Sigla').AsString;
    FieldByName('Principal').AsBoolean := cbPrincipal.Checked;
    Post;
  end;
  LimparCamposEndereco;
  edtLogradouro.SetFocus;
end;

procedure TFCadastroCliente.AdicionarTelefoneLista;
var
  listaCampos: TList<TPair<TControl, String>>;
begin
  listaCampos := TList < TPair < TControl, String >>.Create;
  listaCampos.Add(TPair<TControl, String>.Create(edtNumeroTelefone,
    'N�mero do telefone'));

  if Not CamposEntradaObrigatorios(listaCampos) then
  begin
    FreeAndNil(listaCampos);
    MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
    Exit;
  end;

  FreeAndNil(listaCampos);

  if Not rmdTelefones.Active then
  begin
    rmdTelefones.Open;
  end;

  if cbTelefonePrincipal.Checked then
  begin
    rmdTelefones.First;
    while Not rmdTelefones.Eof do
    begin
      rmdTelefones.Edit;
      rmdTelefones.FieldByName('Principal').AsBoolean := False;
      rmdTelefones.Post;
      rmdTelefones.Next;
    end;
  end;

  with rmdTelefones do
  begin
    Append;
    FieldByName('CodigoTelefone').AsInteger := 0;
    FieldByName('Tipo').AsString := cbTipoTelefone.Text;
    FieldByName('Numero').AsString := edtNumeroTelefone.Text;
    FieldByName('Principal').AsBoolean := cbTelefonePrincipal.Checked;
    Post;
  end;
  LimparCamposTelefone;
  edtNumeroTelefone.SetFocus;
end;

procedure TFCadastroCliente.AdicionarEmailLista;
var
  listaCampos: TList<TPair<TControl, String>>;
begin
  listaCampos := TList < TPair < TControl, String >>.Create;
  listaCampos.Add(TPair<TControl, String>.Create(edtEmail,
    'Endere�o de e-mail'));

  if Not CamposEntradaObrigatorios(listaCampos) then
  begin
    FreeAndNil(listaCampos);
    MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
    Exit;
  end;

  FreeAndNil(listaCampos);
  if Not rmdEmails.Active then
  begin
    rmdEmails.Open;
  end;

  if cbEmailPrincipal.Checked then
  begin
    rmdEmails.First;
    while Not rmdEmails.Eof do
    begin
      rmdEmails.Edit;
      rmdEmails.FieldByName('Principal').AsBoolean := False;
      rmdEmails.Post;
      rmdEmails.Next;
    end;
  end;

  with rmdEmails do
  begin
    Append;
    FieldByName('CodigoEmail').AsInteger := 0;
    FieldByName('Principal').AsBoolean := cbEmailPrincipal.Checked;
    FieldByName('Endereco').AsString := edtEmail.Text;
    Post;
  end;
  LimparCamposEmail;
  edtEmail.SetFocus;
end;

procedure TFCadastroCliente.Salvar;
var
  Cliente: TCliente;
  Pessoa: TPessoa;
  Endereco: TEndereco;
  Telefone: TTelefone;
  Email: TEmail;
  Bairro: TBairro;
  Cidade: TCidade;
  cpfCnpj: String;
  rgInscricao: String;
  ClienteBO: TClienteBO;
  listaCampos: TList<TPair<TControl, String>>;
begin
  Try
    listaCampos := TList < TPair < TControl, String >>.Create;
    listaCampos.Add(TPair<TControl, String>.Create(edtNome, 'Nome do cliente'));
    if cbTipoDocumento.ItemIndex = 0 then
      listaCampos.Add(TPair<TControl, String>.Create(edtRazaoSocial,
        'Raz�o Social'));

    if Not CamposEntradaObrigatorios(listaCampos) then
    begin
      FreeAndNil(listaCampos);
      MessageDlg(ObterCamposObrigatorios, mtError, [mbOk], 0, mbOk);
      Exit;
    end;

    FreeAndNil(listaCampos);

    cpfCnpj := TFuncoes.unMask(edtCpfCnpj.Text);
    rgInscricao := edtRgInscricaoEstadual.Text;
    Pessoa := TPessoa.Create(edtCodigoCliente.AsInteger, edtNome.Text,
      memoObservacao.Lines.Text);

    if Trim(cpfCnpj) <> EmptyStr then
    begin
      if cbTipoDocumento.ItemIndex = 0 then
        Pessoa.PessoaJuridica := TPessoaJuridica.Create(cpfCnpj, rgInscricao,
          edtRazaoSocial.Text)
      else
        Pessoa.PessoaFisica := TPessoaFisica.Create(cpfCnpj, rgInscricao);
    end;

    if rmdEnderecos.Active then
    begin
      rmdEnderecos.First;
      while Not rmdEnderecos.Eof do
      begin
        with rmdEnderecos do
        begin
          Endereco := TEndereco.Create(FieldByName('CodigoEndereco').AsInteger,
            FieldByName('Logradouro').AsString, FieldByName('Numero').AsString,
            FieldByName('Complemento').AsString, FieldByName('Principal')
            .AsBoolean, TConvert<TETipoEndereco>.StrConvertEnum
            (FieldByName('Tipo').AsString),
            TBairro.Create(FieldByName('CodigoBairro').AsInteger,
            FieldByName('Bairro').AsString),
            TCidade.Create(FieldByName('CodigoCidade').AsInteger,
            TEstado.Create(FieldByName('CodigoEstado').AsInteger)));

          Pessoa.adicionarEndereco(Endereco);
        end;
        rmdEnderecos.Next;
      end;
    end;

    if rmdTelefones.Active then
    begin
      rmdTelefones.First;
      while Not rmdTelefones.Eof do
      begin
        with rmdTelefones do
        begin
          Telefone := TTelefone.Create(FieldByName('CodigoTelefone').AsInteger,
            FieldByName('Numero').AsString,
            TConvert<TETipoTelefone>.StrConvertEnum(FieldByName('Tipo')
            .AsString), FieldByName('Principal').AsBoolean);
          Pessoa.adicionarTelefone(Telefone);
        end;
        rmdTelefones.Next;
      end;
    end;

    if rmdEmails.Active then
    begin
      rmdEmails.First;
      while Not rmdEmails.Eof do
      begin
        with rmdEmails do
        begin
          Email := TEmail.Create(FieldByName('CodigoEmail').AsInteger,
            FieldByName('Endereco').AsString, FieldByName('Principal')
            .AsBoolean);
          Pessoa.adicionarEmail(Email);
        end;
        rmdEmails.Next;
      end;
    end;

    Cliente := TCliente.Create(0, cbBloqueado.Checked, False, Pessoa);
    ClienteBO := TClienteBO.Create;

    if (Cliente.Pessoa.CodigoPessoa > 0) then
      ClienteBO.alterar(Cliente)
    else
      ClienteBO.incluir(Cliente);

    Application.MessageBox(pWideChar('Registrado com sucesso!' + sLineBreak + 'C�digo registrado: ' +
      IntToStr(Pessoa.CodigoPessoa)),'Mensagem',  MB_OK+MB_ICONASTERISK);
    LimparCampos;
    HabilitaControlesVisuais(False);
    HabilitaControles(true, False, False, False, False, true);
  Except
    On E: Exception do
    begin
      MessageDlg('Falha ao cadastrar cliente: ' + sLineBreak + E.Message,
        mtError, [mbOk], 0, mbOk);
    end;
  End;
end;

procedure TFCadastroCliente.CarregarCliente(codigoCliente: Int64);
var
  Cliente: TCliente;
  ClienteBO: TClienteBO;
  EnderecoBO: TEnderecoBO;
  TelefoneBO: TTelefoneBO;
  EmailBO: TEmailBO;
  cpfCnpj, rgIe: String;
  dados: TFDMemTable;
begin
  LimparCampos;
  ClienteBO := TClienteBO.Create;
  Cliente := ClienteBO.obterPorCodigo
    (TCliente.Create(FConsultaCliente.CodigoPessoa));

  if Cliente = Nil then
  begin
    MessageDlg('Cliente n�o encontrado', mtInformation, [mbOk], 0, mbOk);
    Exit;
  end;

  edtCodigoCliente.Value := Cliente.codigoCliente;
  edtNome.Text := Cliente.Pessoa.Nome;
  memoObservacao.Lines.Add(Cliente.Pessoa.Observacao);
  cbBloqueado.Checked := Cliente.Bloqueado;

  if Assigned(Cliente.Pessoa.PessoaFisica) then
  begin
    cbTipoDocumento.ItemIndex := 1;
    cpfCnpj := Cliente.Pessoa.PessoaFisica.Cpf;
    rgIe := Cliente.Pessoa.PessoaFisica.Rg;
  end;

  if Assigned(Cliente.Pessoa.PessoaJuridica) then
  begin
    cbTipoDocumento.ItemIndex := 0;
    cpfCnpj := Cliente.Pessoa.PessoaJuridica.Cnpj;
    rgIe := Cliente.Pessoa.PessoaJuridica.InscricaoEstadual;
    edtRazaoSocial.Text := Cliente.Pessoa.PessoaJuridica.RazaoSocial;
  end;

  cbTipoDocumentoChange(nil);

  edtCpfCnpj.Text := cpfCnpj;
  edtRgInscricaoEstadual.Text := rgIe;

  EnderecoBO := TEnderecoBO.Create;
  dados := EnderecoBO.listarPorPessoa(TEndereco.Create(Cliente.codigoCliente));

  rmdEnderecos.Close;
  rmdEnderecos.CopyDataSet(dados);

  TelefoneBO := TTelefoneBO.Create;
  dados := TelefoneBO.listarPorPessoa(TTelefone.Create(Cliente.codigoCliente));

  rmdTelefones.Close;
  rmdTelefones.CopyDataSet(dados);

  EmailBO := TEmailBO.Create;
  dados := EmailBO.listarPorPessoa(TEmail.Create(Cliente.codigoCliente));

  rmdEmails.Close;
  rmdEmails.CopyDataSet(dados);

  HabilitaControles(False, False, true, true, true, true);

  ClienteBO.Free;
  EnderecoBO.Free;
  TelefoneBO.Free;
  EmailBO.Free;
end;

function TFCadastroCliente.Validar: Boolean;
var
  listaErros: TList<String>;
begin

end;

procedure TFCadastroCliente.LimparCamposEndereco;
var
  listaComponentes: TList<TControl>;
begin
  listaComponentes := TList<TControl>.Create;
  listaComponentes.Add(cbTipoEndereco);
  listaComponentes.Add(edtLogradouro);
  listaComponentes.Add(edtNumero);
  listaComponentes.Add(edtBairro);
  listaComponentes.Add(edtComplemento);
  listaComponentes.Add(dbLookupEstados);
  listaComponentes.Add(dbLookupCidades);
  listaComponentes.Add(cbPrincipal);

  LimparCampos(listaComponentes);
  listaComponentes.Free;

end;

procedure TFCadastroCliente.LimparCamposTelefone;
var
  listaComponentes: TList<TControl>;
begin
  listaComponentes := TList<TControl>.Create;
  listaComponentes.Add(cbTipoTelefone);
  listaComponentes.Add(edtNumeroTelefone);
  listaComponentes.Add(cbTelefonePrincipal);

  LimparCampos(listaComponentes);
  listaComponentes.Free;
end;

procedure TFCadastroCliente.LimparCampos;
begin
  inherited;
  edtCodigoCliente.ReadOnly:= False;
end;

procedure TFCadastroCliente.LimparCamposEmail;
var
  listaComponentes: TList<TControl>;
begin
  listaComponentes := TList<TControl>.Create;
  listaComponentes.Add(edtEmail);
  listaComponentes.Add(cbEmailPrincipal);

  LimparCampos(listaComponentes);
  listaComponentes.Free;
end;

end.

inherited FCadastroProdutoGrupo: TFCadastroProdutoGrupo
  Caption = 'Cadastro de grupos'
  ClientHeight = 125
  ExplicitHeight = 154
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 59
    Width = 33
    Height = 13
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [1]
    Left = 143
    Top = 59
    Width = 27
    Height = 13
    Caption = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  inherited Panel1: TPanel
    TabOrder = 2
    inherited btnExcluir: TBitBtn
      TabOrder = 3
    end
    inherited btnSalvar: TBitBtn
      TabOrder = 2
      OnClick = btnSalvarClick
    end
    inherited btnPesquisar: TBitBtn
      OnClick = btnPesquisarClick
    end
  end
  object edtNome: TEdit [3]
    Left = 143
    Top = 78
    Width = 441
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
  end
  object edtCodigo: TJvCalcEdit [4]
    Left = 16
    Top = 78
    Width = 120
    Height = 21
    Alignment = taCenter
    DecimalPlaces = 0
    DisplayFormat = '0000'
    ShowButton = False
    TabOrder = 0
    DecimalPlacesAlwaysShown = False
    OnExit = edtCodigoExit
  end
end

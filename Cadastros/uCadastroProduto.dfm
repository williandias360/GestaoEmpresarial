inherited FCadastroProduto: TFCadastroProduto
  Caption = 'Cadastro de Produto'
  ClientHeight = 290
  ClientWidth = 604
  ExplicitWidth = 610
  ExplicitHeight = 319
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 16
    Top = 59
    Width = 33
    Height = 13
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [1]
    Left = 143
    Top = 59
    Width = 27
    Height = 13
    Caption = 'Nome'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [2]
    Left = 16
    Top = 107
    Width = 82
    Height = 13
    Caption = 'C'#243'digo de barras'
  end
  object Label4: TLabel [3]
    Left = 143
    Top = 107
    Width = 29
    Height = 13
    Caption = 'Grupo'
  end
  object Label5: TLabel [4]
    Left = 343
    Top = 107
    Width = 76
    Height = 13
    Caption = 'Unidade Medida'
  end
  object Label6: TLabel [5]
    Left = 143
    Top = 160
    Width = 58
    Height = 13
    Caption = 'Qtde. Caixa'
  end
  object Label7: TLabel [6]
    Left = 365
    Top = 160
    Width = 67
    Height = 13
    Caption = 'Estoque Atual'
  end
  object Label8: TLabel [7]
    Left = 16
    Top = 160
    Width = 22
    Height = 13
    Caption = 'NCM'
  end
  object Label9: TLabel [8]
    Left = 254
    Top = 160
    Width = 74
    Height = 13
    Caption = 'Estoque M'#237'nimo'
  end
  inherited Panel1: TPanel
    Width = 604
    TabOrder = 9
    ExplicitWidth = 604
    inherited btnSalvar: TBitBtn
      OnClick = btnSalvarClick
    end
    inherited btnPesquisar: TBitBtn
      OnClick = btnPesquisarClick
    end
  end
  object edtNome: TEdit [10]
    Left = 143
    Top = 78
    Width = 316
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
  end
  object edtCodigo: TJvCalcEdit [11]
    Left = 16
    Top = 78
    Width = 120
    Height = 21
    Alignment = taCenter
    CheckOnExit = True
    DecimalPlaces = 0
    DisplayFormat = '0000'
    ShowButton = False
    TabOrder = 10
    DecimalPlacesAlwaysShown = False
    OnExit = edtCodigoExit
  end
  object edtCodigoBarras: TEdit [12]
    Left = 16
    Top = 126
    Width = 120
    Height = 21
    MaxLength = 14
    TabOrder = 1
  end
  object dbLookupProdutoGrupo: TJvDBLookupCombo [13]
    Left = 143
    Top = 126
    Width = 194
    Height = 21
    LookupField = 'CodigoProdutoGrupo'
    LookupDisplay = 'Nome'
    LookupSource = dsProdutoGrupo
    TabOrder = 2
  end
  object dbLookupUnidadeMedida: TJvDBLookupCombo [14]
    Left = 343
    Top = 126
    Width = 116
    Height = 21
    LookupField = 'CodigoUnidadeMedida'
    LookupDisplay = 'Sigla;Nome'
    LookupSource = dsUnidadeMedida
    TabOrder = 3
  end
  object edtQuantidadePorCaixa: TJvCalcEdit [15]
    Left = 143
    Top = 179
    Width = 94
    Height = 21
    DecimalPlaces = 3
    DisplayFormat = ',0.###'
    ShowButton = False
    TabOrder = 5
    Value = 1.000000000000000000
    ZeroEmpty = False
    DecimalPlacesAlwaysShown = False
  end
  object edtEstoqueAtual: TJvCalcEdit [16]
    Left = 365
    Top = 179
    Width = 94
    Height = 21
    DecimalPlaces = 3
    DisplayFormat = ',0.###'
    ReadOnly = True
    ShowButton = False
    TabOrder = 7
    ZeroEmpty = False
    DecimalPlacesAlwaysShown = False
  end
  object edtEstoqueMinimo: TJvCalcEdit [17]
    Left = 254
    Top = 179
    Width = 94
    Height = 21
    DecimalPlaces = 3
    DisplayFormat = ',0.###'
    ShowButton = False
    TabOrder = 6
    ZeroEmpty = False
    DecimalPlacesAlwaysShown = False
  end
  object btnAbrirImagem: TBitBtn [18]
    Left = 502
    Top = 177
    Width = 75
    Height = 25
    Caption = 'Abrir imagem'
    TabOrder = 11
    OnClick = btnAbrirImagemClick
  end
  object GroupBox1: TGroupBox [19]
    Left = 16
    Top = 216
    Width = 580
    Height = 65
    Caption = 'Pre'#231'os'
    TabOrder = 8
    object Label10: TLabel
      Left = 51
      Top = 15
      Width = 33
      Height = 13
      Caption = #192' Vista'
    end
    object Label11: TLabel
      Left = 215
      Top = 15
      Width = 37
      Height = 13
      Caption = 'A Prazo'
    end
    object Label12: TLabel
      Left = 380
      Top = 15
      Width = 28
      Height = 13
      Caption = 'Custo'
    end
    object edtPrecoVista: TJvCalcEdit
      Left = 51
      Top = 34
      Width = 121
      Height = 21
      DisplayFormat = ',0.00'
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edtPrecoPrazo: TJvCalcEdit
      Left = 215
      Top = 34
      Width = 121
      Height = 21
      DisplayFormat = ',0.00'
      ShowButton = False
      TabOrder = 1
      DecimalPlacesAlwaysShown = False
    end
    object edtPrecoCusto: TJvCalcEdit
      Left = 380
      Top = 34
      Width = 121
      Height = 21
      DisplayFormat = ',0.00'
      ShowButton = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
    end
  end
  object GroupBox2: TGroupBox [20]
    Left = 480
    Top = 53
    Width = 116
    Height = 118
    Caption = 'Foto'
    TabOrder = 12
    object imgFotoProduto: TJvImage
      Left = 7
      Top = 19
      Width = 100
      Height = 93
      ParentCustomHint = False
      Stretch = True
    end
  end
  object edtNcm: TEdit [21]
    Left = 16
    Top = 179
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 4
  end
  inherited ImageList1: TImageList
    Left = 256
    Top = 160
  end
  object dsUnidadeMedida: TDataSource
    DataSet = rmdUnidadeMedida
    Left = 536
    Top = 232
  end
  object rmdUnidadeMedida: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 568
    Top = 232
    object rmdUnidadeMedidaCodigoUnidadeMedida: TIntegerField
      FieldName = 'CodigoUnidadeMedida'
    end
    object rmdUnidadeMedidaNome: TStringField
      FieldName = 'Nome'
      Size = 40
    end
    object rmdUnidadeMedidaSigla: TStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object dsProdutoGrupo: TDataSource
    DataSet = rmdProdutoGrupo
    Left = 472
    Top = 232
  end
  object rmdProdutoGrupo: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 504
    Top = 232
    object rmdProdutoGrupoCodigoProdutoGrupo: TIntegerField
      FieldName = 'CodigoProdutoGrupo'
    end
    object rmdProdutoGrupoNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object OpenPictureDialog: TOpenPictureDialog
    Left = 448
    Top = 120
  end
end

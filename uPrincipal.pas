unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.ToolWin, Vcl.ActnMan, Vcl.ActnCtrls, System.ImageList, Vcl.ImgList,
  JvExComCtrls, JvToolBar, PngImageList, Vcl.ExtCtrls;

type
  TFPrincipal = class(TForm)
    StatusBar1: TStatusBar;
    MainMenu1: TMainMenu;
    Cadastros1: TMenuItem;
    Clientes1: TMenuItem;
    Usurios1: TMenuItem;
    N1: TMenuItem;
    Produtos2: TMenuItem;
    Oramento1: TMenuItem;
    Configuraes1: TMenuItem;
    Sair1: TMenuItem;
    N2: TMenuItem;
    Empresa1: TMenuItem;
    Relatrios1: TMenuItem;
    Fornecedor1: TMenuItem;
    PngImageList1: TPngImageList;
    ToolBarPrincipal: TToolBar;
    ToolButtonConsultaClientes: TToolButton;
    TimerDataHora: TTimer;
    UnidadeMedida1: TMenuItem;
    Grupo1: TMenuItem;
    Produto1: TMenuItem;
    ToolButtonConsultarProdutos: TToolButton;
    procedure Clientes1Click(Sender: TObject);
    procedure Fornecedor1Click(Sender: TObject);
    procedure ToolButtonConsultaClientesClick(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Usurios1Click(Sender: TObject);
    procedure Empresa1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerDataHoraTimer(Sender: TObject);
    procedure UnidadeMedida1Click(Sender: TObject);
    procedure Grupo1Click(Sender: TObject);
    procedure Produto1Click(Sender: TObject);
    procedure ToolButtonConsultarProdutosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPrincipal: TFPrincipal;

implementation

uses
  ClienteBO, Cliente, Pessoa, Entities.Enums.EPerfilPessoa, PessoaFisica,
  PessoaJuridica, Email, Endereco, Entities.Enums.ETipoEndereco, Cidade,
  Bairro, Telefone, Entities.Enums.ETipoTelefone, uCadastroCliente,
  System.SysUtils, CidadeBO, EnderecoBO, FireDAC.Comp.Client,
  uCadastroFornecedor, uConsultaCliente, uCadastroUsuario, uCadastroEmpresa,
  uLogin, Globals, uCadastroUnidadeMedida, uCadastroProduto,
  uCadastroProdutoGrupo, uConsultaProduto;

{$R *.dfm}

procedure TFPrincipal.Clientes1Click(Sender: TObject);
begin
  Application.CreateForm(TFCadastroCliente, FCadastroCliente);
  FCadastroCliente.ShowModal;
  FreeAndNil(FCadastroCliente);
end;

procedure TFPrincipal.Empresa1Click(Sender: TObject);
begin
  Application.CreateForm(TFCadastroEmpresa, FCadastroEmpresa);
  FCadastroEmpresa.ShowModal;
  FreeAndNil(FCadastroEmpresa);
end;

procedure TFPrincipal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F2 then
    ToolButtonConsultaClientesClick(nil)
  else if Key = VK_F3 then
    ToolButtonConsultarProdutosClick(nil);

end;

procedure TFPrincipal.FormShow(Sender: TObject);
begin
  Application.CreateForm(TFLogin, FLogin);
  FLogin.ShowModal;
  FreeAndNil(FLogin);
  if TGlobals.getUsuario <> Nil then
  begin
    StatusBar1.Panels[1].Text := 'Usuario Logado: ' +
      TGlobals.getUsuario.Pessoa.Nome;
  end;
end;

procedure TFPrincipal.Fornecedor1Click(Sender: TObject);
begin
  Application.CreateForm(TFCadastroFornecedor, FCadastroFornecedor);
  FCadastroFornecedor.ShowModal;
  FreeAndNil(FCadastroFornecedor);
end;

procedure TFPrincipal.Grupo1Click(Sender: TObject);
begin
  Application.CreateForm(TFCadastroProdutoGrupo, FCadastroProdutoGrupo);
  FCadastroProdutoGrupo.ShowModal;
  FreeAndNil(FCadastroProdutoGrupo);
end;

procedure TFPrincipal.Produto1Click(Sender: TObject);
begin
  Application.CreateForm(TFCadastroProduto, FCadastroProduto);
  FCadastroProduto.ShowModal;
  FreeAndNil(FCadastroProduto);
end;

procedure TFPrincipal.Sair1Click(Sender: TObject);
begin
  if Application.MessageBox('Deseja sair do Sistema?', 'Aten��o',
    MB_YESNO + MB_ICONQUESTION + MB_DEFBUTTON1) = ID_YES then
  begin
    Application.Terminate;
  end;

end;

procedure TFPrincipal.TimerDataHoraTimer(Sender: TObject);
begin
  StatusBar1.Panels[2].Text := FormatDateTime('dd/MM/yyyy HH:mm:ss', Now());
end;

procedure TFPrincipal.ToolButtonConsultaClientesClick(Sender: TObject);
begin
  Application.CreateForm(TFConsultaCliente, FConsultaCliente);
  FConsultaCliente.ShowModal;
  FreeAndNil(FConsultaCliente);
end;

procedure TFPrincipal.ToolButtonConsultarProdutosClick(Sender: TObject);
begin
  Application.CreateForm(TFConsultaProduto, FConsultaProduto);
  FConsultaProduto.ShowModal;
  FreeAndNil(FConsultaProduto);
end;

procedure TFPrincipal.UnidadeMedida1Click(Sender: TObject);
begin
  Application.CreateForm(TFCadastroUnidadeMedida, FCadastroUnidadeMedida);
  FCadastroUnidadeMedida.ShowModal;
  FreeAndNil(FCadastroUnidadeMedida);
end;

procedure TFPrincipal.Usurios1Click(Sender: TObject);
begin
  Application.CreateForm(TFCadastroUsuario, FCadastroUsuario);
  FCadastroUsuario.ShowModal;
  FreeAndNil(FCadastroUsuario);
end;

end.

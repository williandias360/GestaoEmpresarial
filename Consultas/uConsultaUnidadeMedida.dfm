inherited FConsultaUnidadeMedida: TFConsultaUnidadeMedida
  Caption = 'Consulta Unidade Medida'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited GroupBox2: TGroupBox
      inherited edtBusca: TEdit
        OnKeyDown = edtBuscaKeyDown
      end
      inherited btnConfirmar: TBitBtn
        OnClick = btnConfirmarClick
      end
    end
  end
  inherited dbgDadosConsulta: TJvDBGrid
    DataSource = dsUnidadeMedida
    OnDblClick = dbgDadosConsultaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoUnidadeMedida'
        Title.Caption = 'Codigo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 260
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Sigla'
        Width = 80
        Visible = True
      end>
  end
  object dsUnidadeMedida: TDataSource
    DataSet = rmdUnidadeMedida
    Left = 336
    Top = 192
  end
  object rmdUnidadeMedida: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 400
    Top = 192
    object rmdUnidadeMedidaCodigoUnidadeMedida: TIntegerField
      FieldName = 'CodigoUnidadeMedida'
    end
    object rmdUnidadeMedidaNome: TStringField
      FieldName = 'Nome'
      Size = 40
    end
    object rmdUnidadeMedidaSigla: TStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
end

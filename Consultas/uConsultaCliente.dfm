inherited FConsultaCliente: TFConsultaCliente
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Consulta de cliente'
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited GroupBox1: TGroupBox
      Left = 2
      ExplicitLeft = 2
    end
    inherited GroupBox2: TGroupBox
      inherited Label3: TLabel
        Left = 16
        ExplicitLeft = 16
      end
      inherited edtBusca: TEdit
        Left = 16
        Width = 481
        OnKeyDown = edtBuscaKeyDown
        ExplicitLeft = 16
        ExplicitWidth = 481
      end
      inherited btnConfirmar: TBitBtn
        Left = 365
        OnClick = btnConfirmarClick
        ExplicitLeft = 365
      end
    end
  end
  inherited dbgDadosConsulta: TJvDBGrid
    DataSource = dsClientes
    OnDblClick = dbgDadosConsultaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoCliente'
        Title.Caption = 'Codigo Cliente'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RazaoSocial'
        Title.Caption = 'Raz'#227'o Social'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CpfCnpj'
        Title.Caption = 'Cpf/Cnpj'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RgIe'
        Title.Caption = 'Rg/Ie'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Logradouro'
        Width = 290
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Numero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Bairro'
        Width = 190
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cidade'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Estado'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Telefone'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Limite'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Bloqueado'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EnviarEmailFinalizarVenda'
        Title.Caption = 'Enviar e-mail na venda'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'DataCriacao'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataModificacao'
        Visible = True
      end>
  end
  object dsClientes: TDataSource
    DataSet = rmdClientes
    Left = 408
    Top = 160
  end
  object rmdClientes: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 440
    Top = 160
    object rmdClientesCodigoCliente: TLargeintField
      FieldName = 'CodigoCliente'
    end
    object rmdClientesNome: TStringField
      FieldName = 'Nome'
      Size = 350
    end
    object rmdClientesRazaoSocial: TStringField
      FieldName = 'RazaoSocial'
      Size = 200
    end
    object rmdClientesCpfCnpj: TStringField
      FieldName = 'CpfCnpj'
      Size = 14
    end
    object rmdClientesRgIe: TStringField
      FieldName = 'RgIe'
      Size = 19
    end
    object rmdClientesLogradouro: TStringField
      FieldName = 'Logradouro'
      Size = 250
    end
    object rmdClientesNumero: TStringField
      FieldName = 'Numero'
      Size = 9
    end
    object rmdClientesBairro: TStringField
      FieldName = 'Bairro'
      Size = 80
    end
    object rmdClientesCidade: TStringField
      FieldName = 'Cidade'
      Size = 250
    end
    object rmdClientesEstado: TStringField
      FieldName = 'Estado'
      Size = 2
    end
    object rmdClientesTelefone: TStringField
      FieldName = 'Telefone'
      Size = 13
    end
    object rmdClientesBloqueado: TBooleanField
      FieldName = 'Bloqueado'
    end
    object rmdClientesEnviarEmailFinalizarVenda: TBooleanField
      FieldName = 'EnviarEmailFinalizarVenda'
    end
    object rmdClientesDataCriacao: TSQLTimeStampField
      FieldName = 'DataCriacao'
      DisplayFormat = 'dd/MM/yyyy HH:mm:ss'
    end
    object rmdClientesDataModificacao: TSQLTimeStampField
      FieldName = 'DataModificacao'
      DisplayFormat = 'dd/MM/yyyy HH:mm:ss'
    end
    object rmdClientesLimite: TCurrencyField
      FieldName = 'Limite'
      DisplayFormat = '#0.00'
    end
  end
end

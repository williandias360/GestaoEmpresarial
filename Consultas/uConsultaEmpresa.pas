unit uConsultaEmpresa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormConsultaPadrao, Data.DB,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFConsultaEmpresa = class(TFFormConsultaPadrao)
    rmdEmpresa: TFDMemTable;
    dsEmpresa: TDataSource;
    rmdEmpresaCodigoEmpresa: TIntegerField;
    rmdEmpresaNome: TStringField;
    rmdEmpresaRazaoSocial: TStringField;
    rmdEmpresaCpfCnpj: TStringField;
    rmdEmpresaRgIe: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure edtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgDadosConsultaDblClick(Sender: TObject);
  private
    FCodigoEmpresa: Integer;
    procedure SelecionarEmpresa;

  const
    filtroBuscarPor: array [0 .. 3] of String = ('Nome', 'C�digo',
      'Cpf', 'Cnpj');
    filtroOrdenarPor: array [0 .. 2] of String = ('Nome', 'C�digo',
      'DataCriacao');
    { Private declarations }
  public
    { Public declarations }
    property CodigoEmpresa: Integer read FCodigoEmpresa write FCodigoEmpresa;
  end;

var
  FConsultaEmpresa: TFConsultaEmpresa;

implementation

uses
  System.Generics.Collections, EmpresaBO;

{$R *.dfm}

procedure TFConsultaEmpresa.dbgDadosConsultaDblClick(Sender: TObject);
begin
  inherited;
  SelecionarEmpresa;
end;

procedure TFConsultaEmpresa.edtBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  listaParametros: TDictionary<String, Variant>;
  EmpresaBO: TEmpresaBO;
begin
  inherited;
  if Key = VK_SPACE then
  begin
    listaParametros := TDictionary<String, Variant>.Create();
    case cbFiltrarPor.ItemIndex of
      0:
        listaParametros.Add('Nome', edtBusca.Text);
      1:
        listaParametros.Add('CodigoEmpresa', StrToInt(Trim(edtBusca.Text)));
      2:
        listaParametros.Add('Cpf', edtBusca.Text);
    else
      listaParametros.Add('Cnpj', edtBusca.Text);
    end;

    EmpresaBO := TEmpresaBO.Create;
    if Not rmdEmpresa.Active then
      rmdEmpresa.Open;
    rmdEmpresa.EmptyDataSet;

    rmdEmpresa.CopyDataSet(EmpresaBO.ListarEmpresaPor(listaParametros),
      [coRestart, coAppend]);
    atribuirTotalRegistrosStatusBar(rmdEmpresa.RecordCount);
  end;
end;

procedure TFConsultaEmpresa.FormCreate(Sender: TObject);
begin
  inherited;
  CarregarFiltrosBusca(filtroBuscarPor);
  CarregarFiltrosOrdenacao(filtroOrdenarPor);
end;

procedure TFConsultaEmpresa.SelecionarEmpresa;
begin
  if rmdEmpresa.Active and Not rmdEmpresa.IsEmpty then
  begin
    FCodigoEmpresa:= rmdEmpresa.FieldByName('CodigoEmpresa').AsInteger;
    Close;
  end;
end;

end.

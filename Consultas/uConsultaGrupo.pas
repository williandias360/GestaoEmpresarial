unit uConsultaGrupo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormConsultaPadrao, Data.DB,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFConsultaGrupo = class(TFFormConsultaPadrao)
    rmdProdutoGrupo: TFDMemTable;
    dsProdutoGrupo: TDataSource;
    rmdProdutoGrupoNome: TStringField;
    rmdProdutoGrupoDataCriacao: TDateTimeField;
    rmdProdutoGrupoCodigoProdutoGrupo: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure edtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgDadosConsultaDblClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoProdutoGrupo: Integer;
    procedure SelecionarProdutoGrupo;

  const
    filtroBuscarPor: array [0 .. 1] of String = ('Nome', 'C�digo');
    filtroOrdenarPor: array [0 .. 2] of String = ('Nome', 'C�digo',
      'DataCriacao');

  public
    { Public declarations }
    property CodigoProdutoGrupo: Integer read FCodigoProdutoGrupo
      write FCodigoProdutoGrupo;
  end;

var
  FConsultaGrupo: TFConsultaGrupo;

implementation

uses
  System.Generics.Collections, ProdutoGrupoBO;

{$R *.dfm}

procedure TFConsultaGrupo.btnConfirmarClick(Sender: TObject);
begin
  inherited;
  SelecionarProdutoGrupo;
end;

procedure TFConsultaGrupo.dbgDadosConsultaDblClick(Sender: TObject);
begin
  inherited;
  SelecionarProdutoGrupo;
end;

procedure TFConsultaGrupo.edtBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  listaParametros: TDictionary<String, Variant>;
  ProdutoGrupoBO:TProdutoGrupoBO;
begin
  inherited;
  if Key = VK_SPACE then
  begin
     listaParametros := TDictionary<String, Variant>.Create();
    case cbFiltrarPor.ItemIndex of
      0:
        listaParametros.Add('Nome', edtBusca.Text);
      1:
        listaParametros.Add('CodigoProdutoGrupo',
          StrToInt(Trim(edtBusca.Text)));
    end;

    ProdutoGrupoBO:= TProdutoGrupoBO.Create;
    if Not rmdProdutoGrupo.Active
    then rmdProdutoGrupo.Open;

    rmdProdutoGrupo.EmptyDataSet;
    rmdProdutoGrupo.CopyDataSet(ProdutoGrupoBO.ListarProdutoGrupoPor(listaParametros));
    atribuirTotalRegistrosStatusBar(rmdProdutoGrupo.RecordCount);
  end;
end;

procedure TFConsultaGrupo.FormCreate(Sender: TObject);
begin
  inherited;
  CarregarFiltrosBusca(filtroBuscarPor);
  CarregarFiltrosOrdenacao(filtroOrdenarPor);
end;

procedure TFConsultaGrupo.SelecionarProdutoGrupo;
begin
   if rmdProdutoGrupo.Active And Not rmdProdutoGrupo.IsEmpty then
   begin
     CodigoProdutoGrupo:= rmdProdutoGrupo.FieldByName('CodigoProdutoGrupo').AsInteger;
     Close;
   end;
end;

end.

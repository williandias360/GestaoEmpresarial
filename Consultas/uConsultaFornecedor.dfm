inherited FConsultaFornecedor: TFConsultaFornecedor
  Caption = 'Consulta Fornecedor'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited GroupBox2: TGroupBox
      inherited edtBusca: TEdit
        OnKeyDown = edtBuscaKeyDown
      end
      inherited btnConfirmar: TBitBtn
        OnClick = btnConfirmarClick
      end
    end
  end
  inherited dbgDadosConsulta: TJvDBGrid
    DataSource = dsFornecedores
    OnDblClick = dbgDadosConsultaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoFornecedor'
        Title.Caption = 'Codigo Fornecedor'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CpfCnpj'
        Title.Caption = 'Cpf/Cnpj'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RgIe'
        Width = 150
        Visible = True
      end>
  end
  object rmdFornecedores: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 440
    Top = 160
    object rmdFornecedoresCodigoFornecedor: TIntegerField
      FieldName = 'CodigoFornecedor'
    end
    object rmdFornecedoresNome: TStringField
      FieldName = 'Nome'
      Size = 250
    end
    object rmdFornecedoresCpfCnpj: TStringField
      FieldName = 'CpfCnpj'
      Size = 14
    end
    object rmdFornecedoresRgIe: TStringField
      FieldName = 'RgIe'
    end
  end
  object dsFornecedores: TDataSource
    DataSet = rmdFornecedores
    Left = 408
    Top = 160
  end
end

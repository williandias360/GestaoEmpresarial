unit uConsultaUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormConsultaPadrao, Data.DB,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFConsultaUsuario = class(TFFormConsultaPadrao)
    rmdUsuarios: TFDMemTable;
    dsUsuarios: TDataSource;
    rmdUsuariosCodigoUsuario: TIntegerField;
    rmdUsuariosNome: TStringField;
    rmdUsuariosLogin: TStringField;
    rmdUsuariosDataCriacao: TDateTimeField;
    procedure FormCreate(Sender: TObject);
    procedure edtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgDadosConsultaDblClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoUsuario: Integer;
    procedure SelecionarUsuario;
    const
    filtroBuscarPor: array [0 .. 1] of String = ('Nome', 'C�digo');
    filtroOrdenarPor: array [0 .. 2] of String = ('Nome', 'C�digo',
      'DataCriacao');

  public
    { Public declarations }
    property CodigoUsuario: Integer read FCodigoUsuario write FCodigoUsuario;
  end;

var
  FConsultaUsuario: TFConsultaUsuario;

implementation

uses
  System.Generics.Collections, UsuarioBO;

{$R *.dfm}

procedure TFConsultaUsuario.btnConfirmarClick(Sender: TObject);
begin
  inherited;
  SelecionarUsuario;
end;

procedure TFConsultaUsuario.dbgDadosConsultaDblClick(Sender: TObject);
begin
  inherited;
  SelecionarUsuario;
end;

procedure TFConsultaUsuario.edtBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
  listaParametros:TDictionary<String, Variant>;
  UsuarioBO:TUsuarioBO;

begin
  inherited;
  if Key = VK_SPACE then
  begin
    listaParametros:= TDictionary<String,Variant>.Create();
    case cbFiltrarPor.ItemIndex of
      0:
        listaParametros.Add('Nome', edtBusca.Text);
      1:
        listaParametros.Add('CodigoUsuario', StrToInt(Trim(edtBusca.Text)));
    end;

    UsuarioBO:= TUsuarioBO.Create;
    if Not rmdUsuarios.Active
    then rmdUsuarios.Open;

    rmdUsuarios.EmptyDataSet;
    rmdUsuarios.CopyDataSet(UsuarioBO.listarUsuarioPor(listaParametros), [coRestart, coAppend]);
    atribuirTotalRegistrosStatusBar(rmdUsuarios.RecordCount);

  end;

end;

procedure TFConsultaUsuario.FormCreate(Sender: TObject);
begin
  inherited;
  CarregarFiltrosBusca(filtroBuscarPor);
  CarregarFiltrosOrdenacao(filtroOrdenarPor);
end;

procedure TFConsultaUsuario.SelecionarUsuario;
begin
  if rmdUsuarios.Active And Not rmdUsuarios.IsEmpty then
  begin
    CodigoUsuario:= rmdUsuarios.FieldByName('CodigoUsuario').AsInteger;
    Close;
  end;

end;

end.

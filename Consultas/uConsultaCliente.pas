unit uConsultaCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormConsultaPadrao, Data.DB,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFConsultaCliente = class(TFFormConsultaPadrao)
    dsClientes: TDataSource;
    rmdClientes: TFDMemTable;
    rmdClientesNome: TStringField;
    rmdClientesRazaoSocial: TStringField;
    rmdClientesLogradouro: TStringField;
    rmdClientesNumero: TStringField;
    rmdClientesBairro: TStringField;
    rmdClientesCidade: TStringField;
    rmdClientesEstado: TStringField;
    rmdClientesTelefone: TStringField;
    rmdClientesCpfCnpj: TStringField;
    rmdClientesRgIe: TStringField;
    rmdClientesBloqueado: TBooleanField;
    rmdClientesEnviarEmailFinalizarVenda: TBooleanField;
    rmdClientesCodigoCliente: TLargeintField;
    rmdClientesDataCriacao: TSQLTimeStampField;
    rmdClientesDataModificacao: TSQLTimeStampField;
    rmdClientesLimite: TCurrencyField;
    procedure FormCreate(Sender: TObject);
    procedure edtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgDadosConsultaDblClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
  private
  { Private declarations }
    const
    filtroBuscarPor: array [0 .. 3] of String = ('Nome', 'C�digo',
      'Cpf', 'Cnpj');
    filtroOrdenarPor: array [0 .. 2] of String = ('Nome', 'C�digo',
      'DataCriacao');
    procedure SelecionarCliente;
  private
    FCodigoCliente: Int64;
  published
      property CodigoPessoa: Int64 read FCodigoCliente;
  public
    { Public declarations }
  end;

var
  FConsultaCliente: TFConsultaCliente;

implementation

uses
  ClienteBO, Cliente;

{$R *.dfm}

procedure TFConsultaCliente.FormCreate(Sender: TObject);
begin
  inherited;
  CarregarFiltrosBusca(filtroBuscarPor);
  CarregarFiltrosOrdenacao(filtroOrdenarPor);
end;

procedure TFConsultaCliente.btnConfirmarClick(Sender: TObject);
begin
  inherited;
  SelecionarCliente;
end;

procedure TFConsultaCliente.SelecionarCliente;
begin
  if rmdClientes.Active and not rmdClientes.IsEmpty then
  begin
    FCodigoCliente := rmdClientes.FieldByName('CodigoCliente').AsInteger;
    Close;
  end;
end;

procedure TFConsultaCliente.dbgDadosConsultaDblClick(Sender: TObject);
begin
  inherited;
  SelecionarCliente;
end;

procedure TFConsultaCliente.edtBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ClienteBO: TClienteBO;
  Cliente: TCliente;
begin
  inherited;
  if Key = VK_SPACE then
  begin
    Cliente := TCliente.Create();
    case cbFiltrarPor.ItemIndex of
      0:
        Cliente.Pessoa.Nome := edtBusca.Text;
      1:
        Cliente.CodigoCliente := StrToInt(Trim(edtBusca.Text));
      2:
        Cliente.Pessoa.PessoaFisica.Cpf := edtBusca.Text;
    else
      Cliente.Pessoa.PessoaJuridica.Cnpj := edtBusca.Text;
    end;
    ClienteBO := TClienteBO.Create;
    if Not rmdClientes.Active then
      rmdClientes.Open;
    rmdClientes.EmptyDataSet;

    rmdClientes.CopyDataSet(ClienteBO.listarClientePor(Cliente),
      [coRestart, coAppend]);
    StatusBar1.Panels[1].Text := 'Registros encontrados: ' +
      IntToStr(rmdClientes.RecordCount);
  end;

end;

end.

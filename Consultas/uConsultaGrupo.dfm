inherited FConsultaGrupo: TFConsultaGrupo
  Caption = 'Consulta de grupos do produto'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited GroupBox2: TGroupBox
      inherited edtBusca: TEdit
        OnKeyDown = edtBuscaKeyDown
      end
      inherited btnConfirmar: TBitBtn
        OnClick = btnConfirmarClick
      end
    end
  end
  inherited dbgDadosConsulta: TJvDBGrid
    DataSource = dsProdutoGrupo
    OnDblClick = dbgDadosConsultaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoProdutoGrupo'
        Title.Caption = 'C'#243'digo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 350
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataCriacao'
        Title.Caption = 'Data Cria'#231#227'o'
        Width = 135
        Visible = True
      end>
  end
  object rmdProdutoGrupo: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 336
    Top = 192
    object rmdProdutoGrupoCodigoProdutoGrupo: TIntegerField
      FieldName = 'CodigoProdutoGrupo'
    end
    object rmdProdutoGrupoNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
    object rmdProdutoGrupoDataCriacao: TDateTimeField
      FieldName = 'DataCriacao'
    end
  end
  object dsProdutoGrupo: TDataSource
    DataSet = rmdProdutoGrupo
    Left = 304
    Top = 192
  end
end

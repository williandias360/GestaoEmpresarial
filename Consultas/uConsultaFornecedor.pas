unit uConsultaFornecedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormConsultaPadrao, Data.DB,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFConsultaFornecedor = class(TFFormConsultaPadrao)
    rmdFornecedores: TFDMemTable;
    dsFornecedores: TDataSource;
    rmdFornecedoresCodigoFornecedor: TIntegerField;
    rmdFornecedoresNome: TStringField;
    rmdFornecedoresCpfCnpj: TStringField;
    rmdFornecedoresRgIe: TStringField;
    procedure edtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure dbgDadosConsultaDblClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
  private
    FCodigoFornecedor: Integer;
    procedure SelecionarFornecedor;

  const
    filtroBuscarPor: array [0 .. 3] of String = ('Nome', 'C�digo',
      'Cpf', 'Cnpj');
    filtroOrdenarPor: array [0 .. 2] of String = ('Nome', 'C�digo',
      'DataCriacao');
    { Private declarations }
  public
    { Public declarations }
    property CodigoFornecedor: Integer read FCodigoFornecedor write FCodigoFornecedor;
  end;

var
  FConsultaFornecedor: TFConsultaFornecedor;

implementation

uses
  System.Generics.Collections, FornecedorBO;

{$R *.dfm}

procedure TFConsultaFornecedor.btnConfirmarClick(Sender: TObject);
begin
  inherited;
  SelecionarFornecedor;
end;

procedure TFConsultaFornecedor.dbgDadosConsultaDblClick(Sender: TObject);
begin
  inherited;
  SelecionarFornecedor;
end;

procedure TFConsultaFornecedor.edtBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  listaParametros: TDictionary<String, Variant>;
  FornecedorBO: TFornecedorBO;
begin
  inherited;
  if Key = VK_SPACE then
  begin
    listaParametros := TDictionary<String, Variant>.Create();
    case cbFiltrarPor.ItemIndex of
      0:
        listaParametros.Add('Nome', edtBusca.Text);
      1:
        listaParametros.Add('CodigoFornecedor', StrToInt(Trim(edtBusca.Text)));
      2:
        listaParametros.Add('Cpf', edtBusca.Text);
    else
      listaParametros.Add('Cnpj', edtBusca.Text);
    end;

    FornecedorBO := TFornecedorBO.Create;
    if Not rmdFornecedores.Active then
      rmdFornecedores.Open;
    rmdFornecedores.EmptyDataSet;

    rmdFornecedores.CopyDataSet(FornecedorBO.listarFornecedorPor
      (listaParametros), [coRestart, coAppend]);
    atribuirTotalRegistrosStatusBar(rmdFornecedores.RecordCount);
  end;

end;

procedure TFConsultaFornecedor.FormCreate(Sender: TObject);
begin
  inherited;
  CarregarFiltrosBusca(filtroBuscarPor);
  CarregarFiltrosOrdenacao(filtroOrdenarPor);
end;

procedure TFConsultaFornecedor.SelecionarFornecedor;
begin
  if rmdFornecedores.Active and Not rmdFornecedores.IsEmpty then
  begin
    FCodigoFornecedor:= rmdFornecedores.FieldByName('CodigoFornecedor').AsInteger;
    Close;
  end;

end;

end.

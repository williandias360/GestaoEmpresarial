unit uConsultaUnidadeMedida;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormConsultaPadrao, Data.DB,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFConsultaUnidadeMedida = class(TFFormConsultaPadrao)
    dsUnidadeMedida: TDataSource;
    rmdUnidadeMedida: TFDMemTable;
    rmdUnidadeMedidaCodigoUnidadeMedida: TIntegerField;
    rmdUnidadeMedidaNome: TStringField;
    rmdUnidadeMedidaSigla: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure edtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgDadosConsultaDblClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoUnidadeMedida: Integer;
    procedure SelecionarUnidadeMedida;

  const
    filtroBuscarPor: array [0 .. 1] of String = ('Nome', 'C�digo');
    filtroOrdenarPor: array [0 .. 2] of String = ('Nome', 'C�digo',
      'DataCriacao');

  public
    { Public declarations }
    property CodigoUnidadeMedida: Integer read FCodigoUnidadeMedida
      write FCodigoUnidadeMedida;
  end;

var
  FConsultaUnidadeMedida: TFConsultaUnidadeMedida;

implementation

uses
  System.Generics.Collections, UnidadeMedidaBO;

{$R *.dfm}
{ TFFormConsultaPadrao1 }

procedure TFConsultaUnidadeMedida.btnConfirmarClick(Sender: TObject);
begin
  inherited;
  SelecionarUnidadeMedida;
end;

procedure TFConsultaUnidadeMedida.dbgDadosConsultaDblClick(Sender: TObject);
begin
  inherited;
  SelecionarUnidadeMedida;
end;

procedure TFConsultaUnidadeMedida.edtBuscaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  listaParametros: TDictionary<String, Variant>;
  UnidadeMedidaBO: TUnidadeMedidaBO;
begin
  inherited;
  if Key = VK_SPACE then
  begin
    listaParametros := TDictionary<String, Variant>.Create();
    case cbFiltrarPor.ItemIndex of
      0:
        listaParametros.Add('Nome', edtBusca.Text);
      1:
        listaParametros.Add('CodigoUnidadeMedida',
          StrToInt(Trim(edtBusca.Text)));
    end;

    UnidadeMedidaBO:= TUnidadeMedidaBO.Create;
    if Not rmdUnidadeMedida.Active
    then rmdUnidadeMedida.Open;

    rmdUnidadeMedida.EmptyDataSet;
    rmdUnidadeMedida.CopyDataSet(UnidadeMedidaBO.ListarUnidadeMedidaPor(listaParametros));
    atribuirTotalRegistrosStatusBar(rmdUnidadeMedida.RecordCount);
  end;

end;

procedure TFConsultaUnidadeMedida.FormCreate(Sender: TObject);
begin
  inherited;
  CarregarFiltrosBusca(filtroBuscarPor);
  CarregarFiltrosOrdenacao(filtroOrdenarPor);
end;

procedure TFConsultaUnidadeMedida.SelecionarUnidadeMedida;
begin
   if rmdUnidadeMedida.Active And Not rmdUnidadeMedida.IsEmpty then
   begin
      CodigoUnidadeMedida:= rmdUnidadeMedida.FieldByName('CodigoUnidadeMedida').AsInteger;
      Close;
   end;

end;

end.

inherited FConsultaProduto: TFConsultaProduto
  Caption = 'Consulta de Produtos'
  ClientWidth = 853
  OnCreate = FormCreate
  ExplicitWidth = 859
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 853
    ExplicitWidth = 853
    inherited GroupBox2: TGroupBox
      Width = 678
      ExplicitWidth = 678
      inherited edtBusca: TEdit
        Width = 672
        OnKeyDown = edtBuscaKeyDown
        ExplicitWidth = 672
      end
      inherited btnConfirmar: TBitBtn
        Left = 545
        ExplicitLeft = 545
      end
    end
  end
  inherited dbgDadosConsulta: TJvDBGrid
    Width = 853
    DataSource = dsProdutos
    OnDblClick = dbgDadosConsultaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoProduto'
        Title.Caption = 'C'#243'digo Produto'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Sigla'
        Title.Caption = 'Medida'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EstoqueAtual'
        Title.Caption = 'Estoque Atual'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PrecoVista'
        Title.Caption = #192' Vista'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QuantidadePorCaixa'
        Title.Caption = 'Qntd Por Caixa'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ProdutoGrupo'
        Title.Caption = 'Grupo'
        Visible = True
      end>
  end
  inherited StatusBar1: TStatusBar
    Width = 853
    ExplicitWidth = 853
  end
  object rmdProdutos: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 336
    Top = 192
    object rmdProdutosCodigoProduto: TIntegerField
      Alignment = taCenter
      FieldName = 'CodigoProduto'
    end
    object rmdProdutosNome: TStringField
      FieldName = 'Nome'
      Size = 200
    end
    object rmdProdutosQuantidadePorCaixa: TCurrencyField
      FieldName = 'QuantidadePorCaixa'
      currency = False
    end
    object rmdProdutosEstoqueAtual: TCurrencyField
      FieldName = 'EstoqueAtual'
      currency = False
    end
    object rmdProdutosProdutoGrupo: TStringField
      FieldName = 'ProdutoGrupo'
      Size = 60
    end
    object rmdProdutosPrecoVista: TCurrencyField
      FieldName = 'PrecoVista'
    end
    object rmdProdutosSigla: TStringField
      FieldName = 'Sigla'
      Size = 3
    end
  end
  object dsProdutos: TDataSource
    DataSet = rmdProdutos
    Left = 384
    Top = 192
  end
end

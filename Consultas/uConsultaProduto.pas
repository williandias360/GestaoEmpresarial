unit uConsultaProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFormConsultaPadrao, Data.DB,
  Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFConsultaProduto = class(TFFormConsultaPadrao)
    rmdProdutos: TFDMemTable;
    dsProdutos: TDataSource;
    rmdProdutosCodigoProduto: TIntegerField;
    rmdProdutosNome: TStringField;
    rmdProdutosQuantidadePorCaixa: TCurrencyField;
    rmdProdutosEstoqueAtual: TCurrencyField;
    rmdProdutosProdutoGrupo: TStringField;
    rmdProdutosPrecoVista: TCurrencyField;
    rmdProdutosSigla: TStringField;
    procedure edtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure dbgDadosConsultaDblClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoProduto: Integer;
    const
      filtroBuscarPor:array[0..1] of String = ('Nome', 'C�digo');
    procedure CarregarProduto;
  public
    { Public declarations }
    property CodigoProduto: Integer read FCodigoProduto write FCodigoProduto;
  end;

var
  FConsultaProduto: TFConsultaProduto;

implementation

uses
  System.Generics.Collections, ProdutoBO;

{$R *.dfm}

procedure TFConsultaProduto.dbgDadosConsultaDblClick(Sender: TObject);
begin
  inherited;
  CarregarProduto;
end;

procedure TFConsultaProduto.edtBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  listaParametros: TDictionary<String, Variant>;
  ProdutoBO:TProdutoBO;
begin
   if Key = VK_SPACE then
   begin
      listaParametros:= TDictionary<String,Variant>.Create;
      case cbFiltrarPor.ItemIndex of
        0: listaParametros.Add('Nome', edtBusca.Text);
        1:listaParametros.Add('CodigoProduto', edtBusca.Text);
      end;

      ProdutoBO:= TProdutoBO.Create;
      if Not rmdProdutos.Active
      then rmdProdutos.Open;

      rmdProdutos.EmptyDataSet;
      rmdProdutos.CopyDataSet(ProdutoBO.ListarProdutoPor(listaParametros),[coRestart, coAppend]);
      atribuirTotalRegistrosStatusBar(rmdProdutos.RecordCount);

   end;
end;

procedure TFConsultaProduto.FormCreate(Sender: TObject);
begin
  inherited;
  CarregarFiltrosBusca(filtroBuscarPor);
end;

procedure TFConsultaProduto.CarregarProduto;
begin
  if rmdProdutos.Active And Not rmdProdutos.IsEmpty then
  begin
     FCodigoProduto:= rmdProdutos.FieldByName('CodigoProduto').AsInteger;
     Close;
  end;
end;

end.

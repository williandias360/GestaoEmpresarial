inherited FConsultaEmpresa: TFConsultaEmpresa
  Caption = 'Consulta de empresa'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited GroupBox2: TGroupBox
      inherited edtBusca: TEdit
        OnKeyDown = edtBuscaKeyDown
      end
    end
  end
  inherited dbgDadosConsulta: TJvDBGrid
    DataSource = dsEmpresa
    OnDblClick = dbgDadosConsultaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoEmpresa'
        Title.Caption = 'Codigo Empresa'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RazaoSocial'
        Title.Caption = 'Raz'#227'o Social'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CpfCnpj'
        Title.Caption = 'Cpf/Cnpj'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RgIe'
        Title.Caption = 'Rg/Ie'
        Width = 80
        Visible = True
      end>
  end
  object rmdEmpresa: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 336
    Top = 192
    object rmdEmpresaCodigoEmpresa: TIntegerField
      FieldName = 'CodigoEmpresa'
    end
    object rmdEmpresaNome: TStringField
      FieldName = 'Nome'
      Size = 200
    end
    object rmdEmpresaRazaoSocial: TStringField
      FieldName = 'RazaoSocial'
      Size = 200
    end
    object rmdEmpresaCpfCnpj: TStringField
      FieldName = 'CpfCnpj'
      Size = 14
    end
    object rmdEmpresaRgIe: TStringField
      FieldName = 'RgIe'
    end
  end
  object dsEmpresa: TDataSource
    DataSet = rmdEmpresa
    Left = 376
    Top = 192
  end
end

inherited FConsultaUsuario: TFConsultaUsuario
  Caption = 'Consulta de Usu'#225'rios'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited GroupBox2: TGroupBox
      inherited edtBusca: TEdit
        OnKeyDown = edtBuscaKeyDown
      end
      inherited btnConfirmar: TBitBtn
        OnClick = btnConfirmarClick
      end
    end
  end
  inherited dbgDadosConsulta: TJvDBGrid
    DataSource = dsUsuarios
    OnDblClick = dbgDadosConsultaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CodigoUsuario'
        Title.Caption = 'Codigo Usuario'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 350
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Login'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataCriacao'
        Title.Caption = 'Data Cria'#231#227'o'
        Width = 130
        Visible = True
      end>
  end
  object rmdUsuarios: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 336
    Top = 192
    object rmdUsuariosCodigoUsuario: TIntegerField
      FieldName = 'CodigoUsuario'
    end
    object rmdUsuariosNome: TStringField
      FieldName = 'Nome'
      Size = 350
    end
    object rmdUsuariosLogin: TStringField
      FieldName = 'Login'
      Size = 10
    end
    object rmdUsuariosDataCriacao: TDateTimeField
      FieldName = 'DataCriacao'
    end
  end
  object dsUsuarios: TDataSource
    DataSet = rmdUsuarios
    Left = 408
    Top = 192
  end
end

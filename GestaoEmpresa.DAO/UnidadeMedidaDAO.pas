unit UnidadeMedidaDAO;

interface

uses
  BancoDados, UnidadeMedida, FireDAC.Comp.Client, System.Generics.Collections;

type
  TUnidadeMedidaDAO = class abstract(TBancoDados<TUnidadeMedida>)
  private
    procedure comandoInserir(entidade: TUnidadeMedida); override;
    procedure comandoAlterar(entidade: TUnidadeMedida); override;
    procedure comandoExcluir(entidade: TUnidadeMedida); override;
  protected
    function listarPor(listaParametros: TDictionary<String, Variant> = nil;
      listaOrdenacao: TDictionary<String, Variant> = nil): TFDMemTable;
  end;

implementation

uses
  Data.DB, System.Classes, System.SysUtils;

{ TUnidadeMedidaDAO }

procedure TUnidadeMedidaDAO.comandoAlterar(entidade: TUnidadeMedida);
var
  str: String;
begin
  str := 'Update Unidademedida ';
  str := str + 'Set Nome = :Nome, ';
  str := str + '    Sigla = :Sigla, ';
  str := str + '    Datamodificacao = Current_Timestamp ';
  str := str + 'Where CodigoUnidadeMedida = :CodigoUnidadeMedida; ';

  setSql(str);
  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftString, 'Sigla', entidade.Sigla);
  addParametro(ftInteger, 'CodigoUnidadeMedida', entidade.CodigoUnidadeMedida);

  executar;

end;

procedure TUnidadeMedidaDAO.comandoExcluir(entidade: TUnidadeMedida);
var
  str: String;
begin
  str := 'Delete From UnidadeMedida ';
  str := str + 'Where CodigoUnidadeMedida = :CodigoUnidadeMedida; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoUnidadeMedida', entidade.CodigoUnidadeMedida);

  executar;
end;

procedure TUnidadeMedidaDAO.comandoInserir(entidade: TUnidadeMedida);
var
  str: String;
  Data: TFDMemTable;
begin
  str := 'Insert Into Unidademedida (CodigoUnidadeMedida, Nome, Sigla, DataCriacao) ';
  str := str + 'Values ((Select Coalesce(Max(CodigoUnidadeMedida), 0) + 1 ';
  str := str +
    '         From Unidademedida), :Nome, :Sigla, Current_Timestamp) ';
  str := str + 'Returning CodigoUnidadeMedida; ';

  setSql(str);
  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftString, 'Sigla', entidade.Sigla);

  Data := listarDados;
  if Not Data.IsEmpty then
    entidade.CodigoUnidadeMedida := Data.FieldByName('CodigoUnidadeMedida')
      .AsInteger;

end;

function TUnidadeMedidaDAO.listarPor(listaParametros, listaOrdenacao
  : TDictionary<String, Variant>): TFDMemTable;
var
  str, condicoes: String;
  listaCondicao: TStringList;
begin
  condicoes := EmptyStr;
  listaCondicao := TStringList.Create;
  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoUnidadeMedida') then
    begin
      listaCondicao.Add
        ('UnidadeMedida.CodigoUnidadeMedida = :CodigoUnidadeMedida');
      addParametro(ftInteger, 'CodigoUnidadeMedida',
        listaParametros.Items['CodigoUnidadeMedida']);
    end;

    if listaParametros.ContainsKey('Nome') then
    begin
      listaCondicao.Add('UnidadeMedida.Nome CONTAINING :Nome');
      addParametro(ftString, 'Nome', listaParametros.Items['Nome']);
    end;

    if listaParametros.ContainsKey('CodigoEmpresa') then
    begin
      listaCondicao.Add('UnidadeMedidaEmpresa.CodigoEmpresa = :CodigoEmpresa');
      addParametro(ftInteger, 'CodigoEmpresa', listaParametros.Items['CodigoEmpresa']);
    end;
  end;

  if listaCondicao.Count > 0 then
  begin
    condicoes := ' WHERE ' + listaCondicao.Text.Join(' AND ',
      listaCondicao.ToStringArray);
  end;

  str := 'Select ';
  str := str + '    Unidademedida.Codigounidademedida, ';
  str := str + '    Unidademedida.Nome, ';
  str := str + '    Unidademedida.Sigla, ';
  str := str + '    Unidademedida.Datacriacao, ';
  str := str + '    Unidademedida.Datamodificacao ';
  str := str + 'From Unidademedida ';
  str := str + 'Join Unidademedidaempresa ';
  str := str +
    'On Unidademedidaempresa.Codigounidademedida = Unidademedida.Codigounidademedida ';

  str := str + condicoes;

  setSql(str);
  Result := listarDados;

end;

end.

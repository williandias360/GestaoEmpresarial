unit ProdutoGrupoDAO;

interface

uses
  BancoDados, ProdutoGrupo, FireDAC.Comp.Client, System.Generics.Collections;

type
  TProdutoGrupoDAO = class abstract(TBancoDados<TProdutoGrupo>)
  private
    procedure comandoInserir(entidade: TProdutoGrupo); override;
    procedure comandoAlterar(entidade: TProdutoGrupo); override;
    procedure comandoExcluir(entidade: TProdutoGrupo); override;
  protected
    function listarPor(listaParametros: TDictionary<String, Variant> = nil;
      listaOrdenacao: TDictionary<String, Variant> = nil): TFDMemTable;
  end;

implementation

uses
  Data.DB, System.Classes, System.SysUtils;

{ TProdutoGrupo }

procedure TProdutoGrupoDAO.comandoAlterar(entidade: TProdutoGrupo);
var
  str: String;
begin
  str := 'Update Produtogrupo ';
  str := str + 'Set Nome = :Nome, ';
  str := str + '    Disponivel = :Disponivel, ';
  str := str + '    Datamodificacao = Current_Timestamp ';
  str := str + 'Where Codigoprodutogrupo = :Codigoprodutogrupo; ';

  setSql(str);
  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);
  addParametro(ftInteger, 'CodigoProdutoGrupo', entidade.CodigoProdutoGrupo);

  executar;

end;

procedure TProdutoGrupoDAO.comandoExcluir(entidade: TProdutoGrupo);
var
  str: String;
begin
  str := 'Update Produtogrupo ';
  str := str + 'SET Disponivel = FALSE, ';
  str := str + 'DataModificacao = Current_timestamp ';
  str := str + 'Where Codigoprodutogrupo = :Codigoprodutogrupo; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoProdutoGrupo', entidade.CodigoProdutoGrupo);

  executar;

end;

procedure TProdutoGrupoDAO.comandoInserir(entidade: TProdutoGrupo);
var
  str: String;
  Data: TFDMemTable;
begin
  str := 'Insert Into ProdutoGrupo (Codigoprodutogrupo, Nome, Disponivel, Datacriacao) ';
  str := str + 'Values ((Select Coalesce(Max(Codigoprodutogrupo), 0) + 1 ';
  str := str +
    '         From ProdutoGrupo), :Nome, :Disponivel, Current_Timestamp) ';
  str := str + 'Returning Codigoprodutogrupo; ';

  setSql(str);
  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);

  Data := listarDados;
  if Not Data.IsEmpty then
    entidade.CodigoProdutoGrupo := Data.FieldByName('CodigoProdutoGrupo')
      .AsInteger;

end;

function TProdutoGrupoDAO.listarPor(listaParametros, listaOrdenacao
  : TDictionary<String, Variant>): TFDMemTable;
var
  str, condicoes: String;
  listaCondicoes: TStringList;
begin
  condicoes := EmptyStr;
  listaCondicoes := TStringList.Create;
  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoProdutoGrupo') then
    begin
      listaCondicoes.Add
        ('ProdutoGrupo.CodigoProdutoGrupo = :CodigoProdutoGrupo');
      addParametro(ftInteger, 'CodigoProdutoGrupo',
        listaParametros.Items['CodigoProdutoGrupo']);
    end;

    if listaParametros.ContainsKey('Nome') then
    begin
      listaCondicoes.Add('ProdutoGrupo.Nome CONTAINING :Nome');
      addParametro(ftString, 'Nome', listaParametros.Items['Nome']);
    end;

    if listaParametros.ContainsKey('Disponivel') then
    begin
      listaCondicoes.Add('ProdutoGrupo.Disponivel = :Disponivel');
      addParametro(ftBoolean, 'Disponivel',
        listaParametros.Items['Disponivel']);
    end;

    if listaParametros.ContainsKey('CodigoEmpresa') then
    begin
      listaCondicoes.Add('ProdutoGrupoEmpresa.CodigoEmpresa = :CodigoEmpresa');
      addParametro(ftInteger, 'CodigoEmpresa',
        listaParametros.Items['CodigoEmpresa']);
    end;
  end;

  if listaCondicoes.Count > 0 then
  begin
    condicoes := ' WHERE ' + listaCondicoes.Text.Join(' AND ',
      listaCondicoes.ToStringArray);
  end;
  str := 'Select ';
  str := str + '    Produtogrupo.Codigoprodutogrupo, ';
  str := str + '    Produtogrupo.Nome, ';
  str := str + '    Produtogrupo.Disponivel, ';
  str := str + '    Produtogrupo.Datacriacao, ';
  str := str + '    Produtogrupo.Datamodificacao ';
  str := str + 'From Produtogrupo ';
  str := str + 'Join Produtogrupoempresa ';
  str := str +
    '    On Produtogrupoempresa.Codigoprodutogrupo = Produtogrupo.Codigoprodutogrupo  ';

  str := str + condicoes;

  setSql(str);
  Result := listarDados;
end;

end.

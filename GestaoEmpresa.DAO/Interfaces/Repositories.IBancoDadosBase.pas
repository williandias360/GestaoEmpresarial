unit Repositories.IBancoDadosBase;

interface

uses
  FireDAC.Comp.Client, System.Generics.Collections;

type
  TIBancoDadosBase<TValue> = interface
    function listarPor(listaParametros: TDictionary<String, Variant> = nil): TFDMemTable;
  end;

implementation

end.

unit Repositories.IEnderecoDAO;

interface

uses
  Repositories.IBancoDadosBase, Endereco;
type
  TIEnderecoDAO = interface(TIBancoDadosBase<TEndereco>)
  procedure excluirEnderecoPorPessoa(codigoPessoa:Int64);
  end;

implementation

end.

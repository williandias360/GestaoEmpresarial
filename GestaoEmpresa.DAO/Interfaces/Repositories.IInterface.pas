unit Repositories.IInterface;

interface

type
  IInterface = interface
     ['{340D0F71-711D-4DDC-9A72-72704599DEE7}']
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;

    function _AddRef: Integer; stdcall;

    function _Release: Integer; stdcall;

  end;

implementation

end.

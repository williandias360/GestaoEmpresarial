unit Repositories.ICidadeDAO;

interface

uses
  Repositories.IBancoDadosBase, Cidade, FireDAC.Comp.Client;

type
  TICidadeDAO = interface(TIBancoDadosBase<TCidade>)
     function listarPorUf(codigoEstado:Integer):TFDMemTable;
  end;

implementation

end.

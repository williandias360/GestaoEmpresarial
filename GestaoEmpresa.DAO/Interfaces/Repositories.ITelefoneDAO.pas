unit Repositories.ITelefoneDAO;

interface

uses
  Repositories.IBancoDadosBase, Telefone;

type
  TITelefoneDAO = interface(TIBancoDadosBase<TTelefone>)
    procedure excluirTelefonePorPessoa(codigoPessoa: Int64);
  end;

implementation

end.

unit Repositories.IEmailDAO;

interface

uses
  Repositories.IBancoDadosBase, Email;

type
  TIEmailDAO = interface(TIBancoDadosBase<TEmail>)
    procedure exlcuirEmailPorPessoa(codigoPessoa: Int64);
  end;

implementation

end.

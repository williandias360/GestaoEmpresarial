unit Repositories.InterfaceObject;

interface

type
  TInterfacedObject = class(TObject, IInterface)
  protected

    FRefCount: Integer;

    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;

    function _AddRef: Integer; stdcall;

    function _Release: Integer; stdcall;

  public

    procedure AfterConstruction; override;

    procedure BeforeDestruction; override;

    class function NewInstance: TObject; override;

    property RefCount: Integer read FRefCount;

  end;

implementation

{ TInterfacedObject }

procedure TInterfacedObject.AfterConstruction;
begin
  inherited;

end;

procedure TInterfacedObject.BeforeDestruction;
begin
  inherited;

end;

class function TInterfacedObject.NewInstance: TObject;
begin

end;

function TInterfacedObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin

end;

function TInterfacedObject._AddRef: Integer;
begin

end;

function TInterfacedObject._Release: Integer;
begin

end;

end.

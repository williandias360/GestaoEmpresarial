unit Repositories.IBancoDados;

interface

uses
  System.Generics.Collections, System.SysUtils, Data.DB, FireDAC.Comp.Client;

type
  TIBancoDados = interface
    procedure criarConexao;
    procedure abrirConexao;
    procedure iniciarTransacao;
    procedure commitarTransacao;
    procedure rollbackTransacao;
    procedure fecharConexao;
    procedure setSql(sql: String);
    procedure addParametro(tipo: TFieldType; nome: String; valor: Variant);
    procedure executar;
  End;

implementation

end.

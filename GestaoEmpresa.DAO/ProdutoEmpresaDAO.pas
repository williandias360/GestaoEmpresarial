unit ProdutoEmpresaDAO;

interface

uses
  BancoDados, ProdutoEmpresa;

type
  TProdutoEmpresaDAO = class abstract(TBancoDados<TProdutoEmpresa>)
  protected
    procedure comandoInserir(entidade: TProdutoEmpresa); override;
    procedure comandoAlterar(entidade: TProdutoEmpresa); override;
    procedure comandoExcluir(entidade: TProdutoEmpresa); override;
  end;

implementation

uses
  Data.DB;

{ TProdutoEmpresaDAO }

procedure TProdutoEmpresaDAO.comandoAlterar(entidade: TProdutoEmpresa);
begin
  inherited;

end;

procedure TProdutoEmpresaDAO.comandoExcluir(entidade: TProdutoEmpresa);
begin
  inherited;

end;

procedure TProdutoEmpresaDAO.comandoInserir(entidade: TProdutoEmpresa);
var
  str: String;
begin
  str := 'Insert Into Produtoempresa (Codigoproduto, Codigoempresa) ';
  str := str + 'Values (:Codigoproduto, :Codigoempresa); ';

  setSql(str);
  addParametro(ftInteger, 'CodigoProduto', entidade.CodigoProduto);
  addParametro(ftInteger, 'CodigoEmpresa', entidade.CodigoEmpresa);

  executar;

end;

end.

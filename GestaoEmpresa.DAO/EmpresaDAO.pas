unit EmpresaDAO;

interface

uses BancoDados, Empresa, System.Generics.Collections, FireDAC.Comp.Client;

type
  TEmpresaDAO = class abstract(TBancoDados<TEmpresa>)
  private
    procedure comandoInserir(entidade: TEmpresa); override;
    procedure comandoAlterar(entidade: TEmpresa); override;
    procedure comandoExcluir(entidade: TEmpresa); override;
  protected
    function obterPorCodigo(codigo: Int64): TEmpresa;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;

  end;

implementation

uses
  Data.DB, Entities.Enums.ETipoRegimeTributario, EnumConverterStrings,
  System.Classes, System.SysUtils;

{ TEmpresaDAO }

procedure TEmpresaDAO.comandoAlterar(entidade: TEmpresa);
var
  str: String;
begin
  str := 'Update Empresa ';
  str := str + 'Set TipoRegimeTributario = :TipoRegimeTributario, ';
  str := str + '    Disponivel = :Disponivel ';
  str := str + 'Where CodigoEmpresa = :CodigoEmpresa; ';

  addParametro(ftInteger, 'CodigoEmpresa', entidade.Pessoa.CodigoPessoa);
  addParametro(ftString, 'TipoRegimeTributario',
    TConvert<TETipoRegimeTributario>.EnumConvertStr
    (entidade.TipoRegimeTributario));
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);

  setSql(str);
  executar;

end;

procedure TEmpresaDAO.comandoExcluir(entidade: TEmpresa);
var
  str: String;
begin
  str := 'Delete From Empresa ';
  str := str + 'Where Codigoempresa = :CodigoEmpresa; ';
  setSql(str);

  addParametro(ftInteger, 'CodigoEmpresa', entidade.CodigoEmpresa);
  executar;

end;

procedure TEmpresaDAO.comandoInserir(entidade: TEmpresa);
var
  str: String;
begin

  str := 'Insert Into Empresa (CodigoEmpresa, TipoRegimeTributario, Disponivel) ';
  str := str + 'Values (:CodigoEmpresa, :TipoRegimeTributario, :Disponivel); ';
  setSql(str);

  addParametro(ftInteger, 'CodigoEmpresa', entidade.Pessoa.CodigoPessoa);
  addParametro(ftString, 'TipoRegimeTributario',
    TConvert<TETipoRegimeTributario>.EnumConvertStr
    (entidade.TipoRegimeTributario));
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);
  executar;
end;

function TEmpresaDAO.listarPor(listaParametros: TDictionary<String, Variant>)
  : TFDMemTable;
var
  str, condicoes: String;
  listaCondicao: TStringList;
  parametro: TPair<String, Variant>;
begin
  condicoes := '';
  listaCondicao := TStringList.Create;
  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoEmpresa') then
    begin
      listaCondicao.Add('Empresa.CodigoEmpresa = :CodigoEmpresa');
      addParametro(ftInteger, 'CodigoEmpresa',
        listaParametros.Items['CodigoEmpresa']);
    end;

    if listaParametros.ContainsKey('Nome') then
    begin
      listaCondicao.Add('Pessoa.Nome CONTAINING :Nome');
      addParametro(ftString, 'Nome', listaParametros.Items['Nome']);
    end;

    if listaParametros.ContainsKey('Cpf') then
    begin
      listaCondicao.Add('PessoaFisica.Cpf CONTAINING :Cpf');
      addParametro(ftString, 'Cpf', listaParametros.Items['Cpf']);
    end;

    if listaParametros.ContainsKey('Cnpj') then
    begin
      listaCondicao.Add('PessoaJuridica.Cnpj CONTAINING :Cnpj');
      addParametro(ftString, 'Cnpj', listaParametros.Items['Cnpj']);
    end;

    if listaParametros.ContainsKey('Disponivel') then
    begin
      listaCondicao.Add('Empresa.Disponivel =:Disponivel');
      addParametro(ftBoolean, 'Disponivel',
        listaParametros.Items['Disponivel']);
    end;

  end;

  if listaCondicao.Count > 0 then
  begin
    condicoes := ' WHERE ' + listaCondicao.Text.Join(' AND ',
      listaCondicao.ToStringArray);
  end;

  str := 'select ';
  str := str + '    Empresa.CodigoEmpresa, ';
  str := str + '    Pessoa.Nome, ';
  str := str + '    Pessoa.Observacao, ';
  str := str + '    Pessoa.DataModificacao, ';
  str := str + '    Pessoa.DataCriacao, ';
  str := str + '    Empresa.Tiporegimetributario, ';
  str := str + '    Empresa.Disponivel, ';
  str := str +
    '    Coalesce(Pessoafisica.Cpf, Pessoajuridica.Cnpj) AS CpfCnpj, ';
  str := str +
    '    Coalesce(Pessoafisica.Rg, Pessoajuridica.Inscricaoestadual) As RgIe, ';
  str := str + '    Pessoajuridica.Razaosocial, ';
  str := str + '    Endereco.Codigoendereco, ';
  str := str + '    Endereco.Logradouro, ';
  str := str + '    Endereco.Numero, ';
  str := str + '    Endereco.Complemento, ';
  str := str + '    Endereco.Tipo, ';
  str := str + '    Bairro.Codigobairro, ';
  str := str + '    Bairro.Nome As Bairro, ';
  str := str + '    Cidade.Codigocidade, ';
  str := str + '    Cidade.Nome AS Cidade, ';
  str := str + '    Estado.Codigoestado, ';
  str := str + '    Estado.Sigla, ';
  str := str + '    Estado.Nome As Estado, ';
  str := str + '    Email.Codigoemail,  ';
  str := str + '    Email.Endereco As Email ';
  str := str + 'from Empresa ';
  str := str + 'Join Pessoa On Pessoa.CodigoPessoa = Empresa.CodigoEmpresa ';
  str := str +
    'LEFT Join PessoaFisica On PessoaFisica.CodigoPessoa = Pessoa.CodigoPessoa ';
  str := str +
    'Left Join PessoaJuridica On PessoaJuridica.CodigoPessoa = Pessoa.Codigopessoa ';
  str := str +
    'LEFT Join Endereco On Endereco.CodigoPessoa = Pessoa.CodigoPessoa AND Endereco.Principal Is True ';
  str := str +
    'LEFT Join Bairro On Bairro.CodigoBairro = Endereco.Codigobairro ';
  str := str +
    'LEFT Join Cidade On Cidade.Codigocidade = Endereco.Codigocidade ';
  str := str + 'LEFT Join Estado On Estado.CodigoEstado = Cidade.Codigoestado ';
  str := str +
    'LEFT Join Email On email.CodigoPessoa = Pessoa.Codigopessoa AND Email.Principal Is True ' ;
  str := str + condicoes;

  setSql(str);
  result:= listarDados;

end;

function TEmpresaDAO.obterPorCodigo(codigo: Int64): TEmpresa;
begin

end;

end.

unit CidadeDAO;

interface

uses
  BancoDados, Cidade, FireDAC.Comp.Client, Pair,
  System.Generics.Collections, System.Classes, ICampo, Tabelas;

type
  TCidadeDAO = class abstract(TBancoDados<TCidade>)
  private
    procedure comandoInserir(entidade: TCidade); override;
    procedure comandoAlterar(entidade: TCidade); override;
    procedure comandoExcluir(entidade: TCidade); override;
    function obterPorCodigo(codigo: Int64): TCidade;
  protected
    function listarPorUf(codigoEstado: Integer): TFDMemTable;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil): TFDMemTable;
  end;

implementation

uses
  Data.DB;

{ TCidadeDAO }

procedure TCidadeDAO.comandoAlterar(entidade: TCidade);
begin
  inherited;

end;

procedure TCidadeDAO.comandoExcluir(entidade: TCidade);
begin
  inherited;

end;

procedure TCidadeDAO.comandoInserir(entidade: TCidade);
begin
  inherited;

end;

function TCidadeDAO.listarPor(
  listaParametros: TDictionary<String, Variant> = nil): TFDMemTable;
begin

end;

function TCidadeDAO.listarPorUf(codigoEstado: Integer): TFDMemTable;
var
  str:String;
begin
  str:= 'Select ';
  str:= str + '    CodigoCidade, ';
  str:= str + '    CodigoEstado, ';
  str:= str + '    Nome, ';
  str:= str + '    CodigoIbge ';
  str:= str + '    From Cidade ';
  str:= str + '    Where CodigoEstado = :CodigoEstado; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoEstado', codigoEstado);

  result:= listarDados;
end;

function TCidadeDAO.obterPorCodigo(codigo: Int64): TCidade;
begin

end;

end.

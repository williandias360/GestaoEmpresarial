unit PessoaJuridicaDAO;

interface

uses
  BancoDados, PessoaJuridica,
  FireDAC.Comp.Client, System.Generics.Collections;

type
  TPessoaJuridicaDAO = class abstract(TBancoDados<TPessoaJuridica>)
  protected
    procedure comandoInserir(entidade: TPessoaJuridica); override;
    procedure comandoAlterar(entidade: TPessoaJuridica); override;
    procedure comandoExcluir(entidade: TPessoaJuridica); override;
    function obterPorCodigo(codigo: Int64): TPessoaJuridica;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;
  end;

implementation

uses
  Data.DB;

{ TPessoaJuridicaDAO }

procedure TPessoaJuridicaDAO.comandoAlterar(entidade: TPessoaJuridica);
begin
  inherited;

end;

procedure TPessoaJuridicaDAO.comandoExcluir(entidade: TPessoaJuridica);
var
  str: String;
begin
  inherited;
  str := 'Delete From PESSOAJURIDICA ';
  str := str + 'Where CODIGOPESSOA = :CODIGOPESSOA; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoPessoa', entidade.CodigoPessoa);

  executar;
end;

procedure TPessoaJuridicaDAO.comandoInserir(entidade: TPessoaJuridica);
var
  str: String;
begin
  inherited;
  str := 'INSERT INTO PESSOAJURIDICA (CODIGOPESSOA, RAZAOSOCIAL, CNPJ, INSCRICAOESTADUAL) ';
  str := str +
    'VALUES (:CODIGOPESSOA, :RAZAOSOCIAL, :CNPJ, :INSCRICAOESTADUAL); ';

  setSql(str);
  addParametro(ftInteger, 'CODIGOPESSOA', entidade.CodigoPessoa);
  addParametro(ftString, 'RAZAOSOCIAL', entidade.RazaoSocial);
  addParametro(ftString, 'CNPJ', entidade.Cnpj);
  addParametro(ftString, 'INSCRICAOESTADUAL', entidade.InscricaoEstadual);

  executar;
end;

function TPessoaJuridicaDAO.listarPor(listaParametros
  : TDictionary<String, Variant> = nil): TFDMemTable;
begin

end;

function TPessoaJuridicaDAO.obterPorCodigo(codigo: Int64): TPessoaJuridica;
begin

end;

end.

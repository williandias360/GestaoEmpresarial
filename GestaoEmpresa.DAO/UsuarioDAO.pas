unit UsuarioDAO;

interface

uses BancoDados, Usuario, System.Generics.Collections, FireDAC.Comp.Client;

type
  TUsuarioDAO = class abstract(TBancoDados<TUsuario>)
  protected
    procedure comandoInserir(entidade: TUsuario); override;
    procedure comandoAlterar(entidade: TUsuario); override;
    procedure comandoExcluir(entidade: TUsuario); override;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;

  end;

implementation

uses
  Data.DB, System.SysUtils, System.Classes;

{ TUsuarioDAO }

procedure TUsuarioDAO.comandoAlterar(entidade: TUsuario);
var
  str: String;
begin
  inherited;
  str := 'Update Usuario ';
  str := str + 'Set Login = :Login, ';
  str := str + '    Senha = :Senha, ';
  str := str + '    Disponivel = :Disponivel, ';
  str := str + '    DataModificacao = :DataModificacao ';
  str := str + 'Where CodigoUsuario = :CodigoUsuario; ';

  setSql(str);

  addParametro(ftString, 'Login', entidade.Login);
  addParametro(ftString, 'Senha', entidade.Senha);
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);
  addParametro(ftTimeStamp, 'DataModificacao', Now);
  addParametro(ftInteger, 'CodigoUsuario',entidade.Pessoa.CodigoPessoa);

  executar;

end;

procedure TUsuarioDAO.comandoExcluir(entidade: TUsuario);
var
  str: String;
begin
  inherited;

  str := 'Delete From Usuario ';
  str := str + 'Where CodigoUsuario = :CodigoUsuario; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoUsuario', entidade.CodigoUsuario);

  executar;

end;

procedure TUsuarioDAO.comandoInserir(entidade: TUsuario);
var
  str: String;
begin
  inherited;
  str := 'Insert Into Usuario (CodigoUsuario, Login, Senha, Disponivel) ';
  str := str + 'Values (:CodigoUsuario, :Login, :Senha, :Disponivel); ';

  setSql(str);

  addParametro(ftInteger, 'CodigoUsuario', entidade.Pessoa.CodigoPessoa);
  addParametro(ftString, 'Login', entidade.Login);
  addParametro(ftString, 'Senha', entidade.Senha);
  addParametro(ftBoolean, 'Disponivel',entidade.Disponivel);

  executar;
end;

function TUsuarioDAO.listarPor(listaParametros: TDictionary<String, Variant>)
  : TFDMemTable;
var
  str, condicoes: String;
  listaCondicao:TStringList;
begin
  condicoes:= '';
  listaCondicao:= TStringList.Create;
  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoUsuario') then
    begin
      listaCondicao.Add('Usuario.CodigoUsuario = :CodigoUsuario');
      addParametro(ftInteger, 'CodigoUsuario', listaParametros.Items['CodigoUsuario']);
    end;

    if listaParametros.ContainsKey('Nome') then
    begin
      listaCondicao.Add('Pessoa.Nome CONTAINING :Nome');
      addParametro(ftString, 'Nome', listaParametros.Items['Nome']);
    end;

    if listaParametros.ContainsKey('CodigoEmpresa') then
    begin
      listaCondicao.Add('PessoaEmpresa.CodigoEmpresa = :CodigoEmpresa');
      addParametro(ftInteger, 'CodigoEmpresa', listaParametros.Items['CodigoEmpresa']);
    end;

    if listaParametros.ContainsKey('Disponivel') then
    begin
      listaCondicao.Add('Usuario.Disponivel = :Disponivel');
      addParametro(ftBoolean, 'Disponivel', listaParametros.Items['Disponivel']);
    end;

    if listaParametros.ContainsKey('Login') then
    begin
      listaCondicao.Add('Usuario.Login = :Login');
      addParametro(ftString, 'Login', listaParametros.Items['Login']);
    end;

    if listaParametros.ContainsKey('Senha') then
    begin
      listaCondicao.Add('Usuario.Senha = :Senha');
      addParametro(ftString, 'Senha', listaParametros.Items['Senha']);
    end;

  end;

  if listaCondicao.Count > 0 then
  begin
    condicoes:= ' WHERE ' + listaCondicao.Text.Join(' AND ',
    listaCondicao.ToStringArray);
  end;

  str:= 'Select  ';
  str:= str + '    Usuario.CodigoUsuario, Pessoa.Nome, Usuario.Login, Usuario.Senha, Usuario.Disponivel, Pessoa.DataCriacao,  ';
  str:= str + '    Pessoa.DataModificacao, Pessoa.Observacao  ';
  str:= str + 'From Usuario  ';
  str:= str + 'Join Pessoa On Pessoa.CodigoPessoa = Usuario.CodigoUsuario  ';
  str:= str + 'Join PessoaEmpresa ON PessoaEmpresa.Codigopessoa = Pessoa.CodigoPessoa ';
  str:= str + 'Left Join Telefone On Telefone.CodigoPessoa = Pessoa.CodigoPessoa And  ';
  str:= str + '      Telefone.Principal Is True  ';
  str:= str + 'Left Join Email On Email.CodigoPessoa = Pessoa.CodigoPessoa And  ';
  str:= str + '      Email.Principal Is True  ';

  str:= str + condicoes;

  setSql(str);
  result:= listarDados;
end;


end.

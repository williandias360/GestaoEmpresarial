unit TelefoneDAO;

interface

uses
  BancoDados, Telefone, FireDAC.Comp.Client,
  System.Generics.Collections;

type
  TTelefoneDAO = class abstract(TBancoDados<TTelefone>)
  protected
    procedure comandoInserir(entidade: TTelefone); override;
    procedure comandoAlterar(entidade: TTelefone); override;
    procedure comandoExcluir(entidade: TTelefone); override;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;
    function obterPorCodigo(codigo: Int64): TTelefone;
    procedure excluirTelefonePorPessoa(codigoPessoa: Int64);
  end;

implementation

uses
  Data.DB, System.SysUtils, System.Classes, EnumConverterStrings,
  Entities.Enums.ETipoTelefone;

{ TTelefoneDAO }

procedure TTelefoneDAO.comandoAlterar(entidade: TTelefone);
var
  str: String;
begin
  inherited;
  str := 'UPDATE Telefone ';
  str := str + 'SET Numero = :Numero, ';
  str := str + '    Principal = :Principal, ';
  str := str + '    Tipo = :Tipo, ';
  str := str + '    Datamodificacao = :Datamodificacao ';
  str := str + 'WHERE Codigotelefone = :Codigotelefone; ';

  setSql(str);
  addParametro(ftString, 'Numero', entidade.Numero);
  addParametro(ftBoolean, 'Principal', entidade.Principal);
  addParametro(ftString, 'Tipo', TConvert<TETipoTelefone>.EnumConvertStr
    (entidade.Tipo));
  addParametro(ftTimeStamp, 'DataModificacao', Now());
  addParametro(ftInteger, 'CodigoTelefone', entidade.CodigoTelefone);

  executar;

end;

procedure TTelefoneDAO.comandoExcluir(entidade: TTelefone);
begin
  inherited;

end;

procedure TTelefoneDAO.comandoInserir(entidade: TTelefone);
var
  str: String;
  Data: TFDMemTable;
begin
  inherited;

  str := 'INSERT INTO TELEFONE (CODIGOTELEFONE, CODIGOPESSOA, NUMERO, TIPO, PRINCIPAL, DATACRIACAO) ';
  str := str + 'VALUES ((SELECT COALESCE(MAX(CODIGOTELEFONE), 0) + 1 ';
  str := str +
    '         FROM TELEFONE), :CODIGOPESSOA, :NUMERO, :TIPO, :PRINCIPAL, :DATACRIACAO) ';
  str := str + 'RETURNING CODIGOTELEFONE; ';

  setSql(str);
  addParametro(ftInteger, 'CODIGOPESSOA', entidade.codigoPessoa);
  addParametro(ftString, 'NUMERO', entidade.Numero);
  addParametro(ftString, 'TIPO', TConvert<TETipoTelefone>.EnumConvertStr
    (entidade.Tipo));
  addParametro(ftBoolean, 'PRINCIPAL', entidade.Principal);
  addParametro(ftTimeStamp, 'DATACRIACAO', Now);

  Data := listarDados;

  if Not Data.IsEmpty then
    entidade.CodigoTelefone := Data.FieldByName('CodigoTelefone').AsInteger;
end;

procedure TTelefoneDAO.excluirTelefonePorPessoa(codigoPessoa: Int64);
var
  str: String;
begin
  str := 'DELETE FROM Telefone ';
  str := str + 'WHERE Codigopessoa = :Codigopessoa; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoPessoa', codigoPessoa);

  executar;
end;

function TTelefoneDAO.listarPor(listaParametros
  : TDictionary<String, Variant> = nil): TFDMemTable;
var
  str, condicoes: String;
  listaCondicao: TStringList;
begin
  condicoes := '';
  listaCondicao := TStringList.Create;
  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoPessoa') then
    begin
      listaCondicao.Add('Telefone.CodigoPessoa = :CodigoPessoa');
      addParametro(ftInteger, 'CodigoPessoa',
        listaParametros.Items['CodigoPessoa']);
    end;

    if listaParametros.ContainsKey('CodigoTelefone') then
    begin
      listaCondicao.Add('Telefone.CodigoTelefone = :CodigoTelefone');
      addParametro(ftInteger, 'CodigoTelefone',
        listaParametros.Items['CodigoTelefone']);
    end;
  end;

  if listaCondicao.Count > 0 then
  begin
    condicoes := 'WHERE ' + listaCondicao.Text.Join(' AND ',
      listaCondicao.ToStringArray);
  end;

  str := 'Select ';
  str := str + '    Telefone.Codigotelefone, ';
  str := str + '    Telefone.Codigopessoa, ';
  str := str + '    Telefone.Numero, ';
  str := str + '    Telefone.Tipo, ';
  str := str + '    Telefone.Principal, ';
  str := str + '    Telefone.Datacriacao, ';
  str := str + '    Telefone.Datamodificacao ';
  str := str + 'From Telefone   ';
  str := str + condicoes;

  setSql(str);
  result := listarDados;
end;

function TTelefoneDAO.obterPorCodigo(codigo: Int64): TTelefone;
begin

end;

end.

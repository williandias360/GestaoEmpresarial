unit BancoDados;

interface

uses
  System.Generics.Collections, FireDAC.Stan.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Option, System.Variants,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Phys.FBDef, FireDAC.Phys, FireDAC.Phys.IBBase,
  FireDAC.Phys.FB, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Comp.Client, inifiles, Data.DB;

type
  TBancoDados<TValue> = class abstract(TInterfacedObject)
  private
    FTriggerAntesIncluir: Boolean;
    FTriggerDepoisInclui: Boolean;
    FTriggerAntesAlterar: Boolean;
    FTriggerDepoisAlterar: Boolean;
    FTriggerAntesExcluir: Boolean;
    FTriggerDepoisExcluir: Boolean;
    FConexao: TFDConnection;
    procedure criarConexao;
    procedure abrirConexao;
    procedure iniciarTransacao;
    procedure commitarTransacao;
    procedure rollbackTransacao;
    procedure fecharConexao;
    function emTransacao: Boolean;
    procedure inicializarTriggers();

  var
    parametros: TParams;
    comandoSQL: String;
    Query: TFDQuery;
    donoTransacao: Boolean;

  public
    constructor Create; overload;
    destructor Destroy; override;
    constructor Create(AConnection: TFDConnection); overload;

    procedure incluir(entidade: TValue); virtual;
    procedure alterar(entidade: TValue); virtual;
    procedure excluir(entidade: TValue); overload; virtual;
    procedure excluir(codigo: Integer); overload; virtual;

    property TriggerAntesIncluir: Boolean write FTriggerAntesIncluir;
    property TriggerDepoisInclui: Boolean write FTriggerDepoisInclui;
    property TriggerAntesAlterar: Boolean write FTriggerAntesAlterar;
    property TriggerDepoisAlterar: Boolean write FTriggerDepoisAlterar;
    property TriggerAntesExcluir: Boolean write FTriggerAntesExcluir;
    property TriggerDepoisExcluir: Boolean write FTriggerDepoisExcluir;
    property Conexao: TFDConnection read FConexao;

  protected
    procedure setSql(sql: String);
    procedure addParametro(tipo: TFieldType; nome: String; valor: Variant);
    procedure executar;
    function listarDados: TFDMemTable;

    procedure validar(entidade: TValue); virtual; abstract;
    procedure antesIncluir(entidade: TValue); virtual; abstract;
    procedure depoisIncluir(entidade: TValue); virtual; abstract;
    procedure antesAlterar(entidade: TValue); virtual; abstract;
    procedure depoisAlterar(entidade: TValue); virtual; abstract;
    procedure antesExcluir(entidade: TValue); virtual; abstract;
    procedure depoisExcluir(entidade: TValue); virtual; abstract;

    procedure comandoInserir(entidade: TValue); virtual; abstract;
    procedure comandoAlterar(entidade: TValue); virtual; abstract;
    procedure comandoExcluir(entidade: TValue); virtual; abstract;

  end;

implementation

uses
  System.SysUtils, System.Classes;

{ TBancoDados }
{$REGION 'Opera��es'}

procedure TBancoDados<TValue>.incluir(entidade: TValue);
var
  controlarTransacao: Boolean;
begin
  Try
    controlarTransacao := false;
    if Not emTransacao then
    begin
      iniciarTransacao;
      controlarTransacao := true;
      donoTransacao := false;
    end;

    if FTriggerAntesIncluir then
    begin
      validar(entidade);
      antesIncluir(entidade);
    end;

    comandoInserir(entidade);

    if FTriggerDepoisInclui then
    begin
      depoisIncluir(entidade);
    end;

    if controlarTransacao then
    begin
      commitarTransacao;
    end;
  Except
    On E: Exception do
    begin
      if controlarTransacao then
      begin
        rollbackTransacao;
      end;
      raise Exception.Create(E.Message);
    end;
  End;
end;

procedure TBancoDados<TValue>.alterar(entidade: TValue);
var
  controlarTransacao: Boolean;
begin
  Try
    controlarTransacao := false;
    if Not emTransacao then
    begin
      iniciarTransacao;
      controlarTransacao := true;
      donoTransacao := false;
    end;

    if FTriggerAntesAlterar then
    begin
      validar(entidade);
      antesAlterar(entidade);
    end;

    comandoAlterar(entidade);

    if FTriggerDepoisAlterar then
    begin
      depoisAlterar(entidade);
    end;

    if controlarTransacao then
    begin
      commitarTransacao;
    end;
  Except
    On E: Exception do
    begin
      if controlarTransacao then
      begin
        rollbackTransacao;
      end;
      raise Exception.Create(E.Message);
    end;

  End;
end;

procedure TBancoDados<TValue>.excluir(entidade: TValue);
var
  controlarTransacao: Boolean;
begin
  Try
    controlarTransacao := false;
    if Not emTransacao then
    begin
      iniciarTransacao;
      controlarTransacao := true;
      donoTransacao := false;
    end;

    if FTriggerAntesExcluir then
    begin
      antesExcluir(entidade);
    end;

    comandoExcluir(entidade);

    if FTriggerDepoisExcluir then
    begin
      depoisExcluir(entidade);
    end;

    if controlarTransacao then
    begin
      commitarTransacao;
    end;
  Except
    On E: Exception do
    begin
      if controlarTransacao then
      begin
        rollbackTransacao;
      end;
      raise Exception.Create(E.Message);
    end;
  End;
end;

procedure TBancoDados<TValue>.excluir(codigo: Integer);
begin

end;
{$ENDREGION}
{$REGION 'ControleTransacao'}

function TBancoDados<TValue>.emTransacao: Boolean;
begin
  Result := Conexao.InTransaction;
end;

procedure TBancoDados<TValue>.commitarTransacao;
begin
  if Conexao.InTransaction then
    Conexao.Commit;
end;

procedure TBancoDados<TValue>.iniciarTransacao;
begin
  if Not Conexao.InTransaction then
    Conexao.StartTransaction;
end;

procedure TBancoDados<TValue>.rollbackTransacao;
begin
  if Conexao.InTransaction then
    Conexao.Rollback;
end;

procedure TBancoDados<TValue>.inicializarTriggers;
begin
  FTriggerAntesIncluir := true;
  FTriggerDepoisInclui := true;
  FTriggerAntesAlterar := true;
  FTriggerDepoisAlterar := true;
  FTriggerAntesExcluir := true;
  FTriggerDepoisExcluir := true;
end;
{$ENDREGION}
{$REGION 'Inicializadores'}

constructor TBancoDados<TValue>.Create(AConnection: TFDConnection);
begin
  FConexao := AConnection;
  parametros := TParams.Create(nil);
  comandoSQL := EmptyStr;
  inicializarTriggers;
end;

constructor TBancoDados<TValue>.Create;
begin
  parametros := TParams.Create(nil);
  comandoSQL := EmptyStr;
  criarConexao;
  inicializarTriggers;
  donoTransacao := true;
end;

procedure TBancoDados<TValue>.criarConexao;
var
  ArquivoIni: TIniFile;
  CaminhoArquivo, CaminhoBanco: String;
begin
  CaminhoArquivo := ExpandFileName(GetCurrentDir + '\..\..\') + 'Banco.ini';
  if (FileExists(CaminhoArquivo)) then
  begin
    ArquivoIni := TIniFile.Create(CaminhoArquivo);
    CaminhoBanco := ArquivoIni.ReadString('Banco', 'CaminhoBanco', '');

    FConexao := TFDConnection.Create(nil);
    with Conexao do
    begin
      Connected := false;
      Params.Values['User_Name'] := 'SYSDBA';
      Params.Values['Password'] := 'masterkey';
      Params.Values['SQLDialect'] := '3';
      Params.Values['DriverID'] := 'FB';
      Params.Values['Database'] := CaminhoBanco;
      LoginPrompt := false;
      FormatOptions.StrsTrim2Len := true;
      ResourceOptions.AssignedValues := [rvAutoReconnect];
    end;
  end;

end;

destructor TBancoDados<TValue>.Destroy;
begin
  inherited;
  fecharConexao;
end;
{$ENDREGION}

procedure TBancoDados<TValue>.executar;
begin
  Try
    abrirConexao;
    with Self.Query do
    begin
      ExecSQL;
      Close;
    end;
  Finally
    fecharConexao;
  End;
end;

procedure TBancoDados<TValue>.fecharConexao;
begin
  parametros.Clear;
  comandoSQL := EmptyStr;
  FreeAndNil(Self.Query);
  if donoTransacao then
  begin
    Conexao.Connected := false;
  end;
end;

function TBancoDados<TValue>.listarDados: TFDMemTable;
var
  Data: TFDMemTable;
begin
  Try
    Data := TFDMemTable.Create(nil);
    abrirConexao;

    Query.Open();

    // if Not Query.IsEmpty then
    Data.CopyDataSet(Query, [coStructure, coRestart, coAppend]);

    Query.Close;

    Result := Data;

  Finally
    fecharConexao;
  End;

end;

procedure TBancoDados<TValue>.setSql(sql: String);
begin
  comandoSQL := sql.Replace('"', '''');
end;

procedure TBancoDados<TValue>.abrirConexao;
begin
  Conexao.Connected := true;
  Query := TFDQuery.Create(nil);
  with Query do
  begin
    Name := 'q' + FormatDateTime('DDMMYYHHmmsszzz', Now);
    Connection := Conexao;
  end;

  if comandoSQL <> EmptyStr then
    Query.sql.Add(comandoSQL);

  if parametros.Count > 0 then
    Query.Params.Assign(parametros);

end;

procedure TBancoDados<TValue>.addParametro(tipo: TFieldType; nome: String;
  valor: Variant);
var
  valorStream: TMemoryStream;
  valorString: String;

  strStream: TStringStream;
  s: String;
begin
  If tipo = ftBlob then
  begin

    Try
      s := valor;
      strStream := TStringStream.Create(s); // Cria o Stream a partir da String
      Try
        parametros.CreateParam(tipo, nome, ptInput).LoadFromStream(strStream,
          ftBlob); // Carrega o Blob a partir Stream criado
      Finally
        strStream.Free; // Libera o Stream da mem�ria
      end;
    Except
      parametros.CreateParam(tipo, nome, ptInput).Value := valor;
    End;
  end
  else
    parametros.CreateParam(tipo, nome, ptInput).Value := valor;
end;

end.

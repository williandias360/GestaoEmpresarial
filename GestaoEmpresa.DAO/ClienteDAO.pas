unit ClienteDAO;

interface

uses
  BancoDados, Cliente, FireDAC.Comp.Client,
  System.Generics.Collections;

type
  TClienteDAO = class abstract(TBancoDados<TCliente>)
  private
    procedure comandoInserir(entidade: TCliente); override;
    procedure comandoAlterar(entidade: TCliente); override;
    procedure comandoExcluir(entidade: TCliente); override;
  protected
    function obterPorCodigo(codigo: Int64): TCliente;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;
  end;

implementation

uses
  Data.DB, System.SysUtils, System.Classes;

{ TClienteRepositorio }

procedure TClienteDAO.comandoAlterar(entidade: TCliente);
var
  str: String;
begin
  str := 'UPDATE CLIENTE ';
  str := str + 'SET LIMITE = :LIMITE, ';
  str := str + '    BLOQUEADO = :BLOQUEADO, ';
  str := str + '    DISPONIVEL = :DISPONIVEL, ';
  str := str + '    ENVIAREMAILFINALIZARVENDA = :ENVIAREMAILFINALIZARVENDA ';
  str := str + 'WHERE CODIGOCLIENTE = :CODIGOCLIENTE; ';

  setSql(str);
  addParametro(ftCurrency, 'LIMITE', entidade.Limite);
  addParametro(ftBoolean, 'BLOQUEADO', entidade.Bloqueado);
  addParametro(ftBoolean, 'DISPONIVEL', entidade.Disponivel);
  addParametro(ftBoolean, 'ENVIAREMAILFINALIZARVENDA',
    entidade.EnviarEmailFinalizarVenda);
    addParametro(ftInteger, 'CODIGOCLIENTE', entidade.Pessoa.CodigoPessoa);

  executar;

end;

procedure TClienteDAO.comandoExcluir(entidade: TCliente);
var
  str:String;
begin
   str:= 'DELETE FROM CLIENTE ';
   str:= str + 'WHERE CODIGOCLIENTE = :CODIGOCLIENTE; ';

   setSql(str);
   addParametro(ftInteger, 'CODIGOCLIENTE', entidade.CodigoCliente);

   executar;

end;

procedure TClienteDAO.comandoInserir(entidade: TCliente);
var
  str: String;
begin
  str := 'INSERT INTO CLIENTE (CODIGOCLIENTE, LIMITE, BLOQUEADO, DISPONIVEL, ENVIAREMAILFINALIZARVENDA) ';
  str := str +
    'VALUES (:CODIGOCLIENTE, :LIMITE, :BLOQUEADO, :DISPONIVEL, :ENVIAREMAILFINALIZARVENDA); ';

  setSql(str);
  addParametro(ftInteger, 'CODIGOCLIENTE', entidade.Pessoa.CodigoPessoa);
  addParametro(ftCurrency, 'LIMITE', entidade.Limite);
  addParametro(ftBoolean, 'BLOQUEADO', entidade.Bloqueado);
  addParametro(ftBoolean, 'DISPONIVEL', entidade.Disponivel);
  addParametro(ftBoolean, 'ENVIAREMAILFINALIZARVENDA',
    entidade.EnviarEmailFinalizarVenda);

  executar;
end;

function TClienteDAO.listarPor(listaParametros
  : TDictionary<String, Variant> = nil): TFDMemTable;
var
  str, condicoes: String;
  listaCondicao: TStringList;
begin
  condicoes := '';
  listaCondicao := TStringList.Create;

  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoCliente') then
    begin
      listaCondicao.Add('Cliente.CodigoCliente = :CodigoCliente');
      addParametro(ftInteger, 'CodigoCliente',
        listaParametros.Items['CodigoCliente']);
    end;

    if listaParametros.ContainsKey('Nome') then
    begin
      listaCondicao.Add('Pessoa.Nome CONTAINING :Nome');
      addParametro(ftString, 'Nome', listaParametros.Items['Nome']);
    end;

    if listaParametros.ContainsKey('Cpf') then
    begin
      listaCondicao.Add('PessoaFisica.Cpf CONTAINING :Cpf');
      addParametro(ftString, 'Cpf', listaParametros.Items['Cpf']);
    end;

    if listaParametros.ContainsKey('Cnpj') then
    begin
      listaCondicao.Add('PessoaJuridica.Cnpj CONTAINING :Cnpj');
      addParametro(ftString, 'Cnpj', listaParametros.Items['Cnpj']);
    end;
  end;

  if listaCondicao.Count > 0 then
  begin
    condicoes := ' WHERE ' + listaCondicao.Text.Join(' AND ',
      listaCondicao.ToStringArray);
  end;

  str := 'Select ';
  str := str + '    PESSOA.CODIGOPESSOA As CODIGOCLIENTE, ';
  str := str + '    PESSOA.NOME, ';
  str := str + '    PESSOA.OBSERVACAO, ';
  str := str + '    PESSOA.DATACRIACAO, ';
  str := str + '    PESSOA.DATAMODIFICACAO, ';
  str := str + '    CLIENTE.LIMITE, ';
  str := str + '    CLIENTE.BLOQUEADO, ';
  str := str + '    CLIENTE.DISPONIVEL, ';
  str := str + '    CLIENTE.ENVIAREMAILFINALIZARVENDA, ';
  str := str +
    '    Iif(PESSOAFISICA.CPF Is Null, PESSOAJURIDICA.CNPJ, PESSOAFISICA.CPF) As CPFCNPJ, ';
  str := str +
    '    Iif(PESSOAFISICA.RG Is Null, PESSOAJURIDICA.INSCRICAOESTADUAL, PESSOAFISICA.RG) As RGIE, ';
  str := str + '    PESSOAJURIDICA.RAZAOSOCIAL, ';
  str := str + '    ENDERECO.CODIGOENDERECO, ';
  str := str + '    ENDERECO.LOGRADOURO, ';
  str := str + '    ENDERECO.NUMERO, ';
  str := str + '    BAIRRO.CODIGOBAIRRO, ';
  str := str + '    BAIRRO.NOME As BAIRRO, ';
  str := str + '    CIDADE.CODIGOCIDADE, ';
  str := str + '    CIDADE.NOME As CIDADE, ';
  str := str + '    ESTADO.SIGLA As ESTADO, ';
  str := str + '    TELEFONE.CODIGOTELEFONE, ';
  str := str + '    TELEFONE.TIPO, ';
  str := str + '    TELEFONE.NUMERO As TELEFONE, ';
  str := str + '    EMAIL.ENDERECO As EMAIL ';
  str := str + 'From PESSOA ';
  str := str +
    'Inner Join CLIENTE On CLIENTE.CODIGOCLIENTE = PESSOA.CODIGOPESSOA ';
  str := str +
    'Left Join PESSOAFISICA On PESSOAFISICA.CODIGOPESSOA = PESSOA.CODIGOPESSOA ';
  str := str +
    'Left Join PESSOAJURIDICA On PESSOAJURIDICA.CODIGOPESSOA = PESSOA.CODIGOPESSOA ';
  str := str +
    'Left Join ENDERECO On ENDERECO.CODIGOPESSOA = PESSOA.CODIGOPESSOA And ';
  str := str + '      ENDERECO.PRINCIPAL Is True ';
  str := str +
    'Left Join BAIRRO On BAIRRO.CODIGOBAIRRO = ENDERECO.CODIGOBAIRRO ';
  str := str +
    'Left Join CIDADE On CIDADE.CODIGOCIDADE = ENDERECO.CODIGOCIDADE ';
  str := str + 'Left Join ESTADO On ESTADO.CODIGOESTADO = CIDADE.CODIGOESTADO ';
  str := str +
    'Left Join TELEFONE On TELEFONE.CODIGOPESSOA = PESSOA.CODIGOPESSOA And ';
  str := str + '      TELEFONE.PRINCIPAL Is True ';
  str := str +
    'Left Join EMAIL On EMAIL.CODIGOPESSOA = PESSOA.CODIGOPESSOA And ';
  str := str + '      EMAIL.PRINCIPAL Is True    ';
  str := str + condicoes;
  setSql(str);

  result := listarDados;
end;

function TClienteDAO.obterPorCodigo(codigo: Int64): TCliente;
begin

end;

end.

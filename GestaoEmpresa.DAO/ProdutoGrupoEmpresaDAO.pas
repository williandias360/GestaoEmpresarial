unit ProdutoGrupoEmpresaDAO;

interface

uses
  BancoDados, ProdutoGrupoEmpresa;

type
  TProdutoGrupoEmpresaDAO = class abstract(TBancoDados<TProdutoGrupoEmpresa>)
  private
    procedure comandoInserir(entidade: TProdutoGrupoEmpresa); override;
    procedure comandoAlterar(entidade: TProdutoGrupoEmpresa); override;
    procedure comandoExcluir(entidade: TProdutoGrupoEmpresa); override;
  end;

implementation

uses
  Data.DB;

{ TProdutoGrupoEmpresaDAO }

procedure TProdutoGrupoEmpresaDAO.comandoAlterar
  (entidade: TProdutoGrupoEmpresa);
begin
  inherited;

end;

procedure TProdutoGrupoEmpresaDAO.comandoExcluir
  (entidade: TProdutoGrupoEmpresa);
var
  str: String;
begin
  str := 'Delete From Produtogrupoempresa ';
  str := str + 'Where Codigoprodutogrupo = :Codigoprodutogrupo; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoProdutoGrupo', entidade.CodigoProdutoGrupo);
  executar;

end;

procedure TProdutoGrupoEmpresaDAO.comandoInserir
  (entidade: TProdutoGrupoEmpresa);
var
  str: String;
begin
  str := 'Insert Into Produtogrupoempresa (Codigoprodutogrupo, Codigoempresa) ';
  str := str + 'Values (:Codigoprodutogrupo, :Codigoempresa); ';

  setSql(str);
  addParametro(ftInteger, 'CodigoProdutoGrupo', entidade.CodigoProdutoGrupo);
  addParametro(ftInteger, 'CodigoEmpresa', entidade.CodigoEmpresa);

  executar;

end;

end.

unit ProdutoDAO;

interface

uses
  BancoDados, Produto, System.Generics.Collections, FireDAC.Comp.Client;

type
  TProdutoDAO = class abstract(TBancoDados<TProduto>)
  protected
    procedure comandoInserir(entidade: TProduto); override;
    procedure comandoAlterar(entidade: TProduto); override;
    procedure comandoExcluir(entidade: TProduto); override;
    function ObterPorCodigo(codigo: Int64): TProduto;
    function ListarPor(listaParametros: TDictionary<String, Variant> = nil;
      listaOrdenarPor: TDictionary<String, String> = nil): TFDMemTable;
  end;

implementation

uses
  Data.DB, StrUtils, System.Variants, Math, System.Classes, System.SysUtils,
  ProdutoPrecoTipo;

{ TProdutoDAO }

procedure TProdutoDAO.comandoAlterar(entidade: TProduto);
var
  str: String;
begin
  str := 'Update Produto ';
  str := str + 'Set Codigoprodutogrupo = :Codigoprodutogrupo, ';
  str := str + '    Codigounidademedida = :Codigounidademedida, ';
  str := str + '    Nome = :Nome, ';
  str := str + '    Codigobarras = :Codigobarras, ';
  str := str + '    Quantidadeporcaixa = :Quantidadeporcaixa, ';
  str := str + '    EstoqueMinimo = :EstoqueMinimo, ';
  str := str + '    Ncm = :Ncm, ';
  str := str + '    Disponivel = :Disponivel, ';
  str := str + '    Datamodificacao = Current_Timestamp ';
  str := str + 'Where Codigoproduto = :Codigoproduto; ';

  setSql(str);
  if entidade.CodigoProdutoGrupo > 0 then
    addParametro(ftInteger, 'CodigoProdutoGrupo', entidade.CodigoProdutoGrupo)
  else
    addParametro(ftInteger, 'CodigoProdutoGrupo', null);

  if entidade.CodigoUnidadeMedida > 0 then
    addParametro(ftInteger, 'CodigoUnidadeMedida', entidade.CodigoUnidadeMedida)
  else
    addParametro(ftInteger, 'CodigoUnidadeMedida', null);

  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftString, 'CodigoBarras', entidade.CodigoBarras);
  addParametro(ftFloat, 'QuantidadePorCaixa', entidade.QuantidadePorCaixa);
  addParametro(ftString, 'Ncm', entidade.Ncm);
  addParametro(ftFloat, 'EstoqueMinimo', entidade.EstoqueMinimo);
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);
  addParametro(ftInteger, 'CodigoProduto', entidade.CodigoProduto);

  executar;

end;

procedure TProdutoDAO.comandoExcluir(entidade: TProduto);
var
  str: String;
begin
  str := 'Update Produto ';
  str := str + 'Set Disponivel = False, ';
  str := str + '    Datamodificacao = Current_Timestamp ';
  str := str + 'Where Codigoproduto = :Codigoproduto; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoProduto', entidade.CodigoProduto);

  executar;
end;

procedure TProdutoDAO.comandoInserir(entidade: TProduto);
var
  str: String;
  dados: TFDMemTable;
begin
  str := 'Insert Into Produto (Codigoproduto, Codigoprodutogrupo, Codigounidademedida, Nome, Codigobarras, Quantidadeporcaixa, ';
  str := str + '                    EstoqueAtual, EstoqueMinimo, Ncm, Disponivel, Datacriacao) ';
  str := str + 'Values ((Select ';
  str := str + '             Coalesce(Max(Codigoproduto), 0) + 1 ';
  str := str +
    '         From Produto), :Codigoprodutogrupo, :Codigounidademedida, :Nome, :Codigobarras, :Quantidadeporcaixa, ';
  str := str + '       :EstoqueAtual, :EstoqueMinimo, :Ncm, :Disponivel, Current_Timestamp) ';
  str := str + 'Returning Codigoproduto; ';

  setSql(str);
  if entidade.CodigoProdutoGrupo > 0 then
    addParametro(ftInteger, 'CodigoProdutoGrupo', entidade.CodigoProdutoGrupo)
  else
    addParametro(ftInteger, 'CodigoProdutoGrupo', null);

  if entidade.CodigoUnidadeMedida > 0 then
    addParametro(ftInteger, 'CodigoUnidadeMedida', entidade.CodigoUnidadeMedida)
  else
    addParametro(ftInteger, 'CodigoUnidadeMedida', null);

  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftString, 'CodigoBarras', entidade.CodigoBarras);
  addParametro(ftFloat, 'QuantidadePorCaixa', entidade.QuantidadePorCaixa);
  addParametro(ftFloat, 'EstoqueMinimo', entidade.EstoqueMinimo);
  addParametro(ftString, 'Ncm', entidade.Ncm);
  addParametro(ftFloat, 'EstoqueAtual', entidade.EstoqueAtual);
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);

  dados := listarDados;

  if Not dados.IsEmpty then
    entidade.CodigoProduto := dados.FieldByName('CodigoProduto').AsInteger;

end;

function TProdutoDAO.ListarPor(listaParametros: TDictionary<String, Variant>;
  listaOrdenarPor: TDictionary<String, String>): TFDMemTable;
var
  str, condicoes, ordenacoes: String;
  listaCondicao: TStringList;
  ordem: TPair<string, string>;
begin
  condicoes := EmptyStr;
  listaCondicao := TStringList.Create;

  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoProduto') then
    begin
      listaCondicao.Add('Produto.CodigoProduto = :CodigoProduto');
      addParametro(ftInteger, 'CodigoProduto',
        listaParametros.Items['CodigoProduto']);
    end;

    if listaParametros.ContainsKey('Nome') then
    begin
      listaCondicao.Add('Produto.Nome CONTAINING :Nome');
      addParametro(ftString, 'Nome', listaParametros.Items['Nome']);
    end;

    if listaParametros.ContainsKey('CodigoEmpresa') then
    begin
      listaCondicao.Add('ProdutoEmpresa.CodigoEmpresa = :CodigoEmpresa');
      addParametro(ftInteger, 'CodigoEmpresa',
        listaParametros.Items['CodigoEmpresa']);
    end;

    if listaParametros.ContainsKey('Disponivel') then
    begin
      listaCondicao.Add('Produto.Disponivel = :Disponivel');
      addParametro(ftBoolean, 'Disponivel',
        listaParametros.Items['Disponivel']);
    end;

  end;

  addParametro(ftInteger, 'CodigoProdutoPrecoTipo',
    TProdutoPrecoTipo.TIPO_PRECO_VISTA);

  if listaCondicao.Count > 0 then
  begin
    condicoes := ' WHERE ' + listaCondicao.Text.Join(' AND ',
      listaCondicao.ToStringArray);
  end;

  str := 'Select ';
  str := str + '    Produto.Codigoproduto, ';
  str := str + '    Produto.Nome, ';
  str := str + '    Produto.Codigoprodutogrupo, ';
  str := str + '    Produto.Codigounidademedida, ';
  str := str + '    Produto.Quantidadeporcaixa, ';
  str := str + '    Produto.Disponivel, ';
  str := str + '    Produto.Codigobarras, ';
  str := str + '    Produto.Ncm, ';
  str := str + '    Produto.EstoqueMinimo, ';
  str := str + '    Produto.Estoqueatual, ';
  str := str + '    Produtogrupo.Nome As Produtogrupo, ';
  str := str + '    Unidademedida.Nome As Unidademedida, ';
  str := str + '    Unidademedida.Sigla, ';
  str := str + '    Produtopreco.Valor As Precovista ';
  str := str + 'From Produto ';
  str := str +
    'Join Produtoempresa On Produtoempresa.Codigoproduto = Produto.Codigoproduto ';
  str := str +
    'Join Produtopreco On Produtopreco.Codigoproduto = Produto.Codigoproduto ';
  str := str +
    'And Produtopreco.Codigoprodutoprecotipo = :Codigoprodutoprecotipo ';
  str := str +
    'Left Join Unidademedida On Unidademedida.Codigounidademedida = Produto.Codigounidademedida ';
  str := str +
    'Left Join Produtogrupo On Produtogrupo.Codigoprodutogrupo = Produto.Codigoprodutogrupo    ';
  str := str + condicoes;

  if (Not Assigned(listaOrdenarPor)) Or (listaOrdenarPor.Count = 0) then
  begin
    listaOrdenarPor := TDictionary<String, String>.Create();
    listaOrdenarPor.Add('Produto.Nome', 'ASC');
  end;

  listaCondicao.Clear();
  for ordem in listaOrdenarPor.ToArray do
  begin
    listaCondicao.Add(ordem.Key + ' ' + ordem.Value);
  end;

  str := str + ' ORDER BY  ' + listaCondicao.Text.Join(',',
    listaCondicao.ToStringArray);

  setSql(str);

  result := listarDados;

end;

function TProdutoDAO.ObterPorCodigo(codigo: Int64): TProduto;
begin

end;

end.

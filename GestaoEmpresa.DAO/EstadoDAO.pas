unit EstadoDAO;

interface

uses
  BancoDados, Estado, FireDAC.Comp.Client,
  System.Generics.Collections;

type
  TEstadoDAO = class abstract(TBancoDados<TEstado>)
  protected
    procedure comandoInserir(entidade: TEstado); override;
    procedure comandoAlterar(entidade: TEstado); override;
    procedure comandoExcluir(entidade: TEstado); override;
    function obterPorCodigo(codigo: Int64): TEstado;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil): TFDMemTable;
  end;

implementation

{ TEstadoDAO }

procedure TEstadoDAO.comandoAlterar(entidade: TEstado);
begin
  inherited;

end;

procedure TEstadoDAO.comandoExcluir(entidade: TEstado);
begin
  inherited;

end;

procedure TEstadoDAO.comandoInserir(entidade: TEstado);
begin
  inherited;

end;

function TEstadoDAO.listarPor(listaParametros: TDictionary<String, Variant> = nil): TFDMemTable;
var
  str: String;
begin
  str := ' Select ';
  str := str + '    CodigoEstado, ';
  str := str + '    Nome, ';
  str := str + '    Sigla ';
  str := str + ' From Estado; ';

  setSql(str);

  result:= listarDados;
end;

function TEstadoDAO.obterPorCodigo(codigo: Int64): TEstado;
begin

end;

end.

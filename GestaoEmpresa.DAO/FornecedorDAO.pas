unit FornecedorDAO;

interface

uses BancoDados, Fornecedor, System.Generics.Collections, FireDAC.Comp.Client;

type
  TFornecedorDAO = class abstract(TBancoDados<TFornecedor>)
  protected
    procedure comandoInserir(entidade: TFornecedor); override;
    procedure comandoAlterar(entidade: TFornecedor); override;
    procedure comandoExcluir(entidade: TFornecedor); override;
    function obterPorCodigo(codigo: Int64): TFornecedor;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;
  end;

implementation

uses
  Data.DB, System.SysUtils, System.Classes;

{ TFornecedorDAO }

procedure TFornecedorDAO.comandoAlterar(entidade: TFornecedor);
var
  str: String;
begin
  str := 'Update Fornecedor ';
  str := str + 'Set DiaPagamento = :DiaPagamento ';
  str := str + 'Where CodigoFornecedor = :CodigoFornecedor ';

  setSql(str);
  addParametro(ftInteger, 'DiaPagamento', entidade.DiaPagamento);
  addParametro(ftInteger, 'CodigoFornecedor', entidade.CodigoFornecedor);
  executar;

end;

procedure TFornecedorDAO.comandoExcluir(entidade: TFornecedor);
var
  str: String;
begin

  str := 'Delete From Fornecedor ';
  str := str + 'Where CodigoFornecedor = :CodigoFornecedor ';
  setSql(str);

  addParametro(ftInteger, 'CodigoFornecedor', entidade.CodigoFornecedor);
  executar;

end;

procedure TFornecedorDAO.comandoInserir(entidade: TFornecedor);
var
  str: String;
begin

  str := 'Insert Into Fornecedor (Codigofornecedor, DiaPagamento, Disponivel) ';
  str := str + 'Values (:Codigofornecedor, :DiaPagamento, :Disponivel) ';

  setSql(str);
  addParametro(ftInteger, 'CodigoFornecedor', entidade.Pessoa.CodigoPessoa);
  addParametro(ftInteger, 'DiaPagamento', entidade.DiaPagamento);
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);

  executar;

end;

function TFornecedorDAO.listarPor(listaParametros: TDictionary<String, Variant>)
  : TFDMemTable;
var
  str, condicoes: String;
  listaCondicao: TStringList;
begin
  condicoes := '';
  listaCondicao := TStringList.Create;
  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoFornecedor') then
    begin
      listaCondicao.Add('Fornecedor.CodigoFornecedor = :CodigoFornecedor');
      addParametro(ftInteger, 'CodigoFornecedor',
        listaParametros.Items['CodigoFornecedor']);
    end;

    if listaParametros.ContainsKey('Nome') then
    begin
      listaCondicao.Add('Pessoa.Nome CONTAINING :Nome');
      addParametro(ftString, 'Nome', listaParametros.Items['Nome']);
    end;

    if listaParametros.ContainsKey('Cpf') then
    begin
      listaCondicao.Add('PessoaFisica.Cpf CONTAINING :Cpf');
      addParametro(ftString, 'Cpf', listaParametros.Items['Cpf']);
    end;

    if listaParametros.ContainsKey('Cnpj') then
    begin
      listaCondicao.Add('PessoaJuridica.Cnpj CONTAINING :Cnpj');
      addParametro(ftString, 'Cnpj', listaParametros.Items['Cnpj']);
    end;

    if listaParametros.ContainsKey('Disponivel') then
    begin
      listaCondicao.Add('Fornecedor.Disponivel =:Disponivel');
      addParametro(ftBoolean, 'Disponivel',
        listaParametros.Items['Disponivel']);
    end;

  end;

  if listaCondicao.Count > 0 then
  begin
    condicoes := ' WHERE ' + listaCondicao.Text.Join(' AND ',
      listaCondicao.ToStringArray);
  end;

  str := 'Select ';
  str := str + '    Fornecedor.Codigofornecedor, ';
  str := str + '    Pessoa.Nome, ';
  str := str + '    Pessoa.Observacao, ';
  str := str + '    Pessoa.Datacriacao, ';
  str := str + '    Pessoa.Datamodificacao, ';
  str := str + '    Fornecedor.Diapagamento, ';
  str := str +
    '    Iif(Pessoafisica.Cpf Is Null, Pessoajuridica.Cnpj, Pessoafisica.Cpf) As Cpfcnpj, ';
  str := str +
    '    Iif(Pessoafisica.Cpf Is Null, Pessoajuridica.Inscricaoestadual, Pessoafisica.Rg) As Rgie, ';
  str := str + '    Pessoajuridica.Razaosocial, ';
  str := str + '    Endereco.Codigoendereco, ';
  str := str + '    Endereco.Logradouro, ';
  str := str + '    Endereco.Numero, ';
  str := str + '    Bairro.Codigobairro, ';
  str := str + '    Bairro.Nome As Bairro, ';
  str := str + '    Cidade.Codigocidade, ';
  str := str + '    Cidade.Nome As Cidade, ';
  str := str + '    Estado.Sigla As Estado, ';
  str := str + '    Telefone.Codigotelefone, ';
  str := str + '    Telefone.Tipo, ';
  str := str + '    Telefone.Numero As Telefone, ';
  str := str + '    Email.Codigoemail, ';
  str := str + '    Email.Endereco As Email ';
  str := str + 'From Fornecedor ';
  str := str +
    'Join Pessoa On Pessoa.Codigopessoa = Fornecedor.Codigofornecedor ';
  str := str +
    'Left Join Pessoafisica On Pessoafisica.Codigopessoa = Pessoa.Codigopessoa ';
  str := str +
    'Left Join Pessoajuridica On Pessoajuridica.Codigopessoa = Pessoa.Codigopessoa ';
  str := str +
    'Left Join Endereco On Endereco.Codigopessoa = Pessoa.Codigopessoa And ';
  str := str + '      Endereco.Principal Is True ';
  str := str +
    'Left Join Bairro On Bairro.Codigobairro = Endereco.Codigobairro ';
  str := str +
    'Left Join Cidade On Cidade.Codigocidade = Endereco.Codigocidade ';
  str := str + 'Left Join Estado On Estado.Codigoestado = Cidade.Codigoestado ';
  str := str +
    'Left Join Telefone On Telefone.Codigopessoa = Pessoa.Codigopessoa ';
  str := str + 'AND Telefone.Principal IS TRUE ';
  str := str + 'Left Join Email On Email.Codigopessoa = Pessoa.Codigopessoa ';
  str := str + 'AND Email.Principal IS TRUE ';
  str := str + condicoes;

  setSql(str);
  result := listarDados;

end;

function TFornecedorDAO.obterPorCodigo(codigo: Int64): TFornecedor;
begin

end;

end.

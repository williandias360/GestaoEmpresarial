unit UnidadeMedidaEmpresaDAO;

interface

uses
  BancoDados, UnidadeMedidaEmpresa;

type
  TUnidadeMedidaEmpresaDAO = class abstract(TBancoDados<TUnidadeMedidaEmpresa>)
  private
    procedure comandoInserir(entidade: TUnidadeMedidaEmpresa); override;
    procedure comandoAlterar(entidade: TUnidadeMedidaEmpresa); override;
    procedure comandoExcluir(entidade: TUnidadeMedidaEmpresa); override;
  end;

implementation

uses
  Data.DB;

{ TUnidadeMedidaEmpresaDAO }

procedure TUnidadeMedidaEmpresaDAO.comandoAlterar
  (entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

procedure TUnidadeMedidaEmpresaDAO.comandoExcluir
  (entidade: TUnidadeMedidaEmpresa);
begin
  inherited;

end;

procedure TUnidadeMedidaEmpresaDAO.comandoInserir
  (entidade: TUnidadeMedidaEmpresa);
var
  str: String;
begin
  str := 'Insert Into Unidademedidaempresa (Codigounidademedida, Codigoempresa) ';
  str := str + 'Values (:Codigounidademedida, :Codigoempresa); ';

  setSql(str);
  addParametro(ftInteger, 'CodigoUnidadeMedida', entidade.CodigoUnidadeMedida);
  addParametro(ftInteger, 'CodigoEmpresa', entidade.CodigoEmpresa);

  executar;

end;

end.

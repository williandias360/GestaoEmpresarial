unit PessoaFisicaDAO;

interface

uses
  BancoDados, PessoaFisica, FireDAC.Comp.Client,
  System.Generics.Collections;

type
  TPessoaFisicaDAO = class abstract(TBancoDados<TPessoaFisica>)
  protected
    procedure comandoInserir(entidade: TPessoaFisica); override;
    procedure comandoAlterar(entidade: TPessoaFisica); override;
    procedure comandoExcluir(entidade: TPessoaFisica); override;
    function obterPorCodigo(codigo: Int64): TPessoaFisica;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;
  end;

implementation

uses
  Data.DB;

{ TPessoaFisicaDAO }

procedure TPessoaFisicaDAO.comandoAlterar(entidade: TPessoaFisica);
begin
  inherited;

end;

procedure TPessoaFisicaDAO.comandoExcluir(entidade: TPessoaFisica);
var
  str: String;
begin
  inherited;

  str := 'Delete From PESSOAFISICA ';
  str := str + 'Where CODIGOPESSOA = :CODIGOPESSOA ';

  setSql(str);
  addParametro(ftInteger, 'CodigoPessoa', entidade.CodigoPessoa);

  executar;

end;

procedure TPessoaFisicaDAO.comandoInserir(entidade: TPessoaFisica);
var
  str: String;
begin
  inherited;

  str := 'INSERT INTO PESSOAFISICA (CODIGOPESSOA, CPF, RG) ';
  str := str + 'VALUES (:CODIGOPESSOA, :CPF, :RG); ';

  setSql(str);
  addParametro(ftInteger, 'CODIGOPESSOA', entidade.CodigoPessoa);
  addParametro(ftString, 'CPF', entidade.Cpf);
  addParametro(ftString, 'RG', entidade.Rg);
  executar;

end;

function TPessoaFisicaDAO.listarPor(listaParametros
  : TDictionary<String, Variant> = nil): TFDMemTable;
begin

end;

function TPessoaFisicaDAO.obterPorCodigo(codigo: Int64): TPessoaFisica;
begin

end;

end.

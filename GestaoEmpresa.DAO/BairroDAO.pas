unit BairroDAO;

interface

uses
  BancoDados, Bairro, FireDAC.Comp.Client,
  System.Generics.Collections;

type
  TBairroDAO = class abstract(TBancoDados<TBairro>)
  protected
    procedure comandoInserir(entidade: TBairro); override;
    procedure comandoAlterar(entidade: TBairro); override;
    procedure comandoExcluir(entidade: TBairro); override;
    function obterPorCodigo(codigo: Int64): TBairro;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil): TFDMemTable;
  end;

implementation

uses
  Data.DB, System.SysUtils;

{ TBairroDAO }

procedure TBairroDAO.comandoAlterar(entidade: TBairro);
var
  str: String;
begin
  inherited;
  str := 'UPDATE Bairro ';
  str := str + 'SET Nome = :Nome, ';
  str := str + '    Datamodificacao = :Datamodificao ';
  str := str + 'WHERE Codigobairro = :Codigobairro; ';

  setSql(str);
  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftTimeStamp, 'DataModificacao', Now());
  addParametro(ftInteger, 'CodigoBairro', entidade.CodigoBairro);

  executar;

end;

procedure TBairroDAO.comandoExcluir(entidade: TBairro);
var
  str:String;
begin
  inherited;

  str:= 'Delete From BAIRRO ';
  str:= str + 'Where CODIGOBAIRRO = :CODIGOBAIRRO ';

  setSql(str);
  addParametro(ftInteger, 'CodigoBairro', entidade.CodigoBairro);

  executar;

end;

procedure TBairroDAO.comandoInserir(entidade: TBairro);
var
  str: String;
  dados: TFDMemTable;
begin
  inherited;
  str := 'INSERT INTO BAIRRO (CODIGOBAIRRO, NOME, DATACRIACAO) ';
  str := str + 'VALUES ((SELECT COALESCE(MAX(CODIGOBAIRRO), 0) + 1 ';
  str := str + '         FROM BAIRRO), :NOME, :DATACRIACAO) ';
  str := str + 'RETURNING CODIGOBAIRRO; ';

  setSql(str);
  addParametro(ftString, 'NOME', entidade.Nome);
  addParametro(ftTimeStamp, 'DATACRIACAO', Now);

  dados := listarDados;

  if Not dados.IsEmpty then
    entidade.CodigoBairro := dados.FieldByName('CodigoBairro').AsInteger;

end;


function TBairroDAO.listarPor(
  listaParametros: TDictionary<String, Variant> = nil): TFDMemTable;
begin

end;

function TBairroDAO.obterPorCodigo(codigo: Int64): TBairro;
begin

end;

end.

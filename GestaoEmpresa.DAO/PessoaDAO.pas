unit PessoaDAO;

interface

uses
  FireDAC.Comp.Client, BancoDados, Pessoa,
  System.Generics.Collections;

type
  TPessoaDAO = class abstract(TBancoDados<TPessoa>)
  protected
    procedure comandoInserir(entidade: TPessoa); override;
    procedure comandoAlterar(entidade: TPessoa); override;
    procedure comandoExcluir(entidade: TPessoa); override;
    function obterPorCodigo(codigo: Int64): TPessoa;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;
  end;

implementation

uses
  Data.DB, System.SysUtils;

{ TPessoaRepositorio }

procedure TPessoaDAO.comandoAlterar(entidade: TPessoa);
var
  str: String;
begin
  str := 'UPDATE PESSOA  ';
  str := str + 'SET NOME = :NOME,  ';
  str := str + '    PERFILCLIENTE = :PERFILCLIENTE,  ';
  str := str + '    PERFILEMPRESA = :PERFILEMPRESA,  ';
  str := str + '    PERFILUSUARIO = :PERFILUSUARIO,  ';
  str := str + '    PERFILFORNECEDOR = :PERFILFORNECEDOR,  ';
  str := str + '    OBSERVACAO = :OBSERVACAO, ';
  str := str + '    DATAMODIFICACAO = :DATAMODIFICACAO ';
  str := str + 'WHERE CODIGOPESSOA = :CODIGOPESSOA;  ';

  setSql(str);
  addParametro(ftString, 'NOME', entidade.Nome);
  addParametro(ftBoolean, 'PERFILCLIENTE', entidade.PerfilCliente);
  addParametro(ftBoolean, 'PERFILEMPRESA', entidade.PerfilEmpresa);
  addParametro(ftBoolean, 'PERFILUSUARIO', entidade.PerfilUsuario);
  addParametro(ftBoolean, 'PERFILFORNECEDOR', entidade.PerfilFornecedor);
  addParametro(ftDateTime, 'DATAMODIFICACAO', Now);
  addParametro(ftBlob, 'OBSERVACAO', entidade.Observacao);
  addParametro(ftInteger, 'CODIGOPESSOA', entidade.CodigoPessoa);

  executar;
end;

procedure TPessoaDAO.comandoExcluir(entidade: TPessoa);
var
  str:String;
begin
  str:= 'DELETE FROM PESSOA ';
  str:= str + 'WHERE CODIGOPESSOA = :CODIGOPESSOA; ';

  setSql(str);
  addParametro(ftInteger, 'CODIGOPESSOA', entidade.CodigoPessoa);

  executar;

end;

procedure TPessoaDAO.comandoInserir(entidade: TPessoa);
var
  str: String;
  Data: TFDMemTable;
begin
  str := 'INSERT INTO PESSOA (CODIGOPESSOA, NOME, PERFILCLIENTE, PERFILEMPRESA, PERFILUSUARIO, PERFILFORNECEDOR, OBSERVACAO, DATACRIACAO) ';
  str := str + 'VALUES ((SELECT COALESCE(MAX(CODIGOPESSOA), 0) + 1 ';
  str := str +
    '         FROM PESSOA), :NOME, :PERFILCLIENTE, :PERFILEMPRESA, :PERFILUSUARIO, :PERFILFORNECEDOR, :OBSERVACAO, :DATACRIACAO) ';
  str := str + 'RETURNING CODIGOPESSOA; ';

  setSql(str);
  addParametro(ftString, 'NOME', entidade.Nome);
  addParametro(ftBoolean, 'PERFILCLIENTE', entidade.PerfilCliente);
  addParametro(ftBoolean, 'PERFILEMPRESA', entidade.PerfilEmpresa);
  addParametro(ftBoolean, 'PERFILUSUARIO', entidade.PerfilUsuario);
  addParametro(ftBoolean, 'PERFILFORNECEDOR', entidade.PerfilFornecedor);
  addParametro(ftBlob, 'OBSERVACAO', entidade.Observacao);
  addParametro(ftTimeStamp, 'DATACRIACAO', Now);

  Data := listarDados;

  if Not Data.IsEmpty then
    entidade.CodigoPessoa := Data.FieldByName('CodigoPessoa').AsInteger;

end;

function TPessoaDAO.listarPor(listaParametros
  : TDictionary<String, Variant> = nil): TFDMemTable;
begin

end;

function TPessoaDAO.obterPorCodigo(codigo: Int64): TPessoa;
begin

end;

end.

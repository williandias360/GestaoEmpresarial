unit PessoaEmpresaDAO;

interface

uses BancoDados, PessoaEmpresa;

type
  TPessoaEmpresaDAO = class abstract(TBancoDados<TPessoaEmpresa>)
  protected
    procedure comandoInserir(entidade: TPessoaEmpresa); override;
    procedure comandoAlterar(entidade: TPessoaEmpresa); override;
    procedure comandoExcluir(entidade: TPessoaEmpresa); override;
  end;

implementation

uses
  Data.DB;

{ TPessoaEmpresaDAO }

procedure TPessoaEmpresaDAO.comandoAlterar(entidade: TPessoaEmpresa);
begin

end;

procedure TPessoaEmpresaDAO.comandoExcluir(entidade: TPessoaEmpresa);
var
  str:String;
begin
  str:= 'Delete From Pessoaempresa ';
  str:= str + 'Where Codigopessoa = :Codigopessoa And CodigoEmpresa = :CodigoEmpresa; ';

   setSql(str);
   addParametro(ftInteger, 'CodigoPessoa', entidade.CodigoPessoa);
   executar;

end;

procedure TPessoaEmpresaDAO.comandoInserir(entidade: TPessoaEmpresa);
var
  str: String;
begin
  inherited;
  str := 'Insert Into Pessoaempresa (Codigopessoa, Codigoempresa) ';
  str := str + 'Values (:Codigopessoa, :Codigoempresa); ';

  setSql(str);
  addParametro(ftInteger, 'CodigoPessoa', entidade.CodigoPessoa);
  addParametro(ftInteger, 'CodigoEmpresa', entidade.CodigoEmpresa);
  executar;
end;

end.

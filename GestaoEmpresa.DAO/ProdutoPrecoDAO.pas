unit ProdutoPrecoDAO;

interface

uses
  BancoDados, ProdutoPreco, System.Generics.Collections, FireDAC.Comp.Client;

type
  TProdutoPrecoDAO = class abstract(TBancoDados<TProdutoPreco>)
  private
    function ListaColunas: TDictionary<String, String>;
  protected
    procedure comandoInserir(entidade: TProdutoPreco); override;
    procedure comandoAlterar(entidade: TProdutoPreco); override;
    procedure comandoExcluir(entidade: TProdutoPreco); override;
    function ListarPor(listaParametros: TDictionary<String, Variant>;
      listaOrdenarPor: TDictionary<String, String> = nil): TFDMemTable;
  end;

implementation

uses
  Data.DB, System.Classes, System.SysUtils;

{ TProdutoPrecoDAO }

procedure TProdutoPrecoDAO.comandoAlterar(entidade: TProdutoPreco);
begin
  inherited;

end;

procedure TProdutoPrecoDAO.comandoExcluir(entidade: TProdutoPreco);
var
  str: String;
begin
  str := 'Delete From Produtopreco ';
  str := str + 'Where Codigoproduto = :Codigoproduto; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoProduto', entidade.CodigoProduto);

  executar;

end;

procedure TProdutoPrecoDAO.comandoInserir(entidade: TProdutoPreco);
var
  str: String;
begin
  str := 'Insert Into Produtopreco (Codigoproduto, Codigoprodutoprecotipo, Valor, Datacriacao) ';
  str := str +
    'Values (:Codigoproduto, :Codigoprodutoprecotipo, :Valor, Current_Timestamp); ';

  setSql(str);

  addParametro(ftInteger, 'CodigoProduto', entidade.CodigoProduto);
  addParametro(ftInteger, 'CodigoProdutoPrecoTipo',
    entidade.CodigoProdutoPrecoTipo);
  addParametro(ftFloat, 'Valor', entidade.Valor);

  executar;

end;

function TProdutoPrecoDAO.ListarPor(listaParametros:TDictionary<String, Variant>; listaOrdenarPor
  : TDictionary<String, String>): TFDMemTable;
var
  str, condicoes, ordenacoes: String;
  listaCondicao: TStringList;
  ordem, coluna: TPair<string, string>;
  colunas: TDictionary<String, String>;
begin
  condicoes := EmptyStr;
  listaCondicao := TStringList.Create;
  colunas:= ListaColunas;
  for coluna in colunas do
  begin
    if listaParametros.ContainsKey(coluna.Key) then
    begin
      listaCondicao.Add(coluna.Value + ' = :' + coluna.Key);
      addParametro(ftUnknown, coluna.Key, listaParametros.Items[coluna.Key]);
    end;
  end;

  if listaCondicao.Count > 0 then
    condicoes:= ' WHERE ' + listaCondicao.Text.Join(' AND ', listaCondicao.ToStringArray);

  str := 'Select ';
  str := str + '    Produtopreco.Codigoproduto, ';
  str := str + '    Produtopreco.Codigoprodutoprecotipo, ';
  str := str + '    Produtoprecotipo.Nome, ';
  str := str + '    Produtopreco.Valor ';
  str := str + 'From Produtopreco ';
  str := str +
    'Join Produtoprecotipo On Produtoprecotipo.Codigoprodutoprecotipo = Produtopreco.Codigoprodutoprecotipo ';
    str:= str + condicoes;

  if Not Assigned(listaOrdenarPor) or (listaOrdenarPor.Count = 0) then
  begin
    listaOrdenarPor:= TDictionary<String,String>.Create();
    listaOrdenarPor.Add('CodigoProdutoPrecoTipo', 'ASC');
  end;

  listaCondicao.Clear;
  for ordem in listaOrdenarPor do
  begin
    if ListaColunas.ContainsKey(ordem.Key) then
    begin
      listaCondicao.Add(ordem.Key + ' ' + ordem.Value);
    end;
  end;

  str:= str + ' ORDER BY ' + listaCondicao.Text.Join(', ', listaCondicao.ToStringArray);

  setSql(str);
  Result:= listarDados;


end;

function TProdutoPrecoDAO.ListaColunas(): TDictionary<String, String>;
var
  colunas: TDictionary<String, String>;
begin
  colunas := TDictionary<String, String>.Create();
  colunas.Add('CodigoProduto', 'ProdutoPreco.CodigoProduto');
  colunas.Add('CodigoProdutoPrecoTipo', 'ProdutoPreco.CodigoProdutoPrecoTipo');


  Result := colunas;
end;

end.

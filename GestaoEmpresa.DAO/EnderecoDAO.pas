unit EnderecoDAO;

interface

uses
  BancoDados, Endereco, FireDAC.Comp.Client,
  System.Generics.Collections;

type
  TEnderecoDAO = class abstract(TBancoDados<TEndereco>)
  protected
    procedure comandoInserir(entidade: TEndereco); override;
    procedure comandoAlterar(entidade: TEndereco); override;
    procedure comandoExcluir(entidade: TEndereco); override;
    function obterPorCodigo(codigo: Int64): TEndereco;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;
    procedure excluirEnderecoPorPessoa(codigoPessoa: Int64);
  end;

implementation

uses
  Data.DB, System.SysUtils, System.Classes, EnumConverterStrings,
  Entities.Enums.ETipoEndereco;

{ TEnderecoDAO }

procedure TEnderecoDAO.comandoAlterar(entidade: TEndereco);
var
  str: String;
begin
  inherited;
  str := 'UPDATE Endereco ';
  str := str + 'SET Codigobairro = :Codigobairro, ';
  str := str + '    Codigocidade = :Codigocidade, ';
  str := str + '    Logradouro = :Logradouro, ';
  str := str + '    Numero = :Numero, ';
  str := str + '    Complemento = :Complemento, ';
  str := str + '    Tipo = :Tipo, ';
  str := str + '    Principal = :Principal, ';
  str := str + '    Datamodificacao = :Datamodificacao ';
  str := str + 'WHERE Codigoendereco = :Codigoendereco; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoBairro', entidade.Bairro.CodigoBairro);
  addParametro(ftInteger, 'CodigoCidade', entidade.Cidade.CodigoCidade);
  addParametro(ftString, 'Logradouro', entidade.Logradouro);
  addParametro(ftString, 'Numero', entidade.Numero);
  addParametro(ftString, 'Complemento', entidade.Complemento);
  addParametro(ftString, 'Tipo', TConvert<TETipoEndereco>.EnumConvertStr(entidade.TipoEndereco));
  addParametro(ftBoolean, 'Principal', entidade.Principal);
  addParametro(ftTimeStamp, 'DataModificacao', Now());
  addParametro(ftInteger, 'CodigoEndereco', entidade.CodigoEndereco);

  executar;

end;

procedure TEnderecoDAO.comandoExcluir(entidade: TEndereco);
var
  str: String;
begin
  inherited;

  str := 'DELETE FROM Endereco ';
  str := str + 'WHERE Codigoendereco = :Codigoendereco; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoEndereco', entidade.CodigoEndereco);

  executar

end;

procedure TEnderecoDAO.comandoInserir(entidade: TEndereco);
var
  str: String;
  dados: TFDMemTable;
begin
  inherited;
  str := 'INSERT INTO ENDERECO (CODIGOENDERECO, CODIGOPESSOA, CODIGOBAIRRO, CODIGOCIDADE, LOGRADOURO, NUMERO, COMPLEMENTO, TIPO, ';
  str := str + '                      PRINCIPAL, DATACRIACAO) ';
  str := str + 'VALUES ((SELECT COALESCE(MAX(CODIGOENDERECO), 0) + 1 ';
  str := str +
    '         FROM ENDERECO), :CODIGOPESSOA, :CODIGOBAIRRO, :CODIGOCIDADE, :LOGRADOURO, :NUMERO, :COMPLEMENTO, :TIPO, ';
  str := str + '        :PRINCIPAL, :DATACRIACAO) ';
  str := str + 'RETURNING CODIGOENDERECO; ';

  setSql(str);
  addParametro(ftInteger, 'CODIGOPESSOA', entidade.codigoPessoa);
  addParametro(ftInteger, 'CODIGOBAIRRO', entidade.Bairro.CodigoBairro);
  addParametro(ftInteger, 'CODIGOCIDADE', entidade.Cidade.CodigoCidade);
  addParametro(ftString, 'LOGRADOURO', entidade.Logradouro);
  addParametro(ftString, 'NUMERO', entidade.Numero);
  addParametro(ftString, 'COMPLEMENTO', entidade.Complemento);
  addParametro(ftString, 'TIPO',  TConvert<TETipoEndereco>.EnumConvertStr(entidade.TipoEndereco));
  addParametro(ftString, 'PRINCIPAL', entidade.Principal);
  addParametro(ftTimeStamp, 'DATACRIACAO', Now);

  dados := listarDados;

  if Not dados.IsEmpty then
    entidade.CodigoEndereco := dados.FieldByName('CodigoEndereco').AsInteger;

end;

procedure TEnderecoDAO.excluirEnderecoPorPessoa(codigoPessoa: Int64);
var
  str: String;
begin
  str := 'DELETE FROM ENDERECO ';
  str := str + 'WHERE CODIGOPESSOA = :CODIGOPESSOA; ';

  setSql(str);
  addParametro(ftInteger, 'CODIGOPESSOA', codigoPessoa);

  executar;
end;

function TEnderecoDAO.listarPor(listaParametros
  : TDictionary<String, Variant> = nil): TFDMemTable;
var
  str, condicoes: String;
  listaCondicao: TStringList;
begin
  condicoes := '';
  listaCondicao := TStringList.Create;
  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoEndereco') then
    begin
      listaCondicao.Add('vEndereco.CodigoEndereco = :CodigoEndereco');
      addParametro(ftInteger, 'CodigoEndereco',
        listaParametros.Items['CodigoEndereco']);
    end;
    if listaParametros.ContainsKey('CodigoPessoa') then
    begin
      listaCondicao.Add('vEndereco.CodigoPessoa = :CodigoPessoa');
      addParametro(ftInteger, 'CodigoPessoa',
        listaParametros.Items['CodigoPessoa']);
    end;
  end;

  if listaCondicao.Count > 0 then
  begin
    condicoes := 'WHERE ' + listaCondicao.Text.Join(' AND ',
      listaCondicao.ToStringArray);
  end;

  str := 'SELECT ';
  str := str + '    vEndereco.Codigoendereco, ';
  str := str + '    vEndereco.Codigopessoa, ';
  str := str + '    vEndereco.Logradouro, ';
  str := str + '    vEndereco.Numero, ';
  str := str + '    vEndereco.Complemento, ';
  str := str + '    vEndereco.Codigobairro, ';
  str := str + '    vEndereco.Bairro, ';
  str := str + '    vEndereco.Codigocidade, ';
  str := str + '    vEndereco.Cidade, ';
  str := str + '    vEndereco.Codigoestado, ';
  str := str + '    vEndereco.Estado, ';
  str := str + '    vEndereco.Sigla, ';
  str := str + '    vEndereco.Tipo, ';
  str := str + '    vEndereco.Principal, ';
  str := str + '    vEndereco.Datacriacao, ';
  str := str + '    vEndereco.Datamodificacao ';
  str := str + 'FROM vEndereco ';
  str := str + condicoes;

  setSql(str);

  result := listarDados;

end;

function TEnderecoDAO.obterPorCodigo(codigo: Int64): TEndereco;

begin

end;

end.

unit EmailDAO;

interface

uses
  BancoDados, Email, FireDAC.Comp.Client,
  System.Generics.Collections;

type
  TEmailDAO = class abstract(TBancoDados<TEmail>)
  protected
    procedure comandoInserir(entidade: TEmail); override;
    procedure comandoAlterar(entidade: TEmail); override;
    procedure comandoExcluir(entidade: TEmail); override;
    function obterPorCodigo(codigo: Int64): TEmail;
    function listarPor(listaParametros: TDictionary<String, Variant> = nil)
      : TFDMemTable;
    procedure exlcuirEmailPorPessoa(codigoPessoa: Int64);
  end;

implementation

uses
  Data.DB, System.SysUtils, System.Classes;

{ TEmailDAO }

procedure TEmailDAO.comandoAlterar(entidade: TEmail);
var
  str: String;
begin
  inherited;
  str := 'UPDATE EMAIL ';
  str := str + 'SET ENDERECO = :ENDERECO, ';
  str := str + '    PRINCIPAL = :PRINCIPAL, ';
  str := str + '    DATAMODIFICACAO = :DATAMODIFICACAO ';
  str := str + 'WHERE CODIGOEMAIL = :CODIGOEMAIL   ';

  setSql(str);
  addParametro(ftString, 'ENDERECO', entidade.Endereco);
  addParametro(ftBoolean, 'PRINCIPAL', entidade.Principal);
  addParametro(ftDateTime, 'DATAMODIFICACAO', Now());
  addParametro(ftInteger, 'CODIGOEMAIL', entidade.CodigoEmail);

  executar;
end;

procedure TEmailDAO.comandoExcluir(entidade: TEmail);
begin
  inherited;

end;

procedure TEmailDAO.comandoInserir(entidade: TEmail);
var
  str: String;
  Data:TFDMemTable;
begin
  inherited;
  str := 'INSERT INTO EMAIL (CODIGOEMAIL, CODIGOPESSOA, ENDERECO, PRINCIPAL, DATACRIACAO) ';
  str := str + 'VALUES ((SELECT COALESCE(MAX(CODIGOEMAIL), 0) + 1 ';
  str := str +
    '         FROM EMAIL), :CODIGOPESSOA, :ENDERECO, :PRINCIPAL, :DATACRIACAO) ';
   str := str + 'RETURNING CODIGOEMAIL; ';
  setSql(str);
  addParametro(ftInteger, 'CODIGOPESSOA', entidade.codigoPessoa);
  addParametro(ftString, 'ENDERECO', entidade.Endereco);
  addParametro(ftBoolean, 'PRINCIPAL', entidade.Principal);
  addParametro(ftTimeStamp, 'DATACRIACAO', Now);

  Data := listarDados;

  if Not Data.IsEmpty then
    entidade.CodigoEmail := Data.FieldByName('CodigoEmail').AsInteger;

end;

procedure TEmailDAO.exlcuirEmailPorPessoa(codigoPessoa: Int64);
var
  str: String;
begin
  str := 'DELETE FROM EMAIL ';
  str := str + 'WHERE CODIGOPESSOA = :CODIGOPESSOA; ';

  setSql(str);
  addParametro(ftInteger, 'CODIGOPESSOA', codigoPessoa);

  executar;

end;

function TEmailDAO.listarPor(listaParametros
  : TDictionary<String, Variant> = nil): TFDMemTable;
var
  str, condicoes: String;
  listaCondicao: TStringList;
begin
  condicoes := '';
  listaCondicao := TStringList.Create;
  if Assigned(listaParametros) then
  begin
    if listaParametros.ContainsKey('CodigoEmail') then
    begin
      listaCondicao.Add('Email.CodigoEmail = :CodigoEmail');
      addParametro(ftInteger, 'CodigoEmail',
        listaParametros.Items['CodigoEmail']);
    end;

    if listaParametros.ContainsKey('CodigoPessoa') then
    begin
      listaCondicao.Add('Email.CodigoPessoa = :CodigoPessoa');
      addParametro(ftInteger, 'CodigoPessoa',
        listaParametros.Items['CodigoPessoa']);
    end;
  end;

  if listaCondicao.Count > 0 then
  begin
    condicoes := 'WHERE ' + listaCondicao.Text.Join(' AND ',
      listaCondicao.ToStringArray);
  end;

  str := 'SELECT ';
  str := str + '    Email.Codigoemail, ';
  str := str + '    Email.Codigopessoa, ';
  str := str + '    Email.Endereco, ';
  str := str + '    Email.Principal, ';
  str := str + '    Email.Datacriacao, ';
  str := str + '    Email.Datamodificacao ';
  str := str + 'FROM Email  ';
  str := str + condicoes;

  setSql(str);
  result:= listarDados;

end;

function TEmailDAO.obterPorCodigo(codigo: Int64): TEmail;
begin

end;

end.

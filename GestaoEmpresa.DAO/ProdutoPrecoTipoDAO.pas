unit ProdutoPrecoTipoDAO;

interface

uses
  BancoDados, ProdutoPrecoTipo;

type
  TProdutoPrecoTipoDAO = class abstract(TBancoDados<TProdutoPrecoTipo>)
  protected
    procedure comandoInserir(entidade: TProdutoPrecoTipo); override;
    procedure comandoAlterar(entidade: TProdutoPrecoTipo); override;
    procedure comandoExcluir(entidade: TProdutoPrecoTipo); override;
  end;

implementation

uses
  Data.DB, FireDAC.Comp.Client;

{ TProdutoPrecoTipoDAO }

procedure TProdutoPrecoTipoDAO.comandoAlterar(entidade: TProdutoPrecoTipo);
var
  str: String;
begin
  str := 'Update Produtoprecotipo ';
  str := str + 'Set Nome = :Nome, ';
  str := str + '    Disponivel = :Disponivel, ';
  str := str + '    Datamodificacao = Current_Timestamp ';
  str := str + 'Where Codigoprodutoprecotipo = :Codigoprodutoprecotipo; ';

  setSql(str);
  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);
  addParametro(ftInteger, 'CodigoProdutoPrecoTipo',
    entidade.CodigoProdutoPrecoTipo);

  executar;

end;

procedure TProdutoPrecoTipoDAO.comandoExcluir(entidade: TProdutoPrecoTipo);
var
  str: String;
begin
  str := 'Update Produtoprecotipo ';
  str := str + 'Set Disponivel = False, ';
  str := str + '    Datamodificacao = Current_Timestamp ';
  str := str + 'Where Codigoprodutoprecotipo = :Codigoprodutoprecotipo; ';

  setSql(str);
  addParametro(ftInteger, 'CodigoProdutoPrecoTipo', entidade.CodigoProdutoPrecoTipo);

  executar;

end;

procedure TProdutoPrecoTipoDAO.comandoInserir(entidade: TProdutoPrecoTipo);
var
  str: String;
  dados: TFDMemTable;
begin
  str := 'Insert Into Produtoprecotipo (Codigoprodutoprecotipo, Nome, Disponivel, Datacriacao) ';
  str := str + 'Values ((Select ';
  str := str + '             Coalesce(Max(Codigoprodutoprecotipo), 0) + 1 ';
  str := str +
    '         From Produtoprecotipo), :Nome, :Disponivel, Current_Timestamp) ';
  str := str + 'Returning Codigoprodutoprecotipo; ';

  setSql(str);
  addParametro(ftString, 'Nome', entidade.Nome);
  addParametro(ftBoolean, 'Disponivel', entidade.Disponivel);

  dados := listarDados;

  if Not dados.IsEmpty then
    entidade.CodigoProdutoPrecoTipo :=
      dados.FieldByName('CodigoProdutoPrecoTipo').AsInteger;

end;

end.

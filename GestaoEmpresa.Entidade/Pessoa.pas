unit Pessoa;

interface

uses
  Entities.Enums.EPerfilPessoa, PessoaFisica,
  PessoaJuridica, Email, Endereco, Telefone, System.Generics.Collections;

type
  TPessoa = class
  public
    constructor Create; overload;
    constructor Create(CodigoPessoa: Int64); overload;
    constructor Create(CodigoPessoa: Int64; Nome, Observacao: String);
      overload; virtual;
    destructor Destroy; overload;
    procedure adicionarEmail(Email: TEmail);
    procedure adicionarEndereco(Endereco: TEndereco);
    procedure adicionarTelefone(Telefone: TTelefone);
  private
    FCodigoPessoa: Int64;
    FNome: String;
    FPerfilCliente: Boolean;
    FPerfilEmpresa: Boolean;
    FPerfilUsuario: Boolean;
    FPerfilFornecedor: Boolean;
    FObservacao: String;
    FDataModificacao: TDateTime;
    FPerfilPessoa: TEPerfilPessoa;
    FPessoaFisica: TPessoaFisica;
    FPessoaJuridica: TPessoaJuridica;
    FListaEmail: TObjectList<TEmail>;
    FListaEndereco: TObjectList<TEndereco>;
    FListaTelefone: TObjectList<TTelefone>;
  published
    property CodigoPessoa: Int64 read FCodigoPessoa write FCodigoPessoa;
    property Nome: String read FNome write FNome;
    property PerfilCliente: Boolean read FPerfilCliente write FPerfilCliente;
    property PerfilEmpresa: Boolean read FPerfilEmpresa write FPerfilEmpresa;
    property PerfilUsuario: Boolean read FPerfilUsuario write FPerfilUsuario;
    property PerfilFornecedor: Boolean read FPerfilFornecedor
      write FPerfilFornecedor;
    property Observacao: String read FObservacao;
    property DataModificacao: TDateTime read FDataModificacao;
    property PerfilPessoa: TEPerfilPessoa read FPerfilPessoa;
    property PessoaFisica: TPessoaFisica read FPessoaFisica write FPessoaFisica;
    property PessoaJuridica: TPessoaJuridica read FPessoaJuridica
      write FPessoaJuridica;
    property ListaEmail: TObjectList<TEmail> read FListaEmail;
    property ListaEndereco: TObjectList<TEndereco> read FListaEndereco;
    property ListaTelefone: TObjectList<TTelefone> read FListaTelefone;
  end;

implementation

{ TPessoa }

constructor TPessoa.Create(CodigoPessoa: Int64; Nome, Observacao: String);
begin
  Inherited Create;
  Self.FCodigoPessoa := CodigoPessoa;
  Self.FNome := Nome;
  Self.FObservacao := Observacao;
end;

constructor TPessoa.Create(CodigoPessoa: Int64);
begin
  Self.FCodigoPessoa := CodigoPessoa;
end;

destructor TPessoa.Destroy;
begin
  FListaEmail.Free;
  FListaEndereco.Free;
  FListaTelefone.Free
end;

procedure TPessoa.adicionarEmail(Email: TEmail);
begin
  if Not Assigned(FListaEmail) then
  begin
    FListaEmail := TObjectList<TEmail>.Create();
  end;

  FListaEmail.Add(Email);

end;

procedure TPessoa.adicionarEndereco(Endereco: TEndereco);
begin
  if Not Assigned(FListaEndereco) then
  begin
    FListaEndereco := TObjectList<TEndereco>.Create();
  end;

  FListaEndereco.Add(Endereco);
end;

procedure TPessoa.adicionarTelefone(Telefone: TTelefone);
begin
  if Not Assigned(FListaTelefone) then
  begin
    FListaTelefone := TObjectList<TTelefone>.Create();
  end;

  FListaTelefone.Add(Telefone);
end;

constructor TPessoa.Create;
begin
  Self.FPessoaFisica := TPessoaFisica.Create();
  Self.FPessoaJuridica := TPessoaJuridica.Create();
end;

end.

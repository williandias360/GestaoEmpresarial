unit Usuario;

interface

uses
  Pessoa;

type
  TUsuario = class
  public
    constructor Create(Login, Senha: String; Pessoa: TPessoa); overload;
  private
    FCodigoUsuario: Int64;
    FLogin: String;
    FSenha: String;
    FDataCriacao: TDateTime;
    FPessoa: TPessoa;
    FDisponivel: Boolean;
  published
    property CodigoUsuario: Int64 read FCodigoUsuario;
    property Login: String read FLogin;
    property Senha: String read FSenha;
    property DataCriacao: TDateTime read FDataCriacao;
    property Disponivel: Boolean read FDisponivel write FDisponivel;
    property Pessoa: TPessoa read FPessoa;
  end;

implementation

{ TUsuario }

constructor TUsuario.Create(Login, Senha: String; Pessoa: TPessoa);
begin
  Self.FLogin := Login;
  Self.FSenha := Senha;
  Self.FPessoa := Pessoa;
  Self.FDisponivel:= True;
end;

end.

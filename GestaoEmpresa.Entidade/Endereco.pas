unit Endereco;

interface

uses
  Entities.Enums.ETipoEndereco, Cidade, Bairro;

type
  TEndereco = class
  public
    constructor Create(CodigoEndereco: Int64;
      Logradouro, Numero, Complemento: String; Principal: Boolean;
      TipoEndereco: TETipoEndereco; Bairro: TBairro; Cidade: TCidade); overload;
    constructor Create(CodigoPessoa: Int64); overload;
    constructor Create(CodigoEndereco: Int64; Bairro: TBairro); overload;
  private
    FCodigoEndereco: Int64;
    FLogradouro: String;
    FNumero: String;
    FComplemento: String;
    FPrincipal: Boolean;
    FTipoEndereco: TETipoEndereco;
    FDataCriacao: TDateTime;
    FDataModificacao: TDateTime;
    FBairro: TBairro;
    FCidade: TCidade;
    FCodigoPessoa: Int64;
  published
    property CodigoEndereco: Int64 read FCodigoEndereco write FCodigoEndereco;
    property CodigoPessoa: Int64 read FCodigoPessoa write FCodigoPessoa;
    property Logradouro: String read FLogradouro;
    property Numero: String read FNumero;
    property Complemento: String read FComplemento;
    property Principal: Boolean read FPrincipal write FPrincipal;
    property TipoEndereco: TETipoEndereco read FTipoEndereco;
    property DataModificacao: TDateTime read FDataModificacao;
    property Bairro: TBairro read FBairro;
    property Cidade: TCidade read FCidade;
  end;

implementation

{ TEndereco }

constructor TEndereco.Create(CodigoEndereco: Int64;
  Logradouro, Numero, Complemento: String; Principal: Boolean;
  TipoEndereco: TETipoEndereco; Bairro: TBairro; Cidade: TCidade);
begin
  inherited Create;
  Self.FCodigoEndereco := CodigoEndereco;
  Self.FLogradouro := Logradouro;
  Self.FNumero := Numero;
  Self.FComplemento := Complemento;
  Self.FPrincipal := Principal;
  Self.FTipoEndereco := TipoEndereco;
  Self.FBairro := Bairro;
  Self.FCidade := Cidade;
end;

constructor TEndereco.Create(CodigoPessoa: Int64);
begin
  Self.FCodigoPessoa := CodigoPessoa;
end;

constructor TEndereco.Create(CodigoEndereco: Int64; Bairro: TBairro);
begin
  Self.FCodigoEndereco := CodigoEndereco;
  Self.FBairro := Bairro;
end;

end.

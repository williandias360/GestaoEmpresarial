unit PessoaJuridica;

interface


type
  TPessoaJuridica = class
  public
    constructor Create; overload;
    constructor Create(Cnpj, InscricaoEstadual, RazaoSocial: String); overload;
    constructor Create(CodigoPessoa:Int64); overload;
    function QueryInterface(const IID: TGUID; out Obj): HRESULT; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  private
    FCnpj: String;
    FInscricaoEstadual: String;
    FRazaoSocial: String;
    FCodigoPessoa: int64;
  published
    property Cnpj: String read FCnpj write FCnpj;
    property InscricaoEstadual: String read FInscricaoEstadual;
    property RazaoSocial: String read FRazaoSocial;
    property CodigoPessoa: int64 read FCodigoPessoa write FCodigoPessoa;
  end;

implementation

{ TPessoaJuridica }

constructor TPessoaJuridica.Create(Cnpj, InscricaoEstadual, RazaoSocial: String);
begin
  Self.FCnpj := Cnpj;
  Self.FInscricaoEstadual := InscricaoEstadual;
  Self.FRazaoSocial := RazaoSocial;
end;

constructor TPessoaJuridica.Create(CodigoPessoa: Int64);
begin
  Self.FCodigoPessoa:= CodigoPessoa;
end;

constructor TPessoaJuridica.Create;
begin

end;

function TPessoaJuridica.QueryInterface(const IID: TGUID; out Obj): HRESULT;
begin

end;

function TPessoaJuridica._AddRef: Integer;
begin

end;

function TPessoaJuridica._Release: Integer;
begin

end;

end.

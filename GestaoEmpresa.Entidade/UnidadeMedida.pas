unit UnidadeMedida;

interface
type
  TUnidadeMedida = class
  public
    constructor Create; overload;
    constructor Create(CodigoUnidadeMedida:Integer;Nome,Sigla:String); overload;
    constructor Create(CodigoUnidadeMedida:Integer); overload;
  private
    FCodigoUnidadeMedida: Integer;
    FNome: String;
    FSigla: String;
  published
    property CodigoUnidadeMedida: Integer read FCodigoUnidadeMedida write FCodigoUnidadeMedida;
    property Nome: String read FNome write FNome;
    property Sigla: String read FSigla write FSigla;
  end;

implementation

{ TUnidadeMedida }

constructor TUnidadeMedida.Create(CodigoUnidadeMedida: Integer; Nome,
  Sigla: String);
begin
  Self.CodigoUnidadeMedida:= CodigoUnidadeMedida;
  Self.Nome:= Nome;
  Self.Sigla:= Sigla;
end;

constructor TUnidadeMedida.Create;
begin

end;

constructor TUnidadeMedida.Create(CodigoUnidadeMedida: Integer);
begin
  Self.CodigoUnidadeMedida:= CodigoUnidadeMedida;
end;

end.

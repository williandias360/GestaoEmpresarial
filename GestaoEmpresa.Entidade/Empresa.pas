unit Empresa;

interface

uses Pessoa, Entities.Enums.ETipoRegimeTributario;

type
  TEmpresa = class
  public
    constructor Create(TipoRegimeTributario: TETipoRegimeTributario; Pessoa: TPessoa); overload;
    constructor Create(CodigoEmpresa:Integer); overload;

  private
    FCodigoEmpresa: Integer;
    FTipoRegimeTributario: TETipoRegimeTributario;
    FDisponivel: Boolean;
    FPessoa: TPessoa;
  published
    property CodigoEmpresa: Integer read FCodigoEmpresa write FCodigoEmpresa;
    property TipoRegimeTributario: TETipoRegimeTributario read FTipoRegimeTributario
      write FTipoRegimeTributario;
    property Disponivel: Boolean read FDisponivel write FDisponivel;
    property Pessoa: TPessoa read FPessoa write FPessoa;
  end;

implementation

{ TEmpresa }



constructor TEmpresa.Create(TipoRegimeTributario: TETipoRegimeTributario; Pessoa: TPessoa);
begin
  Self.FTipoRegimeTributario := TipoRegimeTributario;
  Self.FPessoa := Pessoa;
  Self.Disponivel := True;
end;

constructor TEmpresa.Create(CodigoEmpresa: Integer);
begin
  Self.CodigoEmpresa := CodigoEmpresa;
end;

end.

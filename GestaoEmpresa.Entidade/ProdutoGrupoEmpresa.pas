unit ProdutoGrupoEmpresa;

interface
type
  TProdutoGrupoEmpresa = class
  public
    Constructor Create(CodigoProdutoGrupo, CodigoEmpresa:Integer); overload;
  private
    FCodigoProdutoGrupo: Integer;
    FCodigoEmpresa: Integer;
  published
    property CodigoProdutoGrupo: Integer read FCodigoProdutoGrupo write FCodigoProdutoGrupo;
    property CodigoEmpresa: Integer read FCodigoEmpresa write FCodigoEmpresa;
  end;

implementation

{ TProdutoGrupoEmpresa }

constructor TProdutoGrupoEmpresa.Create(CodigoProdutoGrupo,
  CodigoEmpresa: Integer);
begin
  Self.CodigoProdutoGrupo:= CodigoProdutoGrupo;
  Self.CodigoEmpresa:= CodigoEmpresa;
end;

end.

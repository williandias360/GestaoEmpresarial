unit Bairro;

interface

type
  TBairro = class
  public
    constructor Create(Nome: String); overload;
    constructor Create(CodigoBairro: Int64; Nome: String); overload;
    constructor Create(CodigoBairro:Int64);overload;
  private
    FCodigoBairro: Int64;
    FNome: String;
    FDataModificacao: TDateTime;
  published
    property CodigoBairro: Int64 read FCodigoBairro write FCodigoBairro;
    property Nome: String read FNome;
    property DataModificacao: TDateTime read FDataModificacao;
  end;

implementation

{ TBairro }

constructor TBairro.Create(Nome: String);
begin
  inherited Create;
  Self.FNome := Nome;
end;

constructor TBairro.Create(CodigoBairro: Int64; Nome: String);
begin
  Self.FCodigoBairro := CodigoBairro;
  Self.FNome := Nome;
end;

constructor TBairro.Create(CodigoBairro: Int64);
begin
  Self.FCodigoBairro:= CodigoBairro;
end;

end.

unit Cidade;

interface

uses
  Estado;

type
  TCidade = class
  public
    constructor Create(CodigoCidade:Integer);overload;
    constructor Create(CodigoCidade:Integer; Estado:TEstado);overload;
  private
    FCodigoCidade: Integer;
    FNome: String;
    FCodigoIbge: String;
    FEstado: TEstado;
  published
    property CodigoCidade: Integer read FCodigoCidade;
    property Nome: String read FNome;
    property CodigoIbge: String read FCodigoIbge;
    property Estado: TEstado read FEstado;
  end;

implementation

{ TCidade }

constructor TCidade.Create(CodigoCidade: Integer; Estado: TEstado);
begin
  Self.FCodigoCidade:= CodigoCidade;
  Self.FEstado:= Estado;
end;

constructor TCidade.Create(CodigoCidade: Integer);
begin
  Self.FCodigoCidade:= CodigoCidade;
end;

end.

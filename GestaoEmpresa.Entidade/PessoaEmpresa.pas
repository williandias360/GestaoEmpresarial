unit PessoaEmpresa;

interface

type
  TPessoaEmpresa = class
  public
    constructor Create; overload;
    constructor Create(CodigoPessoa, CodigoEmpresa:Integer); overload;
  private
    FCodigoPessoa: Integer;
    FCodigoEmpresa: Integer;
  published
    property CodigoPessoa: Integer read FCodigoPessoa write FCodigoPessoa;
    property CodigoEmpresa: Integer read FCodigoEmpresa write FCodigoEmpresa;
  end;

implementation

{ TPessoaEmpresa }

constructor TPessoaEmpresa.Create(CodigoPessoa, CodigoEmpresa: Integer);
begin

end;

constructor TPessoaEmpresa.Create;
begin

end;

end.

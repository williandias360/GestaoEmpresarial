unit PessoaFisica;

interface

type
  TPessoaFisica = class
  public
    constructor Create; overload;
    constructor Create(Cpf, Rg:String); overload;
    constructor Create(CodigoPessoa:Int64); overload;
    function QueryInterface(const IID: TGUID; out Obj): HRESULT; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  private
    FCpf: String;
    FRg: String;
    FCodigoPessoa: Int64;
  published
    property Cpf: String read FCpf write FCpf;
    property Rg: String read FRg;
    property CodigoPessoa: Int64 read FCodigoPessoa write FCodigoPessoa;
  end;

implementation

{ TPessoaFisica }

constructor TPessoaFisica.Create(Cpf, Rg: String);
begin
  Self.FCpf:= Cpf;
  Self.FRg:= Rg;
end;

constructor TPessoaFisica.Create(CodigoPessoa: Int64);
begin
  Self.FCodigoPessoa:= CodigoPessoa;
end;

constructor TPessoaFisica.Create;
begin

end;

function TPessoaFisica.QueryInterface(const IID: TGUID; out Obj): HRESULT;
begin

end;

function TPessoaFisica._AddRef: Integer;
begin

end;

function TPessoaFisica._Release: Integer;
begin

end;

end.

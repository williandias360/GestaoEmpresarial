unit Cliente;

interface

uses
  Pessoa;

type
  TCliente = class
  public
    constructor Create; overload;
      constructor Create(CodigoCliente:Int64); overload;
    constructor Create(Limite: Currency;
      Bloqueado, EnviarEmailFinalizarVenda: Boolean; Pessoa: TPessoa); overload;
  procedure atribuirCodigoCliente(codigo:Int64);
  private
    FCodigoCliente: Int64;
    FLimite: Currency;
    FDisponivel: Boolean;
    FEnviarEmailFinalizarVenda: Boolean;
    FPessoa: TPessoa;
    FBloqueado: Boolean;
  published
    property CodigoCliente: Int64 read FCodigoCliente write FCodigoCliente;
    property Limite: Currency read FLimite write FLimite;
    property Disponivel: Boolean read FDisponivel write FDisponivel;
    property Bloqueado: Boolean read FBloqueado write FBloqueado;
    property EnviarEmailFinalizarVenda: Boolean read FEnviarEmailFinalizarVenda;
    property Pessoa: TPessoa read FPessoa write FPessoa;
  end;

implementation


{ TCliente }

procedure TCliente.atribuirCodigoCliente(codigo: Int64);
begin
  Self.FCodigoCliente:= codigo;
end;

constructor TCliente.Create(Limite: Currency;
  Bloqueado, EnviarEmailFinalizarVenda: Boolean; Pessoa: TPessoa);
begin
  Self.FLimite := Limite;
  Self.FBloqueado := Bloqueado;
  Self.FEnviarEmailFinalizarVenda := EnviarEmailFinalizarVenda;
  Self.FPessoa := Pessoa;
  Self.FDisponivel:= true;
  Self.FCodigoCliente:= Pessoa.CodigoPessoa;
end;

constructor TCliente.Create;
begin
 Self.FPessoa:= TPessoa.Create();
end;

constructor TCliente.Create(CodigoCliente: Int64);
begin
  Self.FCodigoCliente:= CodigoCliente;
end;

end.

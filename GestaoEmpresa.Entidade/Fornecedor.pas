unit Fornecedor;

interface

uses
  Pessoa;

type
  TFornecedor = class
  public
    constructor Create(DiaPagamento: Integer;
      Pessoa: TPessoa); overload;
  private
    FCodigoFornecedor: Int64;
    FDiaPagamento: Integer;
    FDisponivel: Boolean;
    FPessoa: TPessoa;
  published
    property CodigoFornecedor: Int64 read FCodigoFornecedor;
    property DiaPagamento: Integer read FDiaPagamento;
    property Disponivel: Boolean read FDisponivel write FDisponivel;
    property Pessoa: TPessoa read FPessoa write FPessoa;
  end;

implementation

{ TFornecedor }

constructor TFornecedor.Create(DiaPagamento: Integer;
  Pessoa: TPessoa);
begin
  Self.FDiaPagamento := DiaPagamento;
  Self.FDisponivel := True;
  Self.FPessoa := Pessoa;
  Self.FCodigoFornecedor:= Pessoa.CodigoPessoa;
end;

end.

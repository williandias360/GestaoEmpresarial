unit Estado;

interface

type
  TEstado = class
  public
    constructor Create(CodigoEstado:Integer);overload;
  private
    FCodigoEstado: Integer;
    FNome: String;
    FSigla: String;
  published
    property CodigoEstado: Integer read FCodigoEstado;
    property Nome: String read FNome;
    property Sigla: String read FSigla;
  end;

implementation

{ TEstado }

constructor TEstado.Create(CodigoEstado: Integer);
begin
  Self.FCodigoEstado:= CodigoEstado;
end;

end.

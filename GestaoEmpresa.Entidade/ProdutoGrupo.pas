unit ProdutoGrupo;

interface

type
  TProdutoGrupo = class
  public
    Constructor Create; overload;
    Constructor Create(CodigoProdutoGrupo: Integer); overload;
    Constructor Create(CodigoProdutoGrupo: Integer; Nome: String); overload;
  private
    FCodigoProdutoGrupo: Integer;
    FNome: String;
    FDisponivel: Boolean;
  published
    property CodigoProdutoGrupo: Integer read FCodigoProdutoGrupo
      write FCodigoProdutoGrupo;
    property Nome: String read FNome write FNome;
    property Disponivel: Boolean read FDisponivel write FDisponivel;

  end;

implementation

{ TProdutoGrupo }

constructor TProdutoGrupo.Create(CodigoProdutoGrupo: Integer; Nome: String);
begin
  Self.CodigoProdutoGrupo := CodigoProdutoGrupo;
  Self.Nome := Nome;
  Self.Disponivel := True;
end;

constructor TProdutoGrupo.Create;
begin

end;

constructor TProdutoGrupo.Create(CodigoProdutoGrupo: Integer);
begin
  Self.CodigoProdutoGrupo := CodigoProdutoGrupo;
end;

end.

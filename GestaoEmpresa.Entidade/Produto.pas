unit Produto;

interface

uses
  System.Classes, ProdutoPreco, System.Generics.Collections;

type
  TProduto = class
  public
    Constructor Create; overload;
    Constructor Create(CodigoProduto:Integer); overload;
    Constructor Create(CodigoProduto: Integer; Nome, CodigoBarras, Ncm: String;
      QuantidadePorCaixa, EstoqueMinimo: Real); overload;
    procedure AdicionarPreco(ProdutoPreco: TProdutoPreco);

  private
    FCodigoProduto: Int64;
    FCodigoProdutoGrupo: Integer;
    FCodigoUnidadeMedida: Integer;
    FNome: String;
    FQuantidadePorCaixa: Real;
    FDisponivel: Boolean;
    FCodigoBarras: String;
    FListaProdutoPreco: TObjectList<TProdutoPreco>;
    FEstoqueAtual: Real;
    FEstoqueMinimo: Real;
    FNcm: String;
    procedure Inicializar;

  published
    property CodigoProduto: Int64 read FCodigoProduto write FCodigoProduto;
    property CodigoProdutoGrupo: Integer read FCodigoProdutoGrupo
      write FCodigoProdutoGrupo;
    property CodigoUnidadeMedida: Integer read FCodigoUnidadeMedida
      write FCodigoUnidadeMedida;
    property Nome: String read FNome write FNome;
    property CodigoBarras: String read FCodigoBarras write FCodigoBarras;
    property Ncm: String read FNcm write FNcm;
    property QuantidadePorCaixa: Real read FQuantidadePorCaixa
      write FQuantidadePorCaixa;
    property EstoqueAtual: Real read FEstoqueAtual write FEstoqueAtual;
    property EstoqueMinimo: Real read FEstoqueMinimo write FEstoqueMinimo;
    property Disponivel: Boolean read FDisponivel write FDisponivel;
    property ListaProdutoPreco: TObjectList<TProdutoPreco>
      read FListaProdutoPreco write FListaProdutoPreco;
  end;

implementation

{ TProduto }

constructor TProduto.Create(CodigoProduto: Integer; Nome, CodigoBarras, Ncm: String;
  QuantidadePorCaixa, EstoqueMinimo: Real);
begin
  Self.CodigoProduto := CodigoProduto;
  Self.Nome := Nome;
  Self.CodigoBarras := CodigoBarras;
  Self.QuantidadePorCaixa := QuantidadePorCaixa;
  Self.EstoqueMinimo:= EstoqueMinimo;
  Self.Ncm:= Ncm;
  Self.Disponivel := True;
  Inicializar;
end;

constructor TProduto.Create;
begin
  Inicializar;
end;

procedure TProduto.AdicionarPreco(ProdutoPreco: TProdutoPreco);
begin
  if Not Assigned(Self.ListaProdutoPreco) then
  begin
    FListaProdutoPreco := TObjectList<TProdutoPreco>.Create;
  end;

  FListaProdutoPreco.Add(ProdutoPreco);

end;

constructor TProduto.Create(CodigoProduto: Integer);
begin
 Self.FCodigoProduto:= CodigoProduto;
end;

procedure TProduto.Inicializar;
begin
  FListaProdutoPreco := TObjectList<TProdutoPreco>.Create;
end;

end.

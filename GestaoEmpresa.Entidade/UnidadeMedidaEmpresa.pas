unit UnidadeMedidaEmpresa;

interface

type
  TUnidadeMedidaEmpresa = class
  public
    Constructor Create(CodigoUnidadeMedida, CodigoEmpresa: Integer); overload;
  private
    FCodigoUnidadeMedida: Integer;
    FCodigoEmpresa: Integer;
  published
    property CodigoUnidadeMedida: Integer read FCodigoUnidadeMedida
      write FCodigoUnidadeMedida;
    property CodigoEmpresa: Integer read FCodigoEmpresa write FCodigoEmpresa;
  end;

implementation

{ TUnidadeMedidaEmpresa }

constructor TUnidadeMedidaEmpresa.Create(CodigoUnidadeMedida,
  CodigoEmpresa: Integer);
begin
  Self.CodigoUnidadeMedida := CodigoUnidadeMedida;
  Self.CodigoEmpresa := CodigoEmpresa;
end;

end.

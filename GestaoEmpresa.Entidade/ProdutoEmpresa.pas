unit ProdutoEmpresa;

interface

type
  TProdutoEmpresa = class
  public
    Constructor Create(CodigoProduto: Int64; CodigoEmpresa: Integer); overload;
  private
    FCodigoProduto: Int64;
    FCodigoEmpresa: Integer;
  published
    property CodigoProduto: Int64 read FCodigoProduto write FCodigoProduto;
    property CodigoEmpresa: Integer read FCodigoEmpresa write FCodigoEmpresa;
  end;

implementation

{ TProdutoEmpresa }

constructor TProdutoEmpresa.Create(CodigoProduto: Int64;
  CodigoEmpresa: Integer);
begin
  Self.CodigoProduto := CodigoProduto;
  Self.CodigoEmpresa := CodigoEmpresa;
end;

end.

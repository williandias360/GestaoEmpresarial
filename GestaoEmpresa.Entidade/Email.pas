unit Email;

interface

type
  TEmail = class
  public
    constructor Create(CodigoEmail:Integer; Endereco: String; Principal: Boolean); overload;
    constructor Create(CodigoPessoa:Int64); overload;
  private
    FCodigoEmail: Integer;
    FEndereco: String;
    FPrincipal: Boolean;
    FCodigoPessoa: int64;
  published
    property CodigoEmail: Integer read FCodigoEmail write FCodigoEmail;
    property Endereco: String read FEndereco;
    property Principal: Boolean read FPrincipal write FPrincipal;
    property CodigoPessoa: int64 read FCodigoPessoa write FCodigoPessoa;
  end;

implementation

{ TEmail }

constructor TEmail.Create(CodigoEmail:Integer; Endereco: String; Principal: Boolean);
begin
  inherited Create;
  Self.FCodigoEmail:= CodigoEmail;
  Self.FEndereco := Endereco;
  Self.FPrincipal := Principal;
end;

constructor TEmail.Create(CodigoPessoa: Int64);
begin
  Self.FCodigoPessoa:= CodigoPessoa;
end;

end.

unit ProdutoPreco;

interface

type
  TProdutoPreco = class
  public
    Constructor Create(CodigoProdutoPrecoTipo: Integer;
      Valor: Real); overload;
      Constructor Create(CodigoProduto:Integer); overload;
  private
    FCodigoProduto: Integer;
    FCodigoProdutoPrecoTipo: Integer;
    FValor: Real;
  published
    property CodigoProduto: Integer read FCodigoProduto write FCodigoProduto;
    property CodigoProdutoPrecoTipo: Integer read FCodigoProdutoPrecoTipo
      write FCodigoProdutoPrecoTipo;
    property Valor: Real read FValor write FValor;
  end;

implementation

{ TProdutoPreco }

constructor TProdutoPreco.Create(CodigoProdutoPrecoTipo: Integer;
  Valor: Real);
begin
  Self.FCodigoProdutoPrecoTipo := CodigoProdutoPrecoTipo;
  Self.FValor := Valor;
end;

constructor TProdutoPreco.Create(CodigoProduto: Integer);
begin
  Self.CodigoProduto:= CodigoProduto;
end;

end.

unit ProdutoPrecoTipo;

interface
type
  TProdutoPrecoTipo = class
  public
    class function TIPO_PRECO_VISTA:Integer;static;
    class function TIPO_PRECO_PRAZO:Integer;static;
    class function TIPO_PRECO_CUSTO:Integer; static;
  private
    FCodigoProdutoPrecoTipo: Integer;
    FNome: String;
    FDisponivel: Boolean;
  published
    property CodigoProdutoPrecoTipo: Integer read FCodigoProdutoPrecoTipo write FCodigoProdutoPrecoTipo;
    property Nome: String read FNome write FNome;
    property Disponivel: Boolean read FDisponivel write FDisponivel;
  end;

implementation

{ TProdutoPrecoTipo }

class function TProdutoPrecoTipo.TIPO_PRECO_CUSTO: Integer;
begin
  result:= 3;
end;

class function TProdutoPrecoTipo.TIPO_PRECO_PRAZO: Integer;
begin
  result:= 2;
end;

class function TProdutoPrecoTipo.TIPO_PRECO_VISTA: Integer;
begin
  result:= 1;
end;

end.

unit Telefone;

interface

uses
 Entities.Enums.ETipoTelefone;

type
  TTelefone = class
  public
    constructor Create(CodigoTelefone: Int64; Numero: String;
      Tipo: TETipoTelefone; Principal: Boolean); overload;
    constructor Create(CodigoPessoa: Int64); overload;
  private
    FCodigoTelefone: Int64;
    FCodigoPessoa: Int64;
    FNumero: String;
    FPrincipal: Boolean;
    FTipo: TETipoTelefone;

  published
    property CodigoTelefone: Int64 read FCodigoTelefone write FCodigoTelefone;
    property CodigoPessoa: Int64 read FCodigoPessoa write FCodigoPessoa;
    property Numero: String read FNumero write FNumero;
    property Tipo: TETipoTelefone read FTipo;
    property Principal: Boolean read FPrincipal write FPrincipal;

  end;

implementation

{ TTelefone }

constructor TTelefone.Create(CodigoTelefone: Int64; Numero: String;
  Tipo: TETipoTelefone; Principal: Boolean);
begin
  inherited Create;
  Self.FCodigoTelefone := CodigoTelefone;
  Self.FNumero := Numero;
  Self.FTipo := Tipo;
  Self.FPrincipal := Principal;
end;

constructor TTelefone.Create(CodigoPessoa: Int64);
begin
  Self.FCodigoPessoa := CodigoPessoa;
end;

end.

/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases V10.0.2                    */
/* Target DBMS:           Firebird 3                                      */
/* Project file:          BancoDados.dez                                  */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Database drop script                            */
/* Created on:            2021-08-15 14:59                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop views                                                             */
/* ---------------------------------------------------------------------- */

DROP VIEW VENDERECO;

/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */

ALTER TABLE PESSOAFISICA DROP CONSTRAINT PESSOA_PESSOAFISICA;

ALTER TABLE PESSOAJURIDICA DROP CONSTRAINT PESSOA_PESSOAJURIDICA;

ALTER TABLE CIDADE DROP CONSTRAINT ESTADO_CIDADE;

ALTER TABLE ENDERECO DROP CONSTRAINT BAIRRO_ENDERECO;

ALTER TABLE ENDERECO DROP CONSTRAINT CIDADE_ENDERECO;

ALTER TABLE ENDERECO DROP CONSTRAINT PESSOA_ENDERECO;

ALTER TABLE USUARIO DROP CONSTRAINT PESSOA_USUARIO;

ALTER TABLE CLIENTE DROP CONSTRAINT PESSOA_CLIENTE;

ALTER TABLE EMAIL DROP CONSTRAINT PESSOA_EMAIL;

ALTER TABLE FORNECEDOR DROP CONSTRAINT PESSOA_FORNECEDOR;

ALTER TABLE TELEFONE DROP CONSTRAINT PESSOA_TELEFONE;

ALTER TABLE EMPRESA DROP CONSTRAINT PESSOA_EMPRESA;

ALTER TABLE PESSOAEMPRESA DROP CONSTRAINT EMPRESA_PESSOAEMPRESA;

ALTER TABLE PESSOAEMPRESA DROP CONSTRAINT PESSOA_PESSOAEMPRESA;

ALTER TABLE PRODUTO DROP CONSTRAINT PRODUTOGRUPO_PRODUTO;

ALTER TABLE PRODUTO DROP CONSTRAINT UNIDADEMEDIDA_PRODUTO;

ALTER TABLE PRODUTO DROP CONSTRAINT ARQUIVO_PRODUTO;

ALTER TABLE PRODUTOEMPRESA DROP CONSTRAINT PRODUTO_PRODUTOEMPRESA;

ALTER TABLE PRODUTOEMPRESA DROP CONSTRAINT EMPRESA_PRODUTOEMPRESA;

ALTER TABLE UNIDADEMEDIDAEMPRESA DROP CONSTRAINT UNIDADEMEDIDA_EMPRESA;

ALTER TABLE UNIDADEMEDIDAEMPRESA DROP CONSTRAINT EMPRESA_UNIDADEMEDIDA;

ALTER TABLE PRODUTOGRUPOEMPRESA DROP CONSTRAINT PRODUTOGRUPO_EMPRESA;

ALTER TABLE PRODUTOGRUPOEMPRESA DROP CONSTRAINT EMPRESA_PRODUTOGRUPO;

ALTER TABLE PRODUTOPRECO DROP CONSTRAINT PRODUTOPRECOTIPO_PRODUTOPRECO;

ALTER TABLE PRODUTOPRECO DROP CONSTRAINT PRODUTO_PRODUTOPRECO;

ALTER TABLE PRODUTOESTOQUE DROP CONSTRAINT PRODUTO_PRODUTOESTOQUE;

/* ---------------------------------------------------------------------- */
/* Drop table "PRODUTOESTOQUE"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PRODUTOESTOQUE DROP CONSTRAINT PK_PRODUTOESTOQUE;

DROP TABLE PRODUTOESTOQUE;

/* ---------------------------------------------------------------------- */
/* Drop table "PRODUTOPRECO"                                              */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PRODUTOPRECO DROP CONSTRAINT PK_PRODUTOPRECO;

DROP TABLE PRODUTOPRECO;

/* ---------------------------------------------------------------------- */
/* Drop table "PRODUTOEMPRESA"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PRODUTOEMPRESA DROP CONSTRAINT PK_PRODUTOEMPRESA;

DROP TABLE PRODUTOEMPRESA;

/* ---------------------------------------------------------------------- */
/* Drop table "PRODUTO"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PRODUTO DROP CONSTRAINT PK_PRODUTO;

DROP TABLE PRODUTO;

/* ---------------------------------------------------------------------- */
/* Drop table "ENDERECO"                                                  */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE ENDERECO DROP CONSTRAINT PK_ENDERECO;

DROP TABLE ENDERECO;

/* ---------------------------------------------------------------------- */
/* Drop table "CIDADE"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE CIDADE DROP CONSTRAINT PK_CIDADE;

DROP TABLE CIDADE;

/* ---------------------------------------------------------------------- */
/* Drop table "ARQUIVO"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE ARQUIVO DROP CONSTRAINT PK_ARQUIVO;

DROP TABLE ARQUIVO;

/* ---------------------------------------------------------------------- */
/* Drop table "PRODUTOPRECOTIPO"                                          */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PRODUTOPRECOTIPO DROP CONSTRAINT PK_PRODUTOPRECOTIPO;

DROP TABLE PRODUTOPRECOTIPO;

/* ---------------------------------------------------------------------- */
/* Drop table "PRODUTOGRUPOEMPRESA"                                       */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PRODUTOGRUPOEMPRESA DROP CONSTRAINT PK_PRODUTOGRUPOEMPRESA;

DROP TABLE PRODUTOGRUPOEMPRESA;

/* ---------------------------------------------------------------------- */
/* Drop table "UNIDADEMEDIDAEMPRESA"                                      */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE UNIDADEMEDIDAEMPRESA DROP CONSTRAINT PK_UNIDADEMEDIDAEMPRESA;

DROP TABLE UNIDADEMEDIDAEMPRESA;

/* ---------------------------------------------------------------------- */
/* Drop table "UNIDADEMEDIDA"                                             */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE UNIDADEMEDIDA DROP CONSTRAINT PK_UNIDADEMEDIDA;

DROP TABLE UNIDADEMEDIDA;

/* ---------------------------------------------------------------------- */
/* Drop table "PRODUTOGRUPO"                                              */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PRODUTOGRUPO DROP CONSTRAINT PK_PRODUTOGRUPO;

DROP TABLE PRODUTOGRUPO;

/* ---------------------------------------------------------------------- */
/* Drop table "PESSOAEMPRESA"                                             */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PESSOAEMPRESA DROP CONSTRAINT PK_PESSOAEMPRESA;

DROP TABLE PESSOAEMPRESA;

/* ---------------------------------------------------------------------- */
/* Drop table "EMPRESA"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

DROP TABLE EMPRESA;

/* ---------------------------------------------------------------------- */
/* Drop table "MIGRACAO"                                                  */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE MIGRACAO DROP CONSTRAINT PK_MIGRACAO;

DROP TABLE MIGRACAO;

/* ---------------------------------------------------------------------- */
/* Drop table "TELEFONE"                                                  */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE TELEFONE DROP CONSTRAINT PK_TELEFONE;

DROP TABLE TELEFONE;

/* ---------------------------------------------------------------------- */
/* Drop table "FORNECEDOR"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE FORNECEDOR DROP CONSTRAINT PK_FORNECEDOR;

DROP TABLE FORNECEDOR;

/* ---------------------------------------------------------------------- */
/* Drop table "EMAIL"                                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE EMAIL DROP CONSTRAINT PK_EMAIL;

DROP TABLE EMAIL;

/* ---------------------------------------------------------------------- */
/* Drop table "CLIENTE"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE CLIENTE DROP CONSTRAINT PK_CLIENTE;

DROP TABLE CLIENTE;

/* ---------------------------------------------------------------------- */
/* Drop table "USUARIO"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE USUARIO DROP CONSTRAINT PK_USUARIO;

DROP TABLE USUARIO;

/* ---------------------------------------------------------------------- */
/* Drop table "BAIRRO"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE BAIRRO DROP CONSTRAINT PK_BAIRRO;

DROP TABLE BAIRRO;

/* ---------------------------------------------------------------------- */
/* Drop table "ESTADO"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE ESTADO DROP CONSTRAINT PK_ESTADO;

DROP TABLE ESTADO;

/* ---------------------------------------------------------------------- */
/* Drop table "PESSOAJURIDICA"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PESSOAJURIDICA DROP CONSTRAINT PK_PESSOAJURIDICA;

DROP TABLE PESSOAJURIDICA;

/* ---------------------------------------------------------------------- */
/* Drop table "PESSOAFISICA"                                              */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PESSOAFISICA DROP CONSTRAINT PK_PESSOAFISICA;

DROP TABLE PESSOAFISICA;

/* ---------------------------------------------------------------------- */
/* Drop table "PESSOA"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE PESSOA DROP CONSTRAINT PK_PESSOA;

DROP TABLE PESSOA;

/* ---------------------------------------------------------------------- */
/* Drop domains                                                           */
/* ---------------------------------------------------------------------- */

DROP DOMAIN ETIPOTELEFONE;

DROP DOMAIN ETIPOENDERECO;

DROP DOMAIN ETIPOREGIMETRIBUTARIO;

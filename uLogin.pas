unit uLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, JvExControls,
  JvDBLookup, Vcl.Imaging.pngimage, Vcl.ExtCtrls, JvComponentBase, JvEnterTab,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TFLogin = class(TForm)
    Label1: TLabel;
    Image1: TImage;
    Label2: TLabel;
    dbLookupEmpresas: TJvDBLookupCombo;
    dbLookupUsuarios: TJvDBLookupCombo;
    Label3: TLabel;
    edtSenha: TEdit;
    btnEntrar: TBitBtn;
    btnCancelar: TBitBtn;
    JvEnterAsTab1: TJvEnterAsTab;
    dsEmpresas: TDataSource;
    rmdEmpresas: TFDMemTable;
    rmdEmpresasCodigoEmpresa: TIntegerField;
    rmdEmpresasNome: TStringField;
    rmdUsuarios: TFDMemTable;
    dsUsuarios: TDataSource;
    rmdUsuariosCodigoUsuario: TIntegerField;
    rmdUsuariosLogin: TStringField;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbLookupEmpresasEnter(Sender: TObject);
    procedure dbLookupEmpresasExit(Sender: TObject);
    procedure dbLookupEmpresasChange(Sender: TObject);
    procedure dbLookupUsuariosEnter(Sender: TObject);
    procedure dbLookupUsuariosExit(Sender: TObject);
    procedure edtSenhaEnter(Sender: TObject);
    procedure edtSenhaExit(Sender: TObject);
    procedure btnEntrarClick(Sender: TObject);
  private
    procedure EfetuarLogin;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLogin: TFLogin;

implementation

{$R *.dfm}

uses EmpresaBO, uFormPadrao, UsuarioBO, Usuario, Globals, Empresa;

procedure TFLogin.btnCancelarClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFLogin.btnEntrarClick(Sender: TObject);
begin
  EfetuarLogin;
end;

procedure TFLogin.dbLookupEmpresasChange(Sender: TObject);
var
  UsuarioBO:TUsuarioBO;
  CodigoEmpresa:Integer;
begin
  rmdUsuarios.Close;
  CodigoEmpresa:= rmdEmpresas.FieldByName('CodigoEmpresa').AsInteger;
  UsuarioBO:= TUsuarioBO.Create;
  rmdUsuarios.CopyDataSet(UsuarioBO.ListarUsuarioPorEmpresa(CodigoEmpresa), [coRestart, coAppend]);
  rmdUsuarios.First;
  if rmdUsuarios.RecordCount = 1 then
    dbLookupUsuarios.KeyValue:= rmdUsuarios.FieldByName('CodigoUsuario').AsInteger;
end;

procedure TFLogin.dbLookupEmpresasEnter(Sender: TObject);
begin
  dbLookupEmpresas.Color:= clInfoBk;
end;

procedure TFLogin.dbLookupEmpresasExit(Sender: TObject);
begin
  dbLookupEmpresas.Color:= clWindow;
end;

procedure TFLogin.dbLookupUsuariosEnter(Sender: TObject);
begin
  dbLookupUsuarios.Color:= clInfoBk;
end;

procedure TFLogin.dbLookupUsuariosExit(Sender: TObject);
begin
  dbLookupUsuarios.Color:= clWindow;
end;

procedure TFLogin.edtSenhaEnter(Sender: TObject);
begin
  edtSenha.Color:= clInfoBk;
end;

procedure TFLogin.edtSenhaExit(Sender: TObject);
begin
  edtSenha.Color:= clWindow;
end;

procedure TFLogin.FormShow(Sender: TObject);
var
  EmpresaBO:TEmpresaBO;
begin
  EmpresaBO:= TEmpresaBO.Create;
  rmdEmpresas.Close;
  rmdEmpresas.CopyDataSet(EmpresaBO.ListarEmpresasDisponivel, [coRestart, coAppend]);
  rmdEmpresas.First;
  dbLookupEmpresas.KeyValue:= rmdEmpresas.FieldByName('CodigoEmpresa').AsInteger;
end;

procedure TFLogin.EfetuarLogin;
var
  UsuarioBO:TUsuarioBO;
  Usuario: TUsuario;
begin
  if dbLookupEmpresas.Text = EmptyStr then
  begin
    MessageDlg('Empresa n�o foi informada', mtError, [mbOK],0, mbOK);
    dbLookupEmpresas.SetFocus;
    Exit;
  end;

  if dbLookupUsuarios.Text = EmptyStr then
  begin
    MessageDlg('Usu�rio n�o foi informado', mtError, [mbOK],0, mbOK);
    dbLookupUsuarios.SetFocus;
    Exit;
  end;

  if Trim(edtSenha.Text) = EmptyStr then
  begin
    MessageDlg('Senha n�o foi informada', mtError, [mbOK],0, mbOK);
    edtSenha.SetFocus;
    Exit;
  end;

  UsuarioBO:= TUsuarioBO.Create;
  Usuario:= UsuarioBO.ObterPorLoginSenha(dbLookupUsuarios.Text, edtSenha.Text);

  if Assigned(Usuario) then
  begin
    TGlobals.SetUsuario(Usuario);
    TGlobals.SetEmpresa(TEmpresa.Create(dbLookupEmpresas.KeyValue));
    Close;
    Exit;
  end;

  MessageDlg('Usu�rio/Senha inv�lidos', mtError, [mbOK],0, mbOK);

end;

end.
